<?php

umask(0000);

/*********************************************************/
/* use app cache 
/*********************************************************/

require_once __DIR__.'/../app/mobileRedirect.php';
require_once __DIR__.'/../app/bootstrap.php.cache';
require_once __DIR__.'/../app/AppKernel.php';
require_once __DIR__.'/../app/AppCache.php';

use Symfony\Component\HttpFoundation\Request;

$debug = false;
//// gospel admin page js has some bug when not debug
//// 在后台里面js在非debug的模式下有些问题，如果是进入后台，使用debug模式
if (strpos($_SERVER['REQUEST_URI'], '/gospel-admin') === 0) {
	$debug = true;
}
$kernel = new AppKernel('prod', $debug);
$kernel->loadClassCache();
// wrap the default AppKernel with the AppCache one
//$cacheDir = '/srv/git/jidunet/cache/http';
//$kernel = new AppCache($kernel, $cacheDir);
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
