<?php
exit;

//use Symfony\Component\ClassLoader\ApcClassLoader;
//use Symfony\Component\HttpFoundation\Request;
//
//$loader = require_once __DIR__.'/../app/bootstrap.php.cache';
////error_reporting(E_ALL);
////ini_set('display_errors', true);
//
//// Use APC for autoloading to improve performance.
//// Change 'sf2' to a unique prefix in order to prevent cache key conflicts
//// with other applications also using APC.
///*
//$loader = new ApcClassLoader('sf2', $loader);
//$loader->register(true);
//*/
//
////require_once __DIR__.'/../app/AppKernel.php';
//$debug = false;
//// gospel admin page js has some bug when not debug
//// 在后台里面js在非debug的模式下有些问题，如果是进入后台，使用debug模式
//if (strpos($_SERVER['REQUEST_URI'], '/gospel-admin') === 0) {
//	$debug = true;
//}
//require_once __DIR__.'/../app/AppCache.php';
//
//$kernel = new AppKernel('prod', $debug);
////$kernel = new AppKernel('prod', false);
//$kernel->loadClassCache();
////$kernel = new AppCache($kernel);
//$request = Request::createFromGlobals();
//$response = $kernel->handle($request);
//$response->send();
//$kernel->terminate($request, $response);

/*********************************************************/
/* use app cache 
/*********************************************************/

require_once __DIR__.'/../app/bootstrap.php.cache';
require_once __DIR__.'/../app/AppKernel.php';
require_once __DIR__.'/../app/AppCache.php';

use Symfony\Component\HttpFoundation\Request;

$debug = true;
//// gospel admin page js has some bug when not debug
//// 在后台里面js在非debug的模式下有些问题，如果是进入后台，使用debug模式
if (strpos($_SERVER['REQUEST_URI'], '/gospel-admin') === 0) {
	$debug = true;
}
$kernel = new AppKernel('bmt', $debug);
$kernel->loadClassCache();
// wrap the default AppKernel with the AppCache one
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
