<?php
include_once 'Mobile_Detect.php';
$detect = new Mobile_Detect;

$isMobile = $detect->isMobile();
$isTablet = $detect->isTablet();

$redirect = false;
if($isMobile && !$isTablet) {
	$uri = $_SERVER['REQUEST_URI'];
	if(in_array($uri, ['/', '/index.php'])) {
		$redirect = true;
		$redirectUrl = 'http://m.christiantimes.cn/';
	}
	if(preg_match('/news\/(\d*)/', $uri, $match)) {
		if(isset($match[1])) {
			$redirect = true;
			$postId = $match[1];
			$redirectUrl = 'http://m.christiantimes.cn/category/88/' . $postId;
		}
	} 
	if(preg_match('/\/en\/news\/(\d*)/', $uri, $match)) {
		if(isset($match[1])) {
			$redirect = true;
			$postId = $match[1];
			$redirectUrl = 'http://m.christiantimes.cn/category/88/' . $postId . '/en';
		}
	} 

} 

if($redirect) {
	header('Location: ' . $redirectUrl);
	exit;
}
