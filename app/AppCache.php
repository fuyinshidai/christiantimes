<?php

require_once __DIR__.'/AppKernel.php';

use Symfony\Bundle\FrameworkBundle\HttpCache\HttpCache;

class AppCache extends HttpCache
{
	/**
	 * 
	 * @see http://symfony.com/doc/current/book/http_cache.html#http-cache-introduction
	 * @return type
	 */
	protected function getOptions()
    {
        return array(
        );
    }
}
