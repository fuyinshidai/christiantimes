select id, title from gospel_post where area_id = 22 and status = 1;

-- 教会的id
-- 执行结果 88, 89, 90, 91, 92, 93
select p2.* from gospel_categories p1 left join gospel_categories p2 on p1.id = p2.parent_id where p1.title = '教会';

-- 筛选北京市里面的文章
-- 执行结果： 13510， 13505， 13497， 13453， 13444， 13435， 13416， 13409， 13394， 13381， 13360， 13328， 13275， 13273， 13268
select id, title from gospel_post where area_id = 22 order by id desc;

-- 更新省头条
update gospel_post set local_headline_text = 1 where id in (13510, 13505, 13497, 13453, 13444, 13435, 13416, 13409, 13394, 13381, 13360, 13328, 13275, 13273, 13268);

select p.id, p.title from gospel_post p left join gospel_post_image i on p.id = i.post_id where i.name is not null and p.category_id in (88, 89, 90, 91, 92, 93) group by p.id order by p.id desc limit 0, 10;