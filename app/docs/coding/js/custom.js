$(document).ready(function(){
	//其他城市
	var ua = navigator.userAgent; 
	var event = (ua.match(/iPad/i)) ? 'touchstart' : 'click'; 
	/** 其他城市下拉菜单 */
	$('.ht-ocity').bind(event, function(e) { 
		var elm = $(this).parent();
		if(elm.hasClass('city-on')) {
			elm.removeClass('city-on');
			$('.other-version').hide();
		} else {
			elm.addClass('city-on');
			$('.other-version').show();
		}
	})
	$('.close').bind(event, function(e) { 
		e.preventDefault();
		$('.ht-city').removeClass('city-on');
		$('.other-version').hide();
	})
	$('.nav-more').bind(event, function(e) { 
		e.preventDefault();
		var elm = $('.nd-whole');
		elm.toggle();
	})
	/*文章页小图放大*/
	$(".fancybox-effects-d").fancybox({
				padding: 0,

				openEffect : 'elastic',
				openSpeed  : 150,

				closeEffect : 'elastic',
				closeSpeed  : 150,

				closeClick : true,

				helpers : {
					overlay : null
				}
			});
	/*文章页滚动图片*/
	$('.asb-img').flexslider();

	// 查看更多专栏作家
	$("#more-columnists").click(function (e) {
		e.preventDefault();
		$(this).prev().find('.more').toggle('normal');
	});
})