<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new Gospel\Bundle\SecurityBundle\GospelSecurityBundle(),
            new Gospel\Bundle\UserBundle\GospelUserBundle(),
			new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle(),
			new FOS\RestBundle\FOSRestBundle(),
			new JMS\SerializerBundle\JMSSerializerBundle($this),
			new Sylius\Bundle\ResourceBundle\SyliusResourceBundle(),
			new WhiteOctober\PagerfantaBundle\WhiteOctoberPagerfantaBundle(),
            new Frontend\WebBundle\FrontendWebBundle(),
            new Backend\Bundle\CoreBundle\BackendCoreBundle(),
            new Gospel\Bundle\CoreBundle\GospelCoreBundle(),
			new Liip\ImagineBundle\LiipImagineBundle(),
			new HWI\Bundle\OAuthBundle\HWIOAuthBundle(),
			new SimpleThings\FormExtraBundle\SimpleThingsFormExtraBundle(),
			new Genemu\Bundle\FormBundle\GenemuFormBundle(),
			new PunkAve\FileUploaderBundle\PunkAveFileUploaderBundle(),
			new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
			new JMS\AopBundle\JMSAopBundle(),
    		new JMS\SecurityExtraBundle\JMSSecurityExtraBundle(),
    		new JMS\DiExtraBundle\JMSDiExtraBundle($this),
			new Simonsimcity\LiipImagine\WatermarkFilterBundle\SimonsimcityLiipImagineWatermarkFilterBundle(),
			new Gregwar\CaptchaBundle\GregwarCaptchaBundle(),
            new Gospel\Bundle\AlipayBundle\GospelAlipayBundle(),
			new Craue\ConfigBundle\CraueConfigBundle(),
            new Gospel\Bundle\NewsletterBundle\GospelNewsletterBundle(),
			new Hype\MailchimpBundle\HypeMailchimpBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
