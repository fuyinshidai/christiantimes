<?php
namespace Gospel\Bundle\NewsletterBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AddressType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('name', null, array(
				'label' => '姓名',
			))
			->add('church', null, array(
				'label' => '所属教会名称',
			))
			->add('address', null, array(
				'label' => '收件地址及邮编',
			))
			->add('number', null, array(
				'label' => '索取数量',
			))
			->add('phone', null, array(
				'label' => '联系电话'
			))
			->add('email', 'email', array(
				'label' => 'E-mail地址',
			))
			;

	}

	public function setDefaultOptions(OptionsResolverInterface $resolve)
	{
		$resolve->setDefaults(array(
			'data_class' => 'Gospel\Bundle\NewsletterBundle\Entity\Address',
		));
	}

	public function getName()
	{
		return 'gospel_newsletter_address';
	}
}
