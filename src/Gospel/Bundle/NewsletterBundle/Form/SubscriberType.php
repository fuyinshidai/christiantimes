<?php
namespace Gospel\Bundle\NewsletterBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SubscriberType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$dataOptions = array(
			'multiple' => true,
			'expanded' => true,
		);
		if ($options['categories']) {
			$dataOptions['data'] = $options['categories'];
		}

		$builder
			->add('email', 'email', array(
				'attr' => array(
					'class' => 'email',
				)
			))
			->add('subscribed_time', 'datetime')
			->add('categories', null, $dataOptions)
			;

	}

	public function setDefaultOptions(OptionsResolverInterface $resolve)
	{
		$resolve->setDefaults(array(
			'data_class' => 'Gospel\Bundle\NewsletterBundle\Entity\Subscriber',
			'categories' => null,
		));
	}

	public function getName()
	{
		return 'gospel_newslettersubscriber';
	}
}
