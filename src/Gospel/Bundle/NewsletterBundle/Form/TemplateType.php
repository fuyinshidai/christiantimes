<?php
namespace Gospel\Bundle\NewsletterBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TemplateType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('title')
			->add('body')
			;

	}

	public function setDefaultOptions(OptionsResolverInterface $resolve)
	{
		$resolve->setDefaults(array(
			'data_class' => 'Gospel\Bundle\NewsletterBundle\Entity\Template',
		));
	}

	public function getName()
	{
		return 'gospel_newslettertemplate';
	}
}
