<?php

/*
 * SubscriberSearchType.php encoding=UTF-8
 *
 * Created on Jan 28, 2015, 9:04:11 AM
 *
 * gospeltimes.cn default 
 * Copyright(c) Beijing Gospeltimes Information Technology, Inc.  All Rights Reserved.
 *
 */
namespace Gospel\Bundle\NewsletterBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class SubscriberSearchType extends AbstractType
{
	public function __construct()
	{}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
				->add('categories', null, array(
					'label' => '分类'
				))
				;
	}

	public function getName()
	{
		return 'gospel_newsletter_subscriber_search';
	}
}
