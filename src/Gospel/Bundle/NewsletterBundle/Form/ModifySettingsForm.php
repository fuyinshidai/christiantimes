<?php

namespace Gospel\Bundle\NewsletterBundle\Form;

use Gospel\Bundle\NewsletterBundle\Form\Type\SettingType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * @author Zhili He<zhili850702@gmail.com>
 * @copyright 2014-2024 gospeltimes ltd. co
 */
class ModifySettingsForm extends AbstractType {

	/**
	 * {@inheritDoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder->add('settings', 'collection', array(
			'type' => new SettingType(),
		));
	}

	/**
	 * {@inheritDoc}
	 */
	public function getName() {
		return 'gospel_newsletter_config_modifySettings';
	}

}
