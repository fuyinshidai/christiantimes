<?php
namespace Gospel\Bundle\NewsletterBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NewsletterType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('title')
			->add('category')
			->add('body')
			->add('scheduledTime', 'datetime')
			;

	}

	public function setDefaultOptions(OptionsResolverInterface $resolve)
	{
		$resolve->setDefaults(array(
			'data_class' => 'Gospel\Bundle\NewsletterBundle\Entity\Newsletter',
		));
	}

	public function getName()
	{
		return 'gospel_newsletter';
	}
}
