<?php

namespace Gospel\Bundle\NewsletterBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Subscriber
 *
 * @ORM\Table(name="subscriber")
 * @ORM\Entity(repositoryClass="Gospel\Bundle\NewsletterBundle\Entity\SubscriberRepository")
 */
class Subscriber
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100, unique=true)
     */
    private $email;

    /**
     * @var string
     *
	 * @ORM\ManyToMany(targetEntity="Gospel\Bundle\NewsletterBundle\Entity\Category")
     */
    private $categories;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="subscribedTime", type="datetime")
     */
    private $subscribedTime;

	public function __construct()
	{
		if (!$this->getSubscribedTime()) {
			$this->setSubscribedTime(new \DateTime);
		}
		$this->categories = new ArrayCollection();
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Subscriber
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Subscriber
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set subscribedTime
     *
     * @param \DateTime $subscribedTime
     * @return Subscriber
     */
    public function setSubscribedTime($subscribedTime)
    {
        $this->subscribedTime = $subscribedTime;
    
        return $this;
    }

    /**
     * Get subscribedTime
     *
     * @return \DateTime 
     */
    public function getSubscribedTime()
    {
        return $this->subscribedTime;
    }



    /**
     * Add categories
     *
     * @param \Gospel\Bundle\NewsletterBundle\Entity\Category $categories
     * @return Subscriber
     */
    public function addCategory(\Gospel\Bundle\NewsletterBundle\Entity\Category $categories)
    {
        $this->categories[] = $categories;
    
        return $this;
    }

    /**
     * Remove categories
     *
     * @param \Gospel\Bundle\NewsletterBundle\Entity\Category $categories
     */
    public function removeCategory(\Gospel\Bundle\NewsletterBundle\Entity\Category $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategories()
    {
        return $this->categories;
    }

	public function __toString()
	{
		return $this->getEmail();
	}
}
