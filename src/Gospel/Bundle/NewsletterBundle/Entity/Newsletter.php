<?php

namespace Gospel\Bundle\NewsletterBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Newsletter
 *
 * @ORM\Table(name="newsletter")
 * @ORM\Entity(repositoryClass="Gospel\Bundle\NewsletterBundle\Entity\NewsletterRepository")
 */
class Newsletter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
	 * @ORM\ManyToOne(targetEntity="Gospel\Bundle\NewsletterBundle\Entity\Category")
     */
    private $category;


    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text")
     */
    private $body;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdTime", type="datetime")
     */
    private $createdTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="scheduledTime", type="datetime")
     */
    private $scheduledTime;

	public function __construct()
	{
		if (!$this->getCreatedTime()) {
			$this->setCreatedTime(new \DateTime);
		}
		if (!$this->getScheduledTime()) {
			$this->setScheduledTime(new \DateTime);
		}
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Newsletter
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set body
     *
     * @param string $body
     * @return Newsletter
     */
    public function setBody($body)
    {
        $this->body = $body;
    
        return $this;
    }

    /**
     * Get body
     *
     * @return string 
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set createdTime
     *
     * @param \DateTime $createdTime
     * @return Newsletter
     */
    public function setCreatedTime($createdTime)
    {
        $this->createdTime = $createdTime;
    
        return $this;
    }

    /**
     * Get createdTime
     *
     * @return \DateTime 
     */
    public function getCreatedTime()
    {
        return $this->createdTime;
    }

    /**
     * Set scheduledTime
     *
     * @param \DateTime $scheduledTime
     * @return Newsletter
     */
    public function setScheduledTime($scheduledTime)
    {
        $this->scheduledTime = $scheduledTime;
    
        return $this;
    }

    /**
     * Get scheduledTime
     *
     * @return \DateTime 
     */
    public function getScheduledTime()
    {
        return $this->scheduledTime;
    }


    /**
     * Set category
     *
     * @param \Gospel\Bundle\NewsletterBundle\Entity\Category $category
     * @return Newsletter
     */
    public function setCategory(\Gospel\Bundle\NewsletterBundle\Entity\Category $category = null)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return \Gospel\Bundle\NewsletterBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

}