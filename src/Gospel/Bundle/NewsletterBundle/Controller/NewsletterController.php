<?php

namespace Gospel\Bundle\NewsletterBundle\Controller;

use Gospel\Bundle\CoreBundle\Controller\BackendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Gospel\Bundle\CoreBundle\Form\PostType;
use Gospel\Bundle\CoreBundle\Entity\Post;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Gospel\Bundle\NewsletterBundle\Entity\Setting;

class NewsletterController extends BackendController
{

    /**
     * Create new resource or just display the form.
     */
    public function createAction(Request $request)
    {
        $config = $this->getConfiguration();

		$post = new Post();
		$options = array(
    		'em' => $this->getDoctrine()->getManager(),
			'user' => $this->getCurrentUser(),
			'category' => $this->makeTree($this->getCategoryTree(), array()),
			'categoryother' => $this->makeTree($this->getCategoryOtherTree(), array()),
			'area' => $this->makeTree($this->getAreaTree(), array(), 1),
			'createAt' => new \DateTime(),
			'entity' => $post,
			'attr' => array(),
		);
		$searchForm = $this->createForm(new PostType(), $post, $options);

        $resource = $this->createNew();
        $form = $this->getForm($resource);

        if (($request->isMethod('POST') || $request->isMethod('PUT') ) && $form->bind($request)->isValid()) {
            $this->create($resource);
            $this->setFlash('success', 'create');

            return $this->redirectTo($resource);
        }

        if ($config->isApiRequest()) {
            return $this->handleView($this->view($form));
        }

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('_form.html'))
            ->setData(array(
                $config->getResourceName() => $resource,
                'form'                     => $form->createView(),
                'searchForm'                     => $searchForm->createView(),
				'categoryNews'	=> $this->getDoctrine()->getRepository('GospelNewsletterBundle:Category')->findOneBy(array(
					'id' => 1
				))
            ))
        ;

        return $this->handleView($view);
    }

    /**
     * Display the form for editing or update the resource.
     */
    public function updateAction(Request $request)
    {
        $config = $this->getConfiguration();

		$post = new Post();
		$options = array(
    		'em' => $this->getDoctrine()->getManager(),
			'user' => $this->getCurrentUser(),
			'category' => $this->makeTree($this->getCategoryTree(), array()),
			'categoryother' => $this->makeTree($this->getCategoryOtherTree(), array()),
			'area' => $this->makeTree($this->getAreaTree(), array(), 1),
			'createAt' => new \DateTime(),
			'entity' => $post,
			'attr' => array(),
		);
		$searchForm = $this->createForm(new PostType(), $post, $options);

        $resource = $this->findOr404();
        $form = $this->getForm($resource);

        if (($request->isMethod('PUT') || $request->isMethod('POST')) && $form->bind($request)->isValid()) {
            $this->update($resource);
            $this->setFlash('success', 'update');

            return $this->redirectTo($resource);
        }

        if ($config->isApiRequest()) {
            return $this->handleView($this->view($form));
        }

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('_form.html'))
            ->setData(array(
                $config->getResourceName() => $resource,
                'form'                     => $form->createView(),
                'searchForm'                     => $searchForm->createView(),
				'categoryNews'	=> $this->getDoctrine()->getRepository('GospelNewsletterBundle:Category')->findOneBy(array(
					'id' => 1,
				))
            ))
        ;

        return $this->handleView($view);
    }

	public function importSubscribersAction()
	{
		$subscribers = $this->getDoctrine()->getRepository('GospelNewsletterBundle:Subscriber')->findAll();
		$emails = array();
		foreach ($subscribers as $row) {
			$emails[] = array('email' => array('email' => $row->getEmail()));
		}
		$mc = $this->get('hype_mailchimp');
		$data = $mc->getList()->batchSubscribe($emails, true);

		return new Response(print_r($data, true));
	}
	
	/**
	 * 同步mailchimp邮件分类,默认一次请求同步50
	 * 
	 * @param Request $request
	 * @return type
	 */
	public function updateSubscriberAction(Request $request)
	{
		$subscribersAll = $this->getDoctrine()->getRepository('GospelNewsletterBundle:Subscriber')->findAll();
		if ($request->isXmlHttpRequest()) {
			$subscribers = $this->getDoctrine()->getRepository('GospelNewsletterBundle:Subscriber')->findBy(
					array(), array(), $request->get('limit', 50), $request->get('offset', 0)
					);
			if ($subscribers) {
				$continue = true;
			} else {
				$continue = false;
			}
			$mc = $this->get('hype_mailchimp');
			$data = array();
			foreach ($subscribers as $key => $row) {
				$email = $row->getEmail();
				$categories = $row->getCategories();

				$mailchimpCategories = array(
					1 => '资讯',
					2 => '话题',
					3 => '杂志',
				);
				foreach($categories as $category) {
					$cates[] = $mailchimpCategories[$category->getId()];
				}
				$merge_vars = array(
					'GROUPINGS' => array(
						array(
							'name' => '订阅分类',
							'groups' => $cates,
						),
					)
				); 
				try {
					$data[] = $mc->getList()->setMerge_vars($merge_vars)->subscribe($email, 'html', true, true);
					$error = null;
				} catch (\Exception $ex) {
					$error = $ex->getCode() . ': with message ' . $ex->getMessage();
				}
			}

			$view = $this
				->view()
				->setFormat('json')
				->setData(array(
					'data' => $data,
					'offset' => (int)$request->get('offset', 0),
					'continue' => $continue,
					'total' => count($subscribersAll),
					'error' => $error,
				))
			;
		} else {
			$config = $this->getConfiguration();
			$view = $this
				->view()
            	->setTemplate($config->getTemplate('update-group.html'))
				->setData(array(
				))
			;

		}

        return $this->handleView($view);
	}

	public function sendAction()
	{
		$request = $this->getRequest();
		$id = $request->get('id');
		$newsletter = $this->getDoctrine()->getRepository('GospelNewsletterBundle:Newsletter')->find($id);
		$subscribers = $this->getDoctrine()->getRepository('GospelNewsletterBundle:Subscriber')->findAll();
		$body = $newsletter->getBody();

		$mc = $this->get('hype_mailchimp');

/*
		$emails = array();
		foreach ($subscribers as $row) {
			$emails[] = array('email' => array('email' => $row->getEmail()));
		}
		$data = $mc->getList()
				->batchSubscribe($emails, false);
*/

		$conditions = array();
		$conditions[] = array('field'=>'interests-2609', 'op'=>'one', 'value'=>$newsletter->getCategory()->getTitle());
		$segment_opts = array('match'=>'all', 'conditions'=>$conditions);

		// ** fetch the ineterest segment, return data has the fields-****

		/*
		 * $data = $mc->getList()->listSegments();
		 * var_dump($data);
		 */

		$listId = $this->container->parameters['hypemailchimp.config']['default_list'];
        $data = $mc->getCampaign()->create('regular', array(
            	'list_id' => $listId,
            	'subject' => $newsletter->getTitle(),
            	'from_email' => 'jidushibao@gmail.com',
            	'from_name' => '基督时报',
		'title' => $newsletter->getTitle(),
	), array(
            'html' => $body,
        ), $segment_opts);

		$data = $mc->getCampaign()
                ->setCi($data['id'])
                ->send();


        $config = $this->getConfiguration();
        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('send.html'))
            ->setData(array('subscribers'=>$subscribers))
        ;

        return $this->handleView($view);
	}

	/**
	 * Check bounced email address
	 * Remove bounced emails
	 */
	public function bounceMailsAction(Request $request)
	{
		$mc = $this->get('hype_mailchimp');
		// reports/bounce-messages (string apikey, string cid, struct opts)
//		$campaignList = $mc->getCampaign()->get();

		$form = $this->createFormBuilder()
				->add('csv', 'file')
				->add('save', 'submit', array('label' => '提交'))
				->getForm()
				;

		$form->handleRequest($request);
		
		$emails = null;

		if ($form->isValid()) {
			$emails = array();
			$csv = $form->get('csv')->getData();
			$csvFile = $csv->getPath().'/'. $csv->getFilename();
			$result = array_map('str_getcsv', file($csvFile));
			array_shift($result);
			foreach ($result as $row) {
				$emails[] = array('email' =>  $row[0]);
			}
			$data = $mc->getList()->batchUnsubscribe($emails, true);
		}
        $config = $this->getConfiguration();
        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('bounce.html'))
            ->setData(array(
				'form' => $form->createView(),
				'emails' => $emails,
			))
        ;

        return $this->handleView($view);
	}

	/**
	 * Send test email
	 */
	public function sendTestAction()
	{
		$request = $this->getRequest();
		$email = $request->get('email');
		$id = $request->get('id');
		$newsletter = $this->getDoctrine()->getRepository('GospelNewsletterBundle:Newsletter')->find($id);
		$body = $newsletter->getBody();
		$message = \Swift_Message::newInstance()
			->setSubject($newsletter->getTitle())
			->setFrom(array('jidushibao@gmail.com' => '基督时报'))
			->setTo($email)
			->setBody($body, 'text/html')
			;


		try {
			//$result = $this->get('swiftmailer.mailer.no_reply')->send($message);
			$result = $this->get('mailer')->send($message);
		} catch (\Exception $exception) { // 发送失败记录相关日志
			$message = sprintf('发送出现错误，邮件连接出现问题<br/> %s: "%s" at %s line %s', get_class($exception), $exception->getMessage(), $exception->getFile(), $exception->getLine());
			$context = array('exception' => $exception);
            $this->get('logger')->critical($message, $context);
			$config = $this->getConfiguration();
			$view = $this
				->view()
				->setTemplate($config->getTemplate('error.html'))
				->setData(array('message'=>$message))
			;

			return $this->handleView($view);
		}
		$this->get('session')->getFlashBag()->add(
			'success',
			"成功发送测试封邮件到该地址:". $email
		);
		return $this->redirect($this->generateUrl('gospel_newsletter_index'));
	}

	public function templateAction()
	{
		$id = $this->getRequest()->get('id');
		$category = $this->getDoctrine()->getRepository('GospelNewsletterBundle:Category')->find($id);
		$template = $this->getDoctrine()->getRepository('GospelNewsletterBundle:Template')->findOneBy(array(
			'title' => $category->getTitle()
		));

		// get the number of the issue
		$newsletters = $this->getDoctrine()->getRepository('GospelNewsletterBundle:Newsletter')->findBy(array(
			'category' => $category,
		));
		$issueNo = count($newsletters) + 1;
		
		// 头部导航
		$menus = $this->container->getParameter('frontend.navigation');



		// if template file exists then using it
		if ($template) {
			// 读取资讯相关内容
			if ($template->getId() == 1) {
				$repoPost = $this->getDoctrine()->getRepository('GospelCoreBundle:Post');
				$headlines = $repoPost->findMany(REC_HEADLINE_PIC, 2);
				$headlines2 = $repoPost->findMany(REC_HEADLINE_PIC, 2, 2);

				$newslist = $repoPost->findMany(REC_HEADLINE_PIC, 10, null);

				// 新闻推荐
				$newslist1 = array_slice($newslist, 0, 4);
				$newslist2 = array_slice($newslist, 4, 4);

				// 热门
				$hots = $repoPost->getHot(3);
				$view = $this->renderView('GospelNewsletterBundle:Template:'.$template->getBody(), array(
					'menus' => $menus,
					'headlines' => $headlines,
					'newslist1' => $newslist1,
					'newslist2' => $newslist2,
					'issueNo' => $issueNo,
				));
				$view = preg_replace('~>\s+<~', '><', $view);
			}

			// 读取专题相关内容
			if ($template->getId() == 2) {
				$repoTopic = $this->getDoctrine()->getRepository('GospelCoreBundle:Topic');
				$topics = $repoTopic->findLatest(1);
				$topic = $topics[0];

				// 热门
				$view = $this->renderView('GospelNewsletterBundle:Template:'.$template->getBody(), array(
					'menus' => $menus,
					'issueNo' => $issueNo,
					'topic' => $topic,
				));
				$view = preg_replace('~>\s+<~', '><', $view);
			}

			$response = new Response($view);
		} else {
			$response = new Response('');
		}

		return $response;
	}

	/**
	 * Export the emails to cvs file
	 */
	public function exportAction()
	{
		$subscribers = $this->getDoctrine()->getRepository('GospelNewsletterBundle:Subscriber')->findAll();
		/*
		$response = new Response();
		$response->headers->set('Content-Type', 'text/csv; charset=utf-8');
		$response->headers->set('Content-Disposition', 'attachment; filename=data.csv');
		$output = fopen('php://output', 'w');
		fputcsv($output, array('email'));
		foreach ($subscribers as $row) {
			fputcsv($output, array($row->getEmail()));
		}
		return $response;
		 */
// get the service container to pass to the closure
        $container = $this->container;
		$subscribers = $this->getDoctrine()->getRepository('GospelNewsletterBundle:Subscriber')->findAll();
        $response = new StreamedResponse(function() use($container) {
			$subscribers = $this->getDoctrine()->getRepository('GospelNewsletterBundle:Subscriber')->findAll();

            // The getExportQuery method returns a query that is used to retrieve
            // all the objects (lines of your csv file) you need. The iterate method
            // is used to limit the memory consumption
			$output = fopen('php://output', 'w');
			fputcsv($output, array('email', 'date'));
			foreach ($subscribers as $row) {
				fputcsv($output, array(
					$row->getEmail(),
					$row->getSubscribedTime()->format('Y-m-d H:i:s'),
				));
			}

        });

		$response->setCharset('utf-8');
        $response->headers->set('Content-Type', 'application/force-download');
        $response->headers->set('Content-Disposition','attachment; filename="export.csv"');

        return $response;
	}

	private function getSetting($name) {
		return $this->getDoctrine()->getRepository('GospelNewsletterBundle:Setting')->findOneBy(array(
			'name' => $name,
		));
	}

	/**
	 * 生成树状结构,根据所在的深读在前面添加"——"
	 */
	public function makeTree($data, $arr, $maxlevel = 10000) 
	{
		foreach($data as $row) {
			if ($maxlevel <= $row['lvl']) {
				continue;
			}
			$arr[$row['id']] = str_repeat("——", $row['lvl']).$row['title'];
			if(is_array($row['__children']) && count($row['__children']) > 0) {
				$arr = $this->makeTree($row['__children'], $arr, $maxlevel);
			}
		}
		return $arr;
	}


	/**
	 * Get post category array
	 * @return array category array
	 */
	public function getCategoryTree()
	{
		$repository = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:Category');
		$arrayTree = $repository->childrenHierarchy();

		// unset the unactive category
		foreach($arrayTree as $key => $row) {
			if (!$row['isActive']) {
				unset($arrayTree[$key]);
			}
		}
		return $arrayTree;
	}

	/**
	 * Get post the other category array
	 * @return array the other category array
	 */
	public function getCategoryOtherTree()
	{
		$repository = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:CategoryOther');
		$arrayTree = $repository->childrenHierarchy();
		foreach($arrayTree as $key => $row) {
			if (!$row['isActive']) {
				unset($arrayTree[$key]);
			}
		}
		return $arrayTree;
	}

	public function getAreaTree()
	{
		$repository = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:Area');
		$arrayTree = $repository->childrenHierarchy();
		return $arrayTree;
	}
}
