<?php

namespace Gospel\Bundle\NewsletterBundle\Controller;

use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Symfony\Component\HttpFoundation\Request;
use Gospel\Bundle\NewsletterBundle\Form\Type\SubscriberSearchType;

class SubscriberController extends ResourceController
{
    public function indexAction(Request $request)
    {
        $config = $this->getConfiguration();

        $resource = $this->createNew();
		$form = $this->createForm(new SubscriberSearchType(), $resource);

        $criteria = $config->getCriteria();
		$gospel = $request->get('gospel_newsletter_subscriber_search', array());
		$categories = array();
		if (isset($gospel['categories']) && count($gospel['categories'])) {
			$criteria['categories'] = array(
				'type' => 'join',
				'join' => 'categories',
				'data' => $gospel['categories'],
			);
		}
        $sorting = $config->getSorting();

        $pluralName = $config->getPluralResourceName();
        $repository = $this->getRepository();

        if ($config->isPaginated()) {
            $resources = $this
                ->getResourceResolver()
                ->getResource($repository, $config, 'createPaginator', array($criteria, $sorting))
            ;

            $resources
                ->setCurrentPage($request->get('page', 1), true, true)
                ->setMaxPerPage($config->getPaginationMaxPerPage())
            ;
        } else {
            $resources = $this
                ->getResourceResolver()
                ->getResource($repository, $config, 'findBy', array($criteria, $sorting, $config->getLimit()))
            ;
        }

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('index.html'))
            ->setTemplateVar($pluralName)
            ->setData(array(
				$pluralName => $resources,
				'form' => $form,
				)
			)
        ;

        return $this->handleView($view);
    }
}
