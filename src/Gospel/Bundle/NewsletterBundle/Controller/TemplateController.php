<?php

namespace Gospel\Bundle\NewsletterBundle\Controller;

use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Gospel\Bundle\CoreBundle\Twig\GospelExtension;

class TemplateController extends ResourceController
{
	public function updateNewsAction(Request $request)
	{
		$orderId = $request->get('id', 1);
		$postId = $request->get('pid', 32475);
		$content = $request->get('content', '<a style="text-decoration:none; color:#000;" date-href-1="" href="http://gospeltimes.dev:8080/app_dev.php/news/32465/%E7%BE%8E%E9%A3%9F%E5%B0%B1%E5%9C%A8%E9%9B%85%E6%AF%94%E6%96%AF%E9%A4%90%E9%A5%AE" _href="http://gospeltimes.dev:8080/app_dev.php/news/32465/%E7%BE%8E%E9%A3%9F%E5%B0%B1%E5%9C%A8%E9%9B%85%E6%AF%94%E6%96%AF%E9%A4%90%E9%A5%AE"><img style="  border:0; margin-top:20px;" width="320" height="200" alt="" data-img-2="" src="http://gospeltimes.dev:8080/app_dev.php/assets/cache/460x336_watermark/assets/media/post/201410/32465/www.gospeltimes.cn-%E9%9B%85%E6%AF%94%E6%96%AF2.png" _src="http://gospeltimes.dev:8080/app_dev.php/assets/cache/460x336_watermark/assets/media/post/201410/32465/www.gospeltimes.cn-%E9%9B%85%E6%AF%94%E6%96%AF2.png"><h1 cellspacing="0" style="margin-bottom:5px; width:320px; font-size:16px; line-height:22px; color:#333; font-weight:bold; height:44px;overflow:hidden;" data-title-2="">美食就在雅比斯餐饮</h1></a>');

		$post = $this->getDoctrine()->getRepository('GospelCoreBundle:Post')->find($postId);
		$newUrl = $request->getSchemeAndHttpHost() . $this->generateUrl(
						'news_show', array(
							'id'=>$post->getId(),'slug'=>GospelExtension::slugfyFilter($post->getAlias()
						)));
		$imgSrc = $this->get('liip_imagine.cache.manager')->getBrowserPath($post->getCoverPath(), '460x336_watermark', true);

		$contentNew = preg_replace('/(data-title-'.$orderId.').*?(<)/', '\1>'.$post->getTitle().'<', $content);
		$contentNew = preg_replace('/(data-desc-'.$orderId.').*?(<)/', '\1>'.$post->getIntroduction().'<', $contentNew);
		$contentNew = preg_replace('/(data-href-'.$orderId.').*?>/', '\1 href="'.$newUrl . '">', $contentNew);
		$contentNew = preg_replace('/(data-img-'.$orderId.').*?>/', '\1 src="'.$imgSrc . '">', $contentNew);
		
		return new Response($contentNew);
	}
}
