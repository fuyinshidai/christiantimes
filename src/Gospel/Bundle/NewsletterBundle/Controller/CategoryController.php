<?php

namespace Gospel\Bundle\NewsletterBundle\Controller;

use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Symfony\Component\HttpFoundation\Request;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class CategoryController extends ResourceController
{
    /**
     * Get collection (paginated by default) of resources.
     */
    public function indexAction(Request $request)
    {
        $config = $this->getConfiguration();

        $criteria = $config->getCriteria();
        $sorting = $config->getSorting();

        $pluralName = $config->getPluralResourceName();
		$repository = $this->getManager()->getRepository('GospelNewsletterBundle:Category');
		$arrayTree = $repository->childrenHierarchy();

        if ($config->isPaginated()) {
            $resources = $this
                ->getResourceResolver()
                ->getResource($repository, $config, 'createPaginator', array($criteria, $sorting))
            ;

            $resources
                ->setCurrentPage($request->get('page', 1), true, true)
                ->setMaxPerPage($config->getPaginationMaxPerPage())
            ;
        } else {
            $resources = $this
                ->getResourceResolver()
                ->getResource($repository, $config, 'findBy', array($criteria, $sorting, $config->getLimit()))
            ;
        }

		$category = $repository->createNew();
        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('index.html'))
            ->setTemplateVar($pluralName)
            ->setData(array('categories'=>$resources,'category'=>$repository->createNew(),'tree'=>$arrayTree))
        ;

        return $this->handleView($view);
    }

    /**
     * Create new resource or just display the form.
     */
    public function createAction(Request $request)
    {
		$repository = $this->getManager()->getRepository('GospelNewsletterBundle:Category');
        $config = $this->getConfiguration();
		$parent_id = $request->get('parent', null);
		$selected = null;
		if($parent_id) {
			$selected = $repository->find($parent_id);
			
		}

        $resource = $this->createNew();
        $form = $this->createForm($this->getConfiguration()->getFormType(), $resource, array('selected'=>$selected));

        if ($request->isMethod('PUT') && $form->bind($request)->isValid()) {
            $this->create($resource);
            $this->setFlash('success', 'create');

            return $this->redirect($this->generateUrl('gospel_newsletter_category_index'));
        }

        if ($config->isApiRequest()) {
            return $this->handleView($this->view($form));
        }
		$arrayTree = $repository->childrenHierarchy();

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('create.html'))
            ->setData(array(
                $config->getResourceName() => $resource,
                'form'                     => $form->createView()
            ))
        ;

        return $this->handleView($view);
    }

    /**
     * Display the form for editing or update the resource.
     */
    public function updateAction(Request $request)
    {
        $config = $this->getConfiguration();

        $resource = $this->findOr404();
        $form = $this->getForm($resource);

        if (($request->isMethod('PUT') || $request->isMethod('POST')) && $form->bind($request)->isValid()) {
            $this->update($resource);
            $this->setFlash('success', 'update');

        }

        if ($config->isApiRequest()) {
            return $this->handleView($this->view($form));
        }

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('update.html'))
            ->setData(array(
                $config->getResourceName() => $resource,
                'form'                     => $form->createView()
            ))
        ;

        return $this->handleView($view);
    }


	public function saveRow($row, $parent)
	{
		$repository = $this->getManager()->getRepository('GospelNewsletterBundle:Category');
		$resource = $this->createNew();
		$resource->setTitle($row['name']);
		$resource->setDescription($row['intro']?$row['intro']:'');
		$resource->setSortOrder($row['sequence']);
		$resource->setSlug($row['serial_name']);
		$resource->setIsActive($row['is_show']);
		$resource->setParent($parent);
		$this->create($resource);
		return $resource;
	}
}
