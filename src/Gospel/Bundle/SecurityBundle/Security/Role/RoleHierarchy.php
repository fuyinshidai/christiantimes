<?php

namespace Gospel\Bundle\SecurityBundle\Security\Role;
use Doctrine\ORM\EntityManager;

class RoleHierarchy extends \Symfony\Component\Security\Core\Role\RoleHierarchy
{
    private $em;

    /**
     * @param array $hierarchy
     */
    public function __construct(array $hierarchy, EntityManager $em)
    {
        $this->em = $em;
        parent::__construct($this->buildRolesTree());
    }

    /**
     * Here we build an array with roles. It looks like a two-levelled tree - just 
     * like original Symfony roles are stored in security.yml
     * @return array
     */
    private function buildRolesTree()
    {
        $hierarchy = array();
        $roles = $this->em->createQuery('select r from GospelUserBundle:Role r')->execute();
        foreach ($roles as $role) {
            /** @var $role Role */
            if ($role->getParent()) {
                if (!isset($hierarchy[$role->getParent()->getIdentifier()])) {
                    $hierarchy[$role->getParent()->getIdentifier()] = array();
                }
                $hierarchy[$role->getParent()->getIdentifier()][] = $role->getIdentifier();
            } else {
                if (!isset($hierarchy[$role->getIdentifier()])) {
                    $hierarchy[$role->getIdentifier()] = array();
                }
            }
        }
        return $hierarchy;
    }
}