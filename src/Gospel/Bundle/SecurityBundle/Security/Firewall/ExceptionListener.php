<?php

namespace Gospel\Bundle\SecurityBundle\Security\Firewall;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Firewall\ExceptionListener as BaseExceptionListener;

/**
 * Handle the user login rediretion if the previous session memory url is a post url
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class ExceptionListener extends BaseExceptionListener
{
	protected function setTargetPath(Request $request)
	{
		// Do not save target path for XHR and non-GET requests
		if ($request->isXmlHttpRequest() || 'GET' !== $request->getMethod()) {
			return;
		}

		parent::setTargetPath($request);
	}
}
