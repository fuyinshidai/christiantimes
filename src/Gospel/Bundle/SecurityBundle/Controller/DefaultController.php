<?php

namespace Gospel\Bundle\SecurityBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('GospelSecurityBundle:Default:index.html.twig', array('name' => $name));
    }
}
