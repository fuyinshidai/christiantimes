<?php

namespace Gospel\Bundle\SecurityBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * 用户登录相关的控制器
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class SecurityController extends Controller
{
	public function loginAction()
	{
		$request = $this->getRequest();
		$session = $request->getSession();

		// get the login error if there is one
		if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
			$error = $request->attributes->get(
				SecurityContext::AUTHENTICATION_ERROR
			);
		} else {
			$error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
			$session->remove(SecurityContext::AUTHENTICATION_ERROR);
		}

		$view = $request->isXmlHttpRequest() ? 'login.raw.twig' : 'login.html.twig';

		return $this->render(
				'GospelSecurityBundle:Security:'.$view,
				array(
					'last_username' => $session->get(SecurityContext::LAST_USERNAME),
					'error' => $error,
				));
	}

	public function logoutAction()
	{

	}
}
