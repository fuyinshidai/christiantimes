<?php

namespace Gospel\Bundle\UserBundle\Controller;

use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Symfony\Component\HttpFoundation\Request;
use JMS\SecurityExtraBundle\Annotation\Secure;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class UserController extends ResourceController
{

    /**
     * Create new resource or just display the form.
	 * @Secure(roles="ROLE_USER_MANAGER")
     */
    public function createAction(Request $request)
    {
        $config = $this->getConfiguration();

        $resource = $this->createNew();
        $form = $this->getForm($resource);

        if ($request->isMethod('POST') && $form->bind($request)->isValid()) {
			$resource->setSalt(substr(md5(time().mt_rand(1, 1000000)), 0, 32));
			$resource->setPassword($this->hashPassword($resource, $form->getData()->getPassword()));
            $this->create($resource);
            $this->setFlash('success', 'create');

            return $this->redirectTo($resource);
        }

        if ($config->isApiRequest()) {
            return $this->handleView($this->view($form));
        }

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('create.html'))
            ->setData(array(
                $config->getResourceName() => $resource,
                'form'                     => $form->createView()
            ))
        ;

        return $this->handleView($view);
    }

    /**
     * Display the form for editing or update the resource.
	 * @Secure(roles="ROLE_USER_MANAGER")
     */
    public function updateAction(Request $request)
    {
        $config = $this->getConfiguration();

        $resource = $this->findOr404();
        $form = $this->getForm($resource);

        if (($request->isMethod('PUT') || $request->isMethod('POST'))) {
			$data = $request->request->get('gospel_user');
			$updatePassword = true;

			// check if we need to update the origin password value
			// Updated only when the repeated password is submitted and the value 
			// the same as the first password field value
			if($form->getData()->getPassword() === $resource->getPassword() && $data['password']['second'] == '') {
				$data['password']['second'] = $resource->getPassword();
				$request->request->set('gospel_user', $data);
				$updatePassword = false;
			}
			$form->bind($request);

			// validate the form when the repeat password is not null
			if(!$updatePassword || $form->isValid()) {
				if($updatePassword) { // only update the password when the repeated
					// password and the first password are the same value
					$resource->setPassword($this->hashPassword($resource, $form->getData()->getPassword()));
				}
				$this->update($resource);
				$this->setFlash('success', 'update');

				return $this->redirectTo($resource);
			}
        }

        if ($config->isApiRequest()) {
            return $this->handleView($this->view($form));
        }

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('update.html'))
            ->setData(array(
                $config->getResourceName() => $resource,
                'form'                     => $form->createView()
            ))
        ;

        return $this->handleView($view);
    }

	public function hashPassword($user, $password) {
		$encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
		return $encoder->encodePassword($password, $user->getSalt());
	}
}
