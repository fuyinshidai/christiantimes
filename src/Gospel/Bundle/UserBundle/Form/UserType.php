<?php

namespace Gospel\Bundle\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class UserType extends AbstractType
{

	public function __construct()
	{
		
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
				->add('username', null, array('label' => '用户名'))
				->add('password', 'repeated', array(
					'type' => 'password',
					'data' => '',
					'invalid_message' => '两次输入的密码必须相同',
					'required' => false,
					'first_options' => array('label' => '密码'),
					'second_options' => array('label' => '确认密码', 'required'=>false),
				))
				->add('email', 'email')
				->add('groups', 'choiceTree', array(
					'label' => '用户组',
					'class' => 'GospelUserBundle:Group',
					'expanded' => true,
					'multiple' => true
					))
				->add('is_active', 'checkbox', array('required' => false, 'label' => '是否激活'))
		;
	}


	public function getName()
	{
		return 'gospel_user';
	}

}
