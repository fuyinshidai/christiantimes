<?php

namespace Gospel\Bundle\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class GroupType extends AbstractType
{
	public function __construct()
	{
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
				->add('name')
				->add('description', null, array('label' => '描述', 'required' => false))
				->add('roles', 'choiceTree', array(
					'label' => '角色',
					'class' => 'GospelUserBundle:Role',
					'expanded' => true,
					'multiple' => true
					))
				;
	}

	public function getName()
	{
		return 'gospel_group';
	}
}
