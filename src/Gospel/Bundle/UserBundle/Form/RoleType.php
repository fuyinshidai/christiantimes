<?php

namespace Gospel\Bundle\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class RoleType extends AbstractType
{
	public function __construct()
	{
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
				->add('name', null, array('label'=>'名称'))
				->add('identifier', null, array('label'=>'标识符'))
				->add('parent', null, array('label'=>'父节点'))
				->add('description', null, array('label'=>'描述'))
				;
	}

	public function getName()
	{
		return 'gospel_role';
	}
}
