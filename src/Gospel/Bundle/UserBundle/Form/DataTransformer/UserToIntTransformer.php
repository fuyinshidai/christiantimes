<?php

namespace Gospel\Bundle\UserBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class UserToIntTransformer implements DataTransformerInterface
{
	/**
	 * @var ObjectManager
	 */
	private $om;

	public function __construct(ObjectManager $om)
	{
		$this->om = $om;
	}

	/**
	 * Transform an object to string
	 * 
	 * @param type $value
	 * @return string
	 */
	public function transform($value)
	{
		if(null == $value) {
			return "";
		}
		return $value;
	}

	public function reverseTransform($value)
	{
		if(!$value) {
			return null;
		}
		$user = $this->om
				->getRepository('GospelUserBundle:User')
				->find($value)
				;
		if(null === $user) {
			throw new TransformationFailedException(sprintf('用户 "%s" 不存在',
					$value)
					);
		}

		return $user;
	}
}

