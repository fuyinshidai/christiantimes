<?php

namespace Gospel\Bundle\UserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Gospel\Bundle\UserBundle\Entity\User;


/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class UserCommand extends ContainerAwareCommand
{
	protected function configure()
	{
		$this
				->setName('gospel:user:create')
				->setDescription('Create user')
				->addArgument('name', InputArgument::REQUIRED, 'Please input the user username')
				->addArgument('password', InputArgument::REQUIRED, 'Please input user password')
				->addArgument('email', InputArgument::REQUIRED, 'Please input user email')
				;
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$name = $input->getArgument('name');
		$password = $input->getArgument('password');
		$email = $input->getArgument('email');
		$salt = substr(md5(time().mt_rand(1, 1000000)), 0, 32);

		$user = new User();
		$user->setSalt($salt);
		$user->setUsername($name);
		$user->setPassword($this->hashPassword($user, $password));
		$user->setEmail($email);
		$user->setIsActive(true);

		$em = $this->getContainer()->get('doctrine')->getManager();
		$em->persist($user);
		$em->flush();

		$output->writeln("<info>Success create user: </info>".$name);
	}

	public function hashPassword($user, $password) {
		$encoder = $this->getContainer()->get('security.encoder_factory')->getEncoder($user);
		return $encoder->encodePassword($password, $user->getSalt());
	}
}
