<?php

namespace Gospel\Bundle\UserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class GreetCommand extends ContainerAwareCommand
{
	protected function configure()
	{
		$this
				->setName('gospel:greet')
				->setDescription('Greet someone')
				->addArgument(
						'name',
						InputArgument::OPTIONAL,
						'Who do you want to greet?'
						)
				->addOption(
						'yell',
						null,
						InputOption::VALUE_NONE,
						'If set, the task will yell in upcase letters'
						)

				;
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$name = $input->getArgument('name');
		$text = 'Hello';
		if ($name) {
			$text .= ' ' . $name;
		}

		if ($input->getOption('yell')) {
			$text = strtoupper($text);
		}

		$output->writeln($text);
	}
}
