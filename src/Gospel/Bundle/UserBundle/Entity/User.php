<?php

namespace Gospel\Bundle\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Gospel\Bundle\UserBundle\Entity\RoleRepository;

/**
 * User
 *
 * @ORM\Table(name="gospel_users")
 * @ORM\Entity(repositoryClass="Gospel\Bundle\UserBundle\Entity\UserRepository")
 * @ORM\ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 */
class User implements AdvancedUserInterface, \Serializable
{
	private $userRoles;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=25)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="displayName", type="string", length=25, nullable=true)
     */
    private $displayName;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=32, nullable=true)
     */
    private $salt;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=40)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=60, unique=true)
     */
    private $email;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

	/**
     * @ORM\ManyToMany(targetEntity="Gospel\Bundle\UserBundle\Entity\Group")
     * @ORM\JoinTable(name="gospel_users_groups",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     *      )
     */
    private $groups;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;
    
        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    
        return $this;
    }

    /**
     * Get salt
     *
     * @return string 
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        if (null === $password || !$password)  {
           return $this; 
       }

       $this->password = $password;
	   return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    
        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

	public function getRoles()
	{
		if (isset($this->userRoles)) {
			return $this->userRoles;
		}
		$roles = array();
		global $kernel;
		if ( 'AppCache' == get_class($kernel) )
		{
		   $kernel = $kernel->getKernel();
		}
		$em = $kernel->getContainer()->get( 'doctrine.orm.entity_manager' );
		$roleRepository = new RoleRepository($em, new \Doctrine\ORM\Mapping\ClassMetadata('Gospel\Bundle\UserBundle\Entity\Role'));
		$subRoles = array();
		foreach($this->getGroups() as $group) {
			$sub_roles = $group->getRoles();
			foreach($sub_roles as $v) {
				$subRoles = array_merge($subRoles, $this->getSubRoles($v, $roleRepository));
				foreach ($subRoles as $row) {
					$roles[] = $row;
				}
				$roles[] = $v->getIdentifier();
			}
		}
		$this->userRoles = $roles;
		return $roles;
	}

	function getSubRoles($role, $repo)
	{
		$result = array();
		$children = $repo->getChildren($role);
		foreach ($children as $row) {
			$result[] = $row->getIdentifier();
			$result = array_merge($result, $this->getSubRoles($row, $repo));
		}
		return $result;
	}

	public function eraseCredentials()
	{
//		$this->password = null;
	}

	public function serialize()
	{
		return serialize(array(
			$this->id,
		));
	}

	public function unserialize($serialized)
	{
		list(
				$this->id
		) = unserialize($serialized);
	}

	public function isAccountNonExpired()
	{
		return true;
	}

	public function isAccountNonLocked()
	{
		return true;
	}

	public function isCredentialsNonExpired()
	{
		return true;
	}

	public function isEnabled()
	{
		return $this->isActive;
	}
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->groups = new \Doctrine\Common\Collections\ArrayCollection();
        $this->posts = new ArrayCollection();
    }
    
    /**
     * Add groups
     *
     * @param \Gospel\Bundle\UserBundle\Entity\Group $groups
     * @return User
     */
    public function addGroup(\Gospel\Bundle\UserBundle\Entity\Group $groups)
    {
        $this->groups[] = $groups;
    
        return $this;
    }

    /**
     * Remove groups
     *
     * @param \Gospel\Bundle\UserBundle\Entity\Group $groups
     */
    public function removeGroup(\Gospel\Bundle\UserBundle\Entity\Group $groups)
    {
        $this->groups->removeElement($groups);
    }

    /**
     * Get groups
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGroups()
    {
        return $this->groups;
    }

	public function __toString()
	{
		return $this->username;
	}

    /**
     * Set displayName
     *
     * @param string $displayName
     * @return User
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;
    
        return $this;
    }

    /**
     * Get displayName
     *
     * @return string 
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }
}
