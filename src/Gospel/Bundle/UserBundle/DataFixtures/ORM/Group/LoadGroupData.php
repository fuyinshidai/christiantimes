<?php

namespace Gospel\Bundle\UserBundle\DataFixtures\ORM\Group;

use Backend\Bundle\CoreBundle\DataFixtures\ORM\DataFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Gospel\Bundle\UserBundle\Entity\Group;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class LoadGroupData extends DataFixture
{
	public function load(ObjectManager $manager)
	{
		$sql = 'ALTER TABLE gospel_group AUTO_INCREMENT = 1';
		$manager->getConnection()->exec($sql);

		$data = array(
			array(
				'超级管理员',
			),
			array(
				'管理员',
			),
			array(
				'转换用户',
			),
		);
		$roleRepository = $this->get("doctrine")->getRepository('GospelUserBundle:Role');

		foreach($data as $key => $obj) {
			$entity = new Group();
			$entity->setName($obj[0]);
			$manager->persist($entity);
			$manager->flush();
		}
	}

	/**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 2;
    }
}
