<?php

namespace Gospel\Bundle\UserBundle\DataFixtures\ORM;

use Backend\Bundle\CoreBundle\DataFixtures\ORM\DataFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Gospel\Bundle\UserBundle\Entity\Role;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class LoadRoleData extends DataFixture
{
	public function load(ObjectManager $manager)
	{
		$sql = 'ALTER TABLE gospel_role AUTO_INCREMENT = 1';
		$manager->getConnection()->exec($sql);

		$roles = array(
			array(
				'超级管理员',
				'ROLE_SUPER_ADMIN',
				'超级管理员',
			),
			array(
				'管理员',
				'ROLE_ADMIN',
				'网站管理员，可以管理网站内容相关部分',
				1,
			),
			array(
				'转换用户',
				'ROLE_ALLOWED_TO_SWITCH',
				'将当前用户转换为其他身份登录',
				1
			),
		);

		foreach($roles as $key => $role) {
			$id = $key + 1;
			$entity = new Role();
			$entity->setName($role[0]);
			$entity->setIdentifier($role[1]);
			$entity->setDescription($role[2]);
			$entity->setParent(isset($role[3])?$this->getReference('Role-'.$role[3]):null);
        	$this->setReference('Role-'.$id, $entity);
			$manager->persist($entity);
			$manager->flush();
		}
	}

	/**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }
}
