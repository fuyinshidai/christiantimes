<?php

namespace Gospel\Bundle\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Gospel\Bundle\UserBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAware;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class LoadUserData extends ContainerAware implements FixtureInterface
{
	public function load(ObjectManager $manager)
	{

		$sql = 'ALTER TABLE gospel_users AUTO_INCREMENT = 1';
		$manager->getConnection()->exec( $sql );

		$factory = $this->container->get('security.encoder_factory');

		$userAdmin = new User();
		$encoder = $factory->getEncoder($userAdmin);
		$salt = substr(md5(time().mt_rand(1, 1000000)), 0, 32);
		$userAdmin->setUsername('admin');
		$userAdmin->setPassword($encoder->encodePassword('adminpass', $salt));
		$userAdmin->setSalt($salt);
		$userAdmin->setEmail('admin@example.com');
		$userAdmin->setIsActive(1);
		$manager->persist($userAdmin);

		$userAdmin = new User();
		$encoder = $factory->getEncoder($userAdmin);
		$salt = substr(md5(time().mt_rand(1, 1000000)), 0, 32);
		$userAdmin->setUsername('ctadmin');
		$userAdmin->setPassword($encoder->encodePassword('ctadminpass', $salt));
		$userAdmin->setSalt($salt);
		$userAdmin->setEmail('ctadmin@example.com');
		$userAdmin->setIsActive(1);
		$manager->persist($userAdmin);

		$manager->flush();
	}
}
