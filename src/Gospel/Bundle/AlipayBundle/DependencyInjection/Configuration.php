<?php

namespace Gospel\Bundle\AlipayBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('gospel_alipay');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.
		$rootNode
            ->children()
                ->scalarNode('pid')
                    ->defaultValue('208840260777xxxx')
                    ->info('What is your alipay pid')
                    ->example('208840260777xxxx')
                ->end()
                ->scalarNode('key')
                    ->defaultValue('xzuyq8mt3z1zs5001527592yq6ynxxxx')
                    ->info('What is your alipay key')
                    ->example('xzuyq8mt3z1zs5001527592yq6ynxxxx')
                ->end()
                ->scalarNode('email')
                    ->defaultValue('mail@mail.com')
                    ->info('Alipay email')
                    ->example('mail@mail.com')
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
