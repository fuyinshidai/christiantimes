<?php

/*
 * 支付宝controller文件，用于用户支付，支付回传等相关功能
 */

namespace Gospel\Bundle\AlipayBundle\Controller;

use Frontend\WebBundle\Controller\WebController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Gospel\Bundle\AlipayBundle\Alipay\AlipaySubmit;
use Gospel\Bundle\AlipayBundle\Alipay\AlipayNotify;
use Gospel\Bundle\CoreBundle\Entity\Donation;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class AlipayController extends WebController
{

	/**
	 * 支付功能
	 */
	public function payAction()
	{
		return $this->render('GospelAlipayBundle:Alipay:pay.html.twig');
	}

	protected function getParam($name)
	{
		return $this->container->getParameter('gospel_alipay.'.$name);
	}


	/**
	 * 生成支付按钮后自动跳转到支付网关
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @return type
	 */
	public function apiAction(Request $request)
	{
		/*		 * ************************请求参数************************* */

		//支付类型
		$payment_type = "1";
		//必填，不能修改
		//服务器异步通知页面路径
		$notify_url = $this->getBaseUrl() . $this->generateUrl('gospel_alipay_notify');
		//需http://格式的完整路径，不能加?id=123这类自定义参数
		//页面跳转同步通知页面路径
		$return_url = $this->getBaseUrl() . $this->generateUrl('gospel_alipay_return');
		//需http://格式的完整路径，不能加?id=123这类自定义参数，不能写成http://localhost/
		//卖家支付宝帐户
		$seller_email = $this->container->getParameter('gospel_alipay.email');
		//必填
		//商户订单号
		$out_trade_no = 'DONATION_JIDUSHIBAO_' . date('Y-m-d-H:i:s', time()) . '_' . $_SERVER['REMOTE_ADDR'];
		//商户网站订单系统中唯一订单号，必填
		//订单名称
		$subject = '基督时报网站奉献_' . time();
		//必填
		//付款金额
		$total_fee = $request->get('WIDtotal_fee');
		if (is_array($total_fee)) {
			$total_fee = array_shift($total_fee);
		}
		//必填
		//订单描述

		$body = '基督时报支持奉献';
		//商品展示地址
		$aboutDonate = $this->getCoreRepo('About')->findOneBy(array('title' => '爱心奉献'));
		$show_url = $this->getBaseUrl() . $this->generateUrl('about', array(
					'id' => $aboutDonate->getId(),
					'slug' => $this->slugfy($aboutDonate->getTitle()),
		));
		//需以http://开头的完整路径，例如：http://www.xxx.com/myorder.html
		//防钓鱼时间戳
		$anti_phishing_key = "";
		//若要使用请调用类文件submit中的query_timestamp函数
		//客户端的IP地址
		$exter_invoke_ip = "";
		//非局域网的外网IP地址，如：221.0.0.1


		/*		 * ********************************************************* */

		$payType = $request->get('type', 'alipay');
		$parameter = array(
			"service" => "create_direct_pay_by_user",
			"partner" => trim($this->getParam('pid')),
			"payment_type" => $payment_type,
			"notify_url" => $notify_url,
			"return_url" => $return_url,
			"seller_email" => $seller_email,
			"out_trade_no" => $out_trade_no,
			"subject" => $subject,
			"total_fee" => $total_fee,
			"body" => $body,
			"show_url" => $show_url,
			"anti_phishing_key" => $anti_phishing_key,
			"exter_invoke_ip" => $exter_invoke_ip,
			"_input_charset" => trim(strtolower('utf-8'))
		);

		switch ($payType) {
			case 'alipay':

				break;

			case 'bank':
				//构造要请求的参数数组，无需改动
				$parameter["paymethod"] = "bankPay";
				$parameter["defaultbank"] = $request->get('WIDdefaultbank', 'CMB');

				break;

			default:
				break;
		}



//建立请求
		$alipaySubmit = new AlipaySubmit($this->getAlipayConfig());
		$html_text = $alipaySubmit->buildRequestForm($parameter, "get", "确认");

		return $this->render('GospelAlipayBundle:Alipay:api.html.twig', array(
					'text' => $html_text,
		));
	}

	public function notifyAction(Request $request)
	{

		//计算得出通知验证结果
		$alipayNotify = new AlipayNotify($this->getAlipayConfig());
		$verify_result = $alipayNotify->verifyNotify();

		if ($verify_result) {//验证成功
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//请在这里加上商户的业务逻辑程序代
			//——请根据您的业务逻辑来编写程序（以下代码仅作参考）——
			//获取支付宝的通知返回参数，可参考技术文档中服务器异步通知参数列表
			//商户订单号
			$out_trade_no = $request->get('trade_no');

			//支付宝交易号

			$trade_no = $request->get('trade_no');

			//交易状态
			$trade_status = $request->get('trade_status');


			if ($request->get('trade_status') == 'TRADE_FINISHED') {
				//判断该笔订单是否在商户网站中已经做过处理
				//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
				//如果有做过处理，不执行商户的业务程序
				//注意：
				//该种交易状态只在两种情况下出现
				//1、开通了普通即时到账，买家付款成功后。
				//2、开通了高级即时到账，从该笔交易成功时间算起，过了签约时的可退款时限（如：三个月以内可退款、一年以内可退款等）后。
				//调试用，写文本函数记录程序运行情况是否正常
				//logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
			} else if ($request->get('trade_status') == 'TRADE_SUCCESS') {
				//判断该笔订单是否在商户网站中已经做过处理
				//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
				//如果有做过处理，不执行商户的业务程序
				//注意：
				//该种交易状态只在一种情况下出现——开通了高级即时到账，买家付款成功后。
				//调试用，写文本函数记录程序运行情况是否正常
				//logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
				
				// 根据用户订单号在数据库中查询
				// 如果没有处理过，则在数据库中保存相关记录
				$this->createDonation($this->getRequest());
			}

			//——请根据您的业务逻辑来编写程序（以上代码仅作参考）——

			$msg = "success";  //请不要修改或删除
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		} else {
			//验证失败
			$msg = "支付失败";

			//调试用，写文本函数记录程序运行情况是否正常
			//logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
		}

		return new Response($msg);
	}

	/**
	 * ?buyer_email=zhili850702%40gmail.com
	 * &buyer_id=2088702986940012
	 * &exterface=create_direct_pay_by_user
	 * &is_success=T
	 * &notify_id=RqPnCoPT3K9%252Fvwbh3I75J3HX4OS2gDOL6Pup7GbRifCv%252F%252BhWc%252BSFkxcqsRRl5MSR1Xvu
	 * &notify_time=2013-12-27+11%3A10%3A10
	 * &notify_type=trade_status_sync
	 * &out_trade_no=DONATION_2013-12-27-11%3A09%3A11
	 * &payment_type=1
	 * &seller_email=fuyinshidai%40gmail.com
	 * &seller_id=2088402607771824
	 * &subject=my+order
	 * &total_fee=0.01
	 * &trade_no=2013122779126601
	 * &trade_status=TRADE_SUCCESS
	 * &sign=ddd1fde5704a190b7327a3c17d3adff3
	 * &sign_type=MD5
	 */
	public function returnAction()
	{
		$this->createDonation($this->getRequest());
		$aboutContact = $this->getCoreRepo('About')->findOneBy(array('title' => '爱心奉献'));
		return $this->redirect($this->generateUrl('about', array(
							'id' => $aboutContact->getId(),
							'slug' => $this->slugfy($aboutContact->getTitle()),
		)));
	}


	protected function getAlipayConfig()
	{
		return array(
			'partner' => $this->getParam('pid'),
			'key' => $this->getParam('key'),
			'sign_type' => 'MD5',
			'input_charset' => 'utf-8',
			'cacert' => dirname(dirname(__FILE__)) . '/Alipay/cacert.pem',
			'transport' => 'http',
		);
	}

	/**
	 * 创建用户奉献，如果数据库没有该记录则添加该记录
	 * http://www.gospeltimes.cn/app_dev.php/gospel-alipay/
	 * return?body=%E7%A6%8F%E9%9F%B3%E6%97%B6%E6%8A%A5%E6%94%AF%E6%8C%81%E5%A5%89%E7%8C%AE&buyer_email=zhili850702%40gmail.com&buyer_id=2088702986940012&exterface=create_direct_pay_by_user&is_success=T&notify_id=RqPnCoPT3K9%252Fvwbh3I75J3OPWXDyTyOcv0eCCDfKVF0TPBZJ0lhwQAarRCPQA2tgP%252BNQ&notify_time=2013-12-30+11%3A54%3A39&notify_type=trade_status_sync&out_trade_no=DONATION_2013-12-30-11%3A54%3A06_52c0ee5e9f8a1&payment_type=1&seller_email=fuyinshidai%40gmail.com&seller_id=2088402607771824&subject=%E7%A6%8F%E9%9F%B3%E6%97%B6%E6%8A%A5%E7%BD%91%E7%AB%99%E5%A5%89%E7%8C%AE_1388375646&total_fee=0.01&trade_no=2013123010248301&trade_status=TRADE_SUCCESS&sign=86505c531c1832648df5aa1c7d4334bc&sign_type=MD5
	 */
	private function createDonation(Request $request)
	{
		// 根据用户订单号在数据库中查询
		$donation = $this->getCoreRepo('Donation')->findBy(array(
			'tradeNo' => $request->get('out_trade_no', ''),
		));

		// 如果没有处理过，则在数据库中保存相关记录
		if (! $donation instanceof Donation) {
			$donation = new Donation();
			$donation->setTradeNo($request->get('out_trade_no', ''));
			$donation->setTotalFee($request->get('total_fee', ''));
			$donation->setStatus($request->get('trade_status', ''));
			$donation->setBuyerEmail($request->get('buyer_emai', ''));
			$donation->setSubject($request->get('subject', ''));
			$donation->setCreatedAt(new \DateTime());

			$em = $this->getDoctrine()->getManager();
			$em->persist($donation);
			$em->flush();

			$this->get('session')->getFlashBag()->add(
					'notice', '感谢您的奉献支持！'
			);
		}
	}
}
