<?php

namespace Gospel\Bundle\AlipayBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('GospelAlipayBundle:Default:index.html.twig', array('name' => $name));
    }
}
