<?php

namespace Gospel\Bundle\CoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class PostCommand extends ContainerAwareCommand
{
	protected function configure()
	{
		$this->setName('post:update')
			->setDescription("Update post")
			->addArgument(
				'name',
				InputArgument::OPTIONAL,
				'要修改的文章分类'
			)
			->addArgument(
				'category',
				InputArgument::OPTIONAL,
				'更新文章到该分类'
			)
			;
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$text = "没有输入文章关键词";
		$name = $input->getArgument('name');
		if ($name) {
			$text = '修改文章标题中包含"'.$name.'"的文章';
		}
		$category = $input->getArgument('category');
		if ($category) {
			$text .= ' => "'.$category.'"';
		}

		if ($name && $category) {
			// 读取标题中包含该关键词的文章
			$em = $this->getContainer()->get('doctrine')->getManager();

			// 读取当前分类
			$queryCategory = $em->getConnection()->prepare("select * from gospel_category_other where title = '$category'");
			$queryCategory->execute();
			$result = $queryCategory->fetch();
			$categoryOtherId = $result['id'];

			$output->writeln("<info>$categoryOtherId</info>");

			$query = $em->getConnection()->prepare('select id, title, categoryother_id cid from gospel_post where title like "%'.$name.'%"');
			$query->execute();
			$clients = $query->fetchAll();
			$total = count($clients);
			$output->writeln("<info>共找到<comment>{$total}</comment>篇文章</info>");
			foreach ($clients as $row) {
				$rowId = $row['id'];
				$output->writeln("<info>{$row['id']}:{$row[cid]}:{$row['title']}</info>");
				$queryUPdate = $em->getConnection()->prepare("update gospel_post set categoryother_id = $categoryOtherId where id = $rowId");
				$queryUPdate->execute();
			}
		}
		$output->writeln("<info>$text</info>");
	}
}
