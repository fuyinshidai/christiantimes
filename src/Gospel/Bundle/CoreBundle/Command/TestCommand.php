<?php

namespace Gospel\Bundle\CoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class TestCommand extends ContainerAwareCommand
{
	protected function configure()
	{
		$this->setName('post:load')
				->setDescription("Load post test");
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$output->writeln("<info>Test loading post</info>");
	}
}
