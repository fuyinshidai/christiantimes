<?php

/*
 * TwigExtension.php encoding=UTF-8
 *
 * Created on Oct 14, 2014, 10:29:52 AM
 *
 * christiantimes.cn default 
 * Copyright(c) Beijing Gospeltimes Information Technology, Inc.  All Rights Reserved.
 *
 */
namespace Gospel\Bundle\CoreBundle\Services;

use \Twig_Filter_Function;
use \Twig_Filter_Method;
use Doctrine\Common\Collections\Collection;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class TwigExtension extends \Twig_Extension
{

    /**
     * Return the functions registered as twig extensions
     * 
     * @return array
     */
    public function getFunctions()
    {
        return array(
            'img_width' => new \Twig_Function_Method($this, 'getImgWidth'),
            'sort' => new \Twig_Function_Method($this, 'sort'),
        );
    }

	public function getImgWidth($path)
	{
		return $path;
	}

	public function sort(Collection $objects, $name, $property = null)
	{
		$values = $objects->getValues();
		usort($values, function ($a, $b) use ($name, $property) {
			$name = 'get' . $name;
			if ($property) {
				$property = 'get' . $property;
				return strcasecmp($a->$name()->$property(), $b->$name()->$property());
			} else {
				return strcasecmp($a->$name(), $b->$name());
			}
		});
		return $values;
	}

    public function getName()
    {
        return 'twig_extension';
    }
}
