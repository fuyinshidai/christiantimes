<?php

/*
 * OSS.php encoding=UTF-8
 *
 * Created on Mar 5, 2015, 10:25:34 AM
 *
 * cccmusic.com default 
 * Copyright(c) Beijing Gospeltimes Information Technology, Inc.  All Rights Reserved.
 *
 */

namespace Gospel\Bundle\CoreBundle\Services;

use JohnLui\AliyunOSS\AliyunOSS;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class OSS
{

	private $ossClient;
	private $OSS_KEY = 'W7ywdZkvf9hFWZFU';
	private $OSSSECRET = '9WGfqWqGutDXGyJwXtThy3IVJglrg1';
	private $DEFAULT_BUCKET = 'christiantimes';

	public function __construct($isInternal = false)
	{
		$serverAddress = $isInternal ? "http://oss-cn-hangzhou.aliyuncs.com" : "http://oss-cn-hangzhou.aliyuncs.com";
		$this->ossClient = AliyunOSS::boot(
						$serverAddress, $this->OSS_KEY, $this->OSSSECRET
		);
		$this->ossClient->setBucket($this->DEFAULT_BUCKET);
	}

	public function upload($ossKey, $filePath)
	{
		$this->ossClient->setBucket($this->DEFAULT_BUCKET);
		$this->ossClient->uploadFile($ossKey, $filePath);
	}

	public function getUrl($ossKey)
	{
		$this->ossClient->setBucket($this->DEFAULT_BUCKET);
		return $this->ossClient->getUrl($ossKey, new \DateTime("+1 day"));
	}

	public function delete($ossKey)
	{
		return $this->ossClient->deleteFile($ossKey);
	}

	public function createBucket($bucketName)
	{
		return $this->ossClient->createBucket($bucketName);
	}

	public function getAllObjectKey($bucketName)
	{
		return $this->ossClient->getAllObjectKey($bucketName);
	}

}
