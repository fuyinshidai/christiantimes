<?php

/*
 * RequestInjector.php encoding=UTF-8
 *
 * Created on Dec 26, 2014, 10:39:05 AM
 *
 * gospeltimes.cn default 
 * Copyright(c) Beijing Gospeltimes Information Technology, Inc.  All Rights Reserved.
 *
 */
namespace Gospel\Bundle\CoreBundle\Classes;

use Symfony\Component\DependencyInjection\Container;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class RequestInjector
{
	protected $container;

	public function __construct(Container $container)
	{
		$this->container = $container;
	}

	/**
	 * 
	 * @return \Symfony\Component\HttpFoundation\Request
	 */
	public function getRequest()
	{
		return $this->container->get('request');
	}
}
