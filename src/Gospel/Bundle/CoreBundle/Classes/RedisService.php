<?php

/*
 * Redis.php encoding=UTF-8
 *
 * Created on Sep 1, 2014, 5:26:53 PM
 *
 * christiantimes.cn default 
 * Copyright(c) Beijing Gospeltimes Information Technology, Inc.  All Rights Reserved.
 *
 */

namespace Gospel\Bundle\CoreBundle\Classes;

use Redis;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class RedisService
{
	public static $REDIS_KEY_INDEX_HTML = 'index-html';
	private $port;

	private $serializer;

	/**
	 *
	 * @var Redis 
	 */
	private $redis;

	private $arrayDelimeter = '<<|>>';

	private $prefix = 'ct:';

	public function __construct($host, $port)
	{
		$this->redis = new Redis;
		$this->redis->connect($host, $port);
		$this->redis->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_PHP);
		$this->redis->setOption(Redis::OPT_PREFIX, $this->prefix);
	}

	/**
	 * keys used for the index page
	 * @return type
	 */
	public function getIndexKeys()
	{
		$keys = array(
			'indexData',
			'headline1',
			'headline2',
			'headlineSociety',
			'headline3',
			'subHeadlineTop',
			'subHeadline2',
			'subheadline3',
			'headlineNew1',
			'headlineRestNew',
			'subHeadlineTopNew',
			'subHeadline2New',
			'subheadline3New',
			'fineColumnsNew',
			'reading',
			'columnist',
			'fineColumnsMustRead',
			'dataCategory',
			self::$REDIS_KEY_INDEX_HTML,
		);

		return $keys;
	}

	public function getVideoKeys()
	{
		$keys = array(
			'indexData',
			'rightVideo',
		);

		return $keys;
	}

	/**
	 * 
	 * @return \Redis
	 */
	public function getInstance()
	{
		return $this->redis;
	}

	/**
	 * Delete redis cache for the homepage chache block
	 */
	public function flushIndex()
	{
		return $this->getInstance()->delete($this->getIndexKeys());
	}

	/**
	 * Delete redis cache for the homepage chache block
	 */
	public function flushVideo()
	{
		return $this->getInstance()->delete($this->getVideoKeys());
	}
}
