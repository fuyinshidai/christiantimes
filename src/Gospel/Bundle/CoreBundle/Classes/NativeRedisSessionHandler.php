<?php

namespace Gospel\Bundle\CoreBundle\Classes;
use \Symfony\Component\HttpFoundation\Session\Storage\Handler\NativeSessionHandler;

/*
 * NativeRedisSessionHandler.php encoding=UTF-8
 *
 * Created on Dec 8, 2014, 11:27:31 AM
 *
 * gospeltimes.cn default 
 * Copyright(c) Beijing Gospeltimes Information Technology, Inc.  All Rights Reserved.
 *
 */

 
/**
 * NativeRedisSessionStorage.
 *
 * Driver for the redis session save hadlers provided by the redis PHP extension.
 *
 * @see https://github.com/nicolasff/phpredis
 *
 * @author Andrej Hudec &lt;pulzarraider@gmail.com&gt;
 * @author Piotr Pelczar &lt;me@athlan.pl&gt;
 */
class NativeRedisSessionHandler extends NativeSessionHandler
{
    /**
     * Constructor.
     *
     * @param string $savePath Path of redis server.
     */
    public function __construct($savePath = "")
    {
        if (!extension_loaded('redis')) {
            throw new \RuntimeException('PHP does not have "redis" session module registered');
        }
 
        if ("" === $savePath) {
            $savePath = ini_get('session.save_path');
        }
 
        if ("" === $savePath) {
            $savePath = "tcp://localhost:6379"; // guess path
        }
 
        ini_set('session.save_handler', 'redis');
        ini_set('session.save_path', $savePath);
    }
}