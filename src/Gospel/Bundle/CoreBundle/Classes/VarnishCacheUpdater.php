<?php

/*
 * VarnishCacheUpdater.php encoding=UTF-8
 * 
 * Used to update varnish cache, purge, refrech, update by url pattern, etc
 *
 * Created on Dec 27, 2014, 10:08:53 AM
 *
 * gospeltimes.cn default 
 * Copyright(c) Beijing Gospeltimes Information Technology, Inc.  All Rights Reserved.
 *
 */

namespace Gospel\Bundle\CoreBundle\Classes;

use Gospel\Bundle\CoreBundle\Classes\RequestInjector;

/**
 * @author QiuLin Wang <wangqiulin.1988@gmail.com>
 * @author Zhili He <zhili850702@gmail.com>
 * @copyright 2014 - 2020 GospelTimes IT CO. LTD all rights reserved.
 * @license GNU General Public License, version 2
 */
class VarnishCacheUpdater {
    /**
     * @var VarnishCacheUpdater Singleton VarnishCacheUpdater instance.
     */
    private static $varnishCacheUpdater = null;
    /**
     * @var string Web server host name or IP.
     */
    private $host;
    /**
     * @var string Web server port.
     */
    private $port;
    /**
     * @var boolean If debug mode enabled.
     */
    private $debug;

    /**
     * Create or get singleton VarnishCacheUpdater instance.
     *
     * @param RequestInjector $host
     * @param string $port
     * @param boolean $debug
     *
     * @return VarnishCacheUpdater 
     */
    public function __construct(RequestInjector $requestInjector, $port, $debug) {
		$request = $requestInjector->getRequest();
        $this->host = $request->getHost();
        $this->port = $port;
        $this->debug = $debug;

    }

    /**
     * Create or get singleton VarnishCacheUpdater instance.
     *
     * @param string $host
     * @param string $port
     * @param boolean $debug
     *
     * @return VarnishCacheUpdater 
     */
    static public function getInstance($host, $port = 80, $debug = false) {
        if (self::$varnishCacheUpdater == null) {
            self::$varnishCacheUpdater = new self($host, $port, $debug);
        }

        return self::$varnishCacheUpdater;
    }

    /**
     * Strip cache of a single cached object pointed by the given url.
     *
     * @param mixed $url The url of the cached object to be purged or the urls array
     */
    public function purge($url) {
		if (is_array($url)) {
			foreach ($url as $v) {
				$this->update('PURGE', $v);
			}
		} else {
			$this->update('PURGE', $url);
		}
    }

    /**
     * Refresh cache of a single cached object pointed by the given url. Useful when the cache update requires much time.
     *
     * @param string $url The url of the cached object to be purged.
     */
    public function refresh($url) {
        return $this->update('REFRESH', $url);
    }

    /**
     * Strip cache of cached objects pointed by the given url pattern.
     *
     * @param string $url The url to be send to.
     * @param string $banUrlPattern The url pattern of the cached objects to be purged.
     */
    public function ban($url, $banUrlPattern) {
        return $this->update('BAN', $url, $banUrlPattern);
    }

    private function update($method, $url, $urlPattern = '/*') {
        if (!in_array($method, array('PURGE', 'BAN', 'REFRESH', 'GET'))) {
            return FALSE;
        }

        $finalUrl = sprintf("http://%s:%d%s", $this->host, $this->port, $url);

        $curlOptionList = array(
            CURLOPT_RETURNTRANSFER    => true,
            CURLOPT_CUSTOMREQUEST     => $method,
            CURLOPT_HEADER            => true,
            CURLOPT_NOBODY            => true,
            CURLOPT_URL               => $finalUrl,
            CURLOPT_CONNECTTIMEOUT_MS => 2000
        );

        if ($method == "BAN") {
            $curlOptionList[CURLOPT_HTTPHEADER] = array(
                "X-Ban-Host: $this->host",
                "X-Ban-Url: $urlPattern"
            );
        }

        if ($this->debug == true) {
            $curlOptionList[CURLOPT_VERBOSE] = true;
        }

        $curlHandler = \curl_init();
        curl_setopt_array($curlHandler, $curlOptionList);
        $result = curl_exec($curlHandler);
        $status = curl_getinfo($curlHandler, CURLINFO_HTTP_CODE);

        if ($result == false || $status != "200") {
            $err = curl_error($curlHandler);
        } else {
            $err = 'OK';
        }
        curl_close($curlHandler);

        $response = array(
            'statusCode' => (int)$status,
            'message'    => $err,
            'body'       => $result
        );

        return $response;
    }

	public function get($url)
	{
        $finalUrl = sprintf("http://%s:%d%s", $this->host, $this->port, $url);

        $curlOptionList = array(
            CURLOPT_RETURNTRANSFER    => true,
            CURLOPT_CUSTOMREQUEST     => 'GET',
            CURLOPT_HEADER            => true,
            CURLOPT_NOBODY            => true,
            CURLOPT_URL               => $finalUrl,
            CURLOPT_CONNECTTIMEOUT_MS => 2000
        );

        if ($this->debug == true) {
            $curlOptionList[CURLOPT_VERBOSE] = true;
        }

        $curlHandler = \curl_init();
        curl_setopt_array($curlHandler, $curlOptionList);
        $result = curl_exec($curlHandler);
        $status = curl_getinfo($curlHandler, CURLINFO_HTTP_CODE);

        if ($result == false || $status != "200") {
            $err = curl_error($curlHandler);
        } else {
            $err = 'OK';
        }
        curl_close($curlHandler);

        $response = array(
            'statusCode' => (int)$status,
            'message'    => $err,
            'body'       => $result
        );

        return $response;
	}
}

?>
