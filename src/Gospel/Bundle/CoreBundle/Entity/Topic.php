<?php

/*
 * 文章的模型
 */
namespace Gospel\Bundle\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Topic
 * 
 * @ORM\Table(name="gospel_topic")
 * @ORM\Entity(repositoryClass="Gospel\Bundle\CoreBundle\Entity\TopicRepository")
 * @ORM\HasLifecycleCallbacks
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class Topic
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

	protected $tempCover, $tempFile;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     */
    protected $link;

    /**
     * @var string
     *
     * @ORM\Column(name="serial", type="string", length=255, nullable=true)
     */
    protected $serial;


	/**
     * @ORM\Column(name="cover_path", type="string", length=255, nullable=true)
     */
    protected $coverPath;

	/**
     * @ORM\Column(name="topic_banner_path", type="string", length=255, nullable=true)
     */
    protected $topicBannerPath;

	/**
     * @Assert\File(maxSize="6000000")
     */
    protected $cover;

	/**
     * @Assert\File(maxSize="6000000")
     */
    protected $topic;

	/**
     * @ORM\ManyToMany(targetEntity="Post")
	 * @ORM\JoinTable(name="gospel_topic_posts")
     */
    protected $posts;

    /**
     * @var string
     *
	 * @ORM\ManyToOne(targetEntity="Gospel\Bundle\CoreBundle\Entity\CategoryTopic")
	 * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=true)
     */
    protected $category;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    protected $content;


    /**
     * @var integer
     *
     * @ORM\Column(name="promotionStatus", type="boolean", nullable=true)
     */
    protected $promotionStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="boolean")
     */
    protected $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createAt", type="datetime")
     */
    protected $createAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="publishedAt", type="datetime")
     */
    protected $publishedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="stopPublish", type="boolean", nullable=true)
     */
    protected $stopPublish;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="stopPublishedAt", type="datetime", nullable=true)
     */
    protected $stopPublishedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime")
     */
    protected $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="alias", type="string", length=255, nullable=true)
     */
    protected $alias;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_keywords", type="string", length=255, nullable=true)
     */
    protected $metaKeywords;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="text", nullable=true)
     */
    protected $metaDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="hits", type="integer", nullable=true)
     */
    protected $hits;

	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Topic
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    
    /**
     * Set content
     *
     * @param string $content
     * @return Topic
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }


    /**
     * Set promotionStatus
     *
     * @param integer $promotionStatus
     * @return Topic
     */
    public function setPromotionStatus($promotionStatus)
    {
        $this->promotionStatus = $promotionStatus;
    
        return $this;
    }

    /**
     * Get promotionStatus
     *
     * @return integer 
     */
    public function getPromotionStatus()
    {
        return $this->promotionStatus;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Topic
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     * @return Topic
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;
    
        return $this;
    }

    /**
     * Get createAt
     *
     * @return \DateTime 
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     * @return Topic
     */
    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;
    
        return $this;
    }

    /**
     * Get publishedAt
     *
     * @return \DateTime 
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     * @return Topic
     */
    public function setStopPublishedAt($stopPublishedAt)
    {
        $this->stopPublishedAt = $stopPublishedAt;
    
        return $this;
    }

    /**
     * Get stopPublishedAt
     *
     * @return \DateTime 
     */
    public function getStopPublishedAt()
    {
        return $this->stopPublishedAt;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
        $this->posts = new \Doctrine\Common\Collections\ArrayCollection();
    }
    

	public function __toString()
	{
		return $this->title;
	}


    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Topic
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set metaKeywords
     *
     * @param string $metaKeywords
     * @return Topic
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;
    
        return $this;
    }

    /**
     * Get metaKeywords
     *
     * @return string 
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     * @return Topic
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
    
        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string 
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set alias
     *
     * @param string $alias
     * @return Topic
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    
        return $this;
    }

    /**
     * Get alias
     *
     * @return string 
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set hits
     *
     * @param \int $hits
     * @return Topic
     */
    public function setHits($hits)
    {
        $this->hits = $hits;
    
        return $this;
    }

    /**
     * Get hits
     *
     * @return \int 
     */
    public function getHits()
    {
        return $this->hits;
    }

    /**
     * Set stopPublish
     *
     * @param boolean $stopPublish
     * @return Topic
     */
    public function setStopPublish($stopPublish)
    {
        $this->stopPublish = $stopPublish;
    
        return $this;
    }

    /**
     * Get stopPublish
     *
     * @return boolean 
     */
    public function getStopPublish()
    {
        return $this->stopPublish;
    }

    /**
     * Set type
     *
     * @param boolean $type
     * @return Topic
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return boolean 
     */
    public function getType()
    {
        return $this->type;
    }


	/**
     * Sets topic.
     *
     * @param UploadedTopic $topic
     */
    public function setTopic(UploadedFile $topic = null)
    {
        $this->topic = $topic;
		// 检查是否有旧的文件，如果有保存到临时变量里面，当更新的时候进行删除
		if (isset($this->topic)) {
			$this->tempTopic = $this->topic;
			$this->topicBannerPath = null;
		} else {
			$this->topicBannerPath = null;
		}
    }

    /**
     * Get topic.
     *
     * @return UploadedTopic
     */
    public function getTopic()
    {
        return $this->topic;
    }

	/**
     * Sets cover.
     *
     * @param UploadedCover $cover
     */
    public function setCover(UploadedFile $cover = null)
    {
		echo __FUNCTION__."++++++++++++++++++++++++++++++++++++++++++==<br />";
		if ($cover == null)
			return;

        $this->cover = $cover;
		// 检查是否有旧的文件，如果有保存到临时变量里面，当更新的时候进行删除
		if (isset($this->coverPath)) {
			$this->tempCover = $this->coverPath;
			//$this->coverPath = null;
		} else {
			$this->coverPath = null;
		}
    }

    /**
     * Get cover.
     *
     * @return UploadedCover
     */
    public function getCover()
    {
        return $this->cover;
    }

	/**
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 */
	public function preUpload()
	{
		if (null !== $this->getCover()) {
			// 生成一个独一无二的名字
			$coverName = sha1(uniqid(mt_rand(), true));
			$this->coverPath = $coverName . '.' . $this->getCover()->guessExtension();
		}

		if (null !== $this->getTopic()) {
			// 生成一个独一无二的名字
			$topicName = sha1(uniqid(mt_rand(), true));
			$this->topicBannerPath = $topicName . '.' . $this->getTopic()->guessExtension();
		}
	}

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null !== $this->getCover()) {
            // 如果是在移动文件的时候出现错误，
            // move()函数会扔出异常，从而阻止内容被保存到数据库
            $this->getCover()->move($this->getAbsoluteCoverPath(), $this->coverPath);

            // 查看是否有旧的文件
            if (isset($this->tempCover)) {
                //删除旧的文件
                @unlink($this->getAbsoluteCoverPath().DIRECTORY_SEPARATOR.$this->tempCover);
            }
        }
        if (null !== $this->getTopic()) {
            // 如果是在移动文件的时候出现错误，
            // move()函数会扔出异常，从而阻止内容被保存到数据库
            $this->getTopic()->move($this->getAbsoluteTopicBannerPath(), $this->topicBannerPath);
            
            // 查看是否有旧的文件
            if (isset($this->tempTopic)) {
                //删除旧的文件
                @unlink($this->getAbsoluteTopicBannerPath().DIRECTORY_SEPARATOR.$this->tempTopic);
            }
        }
        return;
    }

    /**
     * @ORM\PostRemove()
     */
    public function remove()
    {
        if ($coverPath = $this->getCoverPath()) {
            unlink($this->getAbsoluteCoverPath().DIRECTORY_SEPARATOR.$coverPath);
        }
        if ($topicBannerPath = $this->getTopicBannerPath()) {
            unlink($this->getAbsoluteTopicBannerPath().DIRECTORY_SEPARATOR.$topicBannerPath);
        }
    }

    public function getCoverPath()
    {
        return $this->coverPath;
    }

    public function getTopicBannerPath()
    {
        return $this->topicBannerPath;
    }
    
    /**
     * 封面图片的相对路径
     */
	public function getCoverWebPath()
	{
		return 'assets/media/topic';
	}

    /**
     * 视频的相对路径
     */
	public function getTopicWebPath()
	{
		return 'assets/media/topic';
	}

    /**
     * 服务器的根路径
     */
	public function getWebRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return $_SERVER['DOCUMENT_ROOT'];
    }

    public function getAbsoluteCoverPath()
    {
        return $this->getWebRootDir() . DIRECTORY_SEPARATOR . $this->getCoverWebPath();
    }

    public function getAbsoluteTopicBannerPath()
    {
        return $this->getWebRootDir() . DIRECTORY_SEPARATOR . $this->getTopicWebPath();
    }

    /**
     * 封面图片的路径
     */
	public function getCoverWebUrl()
	{
		return $this->getCoverWebPath() . DIRECTORY_SEPARATOR . $this->getCoverPath();
	}



    /**
     * Set coverPath
     *
     * @param string $coverPath
     * @return Topic
     */
    public function setCoverPath($coverPath)
    {
        $this->coverPath = $coverPath;
    
        return $this;
    }

    /**
     * Set topicBannerPath
     *
     * @param string $topicBannerPath
     * @return Topic
     */
    public function setTopicBannerPath($topicBannerPath)
    {
        $this->topicBannerPath = $topicBannerPath;
    
        return $this;
    }

    /**
     * Add post
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\Post $post
     * @return Topic
     */
    public function addPost(\Gospel\Bundle\CoreBundle\Entity\Post $post)
    {
		if ($this->posts->contains($post))
			return $this;
		else {
			$this->posts->add($post);
			
			return $this;
		}
    }

    /**
     * Remove post
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\Post $post
     */
    public function removePost(\Gospel\Bundle\CoreBundle\Entity\Post $post)
    {
		if ($this->posts->contains($post))
	        $this->posts->removeElement($post);
    }

	public function clearPosts($em) {
		if (null != $this->posts) {
			$this->posts->clear();
			$em->flush();
		}
	}

    /**
     * Get posts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPosts()
    {
        return $this->posts;
    }


    /**
     * Set category
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\CategoryTopic $category
     * @return Topic
     */
    public function setCategory(\Gospel\Bundle\CoreBundle\Entity\CategoryTopic $category = null)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return \Gospel\Bundle\CoreBundle\Entity\CategoryTopic
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set serial
     *
     * @param string $serial
     * @return Topic
     */
    public function setSerial($serial)
    {
        $this->serial = $serial;
    
        return $this;
    }

    /**
     * Get serial
     *
     * @return string 
     */
    public function getSerial()
    {
        return $this->serial;
    }

	public function getTopicBannerWebPath()
	{
		return $this->getTopicWebPath().DIRECTORY_SEPARATOR.$this->getTopicBannerPath();
	}

	public function getLink()
	{
		return $this->link;
	}

	public function setLink($link)
	{
		$this->link = $link;
		return $this;
	}
}
