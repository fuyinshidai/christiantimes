<?php

namespace Gospel\Bundle\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PostImage
 *
 * @ORM\Table(name="gospel_post_image")
 * @ORM\Entity(repositoryClass="Gospel\Bundle\CoreBundle\Entity\PostImageRepository")
 */
class PostImage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
	 * @ORM\ManyToOne(targetEntity="Post", inversedBy="images", cascade={"persist"})
	 * @ORM\JoinColumn(name="post_id", referencedColumnName="id")
     */
    private $post;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="caption", type="string", length=255, nullable=true)
     */
    private $caption;

    /**
     * @var string
     *
     * @ORM\Column(name="use_watermark", type="boolean", nullable=true, options={"default" = 1})
     */
    private $useWatermark;

    /**
     * @var string
     *
     * @ORM\Column(name="is_cover", type="boolean", nullable=true)
     */
    private $isCover;
	

    /**
     * @var integer
     *
     * @ORM\Column(name="sortOrder", type="integer")
     */
    private $sortOrder;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set post
     *
     * @param integer $post
     * @return PostImage
     */
    public function setPost($post)
    {
        $this->post = $post;
    
        return $this;
    }

    /**
     * Get post
     *
     * @return integer 
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return PostImage
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return PostImage
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set caption
     *
     * @param string $caption
     * @return PostImage
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;
    
        return $this;
    }

    /**
     * Get caption
     *
     * @return string 
     */
    public function getCaption()
    {
        return $this->caption;
    }

    /**
     * Set useWatermark
     *
     * @param boolean $useWatermark
     * @return PostImage
     */
    public function setUseWatermark($useWatermark)
    {
        $this->useWatermark = $useWatermark;
    
        return $this;
    }

    /**
     * Get useWatermark
     *
     * @return boolean 
     */
    public function getUseWatermark()
    {
        return $this->useWatermark;
    }

    /**
     * Set isCover
     *
     * @param boolean $isCover
     * @return PostImage
     */
    public function setIsCover($isCover)
    {
        $this->isCover = $isCover;
    
        return $this;
    }

    /**
     * Get isCover
     *
     * @return boolean 
     */
    public function getIsCover()
    {
        return $this->isCover;
    }
}