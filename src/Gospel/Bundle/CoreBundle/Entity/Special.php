<?php

namespace Gospel\Bundle\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Special
 *
 * @ORM\Table(name="gospel_special")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="Gospel\Bundle\CoreBundle\Entity\SpecialRepository")
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class Special
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

	/**
	 * 临时文件变量
	 * @var UploadedFile 
	 */
	private $tempFile;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="template", type="string", length=255, nullable=true)
     */
    private $template;

    /**
     * @var string
     *
     * @ORM\Column(name="banner", type="string", length=255, nullable=true)
     */
    private $banner;

	/**
	 * @Assert\File(maxSize="6000000")
	 */
	private $file;

    /**
     * @var integer
     *
     * @ORM\Column(name="headline", type="integer")
     */
    private $headline;

    /**
     * @var integer
     *
     * @ORM\Column(name="subheadline", type="string", length=255)
     */
    private $subheadline;

    /**
     * @var integer
     *
     * @ORM\Column(name="category1", type="integer")
     */
    private $category1;

    /**
     * @var integer
     *
     * @ORM\Column(name="category2", type="integer")
     */
    private $category2;

    /**
     * @var integer
     *
     * @ORM\Column(name="category3", type="integer")
     */
    private $category3;

    /**
     * @var integer
     *
     * @ORM\Column(name="category4", type="integer")
     */
    private $category4;

    /**
     * @var integer
     *
     * @ORM\Column(name="category5", type="integer")
     */
    private $category5;

    /**
     * @var integer
     *
     * @ORM\Column(name="category6", type="integer")
     */
    private $category6;

    /**
     * @var integer
     *
     * @ORM\Column(name="recommend", type="string", length=255)
     */
    private $recommend;

    /**
     * @var integer
     *
     * @ORM\Column(name="interpretation", type="integer")
     */
    private $interpretation;

    /**
     * @var integer
     *
     * @ORM\Column(name="video", type="integer")
     */
    private $video;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createAt", type="datetime")
     */
    private $createAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="publishedAt", type="datetime")
     */
    private $publishedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="stopPublish", type="boolean", nullable=true)
     */
    private $stopPublish;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="stopPublishedAt", type="datetime", nullable=true)
     */
    private $stopPublishedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime")
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="alias", type="string", length=255, nullable=true)
     */
    private $alias;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_keywords", type="string", length=255, nullable=true)
     */
    private $metaKeywords;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="text", nullable=true)
     */
    private $metaDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="hits", type="integer", nullable=true)
     */
    private $hits;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Special
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Special
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set template
     *
     * @param string $template
     * @return Special
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    
        return $this;
    }

    /**
     * Get template
     *
     * @return string 
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set headline
     *
     * @param integer $headline
     * @return Special
     */
    public function setHeadline($headline)
    {
        $this->headline = $headline;
    
        return $this;
    }

    /**
     * Get headline
     *
     * @return integer 
     */
    public function getHeadline()
    {
        return $this->headline;
    }

    /**
     * Set subheadline
     *
     * @param integer $subheadline
     * @return Special
     */
    public function setSubheadline($subheadline)
    {
        $this->subheadline = $subheadline;
    
        return $this;
    }

    /**
     * Get subheadline
     *
     * @return integer 
     */
    public function getSubheadline()
    {
        return $this->subheadline;
    }


    /**
     * Set recommend
     *
     * @param integer $recommend
     * @return Special
     */
    public function setRecommend($recommend)
    {
        $this->recommend = $recommend;
    
        return $this;
    }

    /**
     * Get recommend
     *
     * @return integer 
     */
    public function getRecommend()
    {
        return $this->recommend;
    }

    /**
     * Set interpretation
     *
     * @param integer $interpretation
     * @return Special
     */
    public function setInterpretation($interpretation)
    {
        $this->interpretation = $interpretation;
    
        return $this;
    }

    /**
     * Get interpretation
     *
     * @return integer 
     */
    public function getInterpretation()
    {
        return $this->interpretation;
    }

    /**
     * Set video
     *
     * @param integer $video
     * @return Special
     */
    public function setVideo($video)
    {
        $this->video = $video;
    
        return $this;
    }

    /**
     * Get video
     *
     * @return integer 
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Special
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     * @return Special
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;
    
        return $this;
    }

    /**
     * Get createAt
     *
     * @return \DateTime 
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set publishedAt
     *
     * @param \DateTime $publishedAt
     * @return Special
     */
    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;
    
        return $this;
    }

    /**
     * Get publishedAt
     *
     * @return \DateTime 
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * Set stopPublish
     *
     * @param boolean $stopPublish
     * @return Special
     */
    public function setStopPublish($stopPublish)
    {
        $this->stopPublish = $stopPublish;
    
        return $this;
    }

    /**
     * Get stopPublish
     *
     * @return boolean 
     */
    public function getStopPublish()
    {
        return $this->stopPublish;
    }

    /**
     * Set stopPublishedAt
     *
     * @param \DateTime $stopPublishedAt
     * @return Special
     */
    public function setStopPublishedAt($stopPublishedAt)
    {
        $this->stopPublishedAt = $stopPublishedAt;
    
        return $this;
    }

    /**
     * Get stopPublishedAt
     *
     * @return \DateTime 
     */
    public function getStopPublishedAt()
    {
        return $this->stopPublishedAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Special
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set alias
     *
     * @param string $alias
     * @return Special
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    
        return $this;
    }

    /**
     * Get alias
     *
     * @return string 
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set metaKeywords
     *
     * @param string $metaKeywords
     * @return Special
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;
    
        return $this;
    }

    /**
     * Get metaKeywords
     *
     * @return string 
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     * @return Special
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
    
        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string 
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set hits
     *
     * @param integer $hits
     * @return Special
     */
    public function setHits($hits)
    {
        $this->hits = $hits;
    
        return $this;
    }

    /**
     * Get hits
     *
     * @return integer 
     */
    public function getHits()
    {
        return $this->hits;
    }

    /**
     * Set banner
     *
     * @param string $banner
     * @return Special
     */
    public function setBanner($banner)
    {
        $this->banner = $banner;
    
        return $this;
    }

    /**
     * Get banner
     *
     * @return string 
     */
    public function getBanner()
    {
        return $this->banner;
    }

	/**
	 * 
	 * @return UploadedFile
	 */
	public function getFile()
	{
		return $this->file;
	}

	/**
	 * 
	 * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
	 * @return \Gospel\Bundle\CoreBundle\Entity\Special
	 */
	public function setFile(UploadedFile $file)
	{
		$this->file = $file;
		return $this;
	}

	/**
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 */
	public function preUpload()
	{
		if (null !== $this->getFile()) {
			// 生成独一无二的名字
			$bannerName = sha1(uniqid(mt_rand(), true));
			$this->banner = $bannerName . '.' . $this->getFile()->guessExtension();
		}
	}

	/**
	 * @ORM\PostPersist()
	 * @ORM\PostUpdate()
	 */
	public function upload()
	{
		if (null !== $this->getFile()) {
			// 如果上传过程出现错误会扔出异常防止数据存入数据库
			$this->getFile()->move($this->getAbsolutePath(), $this->banner);
		}
	}

	private function getAbsolutePath()
	{
        return $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . $this->getBannerWebPath();
	}

	public function getBannerWebPath() {
		
		return 'assets/media/special';
	}

	public function __toString()
	{
		return $this->title;
	}

    /**
     * Set category1
     *
     * @param integer $category1
     * @return Special
     */
    public function setCategory1($category1)
    {
        $this->category1 = $category1;
    
        return $this;
    }

    /**
     * Get category1
     *
     * @return integer 
     */
    public function getCategory1()
    {
        return $this->category1;
    }

    /**
     * Set category2
     *
     * @param integer $category2
     * @return Special
     */
    public function setCategory2($category2)
    {
        $this->category2 = $category2;
    
        return $this;
    }

    /**
     * Get category2
     *
     * @return integer 
     */
    public function getCategory2()
    {
        return $this->category2;
    }

    /**
     * Set category3
     *
     * @param integer $category3
     * @return Special
     */
    public function setCategory3($category3)
    {
        $this->category3 = $category3;
    
        return $this;
    }

    /**
     * Get category3
     *
     * @return integer 
     */
    public function getCategory3()
    {
        return $this->category3;
    }

    /**
     * Set category4
     *
     * @param integer $category4
     * @return Special
     */
    public function setCategory4($category4)
    {
        $this->category4 = $category4;
    
        return $this;
    }

    /**
     * Get category4
     *
     * @return integer 
     */
    public function getCategory4()
    {
        return $this->category4;
    }

    /**
     * Set category5
     *
     * @param integer $category5
     * @return Special
     */
    public function setCategory5($category5)
    {
        $this->category5 = $category5;
    
        return $this;
    }

    /**
     * Get category5
     *
     * @return integer 
     */
    public function getCategory5()
    {
        return $this->category5;
    }

    /**
     * Set category6
     *
     * @param integer $category6
     * @return Special
     */
    public function setCategory6($category6)
    {
        $this->category6 = $category6;
    
        return $this;
    }

    /**
     * Get category6
     *
     * @return integer 
     */
    public function getCategory6()
    {
        return $this->category6;
    }
}