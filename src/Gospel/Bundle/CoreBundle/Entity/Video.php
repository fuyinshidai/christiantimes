<?php

/*
 * 文章的模型
 */
namespace Gospel\Bundle\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Gospel\Bundle\CoreBundle\Services\OSS;

/**
 * Video
 * 
 * @ORM\Table(name="gospel_video")
 * @ORM\Entity(repositoryClass="Gospel\Bundle\CoreBundle\Entity\VideoRepository")
 * @ORM\HasLifecycleCallbacks
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class Video
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

	protected $tempCover, $tempFile;

	/**
	 * The string of all the tags
	 * @var type 
	 */
	protected $tagsCommaString;

	/**
	 * 用来设置相关文章的临时字段
	 * @var type 
	 */
	protected $relatedVideos;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    protected $url;


	/**
     * @ORM\Column(name="cover_path", type="string", length=255, nullable=true)
     */
    protected $coverPath;

	/**
     * @ORM\Column(name="video_path", type="string", length=255, nullable=true)
     */
    protected $videoPath;

	/**
     * @Assert\File(maxSize="6000000")
     */
    protected $cover;

	/**
     * @Assert\File(maxSize="214748364")
     */
    protected $video;


    /**
     * @var string
     *
     * @ORM\Column(name="author", type="string", length=255, nullable=true)
     */
    protected $author;

	/**
     * @var integer
	 * 0为远程，1为本地
     *
     * @ORM\Column(name="type", type="boolean", nullable=true)
	 */
	protected $type;


    /**
     * @var string
     *
     * @ORM\Column(name="embed_code", type="text", nullable=true)
     */
    protected $embedCode;

    /**
     * @var string
     *
     * @ORM\Column(name="comefrom", type="string", length=25, nullable=true)
     */
    protected $comefrom;

	/**
	 * @ORM\ManyToMany(targetEntity="Gospel\Bundle\CoreBundle\Entity\Videotag")
	 * @ORM\JoinTable(name="gospel_video_tag_join"
     *      )
	 */
	protected $tags;

	/**
	 * Related videos
	 * 
	 * @ORM\ManyToMany(targetEntity="Gospel\Bundle\CoreBundle\Entity\Video")
	 * @ORM\JoinTable(name="gospel_video_related",
	 * joinColumns={@ORM\JoinColumn(name="video_id", referencedColumnName="id")},
	 * inverseJoinColumns={@ORM\JoinColumn(name="related_id", referencedColumnName="id")}
	 * )
	 */
	protected $related;

    /**
     * @var string
     *
	 * @ORM\ManyToOne(targetEntity="Gospel\Bundle\CoreBundle\Entity\CategoryVideo")
	 * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    protected $category;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    protected $content;

    /**
     * @var integer
     *
     * @ORM\Column(name="commentStatus", type="boolean")
     */
    protected $commentStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="promotionStatus", type="boolean", nullable=true)
     */
    protected $promotionStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="boolean")
     */
    protected $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="editor_recommend", type="boolean", nullable=true)
     */
    protected $editorRecommemd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createAt", type="datetime")
     */
    protected $createAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="publishedAt", type="datetime")
     */
    protected $publishedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="stopPublish", type="boolean", nullable=true)
     */
    protected $stopPublish;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="stopPublishedAt", type="datetime", nullable=true)
     */
    protected $stopPublishedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime")
     */
    protected $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="alias", type="string", length=255, nullable=true)
     */
    protected $alias;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_keywords", type="string", length=255, nullable=true)
     */
    protected $metaKeywords;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="text", nullable=true)
     */
    protected $metaDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="hits", type="integer", nullable=true)
     */
    protected $hits;

	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Video
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Video
     */
    public function setUrl($url)
    {
        $this->url = $url;
    
        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set author
     *
     * @param string $author
     * @return Video
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    
        return $this;
    }

    /**
     * Get author
     *
     * @return string 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set comefrom
     *
     * @param string $comefrom
     * @return Video
     */
    public function setComefrom($comefrom)
    {
        $this->comefrom = $comefrom;
    
        return $this;
    }

    /**
     * Get comefrom
     *
     * @return string 
     */
    public function getComefrom()
    {
        return $this->comefrom;
    }
    
    /**
     * Set content
     *
     * @param string $content
     * @return Video
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set commentStatus
     *
     * @param integer $commentStatus
     * @return Video
     */
    public function setCommentStatus($commentStatus)
    {
        $this->commentStatus = $commentStatus;
    
        return $this;
    }

    /**
     * Get commentStatus
     *
     * @return integer 
     */
    public function getCommentStatus()
    {
        return $this->commentStatus;
    }

    /**
     * Set promotionStatus
     *
     * @param integer $promotionStatus
     * @return Video
     */
    public function setPromotionStatus($promotionStatus)
    {
        $this->promotionStatus = $promotionStatus;
    
        return $this;
    }

    /**
     * Get promotionStatus
     *
     * @return integer 
     */
    public function getPromotionStatus()
    {
        return $this->promotionStatus;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Video
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     * @return Video
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;
    
        return $this;
    }

    /**
     * Get createAt
     *
     * @return \DateTime 
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     * @return Video
     */
    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;
    
        return $this;
    }

    /**
     * Get publishedAt
     *
     * @return \DateTime 
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     * @return Video
     */
    public function setStopPublishedAt($stopPublishedAt)
    {
        $this->stopPublishedAt = $stopPublishedAt;
    
        return $this;
    }

    /**
     * Get stopPublishedAt
     *
     * @return \DateTime 
     */
    public function getStopPublishedAt()
    {
        return $this->stopPublishedAt;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
        $this->related = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add tags
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\Videotag $tags
     * @return Video
     */
    public function addTag(\Gospel\Bundle\CoreBundle\Entity\Videotag $tags)
    {
        $this->tags[] = $tags;
    
        return $this;
    }

    /**
     * Remove tags
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\Videotag $tags
     */
    public function removeTag(\Gospel\Bundle\CoreBundle\Entity\Videotag $tags)
    {
        $this->tags->removeElement($tags);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTags()
    {
        return $this->tags;
    }

	public function getTagsCommaString()
	{
		return $this->tagsCommaString;
	}

	public function setTagsCommaString($value)
	{
		if(null === $value) {
			return;
		}
	}

	public function setRelatedVideos($videos)
	{
		$this->relatedVideos = $videos;
	}

	public function getRelatedVideos()
	{
		$data = array();
		foreach($this->getRelated() as $video) {
			$data[] = '['.$video->getId().']'.$video->getTitle();
		}
		return implode(',',$data);
	}

	public function __toString()
	{
		return $this->title;
	}

    /**
     * Add related
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\Video $related
     * @return Video
     */
    public function addRelated(\Gospel\Bundle\CoreBundle\Entity\Video $related)
    {
		if(!$this->related->contains($related)) {
        	$this->related[] = $related;
		}
    
        return $this;
    }

    /**
     * Remove related
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\Video $related
     */
    public function removeRelated(\Gospel\Bundle\CoreBundle\Entity\Video $related)
    {
        $this->related->removeElement($related);
    }

    /**
     * Get related
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRelated()
    {
        return $this->related;
    }

    /**
     * Set category
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\CategoryVideo $category
     * @return Video
     */
    public function setCategory(\Gospel\Bundle\CoreBundle\Entity\CategoryVideo $category = null)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return \Gospel\Bundle\CoreBundle\Entity\CategoryVideo
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Video
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set metaKeywords
     *
     * @param string $metaKeywords
     * @return Video
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;
    
        return $this;
    }

    /**
     * Get metaKeywords
     *
     * @return string 
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     * @return Video
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
    
        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string 
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set alias
     *
     * @param string $alias
     * @return Video
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    
        return $this;
    }

    /**
     * Get alias
     *
     * @return string 
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set hits
     *
     * @param \int $hits
     * @return Video
     */
    public function setHits($hits)
    {
        $this->hits = $hits;
    
        return $this;
    }

    /**
     * Get hits
     *
     * @return \int 
     */
    public function getHits()
    {
        return $this->hits;
    }

    /**
     * Set stopPublish
     *
     * @param boolean $stopPublish
     * @return Video
     */
    public function setStopPublish($stopPublish)
    {
        $this->stopPublish = $stopPublish;
    
        return $this;
    }

    /**
     * Get stopPublish
     *
     * @return boolean 
     */
    public function getStopPublish()
    {
        return $this->stopPublish;
    }

    /**
     * Set type
     *
     * @param boolean $type
     * @return Video
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return boolean 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set embedCode
     *
     * @param string $embedCode
     * @return Video
     */
    public function setEmbedCode($embedCode)
    {
        $this->embedCode = $embedCode;
    
        return $this;
    }

    /**
     * Get embedCode
     *
     * @return string 
     */
    public function getEmbedCode()
    {
        return $this->embedCode;
    }

	/**
     * Sets video.
     *
     * @param UploadedVideo $video
     */
    public function setVideo(UploadedFile $video = null)
    {
        $this->video = $video;
		// 检查是否有旧的文件，如果有保存到临时变量里面，当更新的时候进行删除
		if (isset($this->video)) {
			$this->tempVideo = $this->video;
			$this->videoPath = null;
		} else {
			$this->videoPath = null;
		}
    }

    /**
     * Get video.
     *
     * @return UploadedVideo
     */
    public function getVideo()
    {
        return $this->video;
    }

	/**
     * Sets cover.
     *
     * @param UploadedCover $cover
     */
    public function setCover(UploadedFile $cover = null)
    {
        $this->cover = $cover;
		// 检查是否有旧的文件，如果有保存到临时变量里面，当更新的时候进行删除
		if (isset($this->coverPath)) {
			$this->tempCover = $this->coverPath;
			$this->coverPath = null;
		} else {
			$this->coverPath = null;
		}
    }

    /**
     * Get cover.
     *
     * @return UploadedCover
     */
    public function getCover()
    {
        return $this->cover;
    }

	/**
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 */
	public function preUpload()
	{
		if (null !== $this->getCover()) {
			// 生成一个独一无二的名字
			$coverName = sha1(uniqid(mt_rand(), true));
			$this->coverPath = $coverName . '.' . $this->getCover()->guessExtension();
		}

		if (null !== $this->getVideo()) {
			// 生成一个独一无二的名字
			$videoName = sha1(uniqid(mt_rand(), true));
			$this->videoPath = $videoName . '.' . $this->getVideo()->guessExtension();
		}
	}

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null !== $this->getCover()) {
            // 如果是在移动文件的时候出现错误，
            // move()函数会扔出异常，从而阻止内容被保存到数据库
            $this->getCover()->move($this->getAbsoluteCoverPath(), $this->coverPath);

            // 查看是否有旧的文件
            if (isset($this->tempCover)) {
                //删除旧的文件
                @unlink($this->getAbsoluteCoverPath().DIRECTORY_SEPARATOR.$this->tempCover);
            }
        }
        if (null !== $this->getVideo()) {
            // 如果是在移动文件的时候出现错误，
            // move()函数会扔出异常，从而阻止内容被保存到数据库
            $this->getVideo()->move($this->getAbsoluteVideoPath(), $this->videoPath);
			$videoFullname = $this->getAbsoluteVideoPath(). DIRECTORY_SEPARATOR . $this->videoPath;
			$oss = new OSS();
			$oss->upload($this->videoPath, $videoFullname);

			// upload video to aliyun oss
			// 将视频上传至阿里云oss
            
            // 查看是否有旧的文件
            if (isset($this->tempVideo)) {
                //删除旧的文件
                @unlink($this->getAbsoluteVideoPath().DIRECTORY_SEPARATOR.$this->tempVideo);
            }
        }
        return;
    }

    /**
     * @ORM\PostRemove()
     */
    public function remove()
    {
        if ($coverPath = $this->getCoverPath()) {
            @unlink($this->getAbsoluteCoverPath().DIRECTORY_SEPARATOR.$coverPath);
        }
        if ($videoPath = $this->getVideoPath()) {
            @unlink($this->getAbsoluteVideoPath().DIRECTORY_SEPARATOR.$videoPath);
        }
    }

    public function getCoverPath()
    {
        return $this->coverPath;
    }

	public function setCoverPath($coverPath)
	{
		$this->coverPath = $coverPath;
		return $this;
	}

    public function getVideoPath()
    {
        return $this->videoPath;
    }
    
    /**
     * 封面图片的相对路径
     */
	public function getCoverWebPath()
	{
		return 'assets/media/video/cover';
	}
    
    /**
     * 封面图片的路径
     */
	public function getCoverWebUrl()
	{
		return $this->getCoverWebPath() . DIRECTORY_SEPARATOR . $this->getCoverPath();
	}

    /**
     * 视频的相对路径
     */
	public function getVideoWebPath()
	{
		return 'assets/media/video/files';
	}

    /**
     * 服务器的根路径
     */
	public function getWebRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return $_SERVER['DOCUMENT_ROOT'];
    }

    public function getAbsoluteCoverPath()
    {
        return $this->getWebRootDir() . DIRECTORY_SEPARATOR . $this->getCoverWebPath();
    }

    public function getAbsoluteVideoPath()
    {
        return $this->getWebRootDir() . DIRECTORY_SEPARATOR . $this->getVideoWebPath();
    }


    /**
     * Set videoPath
     *
     * @param string $videoPath
     * @return Video
     */
    public function setVideoPath($videoPath)
    {
        $this->videoPath = $videoPath;
    
        return $this;
    }

    /**
     * Set editorRecommemd
     *
     * @param boolean $editorRecommemd
     * @return Video
     */
    public function setEditorRecommemd($editorRecommemd)
    {
        $this->editorRecommemd = $editorRecommemd;
    
        return $this;
    }

    /**
     * Get editorRecommemd
     *
     * @return boolean 
     */
    public function getEditorRecommemd()
    {
        return $this->editorRecommemd;
    }

	public function __sleep()
	{
		return array('id', 'url', 'coverPath', 'url');
	}
}
