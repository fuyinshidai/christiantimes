<?php

/*
 * 文章的模型
 */
namespace Gospel\Bundle\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Crm
 * @Annotation
 * @ORM\Table(name="gospel_crm")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="Gospel\Bundle\CoreBundle\Entity\CrmRepository")
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class Crm
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

	private $tempCover;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="fax", type="string", length=255, nullable=true)
     */
    private $fax;

    /**
     * @var string
     *
     * @ORM\Column(name="officePhone", type="string", length=255, nullable=true)
     */
    private $officePhone;

    /**
     * @var string
     *
     * @ORM\Column(name="qq", type="string", length=255, nullable=true)
     */
    private $qq;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;


	/**
     * @ORM\Column(name="cover_path", type="string", length=255, nullable=true)
     */
    public $coverPath;

	/**
     * @ORM\Column(name="crm_path", type="string", length=255, nullable=true)
     */
    public $crmPath;

	/**
     * @Assert\File(maxSize="6000000")
     */
    private $cover;


    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=255, nullable=true)
     */
    private $gender;

    /**
     * @var string
     *
	 * @ORM\ManyToOne(targetEntity="Gospel\Bundle\CoreBundle\Entity\Area")
	 * @ORM\JoinColumn(name="area_id", referencedColumnName="id")
     */
    private $area;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    
    /**
     * @var integer
     *
     * @ORM\Column(name="sortOrder", type="integer", nullable=true)
     */
    private $sortOrder;

    

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createAt", type="datetime", nullable=true)
     */
    private $createAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="publishedAt", type="datetime", nullable=true)
     */
    private $publishedAt;

   

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime", nullable=true)
     */
    private $updatedAt;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Crm
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Crm
     */
    public function setUrl($url)
    {
        $this->url = $url;
    
        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Crm
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    
    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     * @return Crm
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;
    
        return $this;
    }

    /**
     * Get createAt
     *
     * @return \DateTime 
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    
    /**
     * Constructor
     */
    public function __construct()
    {
    }
    
	public function __toString()
	{
		return $this->title;
	}

    
    /**
     * Set area
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\Area $area
     * @return Post
     */
    public function setArea(\Gospel\Bundle\CoreBundle\Entity\Area $area = null)
    {
        $this->area = $area;
    
        return $this;
    }

    /**
     * Get area
     *
     * @return \Gospel\Bundle\CoreBundle\Entity\Area 
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Crm
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

   

	/**
     * Sets cover.
     *
     * @param UploadedCover $cover
     */
    public function setCover(UploadedFile $cover = null)
    {
        $this->cover = $cover;
		// 检查是否有旧的文件，如果有保存到临时变量里面，当更新的时候进行删除
		if (isset($this->coverPath)) {
			$this->tempCover = $this->coverPath;
			$this->coverPath = null;
		} else {
			$this->coverPath = null;
		}
    }

    /**
     * Get cover.
     *
     * @return UploadedCover
     */
    public function getCover()
    {
        return $this->cover;
    }

	/**
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 */
	public function preUpload()
	{
		if (null !== $this->getCover()) {
			// 生成一个独一无二的名字
			$coverName = sha1(uniqid(mt_rand(), true));
			$this->coverPath = $coverName . '.' . $this->getCover()->guessExtension();
		}
	}

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null !== $this->getCover()) {
            // 如果是在移动文件的时候出现错误，
            // move()函数会扔出异常，从而阻止内容被保存到数据库
            $this->getCover()->move($this->getAbsoluteCoverPath(), $this->coverPath);

            // 查看是否有旧的文件
            if (isset($this->tempCover)) {
                //删除旧的文件
                unlink($this->getAbsoluteCoverPath().DIRECTORY_SEPARATOR.$this->tempCover);
            }
        }
        return;
    }

    /**
     * @ORM\PostRemove()
     */
    public function remove()
    {
        if ($coverPath = $this->getCoverPath()) {
            unlink($this->getAbsoluteCoverPath().DIRECTORY_SEPARATOR.$coverPath);
        }
    }

    public function getCoverPath()
    {
        return $this->coverPath;
    }

    /**
     * 封面图片的相对路径
     */
	public function getCoverWebPath()
	{
		return 'assets/media/crm/cover';
	}
    
    /**
     * 封面图片的路径
     */
	public function getCoverWebUrl()
	{
		return $this->getCoverWebPath() . DIRECTORY_SEPARATOR . $this->getCoverPath();
	}

    /**
     * 服务器的根路径
     */
	public function getWebRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return $_SERVER['DOCUMENT_ROOT'];
    }

    public function getAbsoluteCoverPath()
    {
        return $this->getWebRootDir() . DIRECTORY_SEPARATOR . $this->getCoverWebPath();
    }

    public function getAbsoluteCrmPath()
    {
        return $this->getWebRootDir() . DIRECTORY_SEPARATOR . $this->getCrmWebPath();
    }


    /**
     * Set coverPath
     *
     * @param string $coverPath
     * @return Crm
     */
    public function setCoverPath($coverPath)
    {
        $this->coverPath = $coverPath;
    
        return $this;
    }

    /**
     * Set crmPath
     *
     * @param string $crmPath
     * @return Crm
     */
    public function setCrmPath($crmPath)
    {
        $this->crmPath = $crmPath;
    
        return $this;
    }

    /**
     * Get crmPath
     *
     * @return string 
     */
    public function getCrmPath()
    {
        return $this->crmPath;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return Crm
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    
        return $this;
    }

    /**
     * Get gender
     *
     * @return string 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Crm
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Crm
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    
        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Crm
     */
    public function setAddress($address)
    {
        $this->address = $address;
    
        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set sortOrder
     *
     * @param boolean $sortOrder
     * @return Crm
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return boolean 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }
}