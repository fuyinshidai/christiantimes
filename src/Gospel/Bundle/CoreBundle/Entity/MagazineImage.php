<?php

namespace Gospel\Bundle\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MagazineImage
 *
 * @ORM\Table(name="gospel_magazine_image")
 * @ORM\Entity(repositoryClass="Gospel\Bundle\CoreBundle\Entity\MagazineImageRepository")
 */
class MagazineImage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
	 * @ORM\ManyToOne(targetEntity="Magazine", inversedBy="images")
     * @ORM\JoinColumn(name="magazine_id", referencedColumnName="id")
     */
    private $magazine;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set magazine
     *
     * @param string $magazine
     * @return MagazineImage
     */
    public function setMagazine($magazine)
    {
        $this->magazine = $magazine;
    
        return $this;
    }

    /**
     * Get magazine
     *
     * @return string 
     */
    public function getMagazine()
    {
        return $this->magazine;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return MagazineImage
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

	/**
	 * check if this image really exists in its declear path
	 * 检查图片是否真的存在
	 * @return boolean if image exists return true, else return false
	 */
	public function isImageExists()
	{
		$file = $this->getMagazine()->getAbsolutePath() . DIRECTORY_SEPARATOR . $this->getMagazine()->getId() . DIRECTORY_SEPARATOR . $this->getName();
		return file_exists($file) && is_file($file);
	}

	public function getWebSrc()
	{
		return Magazine::getWebPath() . DIRECTORY_SEPARATOR . $this->getMagazine()->getId() . DIRECTORY_SEPARATOR . $this->getName();
	}
}