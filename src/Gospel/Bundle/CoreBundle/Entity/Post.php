<?php

/*
 * 文章的模型
 */
namespace Gospel\Bundle\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * 首页图片头条推荐
 */
define('REC_HEADLINE_PIC', 1); 
/**
 * 首页文字头条推荐
 */
define('REC_HEADLINE_TXT', 2); 

/**
 * 首页文字头条推荐
 */
define('REC_HEADLINE_TXT1', 21); 

/**
 * 首页文字头条推荐
 */
define('REC_HEADLINE_TXT2', 22); 

/**
 * 首页文字头条推荐
 */
define('REC_HEADLINE_TXT3', 23); 


/**
 * 首页推荐
 */
define('REC_HOME', 3); 
// 
/**
 * 深读
 */
define('REC_INSIDE', 4);
/**
 * 编辑推荐
 */
define('REC_EDITOR', 5);

/**
 * 走进传道人
 */
define('REC_PREACHER', 6);

/**
 * 已经发布状态
 */
define('POST_STATUS_PUBLISHED', 1);

/**
 * 文章模板：默认
 */
define('DISPLAY_STYLE_DEFAULT', 'base');

/**
 * 文章模板：轮播
 */
define('DISPLAY_STYLE_SLIDE', 'slide');

/**
 * Post
 * 
 * @ORM\Table(name="gospel_post")
 * @ORM\Entity(repositoryClass="Gospel\Bundle\CoreBundle\Entity\PostRepository")
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class Post
{
	static public $CATEGORY_INTERVIEW = 72;

	static public $CATEGORY_QUESTION = 64;

	static public $CATEGORY_CHURCH_REFLECTION = 11;

	static public $CATEGORY_THEOLOGY = 57;
	static public $CATEGORY_CHURCH = 1;
	static public $CATEGORY_TWO_CHURCH = 154;
	static public $CATEGORY_MINISTRY = 50;
	static public $CATEGORY_SOCIETY = 17;
	static public $CATEGORY_CULTURE = 30;
	static public $CATEGORY_FAMILY = 39;

	/**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

	/**
	 * The string of all the tags
	 * @var type 
	 */
	protected $tagsCommaString;

	/**
	 * 用来设置相关文章的临时字段
	 * @var type 
	 */
	protected $relatedPosts;

	/**
	 * 用来设置相关文章的临时字段
	 * @var type 
	 */
	protected $relatedVideos;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="titleEn", type="string", length=255, nullable=true)
     */
    protected $titleEn;

    /**
     * @var string 标记的作者名称，当在作家里面没有对应的作家的时候显示
     *
     * @ORM\Column(name="writer", type="string", length=255, nullable=true)
     */
    protected $writer;

    /**
     * @var string
     *
	 * @ORM\ManyToOne(targetEntity="Gospel\Bundle\CoreBundle\Entity\Author")
	 * @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     */
    protected $author;

    /**
     * @var string
     *
	 * @ORM\ManyToOne(targetEntity="Gospel\Bundle\CoreBundle\Entity\CategoryAuthor")
	 * @ORM\JoinColumn(name="author_title_id", referencedColumnName="id")
     */
    protected $authorTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="comefrom", type="string", length=25, nullable=true)
     */
    protected $comefrom;

    /**
     * @var string
     *
	 * @ORM\ManyToOne(targetEntity="Gospel\Bundle\UserBundle\Entity\User")
	 * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

	/**
	 * @ORM\ManyToMany(targetEntity="Gospel\Bundle\CoreBundle\Entity\Tag")
	 * @ORM\JoinTable(name="gospel_post_tag"
     *      )
	 */
	protected $tags;

	/**
	 * Related posts
	 * 
	 * @ORM\ManyToMany(targetEntity="Gospel\Bundle\CoreBundle\Entity\Post")
	 * @ORM\JoinTable(name="gospel_post_related",
	 * joinColumns={@ORM\JoinColumn(name="post_id", referencedColumnName="id")},
	 * inverseJoinColumns={@ORM\JoinColumn(name="related_id", referencedColumnName="id")}
	 * )
	 */
	protected $related;

	/**
	 * Related posts
	 * 
	 * @ORM\ManyToMany(targetEntity="Gospel\Bundle\CoreBundle\Entity\Video")
	 * @ORM\JoinTable(name="gospel_post_video_related",
	 * joinColumns={@ORM\JoinColumn(name="post_id", referencedColumnName="id")},
	 * inverseJoinColumns={@ORM\JoinColumn(name="video_id", referencedColumnName="id")}
	 * )
	 */
	protected $relatedVideo;

    /**
     * @var string
     *
	 * @ORM\ManyToOne(targetEntity="Gospel\Bundle\CoreBundle\Entity\Category")
	 * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    protected $category;

    /**
     * @var string 文章的第二个分类
     *
	 * @ORM\ManyToOne(targetEntity="Gospel\Bundle\CoreBundle\Entity\Category")
	 * @ORM\JoinColumn(name="category2_id", referencedColumnName="id")
     */
    protected $category2;

    /**
	 * 圣诞分类
     * @var string
     *
	 * @ORM\ManyToOne(targetEntity="Gospel\Bundle\CoreBundle\Entity\CategoryOther")
	 * @ORM\JoinColumn(name="categoryother_id", referencedColumnName="id")
     */
    protected $categoryother;

    /**
     * @var string
     *
	 * @ORM\ManyToOne(targetEntity="Gospel\Bundle\CoreBundle\Entity\AreaCategory")
	 * @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     */
    protected $region;

    /**
     * @var string
     *
	 * @ORM\ManyToOne(targetEntity="Gospel\Bundle\CoreBundle\Entity\Area")
	 * @ORM\JoinColumn(name="area_id", referencedColumnName="id")
     */
    protected $area;

    /**
     * @var string
     *
     * @ORM\Column(name="introduction", type="text", nullable=true)
     */
    protected $introduction;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    protected $content;

    /**
     * @var string
     *
     * @ORM\Column(name="content_en", type="text", nullable=true)
     */
    private $contentEn;

    /**
     * @var integer
     *
     * @ORM\Column(name="commentStatus", type="boolean", nullable=true)
     */
    protected $commentStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="editor_recommend", type="boolean", nullable=true)
     */
    protected $editorRecommemd;

    /**
     * @var integer
     *
     * @ORM\Column(name="home_headline_pic", type="boolean", nullable=true)
     */
    protected $isHomeHeadlinePic;

    /**
     * @var boolean
     *
     * @ORM\Column(name="home_pic", type="boolean", nullable=true)
     */
    protected $isHomePic;

    /**
     * @var boolean 是否文章原创
     *
     * @ORM\Column(name="donate", type="boolean", nullable=true)
     */
    protected $isDonate;

    /**
     * @var boolean 原创
     *
     * @ORM\Column(name="original", type="boolean", nullable=true)
     */
    protected $isOriginal;

    /**
     * @var string
     *
     * @ORM\Column(name="headline_pic_recommend_desc", type="text", nullable=true)
     */
    protected $homeHeadlinePicDesc;

    /**
     * @var integer
     *
     * @ORM\Column(name="local_headline_pic", type="boolean", nullable=true)
     */
    protected $isLocalHeadlinePic;

    /**
     * @var integer
     *
     * @ORM\Column(name="local_headline_text", type="boolean", nullable=true)
     */
    protected $isLocalHeadlineText;

    /**
     * @var integer
     *
     * @ORM\Column(name="local_promotion", type="boolean", nullable=true)
     */
    protected $isLocalPromotion;

    /**
     * @var integer
     *
     * @ORM\Column(name="local_editor_recommend", type="boolean", nullable=true)
     */
    protected $isLocalEditorRecommend;


    /**
     * @var integer
     *
     * @ORM\Column(name="home_headline_txt", type="boolean", nullable=true)
     */
    protected $isHomeHeadlineTxt;

    /**
     * @var integer
     *
     * @orm\column(name="home_headline_txt1", type="boolean", nullable=true)
     */
    protected $ishomeheadlinetxt1;

    /**
     * @var integer
     *
     * @orm\column(name="home_headline_txt2", type="boolean", nullable=true)
     */
    protected $ishomeheadlinetxt2;

    /**
     * @var integer
     *
     * @orm\column(name="home_headline_txt3", type="boolean", nullable=true)
     */
    protected $ishomeheadlinetxt3;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_preacher", type="boolean", nullable=true)
     */
    protected $isPreacher;

    /**
     * @var integer
     *
     * @ORM\Column(name="section_headline_pic", type="boolean", nullable=true)
     */
    protected $isSectionHeadlinePic;

    /**
     * @var integer
     *
     * @ORM\Column(name="section_headline_txt", type="boolean", nullable=true)
     */
    protected $isSectionHeadlineTxt;

    /**
     * @var integer
     *
     * @ORM\Column(name="selection_headline_pic", type="boolean", nullable=true)
     */
    protected $isSelectionHeadlinePic;

    /**
     * @var integer
     *
     * @ORM\Column(name="selection_headline_txt", type="boolean", nullable=true)
     */
    protected $isSelectionHeadlineTxt;

    /**
     * @var integer
     *
     * @ORM\Column(name="deep_read", type="boolean", nullable=true)
     */
    protected $isDeepRead;

    /**
     * @var boolean
     *
     * @ORM\Column(name="promotionStatus", type="boolean", nullable=true)
     */
    protected $promotionStatus;

    /**
     * @var boolean
     * 文章页面的显示类型
     * @ORM\Column(name="isSlideImage", type="boolean", nullable=true)
     */
    protected $isSlideImage;

    /**
     * @var integer 文章是否发布，如果是false的话文章没有发布
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    protected $status;

    /**
     * @var \DateTime 文章创建时间
     *
     * @ORM\Column(name="createAt", type="datetime")
     */
    protected $createAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="publishedAt", type="datetime", nullable=true)
     */
    protected $publishedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="stopPublish", type="boolean", nullable=true)
     */
    protected $stopPublish;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="stopPublishedAt", type="datetime", nullable=true)
     */
    protected $stopPublishedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime", nullable=true)
     */
    protected $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="alias", type="string", length=255, nullable=true)
     */
    protected $alias;

    /**
     * @var string
     *
     * @ORM\Column(name="aliasEn", type="string", length=255, nullable=true)
     */
    private $aliasEn;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_keywords", type="string", length=255, nullable=true)
     */
    protected $metaKeywords;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="text", nullable=true)
     */
    protected $metaDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="hits", type="integer", nullable=true)
     */
    protected $hits;

	/**
     * @ORM\OneToMany(targetEntity="PostImage", mappedBy="post")
	 * @ORM\OrderBy({"sortOrder" = "ASC"})
     */
    protected $images;

	protected $cover;

    public function setId($id)
    {
        $this->id = $id;
		return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Post
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle($locale = 'zh_CN', $defaultLocale = 'zh_CN')
    {
		if ($locale == $defaultLocale) {
			return $this->title;
		} else {
			$function = 'getTitle'.ucfirst($locale);
			return $this->$function();
		}
    }

    /**
     * Set comefrom
     *
     * @param string $comefrom
     * @return Post
     */
    public function setComefrom($comefrom)
    {
        $this->comefrom = $comefrom;
    
        return $this;
    }

    /**
     * Get comefrom
     *
     * @return string 
     */
    public function getComefrom()
    {
        return $this->comefrom;
    }
    
    /**
     * Set user
     *
     * @param \Gospel\Bundle\UserBundle\Entity\User $user
     * @return Post
     */
    public function setUser(\Gospel\Bundle\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \Gospel\Bundle\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set introduction
     *
     * @param string $introduction
     * @return Post
     */
    public function setIntroduction($introduction)
    {
        $this->introduction = $introduction;
    
        return $this;
    }

    /**
     * Get introduction
     *
     * @return string 
     */
    public function getIntroduction()
    {
        return $this->introduction;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Post
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent($locale = 'zh_CN', $defaultLocale = 'zh_CN')
    {
		if ($locale == $defaultLocale) {
			return $this->content;
		} else {
			$function = 'getContent'.ucfirst($locale);
			return $this->$function();
		}
    }

	public function hasContent()
	{
		return (strlen($this->getTitle()) > 3);
	}

	public function hasContentEn()
	{
		return (strlen($this->getTitleEn()) > 3);
	}

    /**
     * Set contentEn
     *
     * @param string $contentEn
     * @return Post
     */
    public function setContentEn($contentEn)
    {
        $this->contentEn = $contentEn;
    
        return $this;
    }

    /**
     * Get titleEn
     *
     * @return string 
     */
    public function getContentEn()
    {
        return $this->contentEn;
    }

    /**
     * Set commentStatus
     *
     * @param integer $commentStatus
     * @return Post
     */
    public function setCommentStatus($commentStatus)
    {
        $this->commentStatus = $commentStatus;
    
        return $this;
    }

    /**
     * Get commentStatus
     *
     * @return integer 
     */
    public function getCommentStatus()
    {
        return $this->commentStatus;
    }

    /**
     * Set promotionStatus
     *
     * @param integer $promotionStatus
     * @return Post
     */
    public function setPromotionStatus($promotionStatus)
    {
        $this->promotionStatus = $promotionStatus;
    
        return $this;
    }

    /**
     * Get promotionStatus
     *
     * @return integer 
     */
    public function getPromotionStatus()
    {
        return $this->promotionStatus;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Post
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     * @return Post
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;
    
        return $this;
    }

    /**
     * Get createAt
     *
     * @return \DateTime 
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     * @return Post
     */
    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;
    
        return $this;
    }

    /**
     * Get publishedAt
     *
     * @return \DateTime 
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     * @return Post
     */
    public function setStopPublishedAt($stopPublishedAt)
    {
        $this->stopPublishedAt = $stopPublishedAt;
    
        return $this;
    }

    /**
     * Get stopPublishedAt
     *
     * @return \DateTime 
     */
    public function getStopPublishedAt()
    {
        return $this->stopPublishedAt;
    }
    /**
     * Constructor
     */
    public function __construct($id = null)
    {
		if($id) {
			$this->id = $id;
		}
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
        $this->related = new \Doctrine\Common\Collections\ArrayCollection();
        $this->relatedVideo = new \Doctrine\Common\Collections\ArrayCollection();
		$this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add tags
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\Tag $tags
     * @return Post
     */
    public function addTag(\Gospel\Bundle\CoreBundle\Entity\Tag $tags)
    {
        $this->tags[] = $tags;
    
        return $this;
    }

    /**
     * Remove tags
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\Tag $tags
     */
    public function removeTag(\Gospel\Bundle\CoreBundle\Entity\Tag $tags)
    {
        $this->tags->removeElement($tags);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTags()
    {
        return $this->tags;
    }

	public function getTagsCommaString()
	{
		return $this->tagsCommaString;
	}

	public function setTagsCommaString($value)
	{
		if(null === $value) {
			return;
		}
	}

	public function setRelatedPosts($posts)
	{
		$this->relatedPosts = $posts;
	}

	public function getRelatedPosts()
	{
		$data = array();
		foreach($this->getRelated() as $post) {
			$data[] = '['.$post->getId().']'.$post->getTitle();
		}
		return implode(',',$data);
	}

	public function setRelatedVideos($videos)
	{
		$this->relatedVideos = $videos;
	}

	public function getRelatedVideos()
	{
		$data = array();
		foreach($this->getRelatedVideos() as $video) {
			$data[] = '['.$video->getId().']'.$video->getTitle();
		}
		return implode(',',$data);
	}

	public function __toString()
	{
		return $this->title;
	}

    /**
     * Add related
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\Post $related
     * @return Post
     */
    public function addRelated(\Gospel\Bundle\CoreBundle\Entity\Post $related)
    {
		if(!$this->related->contains($related)) {
        	$this->related[] = $related;
		}
    
        return $this;
    }

    /**
     * Remove related
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\Post $related
     */
    public function removeRelated(\Gospel\Bundle\CoreBundle\Entity\Post $related)
    {
        $this->related->removeElement($related);
    }

    /**
     * Get related
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRelated()
    {
        return $this->related;
    }

    /**
     * Set category
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\Category $category
     * @return Post
     */
    public function setCategory(\Gospel\Bundle\CoreBundle\Entity\Category $category = null)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return \Gospel\Bundle\CoreBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set area
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\Area $area
     * @return Post
     */
    public function setArea(\Gospel\Bundle\CoreBundle\Entity\Area $area = null)
    {
        $this->area = $area;
    
        return $this;
    }

    /**
     * Get area
     *
     * @return \Gospel\Bundle\CoreBundle\Entity\Area 
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Post
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set metaKeywords
     *
     * @param string $metaKeywords
     * @return Post
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;
    
        return $this;
    }

    /**
     * Get metaKeywords
     *
     * @return string 
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     * @return Post
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
    
        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string 
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set alias
     *
     * @param string $alias
     * @return Post
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    
        return $this;
    }

    /**
     * Set aliasEn
     *
     * @param string $aliasEn
     * @return Post
     */
    public function setAliasEn($aliasEn)
    {
        $this->aliasEn = $aliasEn;
    
        return $this;
    }

	public function getAliasEn()
	{
		return $this->aliasEn;
	}

    /**
     * Get alias
     *
     * @return string 
     */
    public function getAlias($locale = 'zh_CN', $defaultLocale = 'zh_CN')
    {
		if ($locale == $defaultLocale) {
			return $this->alias ? str_replace(array('/','-'), '.', $this->alias) : str_replace(array('/','-'), '.', $this->getTitle());
		} else {
			$function = 'getAlias'.ucfirst($locale);
			$functionTitle = 'getTitle'.ucfirst($locale);
			return $this->$function() ? str_replace(array('/','-'), '.', $this->$function()) : str_replace(array('/','-', ' '), '-', $this->$functionTitle());
		}
    }

    /**
     * Set hits
     *
     * @param $hits
     * @return Post
     */
    public function setHits($hits)
    {
        $this->hits = $hits;
    
        return $this;
    }

    /**
     * Get hits
     *
     * @return 
     */
    public function getHits()
    {
        return $this->hits;
    }

    /**
     * Set stopPublish
     *
     * @param boolean $stopPublish
     * @return Post
     */
    public function setStopPublish($stopPublish)
    {
        $this->stopPublish = $stopPublish;
    
        return $this;
    }

    /**
     * Get stopPublish
     *
     * @return boolean 
     */
    public function getStopPublish()
    {
        return $this->stopPublish;
    }

    /**
     * Add images
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\PostImage $images
     * @return Post
     */
    public function addImage(\Gospel\Bundle\CoreBundle\Entity\PostImage $images)
    {
        $this->images[] = $images;
    
        return $this;
    }

    /**
     * Remove images
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\PostImage $images
     */
    public function removeImage(\Gospel\Bundle\CoreBundle\Entity\PostImage $images)
    {
        $this->images->removeElement($images);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImages()
    {
        return $this->images;
    }

	public function setImages($images)
	{
		$this->images = $images;
	}

	public function getFirstImage()
	{
		$images = $this->getImages();
		if(is_object($images)) {
			$image =  array_shift($images->toArray());
			return $image;
		}
		return null;
	}

	public function getCoverPath() 
	{
		$cover = $this->getCover();
		if ($cover) {
			if(is_object($this->getCreateAt())) {
				return self::getUploadImagePath().$this->getCreateAt()->format('Ym')
						.'/'.$this->getId().'/'.$cover->getName();
			}
		}
		return 'null';
	}

	public function getImagePath()
	{
		if(is_object($this->getCreateAt())) {
			return self::getUploadImagePath().$this->getCreateAt()->format('Ym')
				.'/'.$this->getId().'/';
		}
		return 'null';
	}

	/**
	 * 取得文章的封面图片
	 * @return type
	 */
	public function getCover()
	{
		if ($this->cover) {
			return $this->cover;
		}
		$cover = null;
		$images = $this->getImages();
		foreach($images as $k => $image) {
			if ($image->getIsCover()) {
				if (file_exists(dirname($_SERVER['SCRIPT_FILENAME']).'/'.$this->getImagePath().$image->getName())) {
					$this->cover = $image;
					return $image;
				}
			}
		}
		foreach($images as $k => $image) {
			if (file_exists(dirname($_SERVER['SCRIPT_FILENAME']).'/'.$this->getImagePath().$image->getName())) {
				$this->cover = $image;
				return $image;
			}
		}


		return false;
	}

	public function setCover($cover)
	{
		$this->cover = $cover;
	}

	static public function getUploadImagePath()
	{
		return 'assets/media/post/';
	}

    /**
     * Set post
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\TopicPost $post
     * @return Post
     */
    public function setPost(\Gospel\Bundle\CoreBundle\Entity\TopicPost $post = null)
    {
        $this->post = $post;
    
        return $this;
    }

    /**
     * Get post
     *
     * @return \Gospel\Bundle\CoreBundle\Entity\TopicPost 
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set editorRecommemd
     *
     * @param boolean $editorRecommemd
     * @return Post
     */
    public function setEditorRecommemd($editorRecommemd)
    {
        $this->editorRecommemd = $editorRecommemd;
    
        return $this;
    }

    /**
     * Get editorRecommemd
     *
     * @return boolean 
     */
    public function getEditorRecommemd()
    {
        return $this->editorRecommemd;
    }


    /**
     * Set isHomeHeadlinePic
     *
     * @param boolean $isHomeHeadlinePic
     * @return Post
     */
    public function setIsHomeHeadlinePic($isHomeHeadlinePic)
    {
        $this->isHomeHeadlinePic = $isHomeHeadlinePic;
    
        return $this;
    }

    /**
     * Get isHomeHeadlinePic
     *
     * @return boolean 
     */
    public function getIsHomeHeadlinePic()
    {
        return $this->isHomeHeadlinePic;
    }

    /**
     * Set isHomePic
     *
     * @param boolean $isHomePic
     * @return Post
     */
    public function setIsHomePic($isHomePic)
    {
        $this->isHomePic = $isHomePic;
    
        return $this;
    }

    /**
     * Get isHomeHeadlinePic
     *
     * @return boolean 
     */
    public function getIsHomePic()
    {
        return $this->isHomePic;
    }

    /**
     * Set isHomeHeadlineTxt
     *
     * @param boolean $isHomeHeadlineTxt
     * @return Post
     */
    public function setIsHomeHeadlineTxt($isHomeHeadlineTxt)
    {
        $this->isHomeHeadlineTxt = $isHomeHeadlineTxt;
    
        return $this;
    }

    /**
     * Get isHomeHeadlineTxt
     *
     * @return boolean 
     */
    public function getIsHomeHeadlineTxt()
    {
        return $this->isHomeHeadlineTxt;
    }

    /**
     * Set isHomeHeadlineTxt1
     *
     * @param boolean $isHomeHeadlineTxt1
     * @return Post
     */
    public function setIshomeheadlinetxt1($isHomeHeadlineTxt1)
    {
        $this->ishomeheadlinetxt1 = $isHomeHeadlineTxt1;
    
        return $this;
    }

    /**
     * Get isHomeHeadlineTxt1
     *
     * @return boolean 
     */
    public function getIsHomeHeadlineTxt1()
    {
        return $this->ishomeheadlinetxt1;
    }

    /**
     * Set isHomeHeadlineTxt2
     *
     * @param boolean $isHomeHeadlineTxt2
     * @return Post
     */
    public function setIshomeheadlinetxt2($isHomeHeadlineTxt2)
    {
        $this->ishomeheadlinetxt2 = $isHomeHeadlineTxt2;
    
        return $this;
    }

    /**
     * Get isHomeHeadlineTxt2
     *
     * @return boolean 
     */
    public function getIsHomeHeadlineTxt2()
    {
        return $this->ishomeheadlinetxt2;
    }

    /**
     * Set isHomeHeadlineTxt3
     *
     * @param boolean $isHomeHeadlineTxt3
     * @return Post
     */
    public function setIsHomeHeadlineTxt3($isHomeHeadlineTxt3)
    {
        $this->ishomeheadlinetxt3 = $isHomeHeadlineTxt3;
    
        return $this;
    }

    /**
     * Get isHomeHeadlineTxt3
     *
     * @return boolean 
     */
    public function getIsHomeHeadlineTxt3()
    {
        return $this->ishomeheadlinetxt3;
    }

    /**
     * Set isDeepRead
     *
     * @param boolean $isDeepRead
     * @return Post
     */
    public function setIsDeepRead($isDeepRead)
    {
        $this->isDeepRead = $isDeepRead;
    
        return $this;
    }

    /**
     * Get isDeepRead
     *
     * @return boolean 
     */
    public function getIsDeepRead()
    {
        return $this->isDeepRead;
    }

    /**
     * Set isSlideImage
     *
     * @param boolean $isSlideImage
     * @return Post
     */
    public function setIsSlideImage($isSlideImage)
    {
        $this->isSlideImage = $isSlideImage;
    
        return $this;
    }

    /**
     * Get isSlideImage
     *
     * @return boolean 
     */
    public function getIsSlideImage()
    {
        return $this->isSlideImage;
    }

    /**
     * Set isPreacher
     *
     * @param boolean $isPreacher
     * @return Post
     */
    public function setIsPreacher($isPreacher)
    {
        $this->isPreacher = $isPreacher;
    
        return $this;
    }

    /**
     * Get isPreacher
     *
     * @return boolean 
     */
    public function getIsPreacher()
    {
        return $this->isPreacher;
    }

    /**
     * Set isLocalHeadlinePic
     *
     * @param boolean $isLocalHeadlinePic
     * @return Post
     */
    public function setIsLocalHeadlinePic($isLocalHeadlinePic)
    {
        $this->isLocalHeadlinePic = $isLocalHeadlinePic;
    
        return $this;
    }

    /**
     * Get isLocalHeadlinePic
     *
     * @return boolean 
     */
    public function getIsLocalHeadlinePic()
    {
        return $this->isLocalHeadlinePic;
    }

    /**
     * Set isLocalHeadlineText
     *
     * @param boolean $isLocalHeadlineText
     * @return Post
     */
    public function setIsLocalHeadlineText($isLocalHeadlineText)
    {
        $this->isLocalHeadlineText = $isLocalHeadlineText;
    
        return $this;
    }

    /**
     * Get isLocalHeadlineText
     *
     * @return boolean 
     */
    public function getIsLocalHeadlineText()
    {
        return $this->isLocalHeadlineText;
    }

    /**
     * Set isLocalPromotion
     *
     * @param boolean $isLocalPromotion
     * @return Post
     */
    public function setIsLocalPromotion($isLocalPromotion)
    {
        $this->isLocalPromotion = $isLocalPromotion;
    
        return $this;
    }

    /**
     * Get isLocalPromotion
     *
     * @return boolean 
     */
    public function getIsLocalPromotion()
    {
        return $this->isLocalPromotion;
    }

    /**
     * Set writer
     *
     * @param string $writer
     * @return Post
     */
    public function setWriter($writer)
    {
        $this->writer = $writer;
    
        return $this;
    }

    /**
     * Get writer
     *
     * @return string 
     */
    public function getWriter()
    {
        return $this->writer;
    }

    /**
     * Set isLocalEditorRecommend
     *
     * @param boolean $isLocalEditorRecommend
     * @return Post
     */
    public function setIsLocalEditorRecommend($isLocalEditorRecommend)
    {
        $this->isLocalEditorRecommend = $isLocalEditorRecommend;
    
        return $this;
    }

    /**
     * Get isLocalEditorRecommend
     *
     * @return boolean 
     */
    public function getIsLocalEditorRecommend()
    {
        return $this->isLocalEditorRecommend;
    }

    /**
     * Set homeHeadlinePicDesc
     *
     * @param string $homeHeadlinePicDesc
     * @return Post
     */
    public function setHomeHeadlinePicDesc($homeHeadlinePicDesc)
    {
        $this->homeHeadlinePicDesc = $homeHeadlinePicDesc;
    
        return $this;
    }

    /**
     * Get homeHeadlinePicDesc
     *
     * @return string 
     */
    public function getHomeHeadlinePicDesc()
    {
        return $this->homeHeadlinePicDesc;
    }

    /**
     * Set CategoryOther
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\CategoryOther $categoryother
     * @return Post
     */
    public function setCategoryOther(\Gospel\Bundle\CoreBundle\Entity\CategoryOther $categoryother = null)
    {
        $this->categoryother = $categoryother;
    
        return $this;
    }

    /**
     * Get Categoryother
     *
     * @return \Gospel\Bundle\CoreBundle\Entity\CategoryOther 
     */
    public function getCategoryOther()
    {
        return $this->categoryother;
    }

    /**
     * Set isSectionHeadlinePic
     *
     * @param boolean $isSectionHeadlinePic
     * @return Post
     */
    public function setIsSectionHeadlinePic($isSectionHeadlinePic)
    {
        $this->isSectionHeadlinePic = $isSectionHeadlinePic;
    
        return $this;
    }

    /**
     * Get isSectionHeadlinePic
     *
     * @return boolean 
     */
    public function getIsSectionHeadlinePic()
    {
        return $this->isSectionHeadlinePic;
    }

    /**
     * Set isSectionHeadlineTxt
     *
     * @param boolean $isSectionHeadlineTxt
     * @return Post
     */
    public function setIsSectionHeadlineTxt($isSectionHeadlineTxt)
    {
        $this->isSectionHeadlineTxt = $isSectionHeadlineTxt;
    
        return $this;
    }

    /**
     * Get isSectionHeadlineTxt
     *
     * @return boolean 
     */
    public function getIsSectionHeadlineTxt()
    {
        return $this->isSectionHeadlineTxt;
    }

    /**
     * Set isSelectionHeadlinePic
     *
     * @param boolean $isSelectionHeadlinePic
     * @return Post
     */
    public function setIsSelectionHeadlinePic($isSelectionHeadlinePic)
    {
        $this->isSelectionHeadlinePic = $isSelectionHeadlinePic;
    
        return $this;
    }

    /**
     * Get isSelectionHeadlinePic
     *
     * @return boolean 
     */
    public function getIsSelectionHeadlinePic()
    {
        return $this->isSelectionHeadlinePic;
    }

    /**
     * Set isSelectionHeadlineTxt
     *
     * @param boolean $isSelectionHeadlineTxt
     * @return Post
     */
    public function setIsSelectionHeadlineTxt($isSelectionHeadlineTxt)
    {
        $this->isSelectionHeadlineTxt = $isSelectionHeadlineTxt;
    
        return $this;
    }

    /**
     * Get isSelectionHeadlineTxt
     *
     * @return boolean 
     */
    public function getIsSelectionHeadlineTxt()
    {
        return $this->isSelectionHeadlineTxt;
    }

    /**
     * Add relatedVideo
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\Video $relatedVideo
     * @return Post
     */
    public function addRelatedVideo(\Gospel\Bundle\CoreBundle\Entity\Video $relatedVideo)
    {
        $this->relatedVideo[] = $relatedVideo;
    
        return $this;
    }

    /**
     * Remove relatedVideo
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\Video $relatedVideo
     */
    public function removeRelatedVideo(\Gospel\Bundle\CoreBundle\Entity\Video $relatedVideo)
    {
        $this->relatedVideo->removeElement($relatedVideo);
    }

    /**
     * Get relatedVideo
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRelatedVideo()
    {
        return $this->relatedVideo;
    }


    /**
     * Set region
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\AreaCategory $region
     * @return Post
     */
    public function setRegion(\Gospel\Bundle\CoreBundle\Entity\AreaCategory $region = null)
    {
        $this->region = $region;
    
        return $this;
    }

    /**
     * Get region
     *
     * @return \Gospel\Bundle\CoreBundle\Entity\AreaCategory 
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set author
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\Author $author
     * @return Post
     */
    public function setAuthor(\Gospel\Bundle\CoreBundle\Entity\Author $author = null)
    {
        $this->author = $author;
    
        return $this;
    }

    /**
     * Get author
     *
     * @return \Gospel\Bundle\CoreBundle\Entity\Author 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set authorTitle
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\CategoryAuthor $authorTitle
     * @return Post
     */
    public function setAuthorTitle(\Gospel\Bundle\CoreBundle\Entity\CategoryAuthor $authorTitle = null)
    {
        $this->authorTitle = $authorTitle;
    
        return $this;
    }

    /**
     * Get authorTitle
     *
     * @return \Gospel\Bundle\CoreBundle\Entity\CategoryAuthor 
     */
    public function getAuthorTitle()
    {
        return $this->authorTitle;
    }

	/**
	 * 移动图片的时候旧的图片文件夹的地址
	 * @return string
	 */
	static public function getOldImageRoot()
	{
		if ($_SERVER['SERVER_ADDR'] == '127.0.0.1') {
			return '/home/wwwroot/christiantimes/public_html/upload/';
//			return '/var/www/www.gospeltimes.cn/data/';
		} else {
			return '/home/wwwroot/christiantimes/public_html/upload/';
		}
	}

	public function __sleep()
	{
		return array('id', 'cover', 'images', 'title', 'introduction');
	}
	
	public function __wakeup()
	{
	}

    /**
     * Set titleEn
     *
     * @param string $titleEn
     * @return Post
     */
    public function setTitleEn($titleEn)
    {
        $this->titleEn = $titleEn;
    
        return $this;
    }

    /**
     * Get titleEn
     *
     * @return string 
     */
    public function getTitleEn()
    {
        return $this->titleEn;
    }


    /**
     * Set isDonate
     *
     * @param boolean $isDonate
     * @return Post
     */
    public function setIsDonate($isDonate)
    {
        $this->isDonate = $isDonate;
    
        return $this;
    }

    /**
     * Get isDonate
     *
     * @return boolean 
     */
    public function getIsDonate()
    {
        return $this->isDonate;
    }


    /**
     * Set isOriginal
     *
     * @param boolean $isOriginal
     * @return Post
     */
    public function setIsOriginal($isOriginal)
    {
        $this->isOriginal = $isOriginal;
    
        return $this;
    }

    /**
     * Get isOriginal
     *
     * @return boolean 
     */
    public function getIsOriginal()
    {
        return $this->isOriginal;
    }

    /**
     * Set category2
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\Category $category2
     * @return Post
     */
    public function setCategory2(\Gospel\Bundle\CoreBundle\Entity\Category $category2 = null)
    {
        $this->category2 = $category2;
    
        return $this;
    }

    /**
     * Get category2
     *
     * @return \Gospel\Bundle\CoreBundle\Entity\Category 
     */
    public function getCategory2()
    {
        return $this->category2;
    }
	
	/**
     * Get categoryfrom
     *
     * @return array();
     */
    public function getCategoryfrom()
    {
        return array(1=>'aa','bb','cc');
    }

}
