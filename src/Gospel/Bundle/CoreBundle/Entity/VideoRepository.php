<?php

namespace Gospel\Bundle\CoreBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * VideoRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class VideoRepository extends EntityRepository
{
	public function findAllOld($em, $total)
	{
		$limit = $total?" limit 0, $total":"";
		$stmt = $em->getConnection()->prepare('select * from ct_video '.$limit);
		$stmt->execute();
		return $stmt->fetchAll();
	}

	public function getOldVideos($em)
	{
		$stmt = $em->getConnection()->prepare('select * from ct_video');
		$stmt->execute();
		return $stmt->fetchAll();
	}

	public function getPromoted($limit, $offset = 0)
	{
		$data =  $this->findBy(array(
//			'promotionStatus' => 1,
			'status' => 1,
			), array('id' => 'DESC'), $limit, $offset);
		if ($limit == 1) {
			$data = isset($data[0])?$data[0]:null;
		}
		return $data;
	}

	public function findLatestCategory($limit, $category = null, $offset = 0)
	{
		$criteria = array(
			'status' => 1,
			);
		if ($category) {
			$criteria['category'] = $category;
		}
		$data =  $this->findBy($criteria, array('id' => 'DESC'), $limit, $offset);
		return $data;

	}

	public function getEditorsChoice($limit, $offset = 0)
	{
		$data =  $this->findBy(array(
			'editorRecommemd' => 1,
			'status' => 1,
			), array('id' => 'DESC'), $limit, $offset);
		if ($limit == 1) {
			$data = isset($data[0])?$data[0]:null;
		}
		return $data;
	}

	/**
	 * 含有分页的查询
	 * @param type $paginator
	 * @param CategoryVideo $category
	 * @param type $page
	 * @return type
	 */
	public function findByCategoryWithPagitaion($paginator, CategoryVideo $category = null, $page, $limit = 15)
	{
		$qb = $this->createQueryBuilder('n');
			$qb
					->where('n.status = 1');
			if ($category instanceof CategoryVideo) {
				$qb
						->andWhere('n.category = '.$category->getId());
			}
			$query = $qb
					->orderBy('n.id', 'DESC')
					->getQuery();
    	$pagination = $paginator->paginate(
			$query,
			$page/*page number*/,
			$limit/*limit per page*/
		);
		return $pagination;
	}

	public function findMostViewed($category = null)
	{

		$criteria = array(
			'status' => 1,
			);
		if ($category instanceof CategoryVideo) {
			$criteria['category'] = $category;
		}
		$data =  $this->findBy($criteria, array('hits' => 'DESC'), 10);
		return $data;
	}
	
	public function findRecommend($limit)
	{
		$criteria = array(
			'status' => 1,
			'promotionStatus' => 1,
			);
		$data =  $this->findBy($criteria, array('hits' => 'DESC'), $limit);
		return $data;

	}

	public function normalize(Video $entity) {
		$data = new \stdClass();
		$data->id = $entity->getId();
		$data->title = $entity->getTitle();
		$data->type = $entity->getType();
		$data->videoWebPath = $entity->getVideoWebPath();

		if ($entity->getVideoPath()) {
			list($videoName, $ext) = explode('.', $entity->getVideoPath());

			// reading the video url
			$webUrl = $entity->getVideoWebPath().'/'.$videoName;
			$data->webUrl = $webUrl;
		}
		$data->coverWebUrl = $entity->getCoverWebUrl();
		$data->path = $entity->getVideoPath();
		$data->embedCode = $entity->getEmbedCode();

		return $data;
	}

	public function search($keyword, $currentPage = 1, $limit = 20) {
		
		$conn = $this->getEntityManager()->getConnection();
		$start = ($currentPage - 1) * $limit;
		$queryCount = 'SELECT count(*) total FROM gospel_video  m '
				. 'where '
				. 'm.title like "%'.$keyword.'%" '
				. 'OR m.content like "%'.$keyword.'%" '
				. 'AND m.status = 1 ';

		$queryAll = 'SELECT m.id, m.title, m.content, m.alias, m.createAt  FROM gospel_video  m '
				. 'where '
				. 'm.title like "%'.$keyword.'%" '
				. 'OR m.content like "%'.$keyword.'%" '
				. 'AND m.status = 1 ';

		$query = $queryAll . 'ORDER BY m.id DESC '
				. 'LIMIT '.$start.', ' . $limit;

		$resultPost = $conn->fetchAll($query);
		$resultCount = $conn->fetchAll($queryCount);

		$result = array(
			'count' => $resultCount[0]['total'],
			'data' => $resultPost,
		);

		return $result;
	}
}
