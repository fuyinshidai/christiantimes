<?php

namespace Gospel\Bundle\CoreBundle\Entity;

use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;

/**
 * AuthorRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class AuthorRepository extends EntityRepository
{

	public function findRecommendAuthors($limit)
	{
		return $this->findBy(array(
			'promotionStatus' => 1,
			'status' => 1,
		), array('sortOrder'=>'DESC'), $limit);
	}

	public function findAllOld($em, $total = null, $start = 0)
	{
		$limit = $total?" limit $start, $total":"";
		$stmt = $em->getConnection()->prepare('select * from ct_writer order by id asc '.$limit);
		$stmt->execute();
		return $stmt->fetchAll();
	}

	public function findAuthors($categories)
	{
		$qb = $this->createQueryBuilder('n');
		$ids = array();
		foreach($categories as $cat) {
			$ids[] = $cat->getId();
		}
		$data = $qb
				->where($qb->expr()->in('n.category', $ids))
				->andWhere('n.status = 1')
				->orderBy('n.id', 'DESC')
				->getQuery()
				->getResult();
		return $data;
	}
}
