<?php

namespace Gospel\Bundle\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TopicPost
 *
 * @ORM\Table(name="gospel_topic_post")
 * @ORM\Entity(repositoryClass="Gospel\Bundle\CoreBundle\Entity\TopicPostRepository")
 */
class TopicPost
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
	 * @ORM\ManyToOne(targetEntity="Topic")
	 * @ORM\JoinColumn(name="topic_id", referencedColumnName="id")
     */
    private $topic;

	/**
 	 * @ORM\OneToOne(targetEntity="Post")
 	 * @ORM\JoinColumn(name="post_id", referencedColumnName="id")
 	 */
    private $post;

    /**
     * @var integer
     *
     * @ORM\Column(name="sortOrder", type="integer")
     */
    private $sortOrder;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return TopicPost
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }


    /**
     * Set topic
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\Topic $topic
     * @return TopicPost
     */
    public function setTopic(\Gospel\Bundle\CoreBundle\Entity\Topic $topic = null)
    {
        $this->topic = $topic;
    
        return $this;
    }

    /**
     * Get topic
     *
     * @return \Gospel\Bundle\CoreBundle\Entity\Topic 
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * Set post
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\Post $post
     * @return TopicPost
     */
    public function setPost(\Gospel\Bundle\CoreBundle\Entity\Post $post = null)
    {
        $this->post = $post;
    
        return $this;
    }

    /**
     * Get post
     *
     * @return \Gospel\Bundle\CoreBundle\Entity\Post 
     */
    public function getPost()
    {
        return $this->post;
    }

	public function __toString()
	{
		return $this->getPost()->getTitle();
	}
}
