<?php

namespace Gospel\Bundle\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Magazine
 *
 * @ORM\Table(name="gospel_magazine")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="Gospel\Bundle\CoreBundle\Entity\MagazineRepository")
 */
class Magazine
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

	/**
	 * File temprary file path
	 * @var type 
	 */
	private $tempFile;

	/**
     * @ORM\OneToMany(targetEntity="MagazineImage", mappedBy="magazine")
     */
    protected $images;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="hit", type="integer", nullable=true)
     */
    private $hit;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string pdf file path stored in database
     *
     * @ORM\Column(name="pdf", type="string", length=255)
     */
    private $pdf;

	/**
     * @Assert\File(maxSize="60000000")
     */
    private $file;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

	public function __construct()
	{
		$this->images = new \Doctrine\Common\Collections\ArrayCollection();
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Magazine
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Magazine
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set pdf
     *
     * @param string $pdf
     * @return Magazine
     */
    public function setPdf($pdf)
    {
        $this->pdf = $pdf;
    
        return $this;
    }

    /**
     * Get pdf
     *
     * @return string 
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Magazine
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

	/**
     * Sets file.
     *
     * @param UploadedCover $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
		// 检查是否有旧的文件，如果有保存到临时变量里面，当更新的时候进行删除
		if (isset($this->pdf)) {
			$this->tempFile = $this->pdf;
			$this->pdf = null;
		} else {
			$this->pdf = null;
		}
    }

    /**
     * Get cover.
     *
     * @return UploadedCover
     */
    public function getFile()
    {
        return $this->file;
    }

	/**
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 */
	public function preUpload()
	{
		if (null !== $this->getFile()) {
			// 生成一个独一无二的名字
			$fileName = sha1(uniqid(mt_rand(), true));
			$this->setPdf($fileName . '.' . $this->getFile()->guessExtension());
		}
	}

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null !== $this->getFile()) {
            // 如果是在移动文件的时候出现错误，
            // move()函数会扔出异常，从而阻止内容被保存到数据库
            $this->getFile()->move($this->getAbsolutePath(), $this->pdf);

            // 查看是否有旧的文件
            if (isset($this->tempFile)) {
                //删除旧的文件
                unlink($this->getAbsolutePath().DIRECTORY_SEPARATOR.$this->tempFile);
            }
        }
        return;
    }

    /**
     * @ORM\PostRemove()
     */
    public function remove()
    {
        if ($filePath = $this->getPdf()) {
            unlink($this->getAbsolutePath().DIRECTORY_SEPARATOR.$filePath);
        }
    }

    public function getAbsolutePath()
    {
        return $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . self::getWebPath();
    }

	/**
	 * 读取服务器文件绝对路径
	 * @return String
	 */
	public function getFullPath()
	{
		return $this->getAbsolutePath() . DIRECTORY_SEPARATOR . $this->getPdf();
	}

	/**
	 * 读取网站前台显示路径
	 * @return string
	 */
	public function getWebName()
	{
		return self::getWebPath() . DIRECTORY_SEPARATOR . $this->getPdf();
	}

	public function getDisplayName()
	{
		return $this->getTitle() . '.' . basename(mime_content_type($this->getFullPath()));
	}

    /**
     * 封面图片的相对路径
     */
	static public function getWebPath()
	{
		return 'assets/media/magazine';
	}

    /**
     * Add images
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\MagazineImage $images
     * @return Post
     */
    public function addImage(\Gospel\Bundle\CoreBundle\Entity\MagazineImage $images)
    {
        $this->images[] = $images;
    
        return $this;
    }

    /**
     * Remove images
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\MagazineImage $images
     */
    public function removeImage(\Gospel\Bundle\CoreBundle\Entity\MagazineImage $images)
    {
        $this->images->removeElement($images);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Magazine
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    
        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set hit
     *
     * @param integer $hit
     * @return Magazine
     */
    public function setHit($hit)
    {
        $this->hit = $hit;
    
        return $this;
    }

    /**
     * Get hit
     *
     * @return integer 
     */
    public function getHit()
    {
        return $this->hit;
    }
}