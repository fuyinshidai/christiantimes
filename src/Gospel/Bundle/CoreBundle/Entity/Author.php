<?php

/*
 * 文章的模型
 */
namespace Gospel\Bundle\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Author
 * 
 * @ORM\Table(name="gospel_author")
 * @ORM\Entity(repositoryClass="Gospel\Bundle\CoreBundle\Entity\AuthorRepository")
 * @ORM\HasLifecycleCallbacks
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class Author
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

	protected $tempCover;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    protected $url;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    protected $email;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    protected $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    protected $address;


	/**
     * @ORM\Column(name="cover_path", type="string", length=255, nullable=true)
     */
    protected $coverPath;

	/**
     * @ORM\Column(name="author_path", type="string", length=255, nullable=true)
     */
    protected $authorPath;

	/**
     * @Assert\File(maxSize="6000000")
     */
    protected $cover;

	/*
     * @var string
	 * @ORM\OneToMany(targetEntity="Gospel\Bundle\CoreBundle\Entity\Post", mappedBy="author")
	 */
    protected $posts;



    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=255, nullable=true)
     */
    protected $gender;

    /**
     * @var string
     *
	 * @ORM\ManyToOne(targetEntity="Gospel\Bundle\CoreBundle\Entity\CategoryAuthor")
	 * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    protected $category;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    protected $content;

    /**
     * @var integer
     *
     * @ORM\Column(name="commentStatus", type="boolean", nullable=true)
     */
    protected $commentStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="sortOrder", type="integer", nullable=true)
     */
    protected $sortOrder;

    /**
     * @var integer
     *
     * @ORM\Column(name="promotionStatus", type="boolean", nullable=true)
     */
    protected $promotionStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    protected $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createAt", type="datetime", nullable=true)
     */
    protected $createAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="publishedAt", type="datetime", nullable=true)
     */
    protected $publishedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="stopPublish", type="boolean", nullable=true)
     */
    protected $stopPublish;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="stopPublishedAt", type="datetime", nullable=true)
     */
    protected $stopPublishedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime", nullable=true)
     */
    protected $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="alias", type="string", length=255, nullable=true)
     */
    protected $alias;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_keywords", type="string", length=255, nullable=true)
     */
    protected $metaKeywords;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="text", nullable=true)
     */
    protected $metaDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="hits", type="integer", nullable=true)
     */
    protected $hits;

	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Author
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Author
     */
    public function setUrl($url)
    {
        $this->url = $url;
    
        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Author
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set commentStatus
     *
     * @param integer $commentStatus
     * @return Author
     */
    public function setCommentStatus($commentStatus)
    {
        $this->commentStatus = $commentStatus;
    
        return $this;
    }

    /**
     * Get commentStatus
     *
     * @return integer 
     */
    public function getCommentStatus()
    {
        return $this->commentStatus;
    }

    /**
     * Set promotionStatus
     *
     * @param integer $promotionStatus
     * @return Author
     */
    public function setPromotionStatus($promotionStatus)
    {
        $this->promotionStatus = $promotionStatus;
    
        return $this;
    }

    /**
     * Get promotionStatus
     *
     * @return integer 
     */
    public function getPromotionStatus()
    {
        return $this->promotionStatus;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Author
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     * @return Author
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;
    
        return $this;
    }

    /**
     * Get createAt
     *
     * @return \DateTime 
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     * @return Author
     */
    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;
    
        return $this;
    }

    /**
     * Get publishedAt
     *
     * @return \DateTime 
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     * @return Author
     */
    public function setStopPublishedAt($stopPublishedAt)
    {
        $this->stopPublishedAt = $stopPublishedAt;
    
        return $this;
    }

    /**
     * Get stopPublishedAt
     *
     * @return \DateTime 
     */
    public function getStopPublishedAt()
    {
        return $this->stopPublishedAt;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
    }
    
	public function __toString()
	{
		return $this->title;
	}

    /**
     * Add related
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\Author $related
     * @return Author
     */
    public function addRelated(\Gospel\Bundle\CoreBundle\Entity\Author $related)
    {
		if(!$this->related->contains($related)) {
        	$this->related[] = $related;
		}
    
        return $this;
    }

    /**
     * Set category
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\Category $category
     * @return Author
     */
    public function setCategory(\Gospel\Bundle\CoreBundle\Entity\CategoryAuthor $category = null)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return \Gospel\Bundle\CoreBundle\Entity\CategoryAuthor
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Author
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set metaKeywords
     *
     * @param string $metaKeywords
     * @return Author
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;
    
        return $this;
    }

    /**
     * Get metaKeywords
     *
     * @return string 
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     * @return Author
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
    
        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string 
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set alias
     *
     * @param string $alias
     * @return Author
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    
        return $this;
    }

    /**
     * Get alias
     *
     * @return string 
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set hits
     *
     * @param \int $hits
     * @return Author
     */
    public function setHits($hits)
    {
        $this->hits = $hits;
    
        return $this;
    }

    /**
     * Get hits
     *
     * @return \int 
     */
    public function getHits()
    {
        return $this->hits;
    }

    /**
     * Set stopPublish
     *
     * @param boolean $stopPublish
     * @return Author
     */
    public function setStopPublish($stopPublish)
    {
        $this->stopPublish = $stopPublish;
    
        return $this;
    }

    /**
     * Get stopPublish
     *
     * @return boolean 
     */
    public function getStopPublish()
    {
        return $this->stopPublish;
    }

    /**
     * Set type
     *
     * @param boolean $type
     * @return Author
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

	/**
     * Sets cover.
     *
     * @param UploadedCover $cover
     */
    public function setCover(UploadedFile $cover = null)
    {
        $this->cover = $cover;
		// 检查是否有旧的文件，如果有保存到临时变量里面，当更新的时候进行删除
		if (isset($this->coverPath)) {
			$this->tempCover = $this->coverPath;
			$this->coverPath = null;
		} else {
			$this->coverPath = null;
		}
    }

    /**
     * Get cover.
     *
     * @return UploadedCover
     */
    public function getCover()
    {
        return $this->cover;
    }

	/**
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 */
	public function preUpload()
	{
		if (null !== $this->getCover()) {
			// 生成一个独一无二的名字
			$coverName = sha1(uniqid(mt_rand(), true));
			$this->coverPath = $coverName . '.' . $this->getCover()->guessExtension();
		}
	}

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null !== $this->getCover()) {
            // 如果是在移动文件的时候出现错误，
            // move()函数会扔出异常，从而阻止内容被保存到数据库
            $this->getCover()->move($this->getAbsoluteCoverPath(), $this->coverPath);

            // 查看是否有旧的文件
            if (isset($this->tempCover)) {
                //删除旧的文件
                unlink($this->getAbsoluteCoverPath().DIRECTORY_SEPARATOR.$this->tempCover);
            }
        }
        return;
    }

    /**
     * @ORM\PostRemove()
     */
    public function remove()
    {
        if ($coverPath = $this->getCoverPath()) {
            unlink($this->getAbsoluteCoverPath().DIRECTORY_SEPARATOR.$coverPath);
        }
    }

    public function getCoverPath()
    {
        return $this->coverPath;
    }

    /**
     * 封面图片的相对路径
     */
	public function getCoverWebPath()
	{
		return 'assets/media/author/cover';
	}
    
    /**
     * 封面图片的路径
     */
	public function getCoverWebUrl()
	{
		return $this->getCoverWebPath() . DIRECTORY_SEPARATOR . $this->getCoverPath();
	}

    /**
     * 服务器的根路径
     */
	public function getWebRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return $_SERVER['DOCUMENT_ROOT'];
    }

    public function getAbsoluteCoverPath()
    {
        return $this->getWebRootDir() . DIRECTORY_SEPARATOR . $this->getCoverWebPath();
    }

    public function getAbsoluteAuthorPath()
    {
        return $this->getWebRootDir() . DIRECTORY_SEPARATOR . $this->getAuthorWebPath();
    }


    /**
     * Set coverPath
     *
     * @param string $coverPath
     * @return Author
     */
    public function setCoverPath($coverPath)
    {
        $this->coverPath = $coverPath;
    
        return $this;
    }

    /**
     * Set authorPath
     *
     * @param string $authorPath
     * @return Author
     */
    public function setAuthorPath($authorPath)
    {
        $this->authorPath = $authorPath;
    
        return $this;
    }

    /**
     * Get authorPath
     *
     * @return string 
     */
    public function getAuthorPath()
    {
        return $this->authorPath;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return Author
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    
        return $this;
    }

    /**
     * Get gender
     *
     * @return string 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Author
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Author
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    
        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Author
     */
    public function setAddress($address)
    {
        $this->address = $address;
    
        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set sortOrder
     *
     * @param boolean $sortOrder
     * @return Author
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return boolean 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

	public function getCategoryPlusTitle()
	{
		return "【".$this->getCategory()->getTitle()."】".$this->getTitle();
	}


}
