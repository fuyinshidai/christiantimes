<?php

/*
 * 文章的模型
 */
namespace Gospel\Bundle\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Link
 * 
 * @ORM\Table(name="gospel_link")
 * @ORM\Entity(repositoryClass="Gospel\Bundle\CoreBundle\Entity\LinkRepository")
 * @ORM\HasLifecycleCallbacks
 * @link Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class Link
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

	private $tempCover;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;


	/**
     * @ORM\Column(name="cover_path", type="string", length=255, nullable=true)
     */
    public $coverPath;

	/**
     * @ORM\Column(name="link_path", type="string", length=255, nullable=true)
     */
    public $linkPath;

	/**
     * @Assert\File(maxSize="6000000")
     */
    private $cover;


    /**
     * @var string
     *
	 * @ORM\ManyToOne(targetEntity="Gospel\Bundle\CoreBundle\Entity\CategoryLink")
	 * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=true)
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @var integer
     *
     * @ORM\Column(name="sortOrder", type="integer", nullable=true)
     */
    private $sortOrder;

    /**
     * @var integer
     *
     * @ORM\Column(name="promotionStatus", type="boolean", nullable=true)
     */
    private $promotionStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createAt", type="datetime", nullable=true)
     */
    private $createAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="publishedAt", type="datetime", nullable=true)
     */
    private $publishedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="stopPublish", type="boolean", nullable=true)
     */
    private $stopPublish;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="stopPublishedAt", type="datetime", nullable=true)
     */
    private $stopPublishedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime", nullable=true)
     */
    private $updatedAt;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Link
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Link
     */
    public function setUrl($url)
    {
        $this->url = $url;
    
        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Link
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set promotionStatus
     *
     * @param integer $promotionStatus
     * @return Link
     */
    public function setPromotionStatus($promotionStatus)
    {
        $this->promotionStatus = $promotionStatus;
    
        return $this;
    }

    /**
     * Get promotionStatus
     *
     * @return integer 
     */
    public function getPromotionStatus()
    {
        return $this->promotionStatus;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Link
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     * @return Link
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;
    
        return $this;
    }

    /**
     * Get createAt
     *
     * @return \DateTime 
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     * @return Link
     */
    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;
    
        return $this;
    }

    /**
     * Get publishedAt
     *
     * @return \DateTime 
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     * @return Link
     */
    public function setStopPublishedAt($stopPublishedAt)
    {
        $this->stopPublishedAt = $stopPublishedAt;
    
        return $this;
    }

    /**
     * Get stopPublishedAt
     *
     * @return \DateTime 
     */
    public function getStopPublishedAt()
    {
        return $this->stopPublishedAt;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
    }
    
	public function __toString()
	{
		return $this->title;
	}

    /**
     * Set category
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\CategoryLink $category
     * @return Link
     */
    public function setCategory(\Gospel\Bundle\CoreBundle\Entity\CategoryLink $category = null)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return \Gospel\Bundle\CoreBundle\Entity\CategoryLink 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Link
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set stopPublish
     *
     * @param boolean $stopPublish
     * @return Link
     */
    public function setStopPublish($stopPublish)
    {
        $this->stopPublish = $stopPublish;
    
        return $this;
    }

    /**
     * Get stopPublish
     *
     * @return boolean 
     */
    public function getStopPublish()
    {
        return $this->stopPublish;
    }

    /**
     * Set type
     *
     * @param boolean $type
     * @return Link
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

	/**
     * Sets cover.
     *
     * @param UploadedCover $cover
     */
    public function setCover(UploadedFile $cover = null)
    {
        $this->cover = $cover;
		// 检查是否有旧的文件，如果有保存到临时变量里面，当更新的时候进行删除
		if (isset($this->coverPath)) {
			$this->tempCover = $this->coverPath;
			$this->coverPath = null;
		} else {
			$this->coverPath = null;
		}
    }

    /**
     * Get cover.
     *
     * @return UploadedCover
     */
    public function getCover()
    {
        return $this->cover;
    }

	/**
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 */
	public function preUpload()
	{
		if (null !== $this->getCover()) {
			// 生成一个独一无二的名字
			$coverName = sha1(uniqid(mt_rand(), true));
			$this->coverPath = $coverName . '.' . $this->getCover()->guessExtension();
		}
	}

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null !== $this->getCover()) {
            // 如果是在移动文件的时候出现错误，
            // move()函数会扔出异常，从而阻止内容被保存到数据库
            $this->getCover()->move($this->getAbsoluteCoverPath(), $this->coverPath);

            // 查看是否有旧的文件
            if (isset($this->tempCover)) {
                //删除旧的文件
                unlink($this->getAbsoluteCoverPath().DIRECTORY_SEPARATOR.$this->tempCover);
            }
        }
        return;
    }

    /**
     * @ORM\PostRemove()
     */
    public function remove()
    {
        if ($coverPath = $this->getCoverPath()) {
			if (file_exists($this->getAbsoluteCoverPath().DIRECTORY_SEPARATOR.$coverPath)) {
				unlink($this->getAbsoluteCoverPath().DIRECTORY_SEPARATOR.$coverPath);
			}
        }
    }

    public function getCoverPath()
    {
        return $this->coverPath;
    }

    /**
     * 封面图片的相对路径
     */
	public function getCoverWebPath()
	{
		return 'assets/media/link/cover';
	}
    
    /**
     * 封面图片的路径
     */
	public function getCoverWebUrl()
	{
		return $this->getCoverWebPath() . DIRECTORY_SEPARATOR . $this->getCoverPath();
	}

    /**
     * 服务器的根路径
     */
	public function getWebRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return $_SERVER['DOCUMENT_ROOT'];
    }

    public function getAbsoluteCoverPath()
    {
        return $this->getWebRootDir() . DIRECTORY_SEPARATOR . $this->getCoverWebPath();
    }

    public function getAbsoluteLinkPath()
    {
        return $this->getWebRootDir() . DIRECTORY_SEPARATOR . $this->getLinkWebPath();
    }


    /**
     * Set coverPath
     *
     * @param string $coverPath
     * @return Link
     */
    public function setCoverPath($coverPath)
    {
        $this->coverPath = $coverPath;
    
        return $this;
    }

    /**
     * Set linkPath
     *
     * @param string $linkPath
     * @return Link
     */
    public function setLinkPath($linkPath)
    {
        $this->linkPath = $linkPath;
    
        return $this;
    }

    /**
     * Get linkPath
     *
     * @return string 
     */
    public function getLinkPath()
    {
        return $this->linkPath;
    }

    /**
     * Set sortOrder
     *
     * @param boolean $sortOrder
     * @return Link
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return boolean 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }
}