<?php

namespace Gospel\Bundle\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AreaCategory
 *
 * @ORM\Table(name="gospel_area_category")
 * @ORM\Entity(repositoryClass="Gospel\Bundle\CoreBundle\Entity\AreaCategoryRepository")
 */
class AreaCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;

    /**
     * @var integer
     *
     * @ORM\Column(name="sortOrder", type="integer")
     */
    protected $sortOrder;

    /**
     * @var string
     *
	 * @ORM\OneToMany(targetEntity="Area", mappedBy="category")
     */
    protected $areas;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return AreaCategory
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return AreaCategory
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->areas = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add areas
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\Area $areas
     * @return AreaCategory
     */
    public function addArea(\Gospel\Bundle\CoreBundle\Entity\Area $areas)
    {
        $this->areas[] = $areas;
    
        return $this;
    }

    /**
     * Remove areas
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\Area $areas
     */
    public function removeArea(\Gospel\Bundle\CoreBundle\Entity\Area $areas)
    {
        $this->areas->removeElement($areas);
    }

    /**
     * Get areas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAreas()
    {
        return $this->areas;
    }

	public function __toString()
	{
		return $this->getTitle();
	}

	public function __sleep()
	{
		return array('id', 'title');
	}
}
