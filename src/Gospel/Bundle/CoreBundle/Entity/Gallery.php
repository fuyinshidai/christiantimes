<?php

/*
 * 文章的模型
 */
namespace Gospel\Bundle\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Gallery
 * 
 * @ORM\Table(name="gospel_gallery")
 * @ORM\Entity(repositoryClass="Gospel\Bundle\CoreBundle\Entity\GalleryRepository")
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class Gallery
{
	static public $CATEGORY_INTERVIEW = 72;

	static public $CATEGORY_QUESTION = 64;

	static public $CATEGORY_CHURCH_REFLECTION = 11;

	static public $CATEGORY_THEOLOGY = 57;
	static public $CATEGORY_CHURCH = 1;
	static public $CATEGORY_TWO_CHURCH = 154;
	static public $CATEGORY_MINISTRY = 50;
	static public $CATEGORY_SOCIETY = 17;
	static public $CATEGORY_CULTURE = 30;
	static public $CATEGORY_FAMILY = 39;

	/**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

	/**
	 * The string of all the tags
	 * @var type 
	 */
	protected $tagsCommaString;

	/**
	 * 用来设置相关文章的临时字段
	 * @var type 
	 */
	protected $relatedGalleries;


    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    protected $description;

    /**
     * @var string 标记的作者名称，当在作家里面没有对应的作家的时候显示
     *
     * @ORM\Column(name="writer", type="string", length=255, nullable=true)
     */
    protected $writer;

    /**
     * @var string
     *
	 * @ORM\ManyToOne(targetEntity="Gospel\Bundle\CoreBundle\Entity\Author", inversedBy="galleries")
	 * @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     */
    protected $author;

    /**
     * @var string
     *
	 * @ORM\ManyToOne(targetEntity="Gospel\Bundle\CoreBundle\Entity\CategoryAuthor", inversedBy="galleries")
	 * @ORM\JoinColumn(name="author_title_id", referencedColumnName="id")
     */
    protected $authorTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="comefrom", type="string", length=25, nullable=true)
     */
    protected $comefrom;

    /**
     * @var string
     *
	 * @ORM\ManyToOne(targetEntity="Gospel\Bundle\UserBundle\Entity\User", inversedBy="nodes")
	 * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

	/**
	 * @ORM\ManyToMany(targetEntity="Gospel\Bundle\CoreBundle\Entity\Tag")
	 * @ORM\JoinTable(name="gospel_gallery_tag"
     *      )
	 */
	protected $tags;

	/**
	 * Related galleries
	 * 
	 * @ORM\ManyToMany(targetEntity="Gospel\Bundle\CoreBundle\Entity\Gallery")
	 * @ORM\JoinTable(name="gospel_gallery_related",
	 * joinColumns={@ORM\JoinColumn(name="gallery_id", referencedColumnName="id")},
	 * inverseJoinColumns={@ORM\JoinColumn(name="related_id", referencedColumnName="id")}
	 * )
	 */
	protected $related;


    /**
     * @var integer
     *
     * @ORM\Column(name="promotionStatus", type="boolean", nullable=true)
     */
    protected $promotionStatus;

    /**
     * @var string
     *
	 * @ORM\ManyToOne(targetEntity="Gospel\Bundle\CoreBundle\Entity\Category")
	 * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    protected $category;

    /**
	 * 圣诞分类
     * @var string
     *
	 * @ORM\ManyToOne(targetEntity="Gospel\Bundle\CoreBundle\Entity\CategoryGallery")
	 * @ORM\JoinColumn(name="categorygallery_id", referencedColumnName="id")
     */
    protected $categorygallery;

    /**
     * @var string
     *
	 * @ORM\ManyToOne(targetEntity="Gospel\Bundle\CoreBundle\Entity\AreaCategory")
	 * @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     */
    protected $region;


    /**
     * @var integer
     *
     * @ORM\Column(name="commentStatus", type="boolean", nullable=true)
     */
    protected $commentStatus;


    /**
     * @var integer 文章是否发布，如果是false的话文章没有发布
     *
     * @ORM\Column(name="status", type="boolean")
     */
    protected $status;

    /**
     * @var \DateTime 文章创建时间
     *
     * @ORM\Column(name="createAt", type="datetime")
     */
    protected $createAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="publishedAt", type="datetime", nullable=true)
     */
    protected $publishedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="stopPublish", type="boolean", nullable=true)
     */
    protected $stopPublish;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="stopPublishedAt", type="datetime", nullable=true)
     */
    protected $stopPublishedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime", nullable=true)
     */
    protected $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="alias", type="string", length=255, nullable=true)
     */
    protected $alias;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_keywords", type="string", length=255, nullable=true)
     */
    protected $metaKeywords;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="text", nullable=true)
     */
    protected $metaDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="hits", type="integer", nullable=true)
     */
    protected $hits;

	/**
     * @ORM\OneToMany(targetEntity="GalleryImage", mappedBy="gallery")
     */
    protected $images;

    public function setId($id)
    {
        $this->id = $id;
		return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Gallery
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Gallery
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    
    /**
     * Set user
     *
     * @param \Gospel\Bundle\UserBundle\Entity\User $user
     * @return Gallery
     */
    public function setUser(\Gospel\Bundle\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \Gospel\Bundle\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }


    /**
     * Set status
     *
     * @param integer $status
     * @return Gallery
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     * @return Gallery
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;
    
        return $this;
    }

    /**
     * Get createAt
     *
     * @return \DateTime 
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     * @return Gallery
     */
    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;
    
        return $this;
    }

    /**
     * Get publishedAt
     *
     * @return \DateTime 
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     * @return Gallery
     */
    public function setStopPublishedAt($stopPublishedAt)
    {
        $this->stopPublishedAt = $stopPublishedAt;
    
        return $this;
    }

    /**
     * Get stopPublishedAt
     *
     * @return \DateTime 
     */
    public function getStopPublishedAt()
    {
        return $this->stopPublishedAt;
    }
    /**
     * Constructor
     */
    public function __construct($id = null)
    {
		if($id) {
			$this->id = $id;
		}
        $this->related = new \Doctrine\Common\Collections\ArrayCollection();
		$this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }
    

	public function setRelatedGalleries($galleries)
	{
		$this->relatedGalleries = $galleries;
	}

	public function getRelatedGalleries()
	{
		$data = array();
		foreach($this->getRelated() as $gallery) {
			$data[] = '['.$gallery->getId().']'.$gallery->getTitle();
		}
		return implode(',',$data);
	}

	public function __toString()
	{
		return $this->title;
	}

    /**
     * Add related
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\Gallery $related
     * @return Gallery
     */
    public function addRelated(\Gospel\Bundle\CoreBundle\Entity\Gallery $related)
    {
		if(!$this->related->contains($related)) {
        	$this->related[] = $related;
		}
    
        return $this;
    }

    /**
     * Remove related
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\Gallery $related
     */
    public function removeRelated(\Gospel\Bundle\CoreBundle\Entity\Gallery $related)
    {
        $this->related->removeElement($related);
    }

    /**
     * Get related
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRelated()
    {
        return $this->related;
    }


    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Gallery
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set metaKeywords
     *
     * @param string $metaKeywords
     * @return Gallery
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;
    
        return $this;
    }

    /**
     * Get metaKeywords
     *
     * @return string 
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     * @return Gallery
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
    
        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string 
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set alias
     *
     * @param string $alias
     * @return Gallery
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    
        return $this;
    }

    /**
     * Get alias
     *
     * @return string 
     */
    public function getAlias()
    {
        return $this->alias ? str_replace(array('/','-'), '.', $this->alias) : str_replace(array('/','-'), '.', $this->getTitle());
    }

    /**
     * Set hits
     *
     * @param $hits
     * @return Gallery
     */
    public function setHits($hits)
    {
        $this->hits = $hits;
    
        return $this;
    }

    /**
     * Get hits
     *
     * @return 
     */
    public function getHits()
    {
        return $this->hits;
    }

    /**
     * Set stopPublish
     *
     * @param boolean $stopPublish
     * @return Gallery
     */
    public function setStopPublish($stopPublish)
    {
        $this->stopPublish = $stopPublish;
    
        return $this;
    }

    /**
     * Get stopPublish
     *
     * @return boolean 
     */
    public function getStopPublish()
    {
        return $this->stopPublish;
    }

    /**
     * Add images
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\GalleryImage $images
     * @return Gallery
     */
    public function addImage(\Gospel\Bundle\CoreBundle\Entity\GalleryImage $images)
    {
        $this->images[] = $images;
    
        return $this;
    }

    /**
     * Remove images
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\GalleryImage $images
     */
    public function removeImage(\Gospel\Bundle\CoreBundle\Entity\GalleryImage $images)
    {
        $this->images->removeElement($images);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImages()
    {
        return $this->images;
    }

	public function getFirstImage()
	{
		$images = $this->getImages();
		if(is_object($images)) {
			$image =  array_shift($images->toArray());
			return $image;
		}
		return null;
	}

	public function getCoverPath() 
	{
		$cover = $this->getCover();
		if ($cover) {
			return self::getUploadImagePath().$this->getCreateAt()->format('Ym')
					.'/'.$this->getId().'/'.$cover->getName();
		}
	}

	public function getImagePath()
	{
		return self::getUploadImagePath().$this->getCreateAt()->format('Ym')
				.'/'.$this->getId().'/';
	}

	/**
	 * 取得文章的封面图片
	 * @return type
	 */
	public function getCover()
	{
		$cover = null;
		$images = $this->getImages();
		foreach($images as $k => $image) {
			if ($image->getIsCover()) {
				$cover = $image;
				return $cover;
			}

			if ($k == 0) {
				$cover = $image;
			}

		}
		return $cover;
	}

	static public function getUploadImagePath()
	{
		return 'assets/media/gallery/';
	}

    /**
     * Set gallery
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\TopicGallery $gallery
     * @return Gallery
     */
    public function setGallery(\Gospel\Bundle\CoreBundle\Entity\Gallery $gallery = null)
    {
        $this->gallery = $gallery;
    
        return $this;
    }

    /**
     * Get gallery
     *
     * @return \Gospel\Bundle\CoreBundle\Entity\Gallery 
     */
    public function getGallery()
    {
        return $this->gallery;
    }


    /**
     * Set CategoryGallery
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\CategoryGallery $categorygallery
     * @return Gallery
     */
    public function setCategoryGallery(\Gospel\Bundle\CoreBundle\Entity\CategoryGallery $categorygallery = null)
    {
        $this->categorygallery = $categorygallery;
    
        return $this;
    }

    /**
     * Get CategoryGallery
     *
     * @return \Gospel\Bundle\CoreBundle\Entity\CategoryGallery 
     */
    public function getCategoryGallery()
    {
        return $this->categorygallery;
    }

    /**
     * Set authorTitle
     *
     * @param \Gospel\Bundle\CoreBundle\Entity\CategoryAuthor $authorTitle
     * @return Gallery
     */
    public function setAuthorTitle(\Gospel\Bundle\CoreBundle\Entity\CategoryAuthor $authorTitle = null)
    {
        $this->authorTitle = $authorTitle;
    
        return $this;
    }

    /**
     * Get authorTitle
     *
     * @return \Gospel\Bundle\CoreBundle\Entity\CategoryAuthor 
     */
    public function getAuthorTitle()
    {
        return $this->authorTitle;
    }

    /**
     * Set promotionStatus
     *
     * @param integer $promotionStatus
     * @return Video
     */
    public function setPromotionStatus($promotionStatus)
    {
        $this->promotionStatus = $promotionStatus;
    
        return $this;
    }

    /**
     * Get promotionStatus
     *
     * @return integer 
     */
    public function getPromotionStatus()
    {
        return $this->promotionStatus;
    }

	public function __sleep()
	{
		return array('id', 'title', 'description');
	}
}
