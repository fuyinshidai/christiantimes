<?php

namespace Gospel\Bundle\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GalleryImage
 *
 * @ORM\Table(name="gospel_gallery_image")
 * @ORM\Entity(repositoryClass="Gospel\Bundle\CoreBundle\Entity\GalleryImageRepository")
 */
class GalleryImage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
	 * @ORM\ManyToOne(targetEntity="Gallery", inversedBy="images", cascade={"persist"})
	 * @ORM\JoinColumn(name="gallery_id", referencedColumnName="id")
     */
    private $gallery;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

	/**
	 * @var integer Record the current image width
	 */
	private $width;

	/**
	 * @var integer Record the current image height
	 */
	private $height;

	/**
	 * @var integer Record the current image position top
	 */
	private $top;

    /**
     * @var string
     *
     * @ORM\Column(name="caption", type="string", length=255, nullable=true)
     */
    private $caption;

    /**
     * @var string
     *
     * @ORM\Column(name="use_watermark", type="boolean", nullable=true, options={"default" = 1})
     */
    private $useWatermark;

    /**
     * @var string
     *
     * @ORM\Column(name="is_cover", type="boolean", nullable=true)
     */
    private $isCover;
	

    /**
     * @var integer
     *
     * @ORM\Column(name="sortOrder", type="integer")
     */
    private $sortOrder;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set gallery
     *
     * @param integer $gallery
     * @return GalleryImage
     */
    public function setGallery($gallery)
    {
        $this->gallery = $gallery;
    
        return $this;
    }

    /**
     * Get gallery
     *
     * @return integer 
     */
    public function getGallery()
    {
        return $this->gallery;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return GalleryImage
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return GalleryImage
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    
        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set caption
     *
     * @param string $caption
     * @return GalleryImage
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;
    
        return $this;
    }

    /**
     * Get caption
     *
     * @return string 
     */
    public function getCaption()
    {
        return $this->caption;
    }

    /**
     * Set useWatermark
     *
     * @param boolean $useWatermark
     * @return GalleryImage
     */
    public function setUseWatermark($useWatermark)
    {
        $this->useWatermark = $useWatermark;
    
        return $this;
    }

    /**
     * Get useWatermark
     *
     * @return boolean 
     */
    public function getUseWatermark()
    {
        return $this->useWatermark;
    }

    /**
     * Set isCover
     *
     * @param boolean $isCover
     * @return GalleryImage
     */
    public function setIsCover($isCover)
    {
        $this->isCover = $isCover;
    
        return $this;
    }

    /**
     * Get isCover
     *
     * @return boolean 
     */
    public function getIsCover()
    {
        return $this->isCover;
    }

	public function getWidth()
	{
		return $this->width;
	}

	public function setWidth($width)
	{
		$this->width = $width;
		return $this;
	}

	public function getHeight()
	{
		return $this->height;
	}

	public function setHeight($height)
	{
		$this->height = $height;
		return $this;
	}

	public function getTop()
	{
		return $this->top;
	}

	public function setTop($top)
	{
		$this->top = $top;
		return $this;
	}
}