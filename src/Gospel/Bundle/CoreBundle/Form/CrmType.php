<?php

namespace Gospel\Bundle\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Gospel\Bundle\CoreBundle\Form\DataTransformer\TagToStringTransformer;
/**
 *
 * @crm Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class CrmType extends AbstractType
{
	public function __construct()
	{
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		// This assume an object manager passed in as an argument
		$entityManager = $options['em'];
		$entity = $options['entity'];
		$now = new \DateTime('now');
		$datetimeToStringTransformer = new DataTransformer\DatetimeToStringTransformer($options['em']);

		$builder
				->add('title', null, array('label'=>'标题', 
					'attr' => array('size'=>60),
					))
				->add('url', null, array('label'=>'链接地址', 
					'attr' => array('size'=>90),
					))
				->add('email', null, array('label'=>'邮箱', 
					'attr' => array('size'=>90),
					))
				->add('phone', null, array('label'=>'电话', 
					'attr' => array('size'=>90),
					))
				->add('address', null, array('label'=>'地址', 
					'attr' => array('size'=>90),
					))
				;
		// 如果是地区版管理员，则需要设置默认的地区，并且将地区选项隐藏
		$areaOptions = array(
							'required' => false,
							'label' => '地区',
							'empty_value' => '请选择地区',
							'choices' => $options['area'],
							'attr' => array('class' => 'tree'),
						);
		$builder
				->add($builder->create('area', 'choice', $areaOptions)
						->addModelTransformer(new DataTransformer\AreaToIntTransformer($options['em']))
				);

		$builder
				->add('cover', 'file', array('label'=>'图片', 'help'=>'（图片尺寸：150x150）',
					'required'=>false,
					))
				->add('content', null, array('label'=>'内容'))
				->add('gender', 'gender', array('label'=>'性别'))
				->add($builder->create('updatedAt','hidden', array(
					'label'=>'更新时间',
					'data' => $now->getTimestamp(),
					))
						->addModelTransformer($datetimeToStringTransformer)
						)
				->add($builder->create('createAt','hidden', array(
					'label'=>'发布时间',
					'data' => isset($options['createAt']) ? $options['createAt']->getTimestamp() : $now->getTimestamp(),
					))
						->addModelTransformer($datetimeToStringTransformer)
						)
				
				;
	}

	public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'Gospel\Bundle\CoreBundle\Entity\Crm',
		));

		$resolver->setRequired(array(
			'em',
			'area',
			'createAt',
			'entity',
		));

		$resolver->setAllowedTypes(array(
			'em' => 'Doctrine\Common\Persistence\ObjectManager',
		));
	}

	public function getName()
	{
		return 'gospel_crm';
	}
}
