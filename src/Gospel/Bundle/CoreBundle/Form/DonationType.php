<?php

namespace Gospel\Bundle\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class DonationType extends AbstractType
{
	public function __construct()
	{
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		// This assume an object manager passed in as an argument
		$entity = $options['entity'];

		$builder
				->add('tradeNo', null, array('label'=>'编号', 
					))
				->add('createdAt', null, array(
					'label' => '创建时间',
        			'data' => new \DateTime('now')))
				;
	}

	public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'Gospel\Bundle\CoreBundle\Entity\Donation',
		));

		$resolver->setRequired(array(
			'entity',
		));

	}

	public function getName()
	{
		return 'gospel_donation';
	}
}
