<?php

namespace Gospel\Bundle\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Gospel\Bundle\CoreBundle\Form\DataTransformer\TagToStringTransformer;
/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class AuthorSearchType extends AbstractType
{
	public function __construct()
	{
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$categoryTransformer = new DataTransformer\CategoryToIntTransformer($options['em']);
		// This assume an object manager passed in as an argument
		$builder
				->add('title', null, array('label'=>'标题', 
					'attr' => array('size'=>60),
					'required'=>false,
					))
				->add('email', null, array('label'=>'邮箱', 
					'attr' => array('size'=>90),
					'required'=>false,
					))
				->add('phone', null, array('label'=>'电话', 
					'attr' => array('size'=>90),
					'required'=>false,
					))
				->add(
						$builder->create('category', 'genemu_jqueryselect2_choice', array(
					'label' => '分类',
					'empty_value' => '请选择分类',
					'attr' => array('class'=>'tree'),
					'choices' => $options['category'],
					'required'=>false,
				))
						->addModelTransformer($categoryTransformer)
						
						)
				->add('promotionStatus', null, array('label'=>'推荐', 'required'=>false))
				->add('gender', 'gender', array('label'=>'性别', 'required'=>false))
				->add('status', null, array('label'=>'发布状态',
					'data' => isset($status)?$status:TRUE,
					'required'=>false,
					'help' => '选中为已发布，为选中为未发布状态'
					))
				;
	}

	public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'Gospel\Bundle\CoreBundle\Entity\Author',
		));

		$resolver->setRequired(array(
			'em',
			'category',
		));

		$resolver->setAllowedTypes(array(
			'em' => 'Doctrine\Common\Persistence\ObjectManager',
		));
	}

	public function getName()
	{
		return 'gospel_author_search';
	}
}
