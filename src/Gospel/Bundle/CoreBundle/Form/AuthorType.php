<?php

namespace Gospel\Bundle\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Gospel\Bundle\CoreBundle\Form\DataTransformer\TagToStringTransformer;
use Gospel\Bundle\CoreBundle\Form\DataTransformer\CategoryAuthorToIntTransformer;
/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class AuthorType extends AbstractType
{
	public function __construct()
	{
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		// This assume an object manager passed in as an argument
		$entityManager = $options['em'];
		$entity = $options['entity'];
		$categoryTransformer = new CategoryAuthorToIntTransformer($options['em']);
		$now = new \DateTime('now');
		$datetimeToStringTransformer = new DataTransformer\DatetimeToStringTransformer($options['em']);
		$comment = $entity->getCommentStatus();
		$status = $entity->getStatus();

		$builder
				->add('title', null, array('label'=>'标题', 
					'attr' => array('size'=>60),
					))
				->add('url', null, array('label'=>'作家链接地址', 
					'attr' => array('size'=>90),
					))
				->add('email', null, array('label'=>'邮箱', 
					'attr' => array('size'=>90),
					))
				->add('phone', null, array('label'=>'电话', 
					'attr' => array('size'=>90),
					))
				->add('address', null, array('label'=>'地址', 
					'attr' => array('size'=>90),
					))
				->add('sortOrder', null, array('label'=>'排序', 
					'help' => '数字小的靠前，数字大的靠后'
					))
				->add(
						$builder->create('category', 'genemu_jqueryselect2_choice', array(
					'label' => '分类',
					'empty_value' => '请选择分类',
					'attr' => array('class'=>'tree'),
					'choices' => $options['category'],
				))
						->addModelTransformer($categoryTransformer)
						
						)
				->add('cover', 'file', array('label'=>'封面图片', 'help'=>'（图片尺寸：150x150）',
					'required'=>false,
					))
				->add('content', null, array('label'=>'内容'))
				->add('commentStatus', null, array('label'=>'评论状态',
					'data' => isset($comment)?$comment:TRUE,
					'required'=>false,
					))
				->add('promotionStatus', null, array('label'=>'推荐'))
				->add('gender', 'gender', array('label'=>'性别'))
				->add('status', null, array('label'=>'发布状态',
					'data' => isset($status)?$status:TRUE,
					'required'=>false,
					'help' => '选中为已发布，为选中为未发布状态'
					))
				->add('publishedAt', null, array(
					'label' => '发布时间',
        			'data' => new \DateTime('now')))
				->add('stopPublish', null, array(
					'label'=>'停止发布',
					'help'=> '停止发布选中后，停止发布时间开始生效',
					))
				->add('stopPublishedAt', null, array(
					'label' => '停止发布时间',
        			'data' => new \DateTime('now')))
				->add($builder->create('updatedAt','hidden', array(
					'label'=>'更新时间',
					'data' => $now->getTimestamp(),
					))
						->addModelTransformer($datetimeToStringTransformer)
						)
				
				->add($builder->create('createAt','hidden', array(
					'label'=>'发布时间',
					'data' => isset($options['createAt']) ? $options['createAt']->getTimestamp() : $now->getTimestamp(),
					))
						->addModelTransformer($datetimeToStringTransformer)
						)
				->add('alias', null, array('label'=>'URL名称', 
					'attr' => array(),
					'help' => '添加在URL里面的内容'
					))
				->add('metaKeywords', null, array('label'=>'关键词', 
					'attr' => array(),
					))
				->add('metaDescription', null, array('label'=>'描述', 
					))
				;
	}

	public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'Gospel\Bundle\CoreBundle\Entity\Author',
		));

		$resolver->setRequired(array(
			'em',
			'category',
			'createAt',
			'entity',
		));

		$resolver->setAllowedTypes(array(
			'em' => 'Doctrine\Common\Persistence\ObjectManager',
		));
	}

	public function getName()
	{
		return 'gospel_author';
	}
}
