<?php

namespace Gospel\Bundle\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Gospel\Bundle\CoreBundle\Form\DataTransformer\TagToStringTransformer;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class PostType extends AbstractType
{

	public function __construct()
	{
		
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		// This assume an object manager passed in as an argument
		$entityManager = $options['em'];
		$entity = $options['entity'];
		$user = $options['user'];
		$categoryTransformer = new DataTransformer\CategoryToIntTransformer($options['em']);
		$categoryOtherTransformer = new DataTransformer\CategoryOtherToIntTransformer($options['em']);
		$userTransformer = new \Gospel\Bundle\UserBundle\Form\DataTransformer\UserToIntTransformer($options['em']);
		$now = new \DateTime('now');
		$datetimeToStringTransformer = new DataTransformer\DatetimeToStringTransformer($options['em']);
		$comment = $entity->getCommentStatus();
		$status = $entity->getStatus();

		$builder
				->add('title', null, array('label' => '标题',
					'attr' => array('size' => 60),
				))
				->add('titleEn', null, array('label' => '英文标题',
					'attr' => array('size' => 60),
					'required' => false,
				))
				->add('aliasEn', null, array('label' => '英文URL名称',
					'attr' => array(),
					'help' => '用于SEO优化，添加在URL的内容',
					'required' => false,
				))
				->add(
						$builder->create('category', 'genemu_jqueryselect2_choice', array(
							'label' => '分类',
							'required' => false,
							'empty_value' => '选择分类',
							'attr' => array('class' => 'tree'),
							'choices' => $options['category'],
						))
						->addModelTransformer($categoryTransformer)
				)
				->add(
						$builder->create('category2', 'genemu_jqueryselect2_choice', array(
							'label' => '分类2',
							'required' => false,
							'empty_value' => '选择分类',
							'attr' => array(
								'class' => 'tree', 
//								'style' => 'display:none;'
								),
							'choices' => $options['category'],
						))
						->addModelTransformer($categoryTransformer)
				)
				->add(
						$builder->create('categoryother', 'genemu_jqueryselect2_choice', array(
							'label' => '分类',
							'required' => false,
							'empty_value' => '所属栏目',
							'attr' => array('class' => 'tree'),
							'choices' => $options['categoryother'],
						))
						->addModelTransformer($categoryOtherTransformer)
				);

		// 如果是地区版管理员，则需要设置默认的地区，并且将地区选项隐藏
		$areaOptions = array(
							'required' => false,
							'label' => '省区',
							'empty_value' => '选择省区',
							'choices' => $options['area'],
							'attr' => array('class' => 'tree'),
						);
		if (isset($options['attr']['defaultArea'])) {
			$areaOptions['data'] = $options['attr']['defaultArea'];
			$areaOptions['attr']['class'] = 'tree hide';
		}
		$builder
				->add($builder->create('area', 'choice', $areaOptions)
						->addModelTransformer(new DataTransformer\AreaToIntTransformer($options['em']))
				);
		$builder
				->add('region', null, array(
					'empty_value' => '选择地区',
					'label' => '地区',
					'required' => false))
				->add('author', 'entity', array(
					'class' => 'GospelCoreBundle:Author',
					'label' => '作者',
					'property' => 'categoryPlusTitle',
					'required' => false,
					'empty_value' => '选择作者',
					))
				->add('writer', null, array('label' => '作者2', 'required' => false,
					'help' => '当上面的作者不存在时添加此栏内容'
					))
				->add($builder->create('user', 'hidden', array('data' => $user->getId()))
						->addModelTransformer($userTransformer))
				->add('tagsCommaString', 'text', array('required' => false,
					'label' => '添加标签',
					'help' => "多个标签之间用<strong>逗号(英文半角逗号,或者中文全角逗号)</strong>隔开",
				))
				->add('introduction', null, array(
					'label' => '摘要',
					'required' => false,
					'attr' => array('style' => 'width:100%;')
				))
				->add('comefrom', null, array(
					'label' => '文章来源',
					'required' => false,
				))
				->add('content', 'textarea', array(
					'label' => '内容'
				))
				->add('contentEn', 'textarea', array(
					'label' => '英文内容',
					'required' => false,
				))
				->add('relatedPosts', null, array(
					'label' => '相关文章',
					'attr' => array('style' => 'width:500px;'),
					'help' => '<a class="btn button" id="search-related" href="#">搜索相关文章</a><br/>直接输入<strong>[id]</strong>按回车键添加',
					'required' => false,
				));

			$builder
					->add('isSlideImage', null, array(
						'label' => '文章类型',
						'required' => false,
						'help' => '选中为大图轮播'
					));
			$builder
					->add('isDonate', null, array(
						'label' => '鼓励赞赏',
						'required' => false,
						'help' => '用户可以前台赞赏该文章'
					));
			$builder
					->add('isOriginal', null, array(
						'label' => '原创文章',
						'required' => false,
						'help' => ''
					));

		// 如果是用户没有推荐权限，则所有的发布选项都不显示
		if (in_array('ROLE_POST_EDIT_OWN', $user->getRoles())) {
		} else {
			// 如果是已经设置了默认的地区选项，说明正在操作该页面的用户为地区版的管理员
			if (isset($options['attr']['defaultArea'])) {

				$builder
						->add('isLocalPromotion', null, array('label' => '推荐到地区版首页', 'required' => false))
						->add('isLocalHeadlinePic', null, array('label' => '地区版首页图片头条', 'required' => false))
						->add('isLocalHeadlineText', null, array('label' => '地区版首页文字头条', 'required' => false))
						->add('isLocalEditorRecommend', null, array('label' => '地区版编辑推荐', 'required' => false))
						;
			} else { // 设置普通的管理员
			}



		}
			$builder
					->add('status', null, array('label' => '发布状态',
//						'data' => isset($status) ? $status : TRUE,
						'required' => false,
						'help' => '选中为已发布，为选中为未发布状态'
					))
					->add('publishedAt', null, array(
						'label' => '发布时间',
						'data' => $entity->getPublishedAt() ? $entity->getPublishedAt() : new \DateTime('now'),
						'years' =>range(2010,date('Y')+3)
						))
					->add('stopPublish', null, array(
						'label' => '停止发布',
						'help' => '停止发布选中后，停止发布时间开始生效',
					))
					->add('stopPublishedAt', null, array(
						'label' => '停止发布时间',
						'data' => new \DateTime('now')))
					->add($builder->create('updatedAt', 'hidden', array(
								'label' => '更新时间',
								'data' => $now->getTimestamp(),
							))
							->addModelTransformer($datetimeToStringTransformer)
					)
					->add($builder->create('createAt', 'hidden', array(
								'label' => '发布时间',
								'data' => isset($options['createAt']) ? $options['createAt']->getTimestamp() : $now->getTimestamp(),
							))
							->addModelTransformer($datetimeToStringTransformer)
					);

		$builder
				->add('isHomeHeadlinePic', null, array('label' => '首页头条图片', 'required' => false))
				->add('isHomePic', null, array('label' => '首页图片', 'required' => false))
				->add('homeHeadlinePicDesc', null, array('label' => '首页图片头条推荐说明', 'required' => false))
				->add('isHomeHeadlineTxt', null, array('label' => '首页头条1', 'required' => false))
				->add('ishomeheadlinetxt1', null, array('label' => '首页头条2', 'required' => false))
				->add('ishomeheadlinetxt2', null, array('label' => '首页头条3', 'required' => false))
				->add('ishomeheadlinetxt3', null, array('label' => '首页头条4', 'required' => false))
				->add('isLocalHeadlinePic', null, array('label' => '地区图片头条', 'required' => false))
				->add('isLocalHeadlineText', null, array('label' => '地区头条', 'required' => false))
				->add('isSectionHeadlinePic', null, array('label' => '频道图片头条', 'required' => false))
				->add('isSectionHeadlineTxt', null, array('label' => '频道头条', 'required' => false))
				->add('isDeepRead', null, array(
					'label' => '深读',
					'required' => false,
				))
			->add('commentStatus', null, array('label' => '评论状态',
				'data' => isset($comment) ? $comment : TRUE,
				'required' => false,
			));


		// ************如果用户没有推荐选项，上面的发布选项则都不显示 *************

		$builder
				->add('alias', null, array('label' => 'URL名称',
					'attr' => array(),
					'help' => '添加在URL里面的内容'
				))
				->add('metaKeywords', null, array('label' => '关键词',
					'attr' => array(),
				))
				->add('metaDescription', null, array('label' => '描述',
				))
		;
	}

	public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'Gospel\Bundle\CoreBundle\Entity\Post',
		));

		$resolver->setRequired(array(
			'em',
			'user',
			'category',
			'categoryother',
			'area',
			'createAt',
			'entity',
		));

		$resolver->setAllowedTypes(array(
			'em' => 'Doctrine\Common\Persistence\ObjectManager',
		));
	}

	public function getName()
	{
		return 'gospel_post';
	}

}
