<?php

namespace Gospel\Bundle\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Gospel\Bundle\CoreBundle\Form\DataTransformer\TagToStringTransformer;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class GalleryType extends AbstractType
{

	public function __construct()
	{
		
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		// This assume an object manager passed in as an argument
		$entityManager = $options['em'];
		$entity = $options['entity'];
		$user = $options['user'];
		$categoryTransformer = new DataTransformer\CategoryToIntTransformer($options['em']);
		$categoryGalleryTransformer = new DataTransformer\CategoryGalleryToIntTransformer($options['em']);
		$userTransformer = new \Gospel\Bundle\UserBundle\Form\DataTransformer\UserToIntTransformer($options['em']);
		$now = new \DateTime('now');
		$datetimeToStringTransformer = new DataTransformer\DatetimeToStringTransformer($options['em']);
		$status = $entity->getStatus();

		$builder
				->add('title', null, array('label' => '标题',
					'attr' => array('size' => 60),
				))
				->add('description', null, array('label' => '简介',
				))
				->add(
						$builder->create('categoryGallery', 'genemu_jqueryselect2_choice', array(
							'label' => '分类',
							'required' => false,
							'empty_value' => '分类',
							'attr' => array('class' => 'tree'),
							'choices' => $options['categoryGallery'],
						))
						->addModelTransformer($categoryGalleryTransformer)
				)
				;

		$builder
				->add($builder->create('user', 'hidden', array('data' => $user->getId()))
						->addModelTransformer($userTransformer))
				->add('relatedGalleries', null, array(
					'label' => '相关图片集',
					'attr' => array('style' => 'width:500px;'),
					'help' => '<a class="btn button" id="search-related" href="#">搜索相关文章</a><br/>直接输入<strong>[id]</strong>按回车键添加',
					'required' => false,
				));


			$builder
					->add('status', null, array('label' => '发布状态',
//						'data' => isset($status) ? $status : TRUE,
						'required' => false,
						'help' => '选中为已发布，为选中为未发布状态'
					))
					->add('promotionStatus', null, array('label'=>'推荐'))
					->add('publishedAt', null, array(
						'label' => '发布时间',
						'data' => new \DateTime('now')))
					->add('stopPublish', null, array(
						'label' => '停止发布',
						'help' => '停止发布选中后，停止发布时间开始生效',
					))
					->add('stopPublishedAt', null, array(
						'label' => '停止发布时间',
						'data' => new \DateTime('now')))
					->add($builder->create('updatedAt', 'hidden', array(
								'label' => '更新时间',
								'data' => $now->getTimestamp(),
							))
							->addModelTransformer($datetimeToStringTransformer)
					)
					->add($builder->create('createAt', 'hidden', array(
								'label' => '发布时间',
								'data' => isset($options['createAt']) ? $options['createAt']->getTimestamp() : $now->getTimestamp(),
							))
							->addModelTransformer($datetimeToStringTransformer)
					);



		// ************如果用户没有推荐选项，上面的发布选项则都不显示 *************

		$builder
				->add('alias', null, array('label' => 'URL名称',
					'attr' => array(),
					'help' => '添加在URL里面的内容'
				))
				->add('metaKeywords', null, array('label' => '关键词',
					'attr' => array(),
				))
				->add('metaDescription', null, array('label' => '描述',
				))
		;
	}

	public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'Gospel\Bundle\CoreBundle\Entity\Gallery',
		));

		$resolver->setRequired(array(
			'em',
			'user',
			'categoryGallery',
			'createAt',
			'entity',
		));

		$resolver->setAllowedTypes(array(
			'em' => 'Doctrine\Common\Persistence\ObjectManager',
		));
	}

	public function getName()
	{
		return 'gospel_gallery';
	}

}
