<?php

namespace Gospel\Bundle\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class SubmissionType extends AbstractType
{
	public function __construct()
	{
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		// This assume an object manager passed in as an argument
		$entity = $options['entity'];

		$builder
				->add('name', null, array('label'=>'姓名', 
					))
				->add('email', 'email', array('label'=>'Email', 
					))
				->add('cover', null, array('label'=>'图片', 
					'required' => false,
					))
				->add('title', null, array('label'=>'标题', 
					))
				->add('content', null, array('label'=>'内容'))
				->add('contact', null, array('label'=>'联系方式', 
					'required' => false,
					))
				->add('captcha', 'captcha', array(
					'label'=>'验证码',
					'reload' => true,
					'as_url' => true,
					))
				->add('createdAt', null, array(
					'label' => '创建时间',
        			'data' => new \DateTime('now')))
				;
	}

	public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'Gospel\Bundle\CoreBundle\Entity\Submission',
		));

		$resolver->setRequired(array(
			'entity',
		));

	}

	public function getName()
	{
		return 'gospel_submission';
	}
}
