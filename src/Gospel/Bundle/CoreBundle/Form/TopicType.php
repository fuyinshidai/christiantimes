<?php

namespace Gospel\Bundle\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class TopicType extends AbstractType
{
	public function __construct()
	{
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		// This assume an object manager passed in as an argument
		$entityManager = $options['em'];
		$entity = $options['entity'];
		$now = new \DateTime('now');
		$datetimeToStringTransformer = new DataTransformer\DatetimeToStringTransformer($options['em']);
		$status = $entity->getStatus();
		$categoryTopicTransformer = new DataTransformer\CategoryTopicToIntTransformer($options['em']);

		$builder
				->add('title', null, array('label'=>'标题', 'required' => false,
					'attr' => array(),
					))
				->add('link', null, array('label'=>'外部链接', 'required' => false,
					'attr' => array(),
					'help' => '(输入完整网址, 以http://开头)',
					))
				->add('serial', null, array('label'=>'专题号', 
					'attr' => array(),
					'help' => '(例如： 第7期, 圣诞特刊等)',
					'required' => false,
					))
				->add('cover', 'file', array('label'=>'封面图片', 'help'=>'尺寸：220×128',
					'required'=>false,
					))
				->add(
						$builder->create('category', 'genemu_jqueryselect2_choice', array(
					'label' => '分类',
					'empty_value' => '请选择分类',
					'attr' => array('class'=>'tree'),
					'choices' => $options['category'],
				))
						->addModelTransformer($categoryTopicTransformer)
						
						)
				->add('topic', 'file', array('label'=>'专题图片', 'help'=>'尺寸：990×267',
					'required'=>false,
					))
				->add('content', null, array('label'=>'内容','required' => false))
				->add('promotionStatus', null, array('label'=>'推荐','required' => false))
				->add('status', null, array('label'=>'发布状态',
					'data' => isset($status)?$status:TRUE,
					'required'=>false,
					'help' => '选中为已发布，为选中为未发布状态'
					))
				->add($builder->create('publishedAt',null, array(
					'label'=>'发布时间', 'required'=>false,
					'data' => $entity->getPublishedAt() ? $entity->getPublishedAt() : $now,
					))
						->addModelTransformer($datetimeToStringTransformer)
						)
				->add('stopPublish', null, array(
					'label'=>'停止发布', 'required'=>false,
					'help'=> '停止发布选中后，停止发布时间开始生效',
					))
				->add('stopPublishedAt', null, array(
					'label' => '停止发布时间',
        			'data' => new \DateTime('now')))
				->add($builder->create('updatedAt','hidden', array(
					'label'=>'更新时间', 'required'=>false,
					'data' => $now->getTimestamp(),
					))
						->addModelTransformer($datetimeToStringTransformer)
						)
				
				->add($builder->create('createAt','hidden', array(
					'label'=>'创建时间', 'required'=>false,
					'data' => isset($options['createAt']) ? $options['createAt']->getTimestamp() : $now->getTimestamp(),
					))
						->addModelTransformer($datetimeToStringTransformer)
						)
				->add('alias', null, array('label'=>'URL名称', 
					'attr' => array(), 'required'=>false,
					'help' => '添加在URL里面的内容'
					))
				->add('metaKeywords', null, array('label'=>'关键词', 
					'attr' => array(), 'required'=>false,
					))
				->add('metaDescription', null, array('label'=>'描述', 'required'=>false,
					))
				;
	}

	public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'Gospel\Bundle\CoreBundle\Entity\Topic',
		));

		$resolver->setRequired(array(
			'em',
			'category',
			'createAt',
			'entity',
		));

		$resolver->setAllowedTypes(array(
			'em' => 'Doctrine\Common\Persistence\ObjectManager',
		));
	}

	public function getName()
	{
		return 'gospel_topic';
	}
}
