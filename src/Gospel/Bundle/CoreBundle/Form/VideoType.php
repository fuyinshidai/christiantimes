<?php

namespace Gospel\Bundle\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Gospel\Bundle\CoreBundle\Form\DataTransformer\TagToStringTransformer;
/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class VideoType extends AbstractType
{
	public function __construct()
	{
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		// This assume an object manager passed in as an argument
		$entityManager = $options['em'];
		$entity = $options['entity'];
		$categoryTransformer = new DataTransformer\CategoryVideoToIntTransformer($options['em']);
		$now = new \DateTime('now');
		$datetimeToStringTransformer = new DataTransformer\DatetimeToStringTransformer($options['em']);
		$comment = $entity->getCommentStatus();
		$status = $entity->getStatus();

		$builder
				->add('title', null, array('label'=>'标题', 
					'attr' => array('size'=>60),
					))
				->add('url', null, array('label'=>'视频文件', 
					'attr' => array('size'=>90),
					))
				->add(
						$builder->create('category', 'genemu_jqueryselect2_choice', array(
					'label' => '分类',
					'empty_value' => '请选择分类',
					'attr' => array('class'=>'tree'),
					'choices' => $options['category'],
				))
						->addModelTransformer($categoryTransformer)
						
						)
				->add('author', null, array('label'=>'作者', 'required'=>false))
				->add('tagsCommaString', 'text', array('required'=>false,
					'label'=>'添加新标签',
					'help'  => "多个标签之间用<strong>逗号(,)</strong>隔开",
					))
				->add('tags', null, array(
					'label'=>'选择已有标签',
					'required'=>false,
//					'expanded' => true,
//					'class' => 'GospelCoreBundle:Tag',
//					'multiple' => true,
					))
				->add('comefrom', null, array(
					'label'=>'文章来源',
					'required'=>false,
					))
				->add('cover', 'file', array('label'=>'封面图片', 'help'=>'（图片尺寸：150x150）',
					'required'=>false,
					))
				->add('video', 'file', array('label'=>'视频文件', 'help'=>'请上传50M以内flv,mp4,ogg,webm格式的文件',
					'required'=>false,
					))
				->add('type', 'choice', array(
					'label'=>'类型',
					'required' => false,
					'expanded' => true,
					'multiple' => false,
					'help' => '选择上传视频类型',
					'choices' => array(
						0 => '远程',
						1 => '本地',
					),
					))
				->add('embedCode', null, array('label'=>'嵌入代码', 'required'=>false))
				->add('content', null, array('label'=>'内容'))
				->add('relatedVideos', null, array(
					'label' => '相关视频',
					'attr'=> array('style'=>'width:500px;'),
					'help' => '<a class="btn button" id="search-related" href="#">搜索相关视频</a><br/>直接输入<strong>[id]</strong>按回车键添加',
					'required' => false,
				))
				->add('commentStatus', null, array('label'=>'评论状态',
					'data' => isset($comment)?$comment:TRUE,
					'required'=>false,
					))
				->add('promotionStatus', null, array('label'=>'推荐'))
				->add('status', null, array('label'=>'发布状态',
					'data' => isset($status)?$status:TRUE,
					'required'=>false,
					'help' => '选中为已发布，为选中为未发布状态'
					))
				->add('editorRecommemd', null, array('label'=>'编辑推荐',
					'required'=>false,
					))
				->add('publishedAt', null, array(
					'label' => '发布时间',
        			'data' => new \DateTime('now')))
				->add('stopPublish', null, array(
					'label'=>'停止发布',
					'help'=> '停止发布选中后，停止发布时间开始生效',
					))
				->add('stopPublishedAt', null, array(
					'label' => '停止发布时间',
        			'data' => new \DateTime('now')))
				->add($builder->create('updatedAt','hidden', array(
					'label'=>'更新时间',
					'data' => $now->getTimestamp(),
					))
						->addModelTransformer($datetimeToStringTransformer)
						)
				
				->add($builder->create('createAt','hidden', array(
					'label'=>'发布时间',
					'data' => isset($options['createAt']) ? $options['createAt']->getTimestamp() : $now->getTimestamp(),
					))
						->addModelTransformer($datetimeToStringTransformer)
						)
				->add('alias', null, array('label'=>'URL名称', 
					'attr' => array(),
					'help' => '添加在URL里面的内容'
					))
				->add('metaKeywords', null, array('label'=>'关键词', 
					'attr' => array(),
					))
				->add('metaDescription', null, array('label'=>'描述', 
					))
				;
	}

	public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'Gospel\Bundle\CoreBundle\Entity\Video',
		));

		$resolver->setRequired(array(
			'em',
			'category',
			'createAt',
			'entity',
		));

		$resolver->setAllowedTypes(array(
			'em' => 'Doctrine\Common\Persistence\ObjectManager',
		));
	}

	public function getName()
	{
		return 'gospel_video';
	}
}
