<?php

namespace Gospel\Bundle\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Gospel\Bundle\CoreBundle\Form\DataTransformer\TagToStringTransformer;
/**
 *
 * @crm Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class CrmSearchType extends AbstractType
{
	public function __construct()
	{
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$categoryTransformer = new DataTransformer\CategoryToIntTransformer($options['em']);
		// This assume an object manager passed in as an argument
		$builder
				->add('title', null, array('label'=>'标题', 
					'attr' => array('size'=>60),
					'required'=>false,
					))
				->add('area', null, array('label'=>'地区', 
					'required'=>false,
					))
				->add('email', null, array('label'=>'邮箱', 
					'attr' => array('size'=>90),
					'required'=>false,
					))
				->add('phone', null, array('label'=>'电话', 
					'attr' => array('size'=>90),
					'required'=>false,
					))
				->add('gender', 'gender', array('label'=>'性别', 'required'=>false))
				;
	}

	public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'Gospel\Bundle\CoreBundle\Entity\Crm',
		));

		$resolver->setRequired(array(
			'em',
			'area',
		));

		$resolver->setAllowedTypes(array(
			'em' => 'Doctrine\Common\Persistence\ObjectManager',
		));
	}

	public function getName()
	{
		return 'gospel_crm_search';
	}
}
