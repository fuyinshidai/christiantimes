<?php

namespace Gospel\Bundle\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Gospel\Bundle\CoreBundle\Form\Type\GenderType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\OptionsResolver\Options;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class TagType extends AbstractType
{

	private $parent;

	public function __construct($parent = null)
	{
		$this->parent = $parent;
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
				->add('name', null, array('label' => '名称'))
		;
	}

	public function getName()
	{
		return 'gospel_tag';
	}

}
