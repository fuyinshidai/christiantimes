<?php

namespace Gospel\Bundle\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class AreaCategoryType extends AbstractType
{

	private $parent;

	public function __construct($parent = null)
	{
		$this->parent = $parent;
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
				->add('title', null, array('label' => '名称'))
				->add('sortOrder', null, array('label' => '排序'))
		;
	}

	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
	}

	public function getName()
	{
		return 'gospel_areacategory';
	}

}
