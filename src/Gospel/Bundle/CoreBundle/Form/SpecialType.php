<?php

namespace Gospel\Bundle\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class SpecialType extends AbstractType
{
	public function __construct()
	{
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$entityManager = $options['em'];
		$entity = $options['entity'];
		$now = new \DateTime('now');
		$datetimeToStringTransformer = new DataTransformer\DatetimeToStringTransformer($options['em']);
		$status = $entity->getStatus();
		$builder
				->add('title', null, array(
					'label' => '标题',
					'attr' => array('size' => 60),
				))
				->add('description', null, array(
					'label' => '描述',
				))
				->add('template', null, array(
					'label' => '模板',
				))
				->add('headline', null, array(
					'label' => '图片头条',
				))
				->add('subheadline', null, array(
					'label' => '文字头条',
					'help' => '不同的id用<strong>|</strong>隔开(在英文键盘下面的|)'
				))
				->add('file', null, array(
					'label' => '条幅',
					'help' => '尺寸: 1100x254'
				))
				->add('category1', null, array(
					'label' => '左侧分类一id',
					'help' => '左侧分类一id'
				))
				->add('category2', null, array(
					'label' => '左侧分类二id',
					'help' => '左侧分类二id'
				))
				->add('category3', null, array(
					'label' => '左侧分类三id',
					'help' => '左侧分类三id'
				))
				->add('category4', null, array(
					'label' => '左侧分类四id',
					'help' => '左侧分类四id'
				))
				->add('category5', null, array(
					'label' => '解读分类一id',
					'help' => '解读分类一id'
				))
				->add('category6', null, array(
					'label' => '右侧分类二id',
					'help' => '右侧分类二id'
				))
				->add('recommend', null, array(
					'label' => '编辑推荐',
					'help' => '不同的id用<strong>|</strong>隔开(在英文键盘下面的|)'
				))
				->add('interpretation', null, array(
					'label' => '解读文章ID',
				))
				->add('video', null, array(
					'label' => '视频id',
				))

				->add('status', null, array('label'=>'发布状态',
					'data' => isset($status)?$status:TRUE,
					'required'=>false,
					'help' => '选中为已发布，为选中为未发布状态'
					))
				->add('publishedAt', null, array(
					'label' => '发布时间',
        			'data' => new \DateTime('now')))
				->add('stopPublish', null, array(
					'label'=>'停止发布',
					'help'=> '停止发布选中后，停止发布时间开始生效',
					))
				->add('stopPublishedAt', null, array(
					'label' => '停止发布时间',
        			'data' => new \DateTime('now')))
				->add($builder->create('updatedAt','hidden', array(
					'label'=>'更新时间',
					'data' => $now->getTimestamp(),
					))
						->addModelTransformer($datetimeToStringTransformer)
						)
				
				->add($builder->create('createAt','hidden', array(
					'label'=>'发布时间',
					'data' => isset($options['createAt']) ? $options['createAt']->getTimestamp() : $now->getTimestamp(),
					))
						->addModelTransformer($datetimeToStringTransformer)
						)
				->add('alias', null, array('label'=>'URL名称', 
					'attr' => array(),
					'help' => '添加在URL里面的内容'
					))
				->add('metaKeywords', null, array('label'=>'关键词', 
					'attr' => array(),
					))
				->add('metaDescription', null, array('label'=>'描述', 
					))
		;
	}

	public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'Gospel\Bundle\CoreBundle\Entity\Special',
		));

		$resolver->setRequired(array(
			'em',
			'createAt',
			'entity',
		));

		$resolver->setAllowedTypes(array(
			'em' => 'Doctrine\Common\Persistence\ObjectManager',
		));

	}

	public function getName()
	{
		return 'gospel_special';
	}
}
