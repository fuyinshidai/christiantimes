<?php

namespace Gospel\Bundle\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Gospel\Bundle\CoreBundle\Form\Type\GenderType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\OptionsResolver\Options;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class CategoryOtherType extends AbstractType
{

	private $parent;

	public function __construct($parent = null)
	{
		$this->parent = $parent;
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
				->add('title', null, array('label' => '名称'))
				->add('parent', 'choiceTree', array(
					'data' => $options['selected'],
					'class' => 'Gospel\Bundle\CoreBundle\Entity\CategoryOther',
					'attr' => array(
						'expended' => true,
					),
					'required' => false,
//					'multiple' => true,
				))
				->add('description', null, array('label' => '描述'))
				->add('slug', null, array('label' => '标识'))
				->add('sortOrder', null, array('label' => '排序', 'required' => false))
				->add('isActive', null, array('label' => '是否显示',
					'required' => false,
				))
		;
	}

	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'class' => 'CmComunBundle:Comun\Nomenclador',
			'selected' => null,
		));
	}

	public function getName()
	{
		return 'gospel_categoryother';
	}

}
