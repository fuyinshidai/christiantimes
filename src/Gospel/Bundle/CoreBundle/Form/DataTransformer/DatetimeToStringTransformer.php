<?php

namespace Gospel\Bundle\CoreBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\Exception\TransformerFailedException;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class DatetimeToStringTransformer implements DataTransformerInterface
{
	private $om;

	public function __construct(ObjectManager $om)
	{
		$this->om = $om;
	}

	public function transform($value)
	{
		if(null == $value)
		{
			return "";
		}

		return $value;
//		return $value->getTimestamp();
	}

	public function reverseTransform($value)
	{
		if(!$value)
		{
			return null;
		}
		if ($value instanceof \DateTime) {
			return $value;
		}
		$date = new \DateTime();
		return $date->setTimestamp($value);
	}
}
