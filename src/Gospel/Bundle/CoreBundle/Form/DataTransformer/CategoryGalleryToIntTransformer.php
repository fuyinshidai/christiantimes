<?php

namespace Gospel\Bundle\CoreBundle\Form\DataTransformer;

use Gospel\Bundle\CoreBundle\Form\DataTransformer\EntityToIntTransformer;
use Doctrine\Common\Persistence\ObjectManager;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class CategoryGalleryToIntTransformer  extends EntityToIntTransformer
{
	public function __construct(ObjectManager $om)
	{
		parent::__construct($om, 'GospelCoreBundle:CategoryGallery');
	}
}