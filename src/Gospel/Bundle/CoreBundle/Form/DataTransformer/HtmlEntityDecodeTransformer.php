<?php

namespace Gospel\Bundle\CoreBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
use Gospel\Bundle\CoreBundle\Entity\Tag;
/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class HtmlEntityDecodeTransformer implements DataTransformerInterface
{

	public function __construct()
	{
	}

	/**
	 * Transform an object to string
	 * 
	 * @param type $value
	 * @return string
	 */
	public function transform($value)
	{
		if(null == $value) {
			return "";
		}
		return $value->getName();
	}

	public function reverseTransform($value)
	{
		if(!$value) {
			return null;
		}
		return $value;
	}
}
