<?php

namespace Gospel\Bundle\CoreBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
abstract class EntityToIntTransformer implements DataTransformerInterface
{
	/**
	 * @var ObjectManager
	 */
	private $om;
	private $entity;

	public function __construct(ObjectManager $om, $entity)
	{
		$this->om = $om;
		$this->setEntity($entity);
	}

	/**
	 * Transform an object to string
	 * 
	 * @param type $value
	 * @return string
	 */
	public function transform($value)
	{
		if(null == $value) {
			return "";
		}
		return $value->getId();
	}

	public function reverseTransform($value)
	{
		if(!$value) {
			return null;
		}
		$user = $this->om
				->getRepository($this->getEntity())
				->find($value)
				;
		if(null === $user) {
			throw new TransformationFailedException(sprintf('"%s" 不存在',
					$value)
					);
		}

		return $user;
	}

	public function setEntity($entity)
	{
		$this->entity = $entity;
	}

	public function getEntity()
	{
		return $this->entity;
	}
}

