<?php

namespace Gospel\Bundle\CoreBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
use Gospel\Bundle\CoreBundle\Entity\Tag;
/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class TagToStringTransformer implements DataTransformerInterface
{
	/**
	 * @var ObjectManager
	 */
	private $om;

	public function __construct(ObjectManager $om)
	{
		$this->om = $om;
	}

	/**
	 * Transform an object to string
	 * 
	 * @param type $value
	 * @return string
	 */
	public function transform($value)
	{
		if(null == $value) {
			return "";
		}
		return $value->getName();
	}

	public function reverseTransform($value)
	{
		if(!$value) {
			return null;
		}
		$tag = $this->om
				->getRepository('GospelCoreBundle:Tag')
				->findOneBy(array('name' => $value))
				;
		if(null === $tag) {
			throw new TransformationFailedException(sprintf('标签 "%s" 不存在',
					$value)
					);
		}

		return $tag;
	}
}
