<?php

/*
 * MagazineType.php encoding=UTF-8
 *
 * Created on Jan 27, 2015, 10:56:03 AM
 *
 * gospeltimes.cn default 
 * Copyright(c) Beijing Gospeltimes Information Technology, Inc.  All Rights Reserved.
 *
 */

namespace Gospel\Bundle\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class MagazineType extends AbstractType
{
	public function __construct()
	{

	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
				->add('title', null, array(
					'label' => '杂志标题'
				))
				->add('slug', null, array(
					'label' => 'url路径（例如：201501）'
				))
				->add('description', null, array(
					'label' => '本期杂志简介'
				))
				->add('file', null, array(
					'label' => 'PDF文件'
				))
				->add('created', null, array(
					'label' => '发布日志'
				))
				;
	}

	public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver)
	{
	}

	public function getName()
	{
		return 'gospel_magazine';
	}
}
