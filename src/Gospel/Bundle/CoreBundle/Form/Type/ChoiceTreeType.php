<?php

namespace Gospel\Bundle\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ChoiceTreeType extends AbstractType
{
	private $nodes;
	private $parentField, $nameField;

	protected $request;

    public function setRequest(Request $request = null)
    {
        $this->request = $request;
    }

	public function __construct()
    {
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
			'selected' => null,
        ));
    }

    public function getParent()
    {
        return 'entity';
    }

    public function getName()
    {
        return 'choiceTree';
    }
}