<?php

namespace Gospel\Bundle\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class BroadcastType extends AbstractType
{
	public function __construct()
	{
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		// This assume an object manager passed in as an argument
		$entity = $options['entity'];
		$status = $entity->getPublishStatus();

		$builder
				->add('title', null, array('label'=>'标题', 
					'attr' => array('size'=>60),
					))
				->add('content', null, array('label'=>'内容'))
				->add('publishStatus', null, array('label'=>'发布状态',
					'data' => isset($status)?$status:TRUE,
					'required'=>false,
					'help' => '选中为已发布，为选中为未发布状态'
					))
				->add('publishedAt', null, array(
					'label' => '发布时间',
        			'data' => new \DateTime('now')))
				;
	}

	public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'Gospel\Bundle\CoreBundle\Entity\Broadcast',
		));

		$resolver->setRequired(array(
			'entity',
		));

	}

	public function getName()
	{
		return 'gospel_broadcast';
	}
}
