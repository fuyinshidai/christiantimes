<?php

/*
 * OSSTest.php encoding=UTF-8
 *
 * Created on Mar 24, 2015, 3:49:33 PM
 *
 * christiantimes.cn default 
 * Copyright(c) Beijing Gospeltimes Information Technology, Inc.  All Rights Reserved.
 *
 */
namespace Gospel\Bundle\AlipayBundle\Tests\Unit;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Gospel\Bundle\CoreBundle\Services\OSS;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class OSSTest extends WebTestCase
{
	/**
	 * @var Gospel\Bundle\CoreBundle\Services\OSS
	 */
	protected $oss;

	public function setUp()
	{
		parent::setUp();
		$this->oss = new OSS();
	}
	public function testUpload()
	{
		$file = __FILE__;
		$ossKey = basename($file);

		$this->oss->delete($ossKey);
		$this->oss->upload($ossKey, $file);
		$result = $this->oss->getUrl($ossKey);
		$this->assertContains($ossKey, $result);
		$this->assertContains('Expires', $result);
	}

	public function testUploadBigVideo()
	{
		$file = '/var/www/christiantimes.cn/web/assets/media/video/files/heaven.webm';
		
		if (file_exists($file)) {
			$ossKey = basename($file);

			$this->oss->upload($ossKey, $file);
			$result = $this->oss->getUrl($ossKey);
			$this->assertContains($ossKey, $result);
		}
	}

	public function testDelete()
	{
		$this->assertNull($this->oss->delete(basename(__FILE__)));
	}

	public function testDownload()
	{
	}
}
