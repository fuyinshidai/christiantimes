<?php

namespace Gospel\Bundle\CoreBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class GospelTestCase extends WebTestCase
{
	public $baseUrl = 'http://gospeltimes.dev/app_dev.php';

	public function login($username, $password)
	{
		$client = static::createClient();
		$crawler = $client->request('GET', '/login');

		$this->assertRegExp(
				'/用户名/', $client->getResponse()->getContent()
		);

		$form = $crawler->selectButton('登 录')->form();
		$form['_username'] = $username;
		$form['_password'] = $password;
		$client->submit($form);
		return $client;
	}

	public function logout($client)
	{
		$crawler = $client->request('GET', '/');
		$link = $crawler->filter('a:contains("登出")')->eq(0)->link();
		$crawler = $client->click($link);
		return $client;
	}

}
