<?php

/*
 * 自动导入文章数据的测试文件
 */

/*
 * 导入完数据之后要做的工作
 * 1. 将注释去掉：/var/www/christiantimes.dev/src/Gospel/Bundle/CoreBundle/Classes/FileUploadHandler.php
 * Line 167, 
 * protected function get_full_url() {
 * 
 * 2. 将注释去掉：/var/www/christiantimes.dev/src/Gospel/Bundle/CoreBundle/Classes/FileUploadHandler.php
 * Line 779
 * //        header($str);
 * 
 * 3. 修改 /var/www/christiantimes.dev/src/Gospel/Bundle/CoreBundle/Entity/Post.php
 * Line 81
 * @ORM\GeneratedValue(strategy="AUTO")
 * 
 * 4. 修改 /var/www/christiantimes.dev/src/Gospel/Bundle/CoreBundle/Classes/FileUploadHandler.php
 * Line 45
 */

/*
 * 远程服务器上运行的时候需要注意的地方：
 * 1. 修改 /var/www/christiantimes.dev/src/Gospel/Bundle/CoreBundle/Classes/FileUploadHandler.php
 * Line 45 其中的上传路径'upload_dir'需要修改
 */

/*
 * 清理数据表
 SET FOREIGN_KEY_CHECKS = 0;
  DELETE FROM gospel_post;
  DELETE FROM gospel_post_image;
  DELETE FROM gospel_post_related;
  DELETE FROM gospel_post_tag;
 SET FOREIGN_KEY_CHECKS = 1;
 * 
 */

namespace Gospel\Bundle\AlipayBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Gospel\Bundle\CoreBundle\Controller\BackendController;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Gospel\Bundle\CoreBundle\Form\PostType;
use Gospel\Bundle\CoreBundle\Entity\Tag;
use Gospel\Bundle\CoreBundle\Entity\Post;
use Gospel\Bundle\CoreBundle\Classes\FileUploadHandler;
use Gospel\Bundle\CoreBundle\Entity\PostImage;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Gospel\Bundle\CoreBundle\Entity\Author;

class PostControllerTest extends WebTestCase
{

	/**
	 * @var \Doctrine\ORM\EntityManager
	 */
	private $em;
	private $emCustom;


	/**
	 * {@inheritDoc}
	 */
	public function setUp()
	{
		static::$kernel = static::createKernel();
		static::$kernel->boot();
		$this->em = static::$kernel->getContainer()
				->get('doctrine')
				->getManager()
		;
		$this->emCustom = static::$kernel->getContainer()
				->get('doctrine')
				->getManager('customer')
		;
	}

	public function getEm()
	{
		return $this->em;
	}

	public function getEmCustom()
	{
		return $this->emCustom;
	}

	/**
	 * The main testing function running point
	 */
	public function testRun()
	{
		$this->markTestSkipped('Must be reviewed');
//		$this->skiptestLoadImage();
		$this->updateContent();
	}

	/**
	 * 测试导入文章
	 * @global type $argv
	 * @global type $argc
	 */
	public function skiptestLoad()
	{
		global $argv, $argc;
		// use the -d parameter
		// phpunit -d 3000  -c app /var/www/christiantimes.dev/src/Gospel/Bundle/CoreBundle/Tests
		$startId = (int) $argv[2];
		$total = (int) $argv[4];
		$startFrom = $startId;
		$inteval = 1;
		$endId = $total + $startFrom;
		for ($start = $startFrom; $start < $endId; $start += $inteval) {
			$this->loadAction($start, $inteval);
		}
	}

	public function skiptestLoadImage()
	{
		global $argv, $argc;
		// use the -d parameter
		// phpunit -d 3000  -c app /var/www/christiantimes.dev/src/Gospel/Bundle/CoreBundle/Tests
		$startId = (int) $argv[2];
		$total = (int) $argv[4];
		$startFrom = $startId;
		$inteval = 1;
		$endId = $total + $startFrom;
		for ($start = $startFrom; $start < $endId; $start += $inteval) {
			$this->loadImagesAction($start, $inteval);
		}
	}


	/**
	 * 测试更新文章的精品栏目分类
	 */
	public function skiptestUpdateFineColumn()
	{
		global $argv, $argc;
		// use the -d parameter
		// phpunit -d 3000  -c app /var/www/christiantimes.dev/src/Gospel/Bundle/CoreBundle/Tests
		$startId = (int) $argv[2];
		$total = (int) $argv[4];
		$startFrom = $startId;
		$inteval = 1;
		$endId = $total + $startFrom;
		for ($start = $startFrom; $start <= $endId; $start += $inteval) {
			$this->updateCategoryAction($start, $inteval);
		}
	}

	/**
	 * 更新文章的精品栏目分类
	 * @param type $start
	 * @param type $total
	 */
	public function updateCategoryAction($start, $total)
	{
		$repositoryPost = $this->getEm()->getRepository('GospelCoreBundle:Post');
		$repositoryCategoryOther = $this->getEm()->getRepository('GospelCoreBundle:Categoryother');
		$result = $repositoryPost->findAllOld($this->emCustom, $total, $start);

		// 所属栏目使用的表示ct_menu
		foreach ($result as $row) {
			$menuId = $row['menu_id'];
			if ($menuId) {
				$Oldcategory = $repositoryPost->findOldMenu($this->emCustom, $menuId);
				if ($Oldcategory['name']) {
					$category = $repositoryCategoryOther->findOneBy(array('title' => $Oldcategory['name']));
					if (is_object($category)) {
						$post = $repositoryPost->find($row['id']);
						if ($post instanceof Post) {
							$post->setCategoryother($category);
							echo $post->getId() . ':'.$post->getTitle().": ". $category->getTitle()."\n";
							$this->em->persist($post);
							$this->em->flush();
						}
					}
				}
			}
		}
	}

	public function loadAction($start, $total)
	{
		$repositoryPost = $this->getEm()->getRepository('GospelCoreBundle:Post');
		$repositoryArea = $this->getEm()->getRepository('GospelCoreBundle:Area');
		$repositoryCategoryArea = $this->getEm()->getRepository('GospelCoreBundle:AreaCategory');
		$repositoryCategory = $this->getEm()->getRepository('GospelCoreBundle:Category');
		$repositoryTag = $this->getEm()->getRepository('GospelCoreBundle:Tag');
		$repositoryAuthor = $this->getEm()->getRepository('GospelCoreBundle:Author');
		$repositoryUser = $this->getEm()->getRepository('GospelUserBundle:User');
		$result = $repositoryPost->findAllOld($this->emCustom, $total, $start);
		$msg = "";
		$updateExistsPost = false;
		$updateExistsPost = true;
		if ($result) {
			$continue = 1;
		} else {
			$continue = 0;
		}
		foreach ($result as $row) {
			$date = new \DateTime("@" . $row['created']);
			$resource = $repositoryPost->find($row['id']);
			if ($resource instanceof Post) {
				foreach ($resource->getImages() as $image) {
					$this->em->remove($image);
					$this->em->flush();
				}

				$this->em->remove($resource);
				$this->em->flush();
			}
			//echo 'Post:' . $row['id'] . " in processing...\n";
			$msg .= 'Post:' . $row['id'] . " created\n";
			if ($row['cat_id'] == 0) {
				$categoryName = '其他';
			} else {
				$categoryName = $repositoryPost->findOneByOldCategoryId($this->getEmCustom(), $row['cat_id']);
			}
			$resource = new Post($row['id']);
			$resource->setTitle($row['title']);
			$resource->setIntroduction($row['intro']);
			$resource->setContent(html_entity_decode($row['content']));
			$resource->setCommentStatus(1);
			$categories = $repositoryCategory->findBy(array('title' => $categoryName));
			// set the category to the child of the same category name
			if (count($categories) > 1) {
				$category0 = $categories[0];
				$category1 = $categories[1];
				if (is_object($category1->getParent()) && ($category1->getParent()->getId() == $category0->getId())) {
					$category = $category1;
				} else {
					$category = $category0;
				}
			} else {
				$category = array_pop($categories);
			}

			$this->assertEquals($categoryName, $category->getTitle());


			// 如果一级地区没有设置，则设置为其他地区
			if ($row['area_id'] == 0) {
				$row['area_id'] = $repositoryCategoryArea->findOneBy(array('title' => '无地区'))->getId();
				$this->assertEquals(13, $row['area_id']);
			}

			$resource = new Post($row['id']);
			$resource->setTitle($row['title']);
			$resource->setIntroduction($row['intro']);
			$resource->setContent(html_entity_decode($row['content']));
			$resource->setCommentStatus(1);
			$resource->setCategory($category);
			$resource->setPromotionStatus(0);
			$resource->setStatus($row['is_show']);
			$resource->setCreateAt($date);
			$resource->setUpdatedAt($date);
			$resource->setPublishedAt($date);
			$resource->setMetaKeywords($row['keywords']);
			// 设置点击量
			$resource->setHits($row['hits']);
			// 文章来源
			$resource->setComefrom($row['source']);

			// 设置相关文章
			foreach (explode(',', $row['relative_article']) as $relate) {
				$relatedPost = $repositoryPost->find($relate);
				if ($relatedPost) {
					$resource->addRelated($relatedPost);
				}
			}
			$resource->setWriter($row['author']);
			// 标签
			$tagString = $row['tags'];
			$tagsData = array();
			$delimeters = array(',', '，', ' ', '-', '_', '，', ' ');
			foreach ($delimeters as $del) {
				$tags_explode = explode($del, $tagString);
				$tagsData = array_merge($tagsData, $tags_explode);
			}
			$resource = $this->updateTag(implode(',', array_unique($tagsData)), $resource);

			// 导入作者title : author_title -> authorTitle
			$postTitle = $row['author_title'];
			if ($postTitle) {
				$resource->setAuthorTitle($this->getEm()->getRepository('GospelCoreBundle:CategoryAuthor')
								->findOneBy(array('title' => $postTitle)));
			}
			$this->em->persist($resource);
			$this->em->flush();
		}
	}

	public function loadImagesAction($start, $total)
	{
		$repositoryPost = $this->getEm()->getRepository('GospelCoreBundle:Post');
		$result = $repositoryPost->findAllOld($this->emCustom, $total, $start);
		$fs = new Filesystem();
		foreach ($result as $row) {
			$oldImages = $repositoryPost->findOldImages($this->emCustom, $row['id']);
			if(is_array($oldImages)) {
				$post = $repositoryPost->find($row['id']);
				if (is_object($post)) {
					$date = new \DateTime("@" . $row['created']);
					foreach($oldImages as $oldImage) {
						$image = new PostImage($row['id']);
						$image->setName(basename($oldImage['image']));
						$image->setPost($post);
						$image->setSortOrder($oldImage['sequence']);
						$image->setCaption($oldImage['intro']);
						$image->setUseWatermark(1);
						$this->em->persist($image);
						$this->em->flush();

						// 移动图片到新的位置
						$oldImageFile = $this->getOldImageRoot() . '/' . $oldImage['image'];
						$newImageFoler = $this->getNewPostImageRoot() . '/'.$date->format('Ym').'/'
								.$post->getId().'/'
								;
						$newImageFile = $newImageFoler . basename($oldImage['image']);
						$fs->mkdir($newImageFoler);
						if(file_exists($oldImageFile)) {
							if (!file_exists($newImageFile)) {
								$fs->copy($oldImageFile, $newImageFile);
							}
						}
						$fileUploader = $this->getUploader($post->getId());
						$options = $fileUploader->options;
						$options['max_width'] = 80;
						$options['max_height'] = 80;
						$fileUploader->create_scaled_image(basename($oldImage['image']), 'thumbnail', $options);
					}
				}
			}
		}
	}

	/**
	 * Check if the tag already in the database, if not has, then create a new 
	 * tag and then add this tag to the new resource
	 * 
	 * @param type $request
	 * @param type Post
	 */
	private function updateTag($tagsCommaString, $resource)
	{
		if ($tagsCommaString) {
			$repo = $this->getEm()->getRepository('GospelCoreBundle:Tag');
			$em = $this->getEm();
			foreach (array(',', '，') as $del) {
				foreach (explode($del, $tagsCommaString) as $tagName) {
					if (null === ($tag = $repo->findOneBy(array('name' => $tagName)))) {
						$tag = new Tag();
						$tag->setName($tagName);
						$tag->setCount(1);
						$em->persist($tag);
						$em->flush();
					}
					$tags = $resource->getTags();
					if (!$tags->contains($tag)) {
						$resource->addTag($tag);
					}

					//增加tag的count
//					$tag->setCount($tag->getCount() + 1);
//					$em->persist($tag);
//					$em->flush();
				}
			}
		}
		return $resource;
	}

	/**
	 * 移动图片的时候旧的图片文件夹的地址
	 * @return string
	 */
	public function getOldImageRoot()
	{
//		if ($_SERVER['SERVER_ADDR'] == '127.0.0.1') {
//		return '/var/www/www.christiantimes.cn/upload/';
//		} else {
			return '/home/wwwroot/christiantimes/public_html/upload/';
//		}
	}

	/**
	 * 移动图片的时候新的图片文件夹的地址
	 * @return string
	 */
	public function getNewPostImageRoot()
	{
//		if ($_SERVER['SERVER_ADDR'] == '127.0.0.1') {
		return '/data/www/christiantimes.cn/web/assets/media/post';
//		} else {
//			return '/home/wwwroot/christiantimes.cn/web/assets/media/post/';
//		}
	}

	/**
	 * 
	 * @param type $id
	 * @return \Gospel\Bundle\CoreBundle\Classes\FileUploadHandler
	 */
	private function getUploader($id = null)
	{
		// 如果是修改文章的时候
		if (strpos($id, 'temp') !== 0) {
			$post = $this->em->getRepository('GospelCoreBundle:Post')->find($id);
			$path = $post->getCreateAt()->format("Ym") . DIRECTORY_SEPARATOR . $id ;
		} else {
			$path = $id;
		}
		$options = array(
            'script_url' => '/gospel-admin/post/file-upload/'.$id,
			'path' => Post::getUploadImagePath().$path.'/',
		);
		return new FileUploadHandler($options);
	}

	public function skiptestUpdateProvince()
	{
		global $argv, $argc;
		// use the -d parameter
		// phpunit -d 3000  -c app /var/www/christiantimes.dev/src/Gospel/Bundle/CoreBundle/Tests
		$startId = (int) $argv[2];
		$total = (int) $argv[4];
		$startFrom = $startId;
		$inteval = 1;
		$endId = $total + $startFrom;
		for ($start = $startFrom; $start <= $endId; $start += $inteval) {
			$this->updateProvinceAction($start, $inteval);
		}
	}

	/**
	 * 更新文章的地区分类
	 * @param type $start
	 * @param type $inteval
	 */
	public function updateProvinceAction($start, $total)
	{
		$repositoryPost = $this->getEm()->getRepository('GospelCoreBundle:Post');
		$repositoryCategoryOther = $this->getEm()->getRepository('GospelCoreBundle:Categoryother');
		$repositoryRegion = $this->getEm()->getRepository('GospelCoreBundle:AreaCategory');
		$repositoryProvince = $this->getEm()->getRepository('GospelCoreBundle:Area');
		$result = $repositoryPost->findAllOld($this->emCustom, $total, $start);

		// 
		foreach ($result as $row) {
			$oldRegionId = $row['area_id'];
			$oldProvinceSlug = $row['area_id2'];
			if ($oldRegionId || $oldProvinceSlug) {
				$post = $repositoryPost->find($row['id']);
				if ($post instanceof Post) {
					if ($oldRegionId) {
						$region = $repositoryRegion->find($oldRegionId);
						if (is_object($region)) {
							$post->setRegion($region);
						}
					}
					if (trim($oldProvinceSlug)) {
						$province = $repositoryProvince->findOneBy(array(
							'slug' => $oldProvinceSlug
						));
						if (is_object($province)) {
							$post->setArea($province);
						}
					}
					$this->em->persist($post);
					$this->em->flush();
				}

			}
		}
	}


	public function skiptestUpdateAuthor()
	{
		//TODO
		// 1. find out all the authors
		// 2. search all the posts for each author name
		// 3. update and save the post author field and unset the writer field
		$repositoryPost = $this->getEm()->getRepository('GospelCoreBundle:Post');
		$repositoryAuthor = $this->getEm()->getRepository('GospelCoreBundle:Author');
		$arthors = $repositoryAuthor->findAll();
		foreach ($arthors as $author) {
			$posts = $repositoryPost->findByWriter($author->getTitle());
			foreach ($posts as $post) {
				if (!$post->setAuthor()) {
					echo $post->getId() . ': ' . $post->getTitle()."\n";
					$post->setWriter('');
					$this->em->persist($post);
					$this->em->flush();
				}
			}
		}
	}

	/**
	 * 更新文章的内容里面多余的空格
	 * @global \Gospel\Bundle\AlipayBundle\Tests\Controller\type $argv
	 * @global \Gospel\Bundle\AlipayBundle\Tests\Controller\type $argc
	 */
	public function updateContent()
	{
		global $argv, $argc;
		// use the -d parameter
		// phpunit -d 3000  -c app /var/www/christiantimes.dev/src/Gospel/Bundle/CoreBundle/Tests
		$startId = (int) $argv[2];
		$total = (int) $argv[4];
		$startFrom = $startId;
		$repositoryPost = $this->getEm()->getRepository('GospelCoreBundle:Post');
		$posts = $repositoryPost->findBy(array(), array('id' => 'DESC'), $total, $startFrom);
		foreach ($posts as $post) {
			echo $post->getId() . ": " . $post->getTitle()."\n";
			$content = $post->getContent();
			$content_new = str_replace(array(
				'<o:p></o:p>',
				'<p>&nbsp;</p>',
				'<p><o:p></o:p></p>',
				'<o:p></o:p><br />',
				'<o:p></o:p><br/>',
				'<o:p></o:p>',
				'<p></p>',
			), '', $content);
//			echo $content_new. "\n";
			$post->setContent($content_new);
			$this->em->persist($post);
			$this->em->flush();
		}
	}

	protected function tearDown()
	{
		parent::tearDown();
		$this->getEm()->close();
	}

}
