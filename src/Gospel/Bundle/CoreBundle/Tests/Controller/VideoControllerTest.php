<?php

/*
 * 视频相关功能测试
 */
namespace Gospel\Bundle\CoreBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Gospel\Bundle\CoreBundle\Entity\Video;
use Symfony\Component\Filesystem\Filesystem;


/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class VideoControllerTest extends WebTestCase
{
	/**
	 * The entity manager
	 * @var type 
	 */
	private $em;

	private $emOld;

	public function setUp()
	{
		static::$kernel = static::createKernel();
		static::$kernel->boot();
		$this->em = static::$kernel->getContainer()
				->get('doctrine')
				->getManager()
				;
		$this->emOld = static::$kernel->getContainer()
				->get('doctrine')
				->getManager('customer')
				;
	}

	/**
	 * 
	 * @return \Doctrine\ORM\EntityManager
	 */
	public function getEm()
	{
		return $this->em;
	}

	/**
	 * 
	 * @return \Doctrine\ORM\EntityManager
	 */
	public function getEmOld()
	{
		return $this->emOld;
	}

	public function testUpdateVideos()
	{
		$this->markTestSkipped("Not updated the video cover");
		$repo = $this->getEm()->getRepository('GospelCoreBundle:Video');
		$repositoryUser = $this->getEm()->getRepository('GospelUserBundle:User');
		$user = $repositoryUser->find(1);
		$repoVideoCategory = $this->getEm()->getRepository('GospelCoreBundle:CategoryVideo');
		$videosOld = $repo->findAllOld($this->getEmOld(), 10000);
		$fs = new Filesystem();
		foreach ($videosOld as $row) {
			// check if the video exists in the new database, if not exists, create that
			$video = $repo->find($row['id']);
			if (!$video) { // create new video
				$video = new Video($row['id']);
				$video->setId($row['id']);
				$video->setAuthor($user);
				$video->setTitle($row['title']);
				$video->setHits($row['hits']);
				$video->setCategory($repoVideoCategory->find($row['cat_id']));

				$date = new \DateTime("@".$row['created']);
				$video->setCreateAt($date);
				$video->setPublishedAt($date);
				$video->setUpdatedAt($date);
				$video->setContent($row['intro']);
				$video->setEmbedCode($row['flash_code']);
				$video->setCommentStatus(1);
				$video->setStatus(1);

				$this->getEm()->persist($video);
				$this->getEm()->flush();
				echo "create video: ".$video->getTitle()."\n";
			} else {
				if (!$video->getCover()) {
					$video->setCoverPath(basename($row['pic']));
					$this->getEm()->persist($video);
					$this->getEm()->flush();
					echo "create video cover : ".$video->getTitle()."\n";
				}
			}

			// get the old image, if the image exists
			// then the new image shold be exists
			// if not exists then copy to the new location
			// then the new image should exists
			$oldImage = '/home/wwwroot/christiantimes/public_html/upload/'.$row['pic'];
			if (file_exists($oldImage) && is_file($oldImage)) {
				$newImageFile = '/data/www/christiantimes.cn/web/assets/media/video/cover/' . basename($row['pic']);
				if (!file_exists($newImageFile)) {
					$fs->mkdir(dirname($newImageFile));
					$fs->copy($oldImage, $newImageFile);
				}
			}
		}
	}

	protected function tearDown()
	{
		parent::tearDown();
		$this->getEm()->close();
	}

}
