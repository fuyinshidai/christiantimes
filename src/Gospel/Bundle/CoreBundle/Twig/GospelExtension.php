<?php

namespace Gospel\Bundle\CoreBundle\Twig;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */

class GospelExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('slugfy', array($this, 'slugfyFilter')),
        );
    }

	/**
     * Return the functions registered as twig extensions
     * 
     * @return array
     */
    public function getFunctions()
    {
        return array(
            'file_exists' => new \Twig_Function_Function('file_exists'),
        );
    }

    static public function slugfyFilter($text, $remove = array('/'))
    {
		$text = preg_replace('!\s+!', '-', $text);
		return str_replace($remove, '.', $text);
    }

    public function getName()
    {
        return 'gospel_extension';
    }
}