<?php

/*
 * MagazineController.php encoding=UTF-8
 *
 * Created on Jan 27, 2015, 11:07:59 AM
 *
 * gospeltimes.cn default 
 * Copyright(c) Beijing Gospeltimes Information Technology, Inc.  All Rights Reserved.
 *
 */

namespace Gospel\Bundle\CoreBundle\Controller;

use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Gospel\Bundle\CoreBundle\Entity\Magazine;
use Gospel\Bundle\CoreBundle\Entity\MagazineImage;
use Gospel\Bundle\CoreBundle\Classes\FileUploadHandler;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class MagazineController extends ResourceController
{
    /**
     * Create new resource or just display the form.
     */
    public function createAction(Request $request)
    {
        $config = $this->getConfiguration();
		// 用来上传图片的临时id，用来创建临时文件夹
		$temp_id = 'temp-'.md5(mt_rand(1, time()));

        $resource = $this->createNew();
        $form = $this->getForm($resource);

        if (($request->isMethod('POST') || $request->isMethod('PUT') ) && $form->bind($request)->isValid()) {
            $this->create($resource);

			// 更新文章的图片
			$this->updateImages($resource);

            $this->setFlash('success', 'create');

            return $this->redirectTo($resource);
        }

        if ($config->isApiRequest()) {
            return $this->handleView($this->view($form));
        }

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('create.html'))
            ->setData(array(
                $config->getResourceName() => $resource,
                'form'                     => $form->createView(),
                'temp_id'                     => $temp_id,
            ))
        ;

        return $this->handleView($view);
    }

    /**
     * Display the form for editing or update the resource.
     */
    public function updateAction(Request $request)
    {
        $config = $this->getConfiguration();

        $resource = $this->findOr404();
        $form = $this->getForm($resource);

        if (($request->isMethod('PUT') || $request->isMethod('POST')) && $form->bind($request)->isValid()) {
            $this->update($resource);
            $this->setFlash('success', 'update');

            return $this->redirectTo($resource);
        }

        if ($config->isApiRequest()) {
            return $this->handleView($this->view($form));
        }

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('update.html'))
            ->setData(array(
                $config->getResourceName() => $resource,
                'form'                     => $form->createView(),
                'temp_id'                     => $resource->getId(),
            ))
        ;

        return $this->handleView($view);
    }

	/**
	 * 处理图片批量上传的方法，包括图片的添加，删除
	 * path: gospel_magazine_image_upload
	 * path 必须以'/'结尾
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function FileUploadAction(Request $request)
	{
		// 当前图片所属的文章的id
		$id = $request->get('id', null);

		// 读取上传类
		$upload = $this->getUploader($id);

		$repo = $this->getDoctrine()->getRepository('GospelCoreBundle:MagazineImage');
		$repoMagazine = $this->getDoctrine()->getRepository('GospelCoreBundle:Magazine');
		$em = $this->getDoctrine()->getManager();


		// 当id为空的时候，为添加新的post的情况
		// 该临时id用来临时上传文件使用，当文件上传结束之后
		// 需要
		// 1. 把该文件夹里面的文件移动到相对应的post的文件夹里面
		// 2. 把该文件夹下面的文件写入到数据库
		// 3. 删除该文件夹
		$session = $this->getRequest()->getSession();
//		$logger = $this->get('logger');
		switch($request->getMethod()) {
			case 'POST':
				$files = $upload->postFiles;
				// 如果是添加文章的时候
				if (strpos($id, 'temp') === 0) {
					$session->set('tempId', $id);
					// 更新session数据
					$temp_data = $session->get('tempUpload', array());
					foreach($files as $file) {
						if ($file->name) {
							$temp_data[$file->name] = $file;
						}
					}
					$session->set('tempUpload', $temp_data);
				} else {
					// 修改文章的时候
					$imageE = $repo->findBy(array('magazine' => $repoMagazine->find($id)));
					// if image not exits then remove it
					foreach ($imageE as $row) {
						if (!$row->isImageExists()) {
							$em->remove($row);
							$em->flush();
						}
					}
					foreach($files as $file) {
						if ($file->name) {
							$image = new MagazineImage();
							$image->setName($file->name);
							$image->setMagazine($repoMagazine->find($id));
							$em->persist($image);
							$this->getConfiguration();
							$em->flush();
						}
					}
				}
				break;
		
			case 'DELETE':	// 删除文件的时候同步更新文件
				$files = $upload->getDeleteData();

				// 如果是添加文章的时候上传文件，把文件信息保存在session里面
				if (strpos($id, 'temp') === 0) {
					$temp_data = $session->get('tempUpload', array());
					foreach($files as $file) {
						unset($temp_data[$file]);
					}
					$session->set('tempUpload', $temp_data);

				} else { // 如果是修改文章的时候需要动态更新数据库的
					foreach($files as $file) {
						$imageE = $repo->findBy(array('magazine' => $repoMagazine->find($id)));
						foreach($imageE as $entity) {
							if($entity->getName() == $file) {
								$em->remove($entity);
								$this->getConfiguration();
								$em->flush();
							}
						}
					}
				}
				break;
			case 'GET':
				// 如果是添加文章的时候
				if (strpos($id, 'temp') === 0) {
				} else {//这个是在修改文章的情况下执行的代码
					$magazine = $repoMagazine->find($id);
					$imagesInDb = $repo->findBy(array('magazine' => $magazine));
					$images = $upload->get();
					$return = array('files' => array());
					foreach($images['files'] as $image) {
						$return['files'][] = $image;
					}
					$upload->generate_response($return);
				}
			default:
				;
		}


		return new Response('');
	}

	/**
	 * 
	 * @param type $id
	 * @return \Gospel\Bundle\CoreBundle\Classes\FileUploadHandler
	 */
	private function getUploader($id = null)
	{
		$path = $id;
		$options = array(
            'script_url' => $this->generateUrl('gospel_magazine_image_upload', array('id'=>$id)),
			'path' => Magazine::getWebPath() . DIRECTORY_SEPARATOR . $path.'/',
		);
		return new FileUploadHandler($options);
	}

	/**
	 * 在添加杂志的时候更新图片的内容
	 * 目的是保存临时文件夹里面的文件到目标文件夹
	 * 同时清理临时数据
	 */
	private function updateImages($resource)
	{
		$session = $this->getRequest()->getSession();
		$tempIdInSession = $session->get('tempId', null);
		// 保存图片到数据库
		if($tempIdInSession) {
			$temp_data = $session->get('tempUpload', array());
			foreach($temp_data as $file) {
				$image = new MagazineImage();
				$image->setName($file->name);
				$image->setMagazine($resource);
				$this->getDoctrine()->getManager()->persist($image);
			}
			$this->getConfiguration();
			$this->getDoctrine()->getManager()->flush();

			// 重命名文件加到对应的文件下面
			$uploader = $this->getUploader($session->get('tempId'));
			$options = $uploader->getOptions();
			$dir = $options['upload_dir'].DIRECTORY_SEPARATOR.$resource->getId();
			$temp_path = dirname($dir);
			$targetDir = dirname(dirname($dir)).DIRECTORY_SEPARATOR.$resource->getId();
			if(!is_dir($targetDir)) {
				$fs = new Filesystem();
				$fs->mkdir($targetDir);

			}
			rename($temp_path, $targetDir);

			// 清除所有的临时文件夹
			$fs = new Filesystem();
			$finder = new Finder();
			$finder->directories()->depth('== 1')->name('/temp.*/')->in(dirname(dirname($dir)));
			foreach ($finder as $file) {
				$fs->remove($file);
			}

		}
		// 清理临时id
		$session->set('tempId', null);
		$session->set('tempUpload', null);
	}

    /**
     * Delete resource.
     */
    public function deleteAction()
    {
        $resource = $this->findOr404();
		$em = $this->getDoctrine()->getManager();
		foreach($resource->getImages() as $image) {
			$em->remove($image);
			$this->getConfiguration();
			$em->flush();
		}

		// 清除所有的临时文件夹
		$uploader = $this->getUploader($resource->getId());
		$options = $uploader->getOptions();

		// 取得上传文件夹
		$dir = $options['upload_dir'].DIRECTORY_SEPARATOR.$resource->getId();

		// 如果存在图片文件路径，则删除该文件夹
		if(is_dir($dir)) {
			$fs = new Filesystem();
			$finder = new Finder();
			$finder->files()->in($dir);
			foreach ($finder as $file) {
				$fs->remove($file);
			}
			rmdir($dir);
		}

        $this->delete($resource);
        $this->setFlash('success', 'delete');

        return $this->redirectToIndex($resource);
    }
}
