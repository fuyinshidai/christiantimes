<?php

namespace Gospel\Bundle\CoreBundle\Controller;

use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Gospel\Bundle\CoreBundle\Entity\CategoryTopic;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class CategoryTopicController extends ResourceController
{
    /**
     * Get collection (paginated by default) of resources.
     */
    public function indexAction(Request $request)
    {
        $config = $this->getConfiguration();

        $criteria = $config->getCriteria();
        $sorting = $config->getSorting();

        $pluralName = $config->getPluralResourceName();
		$repository = $this->getManager()->getRepository('GospelCoreBundle:CategoryTopic');
		$arrayTree = $repository->childrenHierarchy();

        if ($config->isPaginated()) {
            $resources = $this
                ->getResourceResolver()
                ->getResource($repository, $config, 'createPaginator', array($criteria, $sorting))
            ;

            $resources
                ->setCurrentPage($request->get('page', 1), true, true)
                ->setMaxPerPage($config->getPaginationMaxPerPage())
            ;
        } else {
            $resources = $this
                ->getResourceResolver()
                ->getResource($repository, $config, 'findBy', array($criteria, $sorting, $config->getLimit()))
            ;
        }

		$category = $repository->createNew();
        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('index.html'))
            ->setTemplateVar($pluralName)
            ->setData(array('categories'=>$resources,'category'=>$repository->createNew(),'tree'=>$arrayTree))
        ;

        return $this->handleView($view);
    }

    /**
     * Create new resource or just display the form.
     */
    public function createAction(Request $request)
    {
		$repository = $this->getManager()->getRepository('GospelCoreBundle:CategoryTopic');
        $config = $this->getConfiguration();
		$parent_id = $request->get('parent', null);
		$selected = null;
		if($parent_id) {
			$selected = $repository->find($parent_id);
			
		}

        $resource = $this->createNew();
        $form = $this->createForm($this->getConfiguration()->getFormType(), $resource, array('selected'=>$selected));

        if (($request->isMethod('PUT')  || $request->isMethod('POST') )&& $form->bind($request)->isValid()) {
            $this->create($resource);
            $this->setFlash('success', 'create');

            return $this->redirect($this->generateUrl('gospel_topic_category_index'));
        }

        if ($config->isApiRequest()) {
            return $this->handleView($this->view($form));
        }
		$arrayTree = $repository->childrenHierarchy();

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('create.html'))
            ->setData(array(
                $config->getResourceName() => $resource,
                'form'                     => $form->createView()
            ))
        ;

        return $this->handleView($view);
    }

    /**
     * Display the form for editing or update the resource.
     */
    public function updateAction(Request $request)
    {
        $config = $this->getConfiguration();

        $resource = $this->findOr404();
        $form = $this->getForm($resource);

        if (($request->isMethod('PUT') || $request->isMethod('POST')) && $form->bind($request)->isValid()) {
			if($form->getData()->getDeleteImage() && $form->getData()->getFile() === null) {
				$resource->removeUpload();
				$resource->setPath('');
			}
            $this->update($resource);
            $this->setFlash('success', 'update');

			return $this->redirect($this->generateUrl('gospel_topic_category_index'));
        }

        if ($config->isApiRequest()) {
            return $this->handleView($this->view($form));
        }

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('update.html'))
            ->setData(array(
                $config->getResourceName() => $resource,
                'form'                     => $form->createView()
            ))
        ;

        return $this->handleView($view);
    }



	/**
	 * 导入分类数据
	 */
	public function loadAction(Request $request)
	{
		// 
		$arrTitle = array(1=>'编辑', '专栏作家', '观点人士', '学者', '特约通讯员', '特约撰稿人', '评论嘉宾');
		$em = $this->getManager();
		$repo = $this->getRepository('GospelCoreBundle:CategoryTopic');
		foreach ($arrTitle as $name) {
			// check exists
			if ($repo->findOneBy(array('title' => $name))) {
				continue;
			} else {
				$categoryTopic = new CategoryTopic();
				$categoryTopic->setTitle($name);
				$categoryTopic->setParent(null);
				$categoryTopic->setDescription($name);
				$categoryTopic->setSortOrder(1);
				$categoryTopic->setSlug($name);
				$categoryTopic->setIsActive(1);
				$em->persist($categoryTopic);
				$em->flush();
			}
		}

		return new Response('Loading topic category... success');
	}

	public function saveRow($row, $parent)
	{
		$repository = $this->getManager()->getRepository('GospelCoreBundle:CategoryTopic');
		$resource = $this->createNew();
		$resource->setTitle($row['name']);
		$resource->setDescription($row['intro']?$row['intro']:'');
		$resource->setSortOrder($row['sequence']);
		$resource->setSlug($row['serial_name']);
		$resource->setIsActive($row['is_show']);
		$resource->setParent($parent);
		$this->create($resource);
		return $resource;
	}

	public function deleteAction() {
		$resource = $this->findOr404();

		$entities = $this->getDoctrine()->getRepository('GospelCoreBundle:Topic')->findBy(array(
			'category' => $resource,
		));

		if (count($entities)) {
			$msg = '';
			$msg .= "该分两类包含文章，不能直接删除，请首先删除以下图片集或者将以下图片集分类改为非<strong>".$resource->getTitle()."</strong>分类:<br/>";
			foreach ($entities as $entity) {
				$msg .= "id:" . $entity->getId() . " - <a target='_blank' href='".$this->generateUrl('gospel_post_show', array('id'=>$entity->getId()))."'>" . $entity->getTitle()."</a><br/>";
			}

			$this
            ->get('session')
            ->getFlashBag()
            ->add('error', $msg)
        	;
		} else {
			$this->delete($resource);
			$this->setFlash('success', 'delete');
		}

		return $this->redirect($this->generateUrl('gospel_category_other_index'));
	}
}
