<?php

namespace Gospel\Bundle\CoreBundle\Controller;

use Sylius\Bundle\ResourceBundle\Controller\ResourceController;

/**
 * 图片上传使用session变量tempUpload
 * 	初始化的时候清除session变量
 * 	
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class BackendController extends ResourceController
{
	/**
	 * 读取当前登录的用户
	 * @return User Object
	 */
	public function getCurrentUser()
	{
		return $this->get('security.context')->getToken()->getUser();
	}

	/**
	 * 读取当前登录的用户所拥有的role
	 * @return type array(ROLE1, ROLE2)
	 */
	public function getRoles()
	{
		return $this->getCurrentUser()->getRoles();
	}

	/**
	 * 查看当前登录用户是否拥有指定的role
	 * @param type $role
	 * @return boolean true or false
	 */
	public function hasRole($role)
	{
		return in_array($role, $this->getRoles());
	}

	/**
	 * 根据当前的用户组设置用户的筛选条件：
	 * 1. 如果是只能查看自己发布的文章，则添加筛选自己的发布文章选项
	 * 2. 如果是地区版的管理员则只显示该地区的相关内容
	 * @param array $criteria
	 * @return array $criteria
	 */
	public function setCriteriaByRole($criteria)
	{
		// 如果是只能显示自己的文件列表的权限,读取该用户的文件
		if ($this->hasRole('ROLE_POST_LIST_OWN')) {
			$criteria['user'] = $this->getCurrentUser()->getId();
		}

		// 如果是上海版的用户列表
		if ($this->hasRole('ROLE_LOCAL_ADMIN_SHANGHAI')) {
			$criteria['area'] = $this->getDoctrine()->getManager()
					->getRepository('GospelCoreBundle:Area')->findOneBy(array('title'=>'上海'));
		}
		return $criteria;
	}

	/**
	 * 生成表单的时候设置额外的相关的选项
	 * @param array $options
	 * @return array $options
	 */
	public function setOptionsByRole($options)
	{
		// 如果是上海版的用户列表
		if ($this->hasRole('ROLE_LOCAL_ADMIN_SHANGHAI')) {
			$options['attr']['defaultArea'] = $this->getDoctrine()->getManager()
					->getRepository('GospelCoreBundle:Area')->findOneBy(array('title'=>'上海'));
		}

		// 设置用户所拥有的用户组
		return $options;
	}
}
