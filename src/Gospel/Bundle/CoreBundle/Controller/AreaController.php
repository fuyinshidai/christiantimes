<?php

namespace Gospel\Bundle\CoreBundle\Controller;

use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Symfony\Component\HttpFoundation\Request;
use Gospel\Bundle\CoreBundle\Entity\AreaCategory;
use Gospel\Bundle\CoreBundle\Entity\Area;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class AreaController extends ResourceController
{

	/**
	 * Get collection (paginated by default) of resources.
	 */
	public function indexAction(Request $request)
	{
		$config = $this->getConfiguration();

		$criteria = $config->getCriteria();
		$sorting = $config->getSorting();

		$pluralName = $config->getPluralResourceName();
		$repository = $this->getManager()->getRepository('GospelCoreBundle:Area');
		$arrayTree = $repository->childrenHierarchy();

		if ($config->isPaginated()) {
			$resources = $this
					->getResourceResolver()
					->getResource($repository, $config, 'createPaginator', array($criteria, $sorting))
			;

			$resources
					->setCurrentPage($request->get('page', 1), true, true)
					->setMaxPerPage($config->getPaginationMaxPerPage())
			;
		} else {
			$resources = $this
					->getResourceResolver()
					->getResource($repository, $config, 'findBy', array($criteria, $sorting, $config->getLimit()))
			;
		}
		$viewType = '';
		if ($request->isXmlHttpRequest()) {
			$viewType = 'ajax.';
		}

		$view = $this
				->view()
				->setTemplate($config->getTemplate('index.html'))
				->setTemplateVar($pluralName)
				->setData(array('categories' => $resources, 'area' => $repository->createNew(), 'tree' => $arrayTree, 'viewType' => $viewType))
		;

		return $this->handleView($view);
	}

	/**
	 * Create new resource or just display the form.
	 */
	public function createAction(Request $request)
	{
		$repository = $this->getManager()->getRepository('GospelCoreBundle:Area');
		$config = $this->getConfiguration();
		$parent_id = $request->get('parent', null);
		$selected = null;
		if ($parent_id) {
			$selected = $repository->find($parent_id);
		}

		$resource = $this->createNew();
		$form = $this->createForm($this->getConfiguration()->getFormType(), $resource, array('selected' => $selected));

		if ($request->isMethod('PUT') && $form->bind($request)->isValid()) {
			$this->create($resource);
			$this->setFlash('success', 'create');

			return $this->redirect($this->generateUrl('gospel_area_index'));
		}

		if ($config->isApiRequest()) {
			return $this->handleView($this->view($form));
		}

		$view = $this
				->view()
				->setTemplate($config->getTemplate('create.html'))
				->setData(array(
			$config->getResourceName() => $resource,
			'form' => $form->createView()
				))
		;

		return $this->handleView($view);
	}

	/**
	 * Load data form the old database table
	 * @return type
	 */
	public function loadAction(Request $request)
	{
        $config = $this->getConfiguration();
		// 旧的地区数据：
		$arrArea = array(1=>'美洲', '亚太', '欧洲', '非洲', '中东', '东北', '西北', '华东', '中南', '华北', '西南', '港澳台', '无地区');
		$arrArea2 = array(
			6 => array('heilongjiang' => '黑龙江', 'jilin' => '吉林', 'liaoning' => '辽宁'), //东北
			7 => array('shanxi' => '陕西省', 'gansu' => '甘肃省', 'qinghai' => '青海省', 'ningxia' => '宁夏回族自治区', 'xinjiang' => '新疆维吾尔族自治区'), //西北
			8 => array('shanghai' => '上海市', 'jiangsu' => '江苏省', 'zhejiang' => '浙江省', 'anhui' => '安徽省', 'fujian' => '福建省', 'jiangxi' => '江西省', 'shandong' => '山东省'), //华东
			9 => array('henan' => '河南', 'hubei' => '湖北', 'hunan' => '湖南', 'guangdong' => '广东省', 'hainan' => '海南省', 'guangxi' => '广西壮族自治区'), //中南
			10 => array('beijing' => '北京市', 'tianjin' => '天津市', 'hebei' => '河北省', 'shanxi' => '山西省', 'neimenggu' => '内蒙古自治区'), //华北
			11 => array('chongqing' => '重庆市', 'sichuan' => '四川省', 'guizhou' => '贵州省', 'yunnan' => '云南省', 'xizang' => '西藏自治区')//西南
		);

		$arr_area_one = array();

		// 导入一级地区分类
		$em = $this->getDoctrine()->getManager();
		$repoAreaCategory = $em->getRepository('GospelCoreBundle:AreaCategory');
		foreach ($arrArea as $key => $row) {
			// 检查时候已经保存了该记录，如果保存则继续，如果没有保存则保存
			if ($repoAreaCategory->find($key)){
				continue;
			} else {
				$areaCategory = new AreaCategory();
				$areaCategory->setTitle($row);
				$areaCategory->setSortOrder(1);
				$em->persist($areaCategory);
				$this->getConfiguration();
				$em->flush();
			}
			
		}

		// 导入二级分类
		$repoArea = $em->getRepository('GospelCoreBundle:Area');
		foreach ($arrArea2 as $key => $rows) {
			// 读取地区分类
			$areaCategory = $repoAreaCategory->find($key);
			foreach ($rows as $slug => $row) {
				$area = new Area;
				$area->setParent(null);
				$area->setTitle($row);
				if ($areaCategory) {
					$area->setCategory($areaCategory);
				}
				$area->setSortOrder(1);
				$area->setSlug($slug);
				$area->setDescription($row.'-'.$slug);
				$area->setIsActive(1);
				$em->persist($area);
				$this->getConfiguration();
				$em->flush();
			}
		}


		foreach ($arrArea2 as $row) {
			foreach ($row as $key => $val) {
				$arr_area_one[$key] = $val;
			}
		}
		$view = $this
				->view()
				->setTemplate($config->getTemplate('load.html'))
				->setData(array(
			'data' => 'loading area...',
			'msg' => 'success',
				))
		;
		return $this->handleView($view);
	}

	public function deleteAction() {
		$resource = $this->findOr404();

		$entities = $this->getDoctrine()->getRepository('GospelCoreBundle:Post')->findBy(array(
			'area' => $resource,
		));

		if (count($entities)) {
			$msg = '';
			$msg .= "该分两类包含文章，不能直接删除，请首先删除以下图片集或者将以下图片集分类改为非<strong>".$resource->getTitle()."</strong>分类:<br/>";
			foreach ($entities as $entity) {
				$msg .= "id:" . $entity->getId() . " - <a target='_blank' href='".$this->generateUrl('gospel_post_show', array('id'=>$entity->getId()))."'>" . $entity->getTitle()."</a><br/>";
			}

			$this
            ->get('session')
            ->getFlashBag()
            ->add('error', $msg)
        	;
		} else {
			$this->delete($resource);
			$this->setFlash('success', 'delete');
		}

		return $this->redirect($this->generateUrl('gospel_category_other_index'));
	}
}
