<?php

namespace Gospel\Bundle\CoreBundle\Controller;

use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Symfony\Component\HttpFoundation\Request;
use Gospel\Bundle\CoreBundle\Entity\CategoryOther;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class CategoryController extends ResourceController
{
    /**
     * Get collection (paginated by default) of resources.
     */
    public function indexAction(Request $request)
    {
        $config = $this->getConfiguration();

        $criteria = $config->getCriteria();
        $sorting = $config->getSorting();

        $pluralName = $config->getPluralResourceName();
		$repository = $this->getManager()->getRepository('GospelCoreBundle:Category');
		$arrayTree = $repository->childrenHierarchy();

        if ($config->isPaginated()) {
            $resources = $this
                ->getResourceResolver()
                ->getResource($repository, $config, 'createPaginator', array($criteria, $sorting))
            ;

            $resources
                ->setCurrentPage($request->get('page', 1), true, true)
                ->setMaxPerPage($config->getPaginationMaxPerPage())
            ;
        } else {
            $resources = $this
                ->getResourceResolver()
                ->getResource($repository, $config, 'findBy', array($criteria, $sorting, $config->getLimit()))
            ;
        }

		$category = $repository->createNew();
        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('index.html'))
            ->setTemplateVar($pluralName)
            ->setData(array('categories'=>$resources,'category'=>$repository->createNew(),'tree'=>$arrayTree))
        ;

        return $this->handleView($view);
    }

    /**
     * Create new resource or just display the form.
     */
    public function createAction(Request $request)
    {
		$repository = $this->getManager()->getRepository('GospelCoreBundle:Category');
        $config = $this->getConfiguration();
		$parent_id = $request->get('parent', null);
		$selected = null;
		if($parent_id) {
			$selected = $repository->find($parent_id);
			
		}

        $resource = $this->createNew();
        $form = $this->createForm($this->getConfiguration()->getFormType(), $resource, array('selected'=>$selected));

        if ($request->isMethod('POST') && $form->bind($request)->isValid()) {
            $this->create($resource);
            $this->setFlash('success', 'create');

            return $this->redirect($this->generateUrl('gospel_category_index'));
        }

        if ($config->isApiRequest()) {
            return $this->handleView($this->view($form));
        }
		$arrayTree = $repository->childrenHierarchy();

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('create.html'))
            ->setData(array(
                $config->getResourceName() => $resource,
                'form'                     => $form->createView()
            ))
        ;

        return $this->handleView($view);
    }

    /**
     * Display the form for editing or update the resource.
     */
    public function updateAction(Request $request)
    {
        $config = $this->getConfiguration();

        $resource = $this->findOr404();
        $form = $this->getForm($resource);

        if (($request->isMethod('PUT') || $request->isMethod('POST')) && $form->bind($request)->isValid()) {
			if($form->getData()->getDeleteImage() && $form->getData()->getFile() === null) {
				$resource->removeUpload();
				$resource->setPath('');
			}
            $this->update($resource);
            $this->setFlash('success', 'update');

            return $this->redirectTo($resource);
        }

        if ($config->isApiRequest()) {
            return $this->handleView($this->view($form));
        }

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('update.html'))
            ->setData(array(
                $config->getResourceName() => $resource,
                'form'                     => $form->createView()
            ))
        ;

        return $this->handleView($view);
    }

	/**
	 * Load data form the old database table
	 * @return type
	 */
	public function loadAction(Request $request)
	{
		$repository = $this->getManager()->getRepository('GospelCoreBundle:Category');
		$root = $repository->find(1);
        $config = $this->getConfiguration();
		$customerEm =  $this->get('doctrine')->getManager('customer');
		$result = $repository->getByParentId(0, $customerEm);
		$msg = "";
		foreach($result as $row) {
			$resource = $this->saveRow($row, null);
			if($resource) {
				$sub_result = $repository->getByParentId($row['id'], $customerEm);
				foreach($sub_result as $sub_row) {
					$sub_row['parent_id'] = $resource->getId();
					$this->saveRow($sub_row, $resource);
				}
			}

		}

		// save other category
		$result = $repository->getOtherByParentId(0, $customerEm);
		$msg = "";
		foreach($result as $row) {
			$resource = $this->saveOtherRow($row, null);
			if($resource) {
				$sub_result = $repository->getOtherByParentId($row['id'], $customerEm);
				foreach($sub_result as $sub_row) {
					$sub_row['parent_id'] = $resource->getId();
					$this->saveOtherRow($sub_row, $resource);
				}
			}

		}
		$view = $this
				->view()
				->setTemplate($config->getTemplate('load.html'))
				->setData(array(
					'data' => $result,
					'msg' => $msg,
				))
				;
		return $this->handleView($view);
	}

	public function saveRow($row, $parent)
	{
		$repository = $this->getManager()->getRepository('GospelCoreBundle:Category');
		if ($repository->findOneBy(array('title' => $row['name']))) {
			return;
		}
		$resource = $this->createNew();
		$resource->setTitle($row['name']);
		$resource->setDescription($row['intro']?$row['intro']:'');
		$resource->setSortOrder($row['sequence']);
		$resource->setSlug($row['serial_name']);
		$resource->setIsActive($row['is_show']);
		$resource->setParent($parent);
		$this->create($resource);
		return $resource;
	}

	public function saveOtherRow($row, $parent)
	{
		$repository = $this->getManager()->getRepository('GospelCoreBundle:CategoryOther');
		if ($repository->findOneBy(array('title' => $row['name']))) {
			return;
		}
		$resource = new CategoryOther;
		$resource->setTitle($row['name']);
		$resource->setDescription($row['intro']?$row['intro']:'');
		$resource->setSortOrder($row['sequence']);
		$resource->setSlug($row['serial_name']);
		$resource->setIsActive($row['is_show']);
		$resource->setParent($parent);
		$this->create($resource);
		return $resource;
	}

    /**
     * Delete resource.
     */
    public function deleteAction()
    {
        $resource = $this->findOr404();

		
		$posts = $this->getDoctrine()->getRepository('GospelCoreBundle:Post')->findBy(array(
			'category' => $resource,
		));

		if (count($posts)) {
			$msg = '';
			$msg .= "该分两类包含文章，不能直接删除，请首先删除以下文章或者将以下文章分类改为非<strong>".$resource->getTitle()."</strong>分类:<br/>";
			foreach ($posts as $post) {
				$msg .= $post->getTitle()."<br/>";
				$msg .= "id:" . $post->getId() . " - <a target='_blank' href='".$this->generateUrl('gospel_post_show', array('id'=>$post->getId()))."'>" . $post->getTitle()."</a><br/>";
			}

			$this
            ->get('session')
            ->getFlashBag()
            ->add('error', $msg)
        	;
		} else {
			$this->delete($resource);
			$this->setFlash('success', 'delete');
		}

        return $this->redirectToIndex($resource);
    }
}
