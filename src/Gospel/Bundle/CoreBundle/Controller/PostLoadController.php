<?php

namespace Gospel\Bundle\CoreBundle\Controller;

use Symfony\Component\HttpKernel\Controller;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Gospel\Bundle\CoreBundle\Form\PostType;
use Gospel\Bundle\CoreBundle\Entity\Tag;
use Gospel\Bundle\CoreBundle\Entity\Post;
use Gospel\Bundle\CoreBundle\Classes\FileUploadHandler;
use Gospel\Bundle\CoreBundle\Entity\PostImage;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Gospel\Bundle\CoreBundle\Entity\Author;

/**
 * 图片上传使用session变量tempUpload
 * 	初始化的时候清除session变量
 * 	
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class PostLoadController extends BackendController
{
	public function __construct()
	{
	}
	/**
	 * Load data form the old database table
	 * @return type
	 */
	public function loadAction(Request $request)
	{
		$data = array(
			'msg' => '',
		);

		$repositoryPost = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:Post');
		$repositoryPostImage = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:PostImage');
		$customerEm =  $this->get('doctrine')->getManager('customer');
		$result = $repositoryPost->findPic2Old($customerEm, 100000000);
		$em =  $this->get('doctrine')->getManager('default');

		foreach($result as $row) {
			$img = $row['pic2'];
			$resource = $repositoryPost->find($row['id']);
			$date = $resource->getCreateAt();
			$fs = new Filesystem();
			$postImage = $repositoryPostImage->findBy(array('name' => basename($img)));
			if (count($postImage) == 0) {
				$image = new PostImage();
				$image->setName(basename($img));
				$image->setPost($resource);
				$image->setSortOrder(1);
				$image->setCaption('');
				$image->setUseWatermark(1);
				$em->persist($image);
				$em->flush();

				// 移动图片到新的位置
				$oldImageFile = $this->getOldImageRoot() . '/' . $img;
				$newImageFoler = $this->getNewPostImageRoot() . '/'.$resource->getCreateAt()->format('Ym').'/'
						.$resource->getId().'/'
						;
				$newImageFile = $newImageFoler . basename($img);
				if (!file_exists($newImageFoler)) {
					$fs->mkdir($newImageFoler);
				}
				if(file_exists($oldImageFile)) {
					$fs->copy($oldImageFile, $newImageFile);
				}
				$fileUploader = $this->getUploader($resource->getId());
				$options = $fileUploader->options;
				$options['max_width'] = 80;
				$options['max_height'] = 80;
				$fileUploader->create_scaled_image(basename($img), 'thumbnail', $options);

				$data['msg'] .= "Update post : " . $resource->getId() . ": " . $resource->getTitle() . "<br/>";
			}
		}

		$view = $this
            ->view()
			->setTemplate('GospelCoreBundle:Post:fixMissingImages.html.twig')
			->setData($data)
        ;

        return $this->handleView($view);
	}

	/**
	 * 移动图片的时候旧的图片文件夹的地址
	 * @return string
	 */
	public function getOldImageRoot()
	{
		if ($_SERVER['SERVER_ADDR'] == '127.0.0.1') {
			return '/home/wwwroot/christiantimes/public_html/upload/';
//			return '/var/www/www.gospeltimes.cn/data/';
		} else {
			return '/home/wwwroot/christiantimes/public_html/upload/';
		}
	}

	/**
	 * 移动图片的时候新的图片文件夹的地址
	 * @return string
	 */
	public function getNewPostImageRoot()
	{
		if ($_SERVER['SERVER_ADDR'] == '127.0.0.1') {
			return '/data/www/christiantimes.cn/web/assets/media/post';
//			return '/var/www/gospeltimes.dev/symfony/web/assets/media/post';
		} else {
			return '/data/www/christiantimes.cn/web/assets/media/post/';
		}
	}

	/**
	 * 
	 * @param type $id
	 * @return \Gospel\Bundle\CoreBundle\Classes\FileUploadHandler
	 */
	private function getUploader($id = null)
	{
		// 如果是修改文章的时候
		if (strpos($id, 'temp') !== 0) {
			$post = $this->getDoctrine()->getRepository('GospelCoreBundle:Post')->find($id);
			$path = $post->getCreateAt()->format("Ym") . DIRECTORY_SEPARATOR . $id ;
		} else {
			$path = $id;
		}
		$options = array(
            'script_url' => $this->generateUrl('gospel_post_file_upload', array('id'=>$id)),
			'path' => Post::getUploadImagePath().$path.'/',
		);
		return new FileUploadHandler($options);
	}
}
