<?php

namespace Gospel\Bundle\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Gospel\Bundle\CoreBundle\Form\VideoType;
use Gospel\Bundle\CoreBundle\Entity\Videotag;
use Gospel\Bundle\CoreBundle\Entity\Video;
use Symfony\Component\Filesystem\Filesystem;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class VideoController extends ResourceController
{
    public function createAction(Request $request)
    {
        $config = $this->getConfiguration();

        $resource = $this->createNew();
		$form = $this->createForm(new VideoType(), $resource, array(
    		'em' => $this->getDoctrine()->getManager(),
			'category' => $this->makeTree($this->getCategoryTree(), array()),
			'createAt' => new \DateTime(),
			'entity' => $resource,
		));

		$errors = "";
        if (($request->isMethod('POST') || $request->isMethod('PUT') ) && $form->bind($request)->isValid()) {
			// 更新文章标签
			$this->updateTag($request, $resource);
			
			// 创建视频
            $this->create($resource);

			// 更新文章的图片
            $this->setFlash('success', 'create');
			$this->get('redis')->flushVideo();

            return $this->redirectTo($resource);
        }
        if (($request->isMethod('POST') || $request->isMethod('PUT') ) && !$form->bind($request)->isValid()) {
			$errors = $form->getErrorsAsString();
        }


        if ($config->isApiRequest()) {
            return $this->handleView($this->view($form));
        }

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('create.html'))
            ->setData(array(
                $config->getResourceName() => $resource,
                'form'                     => $form->createView(),
				// 传递到另外一个地方的表单
                'form2'                     => $form->createView(),
				'errors' => $errors,
            ))
        ;

        return $this->handleView($view);
    }

    public function updateAction(Request $request)
    {
        $config = $this->getConfiguration();

        $resource = $this->findOr404();
		$form = $this->createForm(new VideoType(), $resource, array(
    		'em' => $this->getDoctrine()->getManager(),
			'category' => $this->makeTree($this->getCategoryTree(), array()),
			'createAt' => $resource->getCreateAt(),
			'entity' => $resource,
		));

        if (($request->isMethod('PUT') || $request->isMethod('POST')) && $form->bind($request)->isValid()) {
			// 更新文章标签
			$this->updateTag($request, $resource);

			// 更新文章的相关文章
			$this->updateRelated($request, $resource);

            $this->update($resource);
            $this->setFlash('success', 'update');
			$this->get('redis')->flushVideo();

            return $this->redirectTo($resource);
        }

        if ($config->isApiRequest()) {
            return $this->handleView($this->view($form));
        }
		$errors = "";
        if (($request->isMethod('POST') || $request->isMethod('PUT') ) && !$form->bind($request)->isValid()) {
			$errors = $form->getErrorsAsString();
        }

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('update.html'))
            ->setData(array(
                $config->getResourceName() => $resource,
                'form'                     => $form->createView(),
                'form2'                     => $form->createView(),
                'temp_id'                     => $resource->getId(),
				'errors' => $errors,
            ))
        ;

        return $this->handleView($view);
    }

	/**
	 * 查看相关文章，可以根据不同的字段进行检索
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @return type
	 */
	public function relatedAction(Request $request)
	{
		$config = $this->getConfiguration();
        $criteria = $config->getCriteria();

		$keyword = $request->get('keywords', null);
		$gospel = $request->get('gospel_video', array());
		foreach($gospel as $k => $p) {
			$criteria[$k] = $p;
		}
		$type = $request->get('type', null);
		if($keyword) {
			$criteria[$type] = array('data'=>$keyword,'type'=>'like');
		}

        $sorting = $config->getSorting();

        $pluralName = $config->getPluralResourceName();
        $repository = $this->getRepository();

        if ($config->isPaginated()) {
            $resources = $this
                ->getResourceResolver()
                ->getResource($repository, $config, 'createPaginator', array($criteria, $sorting))
            ;

            $resources
                ->setCurrentPage($request->get('page', 1), true, true)
                ->setMaxPerPage($config->getPaginationMaxPerPage())
            ;
        } else {
            $resources = $this
                ->getResourceResolver()
                ->getResource($repository, $config, 'findBy', array($criteria, $sorting, $config->getLimit()))
            ;
        }

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('related.html'))
            ->setTemplateVar($pluralName)
            ->setData($resources)
        ;

        return $this->handleView($view);
	}

	public function makeTree($data, $arr) 
	{
		foreach($data as $row) {
			$arr[$row['id']] = str_repeat("——", $row['lvl']).$row['title'];
			if(is_array($row['__children']) && count($row['__children']) > 0) {
				$arr = $this->makeTree($row['__children'], $arr);
			}
		}
		return $arr;
	}

	private function getCategoryTree()
	{
		$repository = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:CategoryVideo');
		$arrayTree = $repository->childrenHierarchy();
		return $arrayTree;
	}

	/**
	 * Check if the tag already in the database, if not has, then create a new 
	 * tag and then add this tag to the new resource
	 * 
	 * @param type $request
	 * @param type $resource
	 */
	private function updateTag($request, $resource)
	{
		$gospel_video = $request->get('gospel_video');
		$tagsCommaString = $gospel_video['tagsCommaString'];
		if($tagsCommaString) {
			$repo = $this->getDoctrine()->getRepository('GospelCoreBundle:Videotag');
			$em = $this->getDoctrine()->getManager();
			foreach(explode(',', $tagsCommaString) as $tagName) {
				if(null === ($tag = $repo->findOneBy(array('name' => $tagName)))) {
					$tag = new Videotag();
					$tag->setName($tagName);
					$em->persist($tag);
					$this->getConfiguration();
					$em->flush();
				}
				$resource->addTag($tag);
			}
		}
		return $resource;
	}

	private function updateRelated($request, $resource)
	{
		$gospel_video = $request->get('gospel_video');
		preg_match_all("/\[(\d*)\]/", $gospel_video['relatedVideos'], $ids);
		$related = $ids[1];

		$resource->getRelated()->clear();
		if(count($related)>0) {
			foreach($related as $relate) {
				$resource->addRelated($this->getRepository()->find($relate));
			}
		}
		return $resource;
	}

	/**
	 * 将旧的视频数据库里面的数据导入到新的数据库里面
	 * @return type
	 */
	public function loadAction()
	{
		$data = array();
        $config = $this->getConfiguration();

		$repoVideo = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:Video');
		$repoVideoCategory = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:CategoryVideo');
		$repositoryUser = $this->getDoctrine()->getManager()->getRepository('GospelUserBundle:User');
		$oldEm = $this->get('doctrine')->getManager('customer');
		$em = $this->getDoctrine()->getManager();

		$oldVideos = $repoVideo->getOldVideos($oldEm);
		
		$user = $repositoryUser->find(1);
		foreach ($oldVideos as $row) {
			$video = $repoVideo->find($row['id']);
			if(!$video) { // 添加新的视频
				$video = new Video($row['id']);
				$video->setId($row['id']);
				$video->setAuthor($user);
				$video->setTitle($row['title']);
				$video->setHits($row['hits']);
				$video->setCategory($repoVideoCategory->find($row['cat_id']));

				$date = new \DateTime("@".$row['created']);
				$video->setCreateAt($date);
				$video->setPublishedAt($date);
				$video->setUpdatedAt($date);
				$video->setContent($row['intro']);
				$video->setEmbedCode($row['flash_code']);
				$video->setCommentStatus(1);
				$video->setStatus(1);

				$tagString = $row['tags'];
				$tagsData = array();
				$delimeters = array(',','，',' ','-','_','，',' ');
				foreach($delimeters as $del) {
					$tags_explode = explode($del, $tagString);
					$tagsData = array_merge($tagsData, $tags_explode);
				}
				$video = $this->updateTagString(implode(',', array_unique($tagsData)), $video);


				$em->persist($video);
				$em->flush();

			} 
		}


		$msg = 'loading videos done';

		$view = $this
				->view()
				->setTemplate($config->getTemplate('load.html'))
				->setData(array(
					'data' => $data,
					'msg' => $msg,
				))
				;
		return $this->handleView($view);
	}

	public function updateCategoryVideoAction()
	{
		$data = array();
        $config = $this->getConfiguration();

		$repoVideo = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:Video');
		$repoVideoCategory = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:CategoryVideo');
		$repositoryUser = $this->getDoctrine()->getManager()->getRepository('GospelUserBundle:User');
		$oldEm = $this->get('doctrine')->getManager('customer');
		$em = $this->getDoctrine()->getManager();

		$oldVideos = $repoVideo->getOldVideos($oldEm);
		
		$user = $repositoryUser->find(1);
		foreach ($oldVideos as $row) {
			$video = $repoVideo->find($row['id']);
			if($video) { // 更新现有视频
				// 更新视频分类
				$category = $repoVideoCategory->find($row['cat_id']);
				if (!$category) {
					$category = $repoVideoCategory->findOneBy(array('title' => '其他'));
				}
				$video->setCategory($category);
				// 更新相关视频
				// $this->setEntityFields($video, $row);
				$video->setUrl($row['source']);
				$video->setId($row['id']);

				// 更新图片
				$video->setCoverPath(basename($row['pic']));
				// 移动图片
				$pic = $row['pic'];
				$fs = new Filesystem();
				if($pic) {
					$oldImageFile = $this->getOldImageFolder($video) . '/' . basename($pic);
					$newImageFoler = $this->getNewImageRoot() . '/' 
					;
					$newImageFile = $newImageFoler . basename($pic);
					$fs->mkdir($newImageFoler);
					if (file_exists($oldImageFile)) {
						$fs->copy($oldImageFile, $newImageFile);
					}
				}

				$em->persist($video);
				$this->getConfiguration();
				$em->flush();
			}
		}


		$msg = 'loading videos done';

		$view = $this
				->view()
				->setTemplate($config->getTemplate('load.html'))
				->setData(array(
					'data' => $data,
					'msg' => $msg,
				))
				;
		return $this->handleView($view);

	}

	/**
	 * Check if the tag already in the database, if not has, then create a new 
	 * tag and then add this tag to the new resource
	 * 
	 * @param type $request
	 * @param type Post
	 */
	private function updateTagString($tagsCommaString, $resource)
	{
		if($tagsCommaString) {
			$repo = $this->getDoctrine()->getRepository('GospelCoreBundle:VideoTag');
			$em = $this->getDoctrine()->getManager();
			foreach(explode(',', $tagsCommaString) as $tagName) {
				if(null === ($tag = $repo->findOneBy(array('name' => $tagName)))) {
					$tag = new VideoTag();
					$tag->setName($tagName);
					$tag->setCount(1);
					$em->persist($tag);
					$this->getConfiguration();
					$em->flush();
				}
				$tags = $resource->getTags();
				if(!$tags->contains($tag)) {
					$resource->addTag($tag);
				}

				//增加tag的count
				$tag->setCount($tag->getCount()+1);
				$this->getDoctrine()->getManager()->persist($tag);
				$this->getConfiguration();
				$this->getDoctrine()->getManager()->flush();
			}
		}
		return $resource;
	}

	private function setEntityFields(Video $resource, $row)
	{
		// 设置相关文章
		foreach(explode(',', $row['relative_id']) as $relate) {
			$relatedPost = $this->getRepository()->find($relate);
			if($relatedPost) {
				$resource->addRelated($relatedPost);
			}
		}
		return $resource;
	}

	public function getOldImageFolder($entity)
	{
		
		if ($_SERVER['SERVER_ADDR'] == '127.0.0.1') {
			return '/var/www/www.christiantimes.cn/upload/video/'.$entity->getCreateAt()->format('Y-m').'/';
		} else {
			return '/data/www/gospeltimes.cn/upload/video/'.$entity->getCreateAt()->format('Y-m').'/';
		}
	}

	/**
	 * 移动图片的时候新的图片文件夹的地址
	 * @return string
	 */
	public function getNewImageRoot()
	{
		if ($_SERVER['SERVER_ADDR'] == '127.0.0.1') {
			return '/var/www/christiantimes.dev/web/assets/media/video/cover';
		} else {
			return '/data/www/christiantimes.cn/web/assets/media/video/cover';
		}
	}

    /**
     * Delete resource.
     */
    public function deleteAction()
    {
        $resource = $this->findOr404();

		
        $this->delete($resource);
        $this->setFlash('success', 'delete');
		$this->get('redis')->flushVideo();

        return $this->redirectToIndex($resource);
    }
}
