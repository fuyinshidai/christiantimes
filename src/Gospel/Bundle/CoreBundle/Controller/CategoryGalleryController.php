<?php

namespace Gospel\Bundle\CoreBundle\Controller;

use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Symfony\Component\HttpFoundation\Request;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class CategoryGalleryController extends ResourceController
{
    /**
     * Get collection (paginated by default) of resources.
     */
    public function indexAction(Request $request)
    {
        $config = $this->getConfiguration();

        $criteria = $config->getCriteria();
        $sorting = $config->getSorting();

        $pluralName = $config->getPluralResourceName();
		$repository = $this->getManager()->getRepository('GospelCoreBundle:CategoryGallery');
		$arrayTree = $repository->childrenHierarchy();

        if ($config->isPaginated()) {
            $resources = $this
                ->getResourceResolver()
                ->getResource($repository, $config, 'createPaginator', array($criteria, $sorting))
            ;

            $resources
                ->setCurrentPage($request->get('page', 1), true, true)
                ->setMaxPerPage($config->getPaginationMaxPerPage())
            ;
        } else {
            $resources = $this
                ->getResourceResolver()
                ->getResource($repository, $config, 'findBy', array($criteria, $sorting, $config->getLimit()))
            ;
        }

		$category = $repository->createNew();
        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('index.html'))
            ->setTemplateVar($pluralName)
            ->setData(array('categories'=>$resources,'category'=>$repository->createNew(),'tree'=>$arrayTree))
        ;

        return $this->handleView($view);
    }

    /**
     * Create new resource or just display the form.
     */
    public function createAction(Request $request)
    {
		$repository = $this->getManager()->getRepository('GospelCoreBundle:CategoryGallery');
        $config = $this->getConfiguration();
		$parent_id = $request->get('parent', null);
		$selected = null;
		if($parent_id) {
			$selected = $repository->find($parent_id);
			
		}

        $resource = $this->createNew();
        $form = $this->createForm($this->getConfiguration()->getFormType(), $resource, array('selected'=>$selected));

        if (($request->isMethod('PUT')  || $request->isMethod('POST') )&& $form->bind($request)->isValid()) {
            $this->create($resource);
            $this->setFlash('success', 'create');

            return $this->redirect($this->generateUrl('gospel_category_gallery_index'));
        }

        if ($config->isApiRequest()) {
            return $this->handleView($this->view($form));
        }
		$arrayTree = $repository->childrenHierarchy();

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('create.html'))
            ->setData(array(
                $config->getResourceName() => $resource,
                'form'                     => $form->createView()
            ))
        ;

        return $this->handleView($view);
    }

    /**
     * Display the form for editing or update the resource.
     */
    public function updateAction(Request $request)
    {
        $config = $this->getConfiguration();

        $resource = $this->findOr404();
        $form = $this->getForm($resource);

        if (($request->isMethod('PUT') || $request->isMethod('POST')) && $form->bind($request)->isValid()) {
            $this->update($resource);
            $this->setFlash('success', 'update');

            return $this->redirectTo($resource);
        }

        if ($config->isApiRequest()) {
            return $this->handleView($this->view($form));
        }

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('update.html'))
            ->setData(array(
                $config->getResourceName() => $resource,
                'form'                     => $form->createView()
            ))
        ;

        return $this->handleView($view);
    }

	public function deleteAction() {
		$resource = $this->findOr404();

		$entities = $this->getDoctrine()->getRepository('GospelCoreBundle:Gallery')->findBy(array(
			'categorygallery' => $resource,
		));

		if (count($entities)) {
			$msg = '';
			$msg .= "该分两类包含图片集，不能直接删除，请首先删除以下图片集或者将以下图片集分类改为非<strong>".$resource->getTitle()."</strong>分类:<br/>";
			foreach ($entities as $entity) {
				$msg .= "id:" . $entity->getId() . " - <a target='_blank' href='".$this->generateUrl('gospel_gallery_show', array('id'=>$entity->getId()))."'>" . $entity->getTitle()."</a><br/>";
			}

			$this
            ->get('session')
            ->getFlashBag()
            ->add('error', $msg)
        	;
		} else {
			$this->delete($resource);
			$this->setFlash('success', 'delete');
		}

		return $this->redirect($this->generateUrl('gospel_category_other_index'));
	}
}
