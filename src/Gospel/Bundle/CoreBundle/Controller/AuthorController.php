<?php

namespace Gospel\Bundle\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Gospel\Bundle\CoreBundle\Form\AuthorType;
use Gospel\Bundle\CoreBundle\Form\AuthorSearchType;
use Gospel\Bundle\CoreBundle\Entity\Author;
use Symfony\Component\Filesystem\Filesystem;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class AuthorController extends ResourceController
{
    /**
     * Get collection (paginated by default) of resources.
     */
    public function indexAction(Request $request)
    {
        $config = $this->getConfiguration();

        $resource = $this->createNew();
		$form = $this->createForm(new AuthorSearchType(), $resource, array(
    		'em' => $this->getDoctrine()->getManager(),
			'category' => $this->makeTree($this->getCategoryTree(), array()),
		));

		$metadata = $this
                 ->getManager()
                 ->getMetadataFactory()
                 ->getMetadataFor("Gospel\Bundle\CoreBundle\Entity\Author");
        $criteria = $config->getCriteria();
		$gospel = $request->get('gospel_author_search', array());
		$likes = array('title', 'email', 'phone');
		foreach($gospel as $field => $val) {
			if($val) {
				if (in_array($field, $likes)) {
					$val = array(
						'type' => 'like',
						'data' => $val,
					);
				}
				if($field == 'category') {
					$val = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:CategoryAuthor')->find($val);
				}
				if (!$metadata->hasField($field) && $field != 'category') {
					// Make sure we only use existing fields (avoid any injection)
					continue;
				}
				$criteria[$field] = $val;
			}
		}
        $sorting = $config->getSorting();

        $pluralName = $config->getPluralResourceName();
        $repository = $this->getRepository();

        if ($config->isPaginated()) {
            $resources = $this
                ->getResourceResolver()
                ->getResource($repository, $config, 'createPaginator', array($criteria, $sorting))
            ;

            $resources
                ->setCurrentPage($request->get('page', 1), true, true)
                ->setMaxPerPage($config->getPaginationMaxPerPage())
            ;
        } else {
            $resources = $this
                ->getResourceResolver()
                ->getResource($repository, $config, 'findBy', array($criteria, $sorting, $config->getLimit()))
            ;
        }

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('index.html'))
            ->setTemplateVar($pluralName)
            ->setData(array(
				$pluralName => $resources,
				'form' => $form,
				)
			)
        ;

        return $this->handleView($view);
    }
    public function createAction(Request $request)
    {
        $config = $this->getConfiguration();

        $resource = $this->createNew();
		$form = $this->createForm(new AuthorType(), $resource, array(
    		'em' => $this->getDoctrine()->getManager(),
			'category' => $this->makeTree($this->getCategoryTree(), array()),
			'createAt' => new \DateTime(),
			'entity' => $resource,
		));

        if ($request->isMethod('POST') || $request->isMethod('PUT')) {
			if ($form->bind($request)->isValid()) {
				// 创建
				$this->create($resource);
				$this->setFlash('success', 'create');

				return $this->redirectTo($resource);
			} else {
				echo $form->getErrorsAsString()."<br />";
			}
        }

        if ($config->isApiRequest()) {
            return $this->handleView($this->view($form));
        }

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('create.html'))
            ->setData(array(
                $config->getResourceName() => $resource,
                'form'                     => $form->createView(),
            ))
        ;

        return $this->handleView($view);
    }

    public function updateAction(Request $request)
    {
        $config = $this->getConfiguration();

        $resource = $this->findOr404();
		$form = $this->createForm(new AuthorType(), $resource, array(
    		'em' => $this->getDoctrine()->getManager(),
			'category' => $this->makeTree($this->getCategoryTree(), array()),
			'createAt' => $resource->getCreateAt(),
			'entity' => $resource,
		));

        if (($request->isMethod('PUT') || $request->isMethod('POST')) && $form->bind($request)->isValid()) {
            $this->update($resource);
            $this->setFlash('success', 'update');

            return $this->redirectToIndex();
        }

        if ($config->isApiRequest()) {
            return $this->handleView($this->view($form));
        }

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('update.html'))
            ->setData(array(
                $config->getResourceName() => $resource,
                'form'                     => $form->createView(),
                'form2'                     => $form->createView(),
                'temp_id'                     => $resource->getId(),
            ))
        ;

        return $this->handleView($view);
    }

    /**
     * Delete resource.
     */
    public function deleteAction()
    {
        $resource = $this->findOr404();

		$em = $this->getDoctrine()->getManager();
		$repoPost = $this->getDoctrine()->getRepository('GospelCoreBundle:Post');

		// find all the post with this author
		$posts = $repoPost->findBy(array(
			'author' => $resource,
		));
		foreach($posts as $post) {
			$post->setWriter($resource->getTitle());
			$post->setAuthor(null);
			$em->persist($post);
			$em->flush();
		}

		
        $this->delete($resource);
        $this->setFlash('success', 'delete');

        return $this->redirectToIndex($resource);
    }

	public function makeTree($data, $arr) 
	{
		foreach($data as $row) {
			$arr[$row['id']] = str_repeat("——", $row['lvl']).$row['title'];
			if(is_array($row['__children']) && count($row['__children']) > 0) {
				$arr = $this->makeTree($row['__children'], $arr);
			}
		}
		return $arr;
	}

	private function getCategoryTree()
	{
		$repository = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:CategoryAuthor');
		$arrayTree = $repository->childrenHierarchy();
		return $arrayTree;
	}

	/**
	 * 导入作家数据
	 */
	public function loadAction()
	{
		$repositoryAuthor = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:Author');
		$repositoryAuthorCategory = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:CategoryAuthor');
        $config = $this->getConfiguration();
		$customerEm =  $this->get('doctrine')->getManager('customer');
		$em =  $this->get('doctrine')->getManager('default');
		$result = $repositoryAuthor->findAllOld($customerEm);

		// 新旧ID对照表
		$data_categories = array(
			1 => 2,
			2 => 5,
			3 => 3,
			4 => 4,
			5 => 8,
		);

		foreach ($result as $key => $row) {
			$author = new Author();
			$author->setId($row['id']);
			$author->setTitle($row['name']);
			$author->setContent($row['intro']);
			$author->setCoverPath(basename($row['pic']));
			$author->setEmail($row['email']);
			$author->setAddress($row['address']);
			$author->setPhone($row['telephone']);
			$author->setStatus(1);
			$author->setSortOrder($row['sequence']);
			$author->setCreateAt(new \DateTime());
			$author->setUpdatedAt(new \DateTime());
			$oldCatId = str_replace('|', '', $row['cat_id']);
			if ($oldCatId) {
				$arr = explode(',', $oldCatId);
				if (is_array($arr) && count($arr) > 1) {
					$oldCatId = $arr[0];
				}
				$catId = $data_categories[$oldCatId];
			} else {
				$catId = 1;
			}
			$category = $repositoryAuthorCategory->find($catId);
			$author->setCategory($category);
			$em->persist($author);
			$em->flush();
		}

	}

	/**
	 * 移动作家的图片
	 */
	public function updateImageAction()
	{
		$repositoryAuthor = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:Author');
        $config = $this->getConfiguration();
		$customerEm =  $this->get('doctrine')->getManager('customer');
		$em =  $this->get('doctrine')->getManager('default');
		$result = $repositoryAuthor->findAllOld($customerEm);

		foreach ($result as $key => $row) {
			$fs = new Filesystem();
			$oldImageFile = '/var/www/www.christiantimes.cn/upload/writer/' . basename($row['pic']);
			$newImageFoler = '/var/www/christiantimes.dev/web/assets/media/author/cover/' 
			;
			$newImageFile = $newImageFoler . basename($row['pic']);
			$fs->mkdir($newImageFoler);
			if (file_exists($oldImageFile)) {
				$fs->copy($oldImageFile, $newImageFile);
			}
		}
		return new Response('Finished');
	}
}
