<?php

namespace Gospel\Bundle\CoreBundle\Controller;

use Gospel\Bundle\CoreBundle\Controller\BackendController;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Gospel\Bundle\CoreBundle\Form\GalleryType;
use Gospel\Bundle\CoreBundle\Entity\Tag;
use Gospel\Bundle\CoreBundle\Entity\Gallery;
use Gospel\Bundle\CoreBundle\Classes\FileUploadHandler;
use Gospel\Bundle\CoreBundle\Entity\GalleryImage;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Gospel\Bundle\CoreBundle\Entity\Author;

/**
 * 图片上传使用session变量tempUpload
 * 	初始化的时候清除session变量
 * 	
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class GalleryController extends BackendController
{
    /**
     * Get collection (paginated by default) of resources.
	 * 有三种显示列表情况：
	 * 1. 没有权限访问
	 * 2. 访问自己上传的文章列表
	 * 3. 访问所有的文章列表
	 * @Secure(roles="ROLE_POST_LIST, ROLE_POST_LIST_OWN")
     */
    public function indexAction(Request $request)
    {
        $config = $this->getConfiguration();

        $criteria = $config->getCriteria();
        $sorting = $config->getSorting();

		$keyword = $request->get('keywords', null);
		$gospel = $request->get('gospel_gallery', array());
		foreach($gospel as $k => $p) {
			$criteria[$k] = $p;
		}
		$type = $request->get('type', null);
		if($keyword) {
			$criteria[$type] = array('data'=>$keyword,'type'=>'like');
		}

		// 如果是只能显示自己的文件列表的权限,读取该用户的文件
		$criteria = $this->setCriteriaByRole($criteria);

        $pluralName = $config->getPluralResourceName();
        $repository = $this->getRepository();

        if ($config->isPaginated()) {
            $resources = $this
                ->getResourceResolver()
                ->getResource($repository, $config, 'createPaginator', array($criteria, $sorting))
            ;

            $resources
                ->setCurrentPage($request->get('page', 1), true, true)
                ->setMaxPerPage($config->getPaginationMaxPerPage())
            ;
        } else {
            $resources = $this
                ->getResourceResolver()
                ->getResource($repository, $config, 'findBy', array($criteria, $sorting, $config->getLimit()))
            ;
        }

        $resource = $this->createNew();
		$form = $this->createForm(new GalleryType(), $resource, array(
    		'em' => $this->getDoctrine()->getManager(),
			'user' => $this->get('security.context')->getToken()->getUser(),
			'categoryGallery' => $this->makeTree($this->getCategoryGalleryTree(), array()),
			'createAt' => new \DateTime(),
			'entity' => $resource,
		));

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('index.html'))
            ->setTemplateVar($pluralName)
            ->setData(array(
				$pluralName => $resources,
				'form' => $form,
				)
			)
        ;

        return $this->handleView($view);
    }

	/**
	 * 
	 * @Secure(roles="ROLE_POST_ADD")
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @return type
	 */
    public function createAction(Request $request)
    {
        $config = $this->getConfiguration();
		// 用来上传图片的临时id，用来创建临时文件夹
		$temp_id = 'temp-'.md5(mt_rand(1, time()));

        $resource = $this->createNew();
		$options = array(
    		'em' => $this->getDoctrine()->getManager(),
			'user' => $this->getCurrentUser(),
			'categoryGallery' => $this->makeTree($this->getCategoryGalleryTree(), array()),
			'createAt' => new \DateTime(),
			'entity' => $resource,
			'attr' => array(),
		);

		// 根据当前的用户所在的组动态设置显示的上传文章选项
		$options = $this->setOptionsByRole($options);

		$form = $this->createForm(new GalleryType(), $resource, $options);
		$form->get('user')->setData($this->get('security.context')->getToken()->getUser());

        if (($request->isMethod('POST') || $request->isMethod('PUT') ) && $form->bind($request)->isValid()) {
			// 更新文章标签
			$gospel_gallery = $request->get('gospel_gallery');

			// 更新文章的相关文章
			$this->updateRelated($request, $resource);
			
			// 创建gallery
			$this->setUser($resource);
            $this->create($resource);

			// 更新文章的图片
			$this->updateGalleryImages($resource);
            $this->setFlash('success', 'create');

            return $this->redirectTo($resource);
        }

        if ($config->isApiRequest()) {
            return $this->handleView($this->view($form));
        }

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('create.html'))
            ->setData(array(
                $config->getResourceName() => $resource,
                'form'                     => $form->createView(),
				// 传递到另外一个地方的表单
                'form2'                     => $form->createView(),
                'temp_id'                     => $temp_id,
            ))
        ;

        return $this->handleView($view);
    }
	/**
	 * 
	 * @Secure(roles="ROLE_POST_EDIT, ROLE_POST_EDIT_OWN")
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @return type
	 */
    public function updateAction(Request $request)
    {
        $config = $this->getConfiguration();

        $resource = $this->findOr404();

		// 检查访问权限
		// 如果是只能够修改自己的文章的情况下，检查该文章是否属于当前登录用户，如果是不属于
		// 扔出异常
		if ($this->hasRole('ROLE_POST_EDIT_OWN')) {
			if ($resource->getUser()->getId() != $this->getUser()->getId()) {
				throwException(new Exception('没有修改权限'));
			}
		}
		$options = array(
    		'em' => $this->getDoctrine()->getManager(),
			'user' => $this->getCurrentUser(), // 读取当前登录的用户
			'categoryGallery' => $this->makeTree($this->getCategoryGalleryTree(), array()),
			'createAt' => $resource->getCreateAt(),
			'entity' => $resource,
			'attr' => array(),
		);
		$options = $this->setOptionsByRole($options);
		$form = $this->createForm(new GalleryType(), $resource, $options);

        if (($request->isMethod('PUT') || $request->isMethod('POST')) && $form->bind($request)->isValid()) {
			// 更新文章标签
			$gospel_gallery = $request->get('gospel_gallery');

			// 更新文章的相关文章
			$this->updateRelated($request, $resource);

			$this->setUser($resource);
            $this->update($resource);
            $this->setFlash('success', 'update');
			

            return $this->redirectTo($resource);
        }

        if ($config->isApiRequest()) {
            return $this->handleView($this->view($form));
        }

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('update.html'))
            ->setData(array(
                $config->getResourceName() => $resource,
                'form'                     => $form->createView(),
                'form2'                     => $form->createView(),
                'temp_id'                     => $resource->getId(),
            ))
        ;

        return $this->handleView($view);
    }

	/**
	 * @see https://github.com/liip/LiipHelloBundle/blob/master/Controller/HelloController.php
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @return type
	 */
	public function updateGalleryImageAction(Request $request)
	{
		$this->getConfiguration();
		$image = $this->getDoctrine()->getRepository('GospelCoreBundle:GalleryImage')->find($request->get('id', null));
		$data = array();
		if ($image) {
			$method = $request->get('name', null);
			$value = $request->get('value', null);
			
			// 如果是设置封面图片，首先将其他的封面图片设置为0
			if ($method == 'setIsCover') {
				$images = $image->getGallery()->getImages();
				foreach ($images as $otherImage) {
					$otherImage->setIsCover(0);
				}
			}

			if ($method) {
				$image->{$method}($value);
				$this->getDoctrine()->getManager()->persist($image);
				// before each flush need getConfiguration() statement
			}
			$this->getConfiguration();
			$this->getDoctrine()->getManager()->flush();
			$data['success'] = 1;
			$data['msg'] = $request->get('id').'-'.$method.'-'.$value;
		}
        $view = $this
            ->view()
			->setFormat('json')
			->setData($data)
        ;

        return $this->handleView($view);
	}

	/**
	 * 处理图片批量上传的方法，包括图片的添加，删除
	 * path: gospel_gallery_file_upload
	 * path 必须以'/'结尾
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function FileUploadAction(Request $request)
	{
		// 当前图片所属的文章的id
		$id = $request->get('id', null);

		// 读取上传类
		$upload = $this->getUploader($id);

		$repo = $this->getDoctrine()->getRepository('GospelCoreBundle:GalleryImage');
		$repoGallery = $this->getDoctrine()->getRepository('GospelCoreBundle:Gallery');
		$em = $this->getDoctrine()->getManager();


		// 当id为空的时候，为添加新的gallery的情况
		// 该临时id用来临时上传文件使用，当文件上传结束之后
		// 需要
		// 1. 把该文件夹里面的文件移动到相对应的gallery的文件夹里面
		// 2. 把该文件夹下面的文件写入到数据库
		// 3. 删除该文件夹
		$session = $this->getRequest()->getSession();
//		$logger = $this->get('logger');
		switch($request->getMethod()) {
			case 'POST':
				$files = $upload->postFiles;
				// 如果是添加文章的时候
				if (strpos($id, 'temp') === 0) {
					$session->set('tempId', $id);
					// 更新session数据
					$temp_data = $session->get('tempUpload', array());
//    				$logger->info('count temp data:'.print_r($session->get('tempUpload'), true));
					// 取得最后一个文件的order值
					if(count($temp_data)>0) {
						$tmp_v = $temp_data;
						$last_v = array_pop($tmp_v);
						$order = $last_v->order + 1;
					} else {
						$order = 1;
					}
					foreach($files as $file) {
						$file->order = $order++;
						$temp_data[$file->name] = $file;
					}
					$session->set('tempUpload', $temp_data);
				} else {
					// 修改文章的时候
					$imageE = $repo->findBy(array('gallery' => $repoGallery->find($id)), array('sortOrder' => 'DESC'));
					$image_f = array_shift($imageE);
					$order = isset($image_f)? $image_f->getSortOrder(): 0;
					foreach($files as $file) {
						$image = new GalleryImage();
						$image->setName($file->name);
						$image->setCaption($file->caption);
						if (isset($file->isCover) && $file->isCover) {
							$image->setIsCover($file->isCover);
						}
						if (isset($file->useWatermark) && $file->useWatermark) {
							$image->setUseWatermark($file->useWatermark);
						}
						$image->setGallery($repoGallery->find($id));
						$image->setSortOrder(++$order);
						$em->persist($image);
					}
					$this->getConfiguration();
					$em->flush();
				}
				break;
		
			case 'DELETE':	// 删除文件的时候同步更新文件
				$files = $upload->getDeleteData();

				// 如果是添加文章的时候上传文件，把文件信息保存在session里面
				if (strpos($id, 'temp') === 0) {
					$temp_data = $session->get('tempUpload', array());
					foreach($files as $file) {
						unset($temp_data[$file]);
					}
					$session->set('tempUpload', $temp_data);

				} else { // 如果是修改文章的时候需要动态更新数据库的
					foreach($files as $file) {
						$imageE = $repo->findBy(array('gallery' => $repoGallery->find($id)), array('sortOrder' => 'DESC'));
						foreach($imageE as $entity) {
							if($entity->getName() == $file) {
								$em->remove($entity);
								$this->getConfiguration();
								$em->flush();
							}
						}
					}
				}
				break;
			case 'GET':
				// 如果是添加文章的时候
				if (strpos($id, 'temp') === 0) {
				} else {//这个是在修改文章的情况下执行的代码
					$gallery = $repoGallery->find($id);
					$imagesInDb = $repo->findBy(array('gallery' => $gallery), array('sortOrder' => 'ASC'));
					$images = $upload->get();
					$return = array('files' => array());
					foreach($imagesInDb as $row) {
						if(is_object($row)) {
							foreach($images['files'] as $image) {
								if($row->getName() == $image->name) {
									$image->id = $row->getId();
									$image->caption = $row->getCaption();
									$image->useWatermark = $row->getUseWatermark();
									$image->isCover = $row->getIsCover();
									$return['files'][] = $image;
								}
							}
						}
					}
					$upload->generate_response($return);
				}
			default:
				;
		}


		return new Response('');
	}

	/**
	 * 
	 * @param type $id
	 * @return \Gospel\Bundle\CoreBundle\Classes\FileUploadHandler
	 */
	private function getUploader($id = null)
	{
		// 如果是修改文章的时候
		if (strpos($id, 'temp') !== 0) {
			$gallery = $this->getDoctrine()->getRepository('GospelCoreBundle:Gallery')->find($id);
			$path = $gallery->getCreateAt()->format("Ym") . DIRECTORY_SEPARATOR . $id ;
		} else {
			$path = $id;
		}
		$options = array(
            'script_url' => $this->generateUrl('gospel_gallery_file_upload', array('id'=>$id)),
			'path' => Gallery::getUploadImagePath().$path.'/',
		);
		return new FileUploadHandler($options);
	}


	private function updateRelated($request, $resource)
	{
		$gospel_gallery = $request->get('gospel_gallery');
		preg_match_all("/\[(\d*)\]/", $gospel_gallery['relatedGalleries'], $ids);
		$related = $ids[1];

		$resource->getRelated()->clear();
		if(count($related)>0) {
			foreach($related as $relate) {
				if ($relate) {
					$entity = $this->getRepository()->find($relate);
					if ($entity instanceof Gallery) {
						$resource->addRelated($entity);
					}
				}
			}
		}
		return $resource;
	}

	private function getAreaTree()
	{
		$repository = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:Area');
		$arrayTree = $repository->childrenHierarchy();
		return $arrayTree;
	}

	private function getCategoryTree()
	{
		$repository = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:Category');
		$arrayTree = $repository->childrenHierarchy();
		return $arrayTree;
	}

	private function getCategoryGalleryTree()
	{
		$repository = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:CategoryGallery');
		$arrayTree = $repository->childrenHierarchy();
		return $arrayTree;
	}

	/**
	 * 生成树状结构,根据所在的深读在前面添加"——"
	 */
	public function makeTree($data, $arr, $maxlevel = 10000) 
	{
		foreach($data as $row) {
			if ($maxlevel <= $row['lvl']) {
				continue;
			}
			$arr[$row['id']] = str_repeat("——", $row['lvl']).$row['title'];
			if(is_array($row['__children']) && count($row['__children']) > 0) {
				$arr = $this->makeTree($row['__children'], $arr, $maxlevel);
			}
		}
		return $arr;
	}

	/**
	 * 查看相关文章，可以根据不同的字段进行检索
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @return type
	 */
	public function relatedAction(Request $request)
	{
		$config = $this->getConfiguration();
        $criteria = $config->getCriteria();

		$keyword = $request->get('keywords', null);
		$gospel = $request->get('gospel_gallery', array());
		foreach($gospel as $k => $p) {
			$criteria[$k] = $p;
		}
		$type = $request->get('type', null);
		if($keyword) {
			$criteria[$type] = array('data'=>$keyword,'type'=>'like');
		}

        $sorting = $config->getSorting();

        $pluralName = $config->getPluralResourceName();
        $repository = $this->getRepository();

        if ($config->isPaginated()) {
            $resources = $this
                ->getResourceResolver()
                ->getResource($repository, $config, 'createPaginator', array($criteria, $sorting))
            ;

            $resources
                ->setCurrentPage($request->get('page', 1), true, true)
                ->setMaxPerPage($config->getPaginationMaxPerPage())
            ;
        } else {
            $resources = $this
                ->getResourceResolver()
                ->getResource($repository, $config, 'findBy', array($criteria, $sorting, $config->getLimit()))
            ;
        }

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('related.html'))
            ->setTemplateVar($pluralName)
            ->setData($resources)
        ;

        return $this->handleView($view);
	}

	/**
	 * 在添加文章的时候更新图片的内容
	 * 目的是保存临时文件夹里面的文件到目标文件夹
	 * 同时清理临时数据
	 */
	private function updateGalleryImages($resource)
	{
		$session = $this->getRequest()->getSession();
		$tempIdInSession = $session->get('tempId', null);
		// 保存图片到数据库
		if($tempIdInSession) {
			$temp_data = $session->get('tempUpload', array());
			foreach($temp_data as $file) {
				$image = new GalleryImage();
				$image->setName($file->name);
				$image->setCaption($file->caption);
				$image->setUseWatermark(isset($file->useWatermark)?$file->useWatermark:'');
				$image->setIsCover(isset($file->isCover)?$file->isCover:'');
				$image->setGallery($resource);
				$image->setSortOrder($file->order);
				$this->getDoctrine()->getManager()->persist($image);
			}
			$this->getConfiguration();
			$this->getDoctrine()->getManager()->flush();

			// 重命名文件加到对应的文件下面
			$session = $this->getRequest()->getSession();
			$uploader = $this->getUploader($session->get('tempId'));
			$options = $uploader->getOptions();
			$dir = $options['upload_dir'].DIRECTORY_SEPARATOR.$resource->getCreateAt()->format('Ym');
			$temp_path = dirname($dir);
			$targetDir = dirname(dirname($dir)).DIRECTORY_SEPARATOR
					.$resource->getCreateAt()->format('Ym').DIRECTORY_SEPARATOR.$resource->getId();
			if(!is_dir($targetDir)) {
				$fs = new Filesystem();
				$fs->mkdir($targetDir);

			}
			rename($temp_path, $targetDir);

			// 清除所有的临时文件夹
			$fs = new Filesystem();
			$finder = new Finder();
			$finder->directories()->depth('== 1')->name('/temp.*/')->in(dirname(dirname($dir)));
			foreach ($finder as $file) {
				$fs->remove($file);
			}

		}
		// 清理临时id
		$session->set('tempId', null);
		$session->set('tempUpload', null);
	}

    /**
	 * @Secure(roles="ROLE_POST_DELETE")
     * Delete resource.
     */
    public function deleteAction()
    {
        $resource = $this->findOr404();
		$em = $this->getDoctrine()->getManager();
		foreach($resource->getImages() as $image) {
			$em->remove($image);
			$this->getConfiguration();
			$em->flush();
		}

		// 清除所有的临时文件夹
		$uploader = $this->getUploader($resource->getId());
		$options = $uploader->getOptions();

		// 取得上传文件夹
		$dir = $options['upload_dir'].DIRECTORY_SEPARATOR.$resource->getCreateAt()->format('Ym');

		// 如果存在图片文件路径，则删除该文件夹
		if(is_dir($dir)) {
			$fs = new Filesystem();
			$finder = new Finder();
			$finder->files()->in($dir);
			foreach ($finder as $file) {
				$fs->remove($file);
			}
			rmdir($dir);
		}

        $this->delete($resource);
        $this->setFlash('success', 'delete');

        return $this->redirectToIndex($resource);
    }

	/**
	 * 当修改或者保存的时候用户的密码会被修改，通过该方法确保密码没有被修改
	 * @param type $resource
	 */
	public function setUser($resource) {

			$stmt = $this->getDoctrine()
                   ->getConnection()
                   ->prepare('SELECT * FROM  gospel_users WHERE id = '.$resource->getUser()->getId());
      $stmt->execute();
      $user = $stmt->fetchAll();
	  $user = $user[0];
	  $userEn = $resource->getUser();
	  $userEn->setPassword($user['password']);
	  $resource->setUser($userEn);
	  return $resource;
	}

	/**
	 * 移动图片的时候旧的图片文件夹的地址
	 * @return string
	 */
	public function getOldImageRoot()
	{
		if ($_SERVER['SERVER_ADDR'] == '127.0.0.1') {
			return '/var/www/www.gospeltimes.cn/data/';
		} else {
			return '/data/www/gospeltimes.cn/data/';
		}
	}

	/**
	 * 移动图片的时候新的图片文件夹的地址
	 * @return string
	 */
	public function getNewGalleryImageRoot()
	{
		if ($_SERVER['SERVER_ADDR'] == '127.0.0.1') {
			return '/var/www/gospeltimes.dev/symfony/web/assets/media/gallery/';
		} else {
			return '/data/www/gospeltimes/web/assets/media/gallery/';
		}
	}

	/**
	 * 根据当前的id位置自动的载入接下来的文章,通过ajax请求
	 * @return type
	 */
	public function autoLoadAction()
	{
        $config = $this->getConfiguration();
		$view = $this
				->view()
				->setTemplate($config->getTemplate('autoLoad.html'))
				->setData(array(
				))
				;
		return $this->handleView($view);
	}
}
