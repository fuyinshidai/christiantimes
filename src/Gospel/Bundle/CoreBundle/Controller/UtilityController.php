<?php

/*
 * Copyright 2018 Zhili He
 * zhilihe.com
 */

namespace Gospel\Bundle\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Description of UtilityController
 *
 * @author Zhili He <zhili850702@gmail.com>
 */
class UtilityController extends Controller{

	public function batchDisablePostsAction() {
        return $this->render('GospelCoreBundle:Utility:batchDisable.html.twig', array('name' => 'abc'));
	}
}
