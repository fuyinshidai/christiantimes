<?php

namespace Gospel\Bundle\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Gospel\Bundle\CoreBundle\Classes\Uploader;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class UEditorController extends Controller
{

	/**
	 * 上传图片的按钮兑换框返回结果
	 * @return type
	 */
	public function DialogImageAction()
	{
		return $this->render('GospelCoreBundle:UEditor:image.html.twig');
	}

	/**
	 * 
	 * @return type
	 */
	public function DialogLinkAction()
	{
		return $this->render('GospelCoreBundle:UEditor:link.html.twig');
	}

	public function DialogSearchReplaceAction()
	{
		return $this->render('GospelCoreBundle:UEditor:searchReplace.html.twig');
	}

	public function DialogAction($tpl)
	{
		return $this->render('GospelCoreBundle:UEditor:' . $tpl . '.html.twig');
	}

	public function DialogImageUploadAction(Request $request = null)
	{
		//上传图片框中的描述表单名称，
		$title = htmlspecialchars($_POST['pictitle'], ENT_QUOTES);
		$path = htmlspecialchars($_POST['dir'], ENT_QUOTES);

		$webPath = $this->getWebPath();
		//上传配置
		$config = array(
			"savePath" => ($path == "1" ? $webPath . "upload/" : $webPath . "upload1/"),
			"webRoot" => $this->getWebRoot(),
			"maxSize" => 1000, // 单位KB
			"allowFiles" => array(".gif", ".png", ".jpg", ".jpeg", ".bmp")
		);

		//生成上传实例对象并完成上传
		$up = new Uploader("upfile", $config);

		/**
		 * 得到上传文件所对应的各个参数,数组结构
		 * array(
		 *     "originalName" => "",   //原始文件名
		 *     "name" => "",           //新文件名
		 *     "url" => "",            //返回的地址
		 *     "size" => "",           //文件大小
		 *     "type" => "" ,          //文件类型
		 *     "state" => ""           //上传状态，上传成功时必须返回"SUCCESS"
		 * )
		 */
		$info = $up->getFileInfo();

		/**
		 * 向浏览器返回数据json数据
		 * {
		 *   'url'      :'a.jpg',   //保存后的文件路径
		 *   'title'    :'hello',   //文件描述，对图片来说在前端会添加到title属性上
		 *   'original' :'b.jpg',   //原始文件名
		 *   'state'    :'SUCCESS'  //上传状态，成功时返回SUCCESS,其他任何值将原样返回至图片上传框中
		 * }
		 */
		return new Response("{'url':'" . $info["url"] . "','title':'" . $title . "','original':'" . $info["originalName"] . "','state':'" . $info["state"] . "'}");
	}

	public function DialogImageManagerAction(Request $request = null)
	{
		//需要遍历的目录列表，最好使用缩略图地址，否则当网速慢时可能会造成严重的延时
		$paths = array($this->getWebPath() . 'upload/', $this->getWebPath() . 'upload1/');

		$action = htmlspecialchars($_POST["action"]);
		if ($action == "get") {
			$files = array();
			foreach ($paths as $path) {
				$tmp = $this->getfiles($path);
				if ($tmp) {
					$files = array_merge($files, $tmp);
				}
			}
			if (!count($files))
				return;
			rsort($files, SORT_STRING);
			$str = "";
			foreach ($files as $file) {
				$str .= $file . "ue_separate_ue";
			}
			return new Response($str);
		}
	}

	public function AttachmentUploadAction()
	{

		//上传配置
		$config = array(
			"savePath" =>  $this->getWebPath() . "attachments/",
			"webRoot" => $this->getWebRoot(),
			"allowFiles" => array(".rar", ".doc", ".docx", ".zip", ".pdf", ".txt", ".swf", ".wmv", ".mp4", ".mp3"), //文件允许格式
			"maxSize" => 100000 //文件大小限制，单位KB
		);
		//生成上传实例对象并完成上传
		$up = new Uploader("upfile", $config);

		/**
		 * 得到上传文件所对应的各个参数,数组结构
		 * array(
		 *     "originalName" => "",   //原始文件名
		 *     "name" => "",           //新文件名
		 *     "url" => "",            //返回的地址
		 *     "size" => "",           //文件大小
		 *     "type" => "" ,          //文件类型
		 *     "state" => ""           //上传状态，上传成功时必须返回"SUCCESS"
		 * )
		 */
		$info = $up->getFileInfo();

		/**
		 * 向浏览器返回数据json数据
		 * {
		 *   'url'      :'a.rar',        //保存后的文件路径
		 *   'fileType' :'.rar',         //文件描述，对图片来说在前端会添加到title属性上
		 *   'original' :'编辑器.jpg',   //原始文件名
		 *   'state'    :'SUCCESS'       //上传状态，成功时返回SUCCESS,其他任何值将原样返回至图片上传框中
		 * }
		 */
		return new Response('{"url":"' . $info["url"] . '","fileType":"' . $info["type"] . '","original":"' . $info["originalName"] . '","state":"' . $info["state"] . '"}');
	}

	/**
	 * 遍历获取目录下的指定类型的文件
	 * @param $path
	 * @param array $files
	 * @return array
	 */
	function getfiles($path, &$files = array())
	{
		if (!is_dir($path)) {
			return array();
		}
		$handle = opendir($path);
		if($handle) {
			while (false !== ( $file = readdir($handle) )) {
				if ($file != '.' && $file != '..') {
					$path2 = $path . '/' . $file;
					if (is_dir($path2)) {
						$this->getfiles($path2, $files);
					} else {
						if (preg_match("/\.(gif|jpeg|jpg|png|bmp)$/i", $file)) {
							$files[] = '../../../' . substr($path2, strlen($this->getWebRoot()));
						}
					}
				}
			}
			return $files;
		}
		return array();
	}

	/**
	 * the root of the documents
	 * @return type
	 */
	public function getWebRoot()
	{
		return $this->get('kernel')->getRootDir() . '/../web/';
	}

	/**
	 * the root path of the store upload image folder
	 * @return type
	 */
	public function getWebPath()
	{
		return $this->getWebRoot() . 'assets/media/';
	}

}
