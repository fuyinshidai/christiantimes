<?php

namespace Gospel\Bundle\CoreBundle\Controller;

use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Gospel\Bundle\CoreBundle\Form\SpecialType;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class SpecialController extends ResourceController
{
	public function createAction(\Symfony\Component\HttpFoundation\Request $request)
	{
		$config = $this->getConfiguration();

		$resource = $this->createNew();
		$form = $this->createForm(new SpecialType(), $resource, array(
			'em' => $this->getManager(),
			'createAt' => new \DateTime(),
			'entity' => $resource,
		));

		if (($request->isMethod('POST') || $request->isMethod('PUT')) && $form->bind($request)->isValid()) {
			$this->create($resource);
			return $this->redirect($this->generateUrl('gospel_special_index'));
		}
		if (!$form->isValid()) {
        	$this->setFlash('error', $form->getErrorsAsString());
		}

		$view = $this
				->view()
				->setTemplate($config->getTemplate('create.html'))
				->setData(array(
					$config->getResourceName() => $resource,
					'form' => $form->createView(),
				))
				;
		return $this->handleView($view);
	}

	public function updateAction(\Symfony\Component\HttpFoundation\Request $request)
	{
		$config = $this->getConfiguration();
		$resource = $this->findOr404();
		$form = $this->createForm(new SpecialType(), $resource, array(
			'em' => $this->getManager(),
			'createAt' => $resource->getCreateAt(),
			'entity' => $resource,
		));

		if (($request->isMethod('PUT') || $request->isMethod('POST')) && $form->bind($request)->isValid()) {
			$this->update($resource);
			$this->setFlash('success', 'update');

			return $this->redirect($this->generateUrl('gospel_special_index'));
		}

		$view = $this
				->view()
				->setTemplate($config->getTemplate('update.html'))
				->setData(array(
					$config->getResourceName() => $resource,
					'form' => $form->createView(),
				))
				;
		return $this->handleView($view);
	}

}
