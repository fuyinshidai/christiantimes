<?php

namespace Gospel\Bundle\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Gospel\Bundle\CoreBundle\Form\TopicType;
use Gospel\Bundle\CoreBundle\Form\PostType;
use Gospel\Bundle\CoreBundle\Entity\Topic;
use Gospel\Bundle\CoreBundle\Entity\TopicPost;
use Gospel\Bundle\CoreBundle\Entity\Post;
use Symfony\Component\Filesystem\Filesystem;
use Gospel\Bundle\CoreBundle\Entity\CategoryTopic;
/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class TopicController extends ResourceController
{
    /**
     * Get collection (paginated by default) of resources.
     */
    public function indexAction(Request $request)
    {
        $config = $this->getConfiguration();

        $criteria = $config->getCriteria();
        //$sorting = $config->getSorting();
				$sorting = array('publishedAt' => 'desc');

        $pluralName = $config->getPluralResourceName();
        $repository = $this->getRepository();

        if ($config->isPaginated()) {
            $resources = $this
                ->getResourceResolver()
                ->getResource($repository, $config, 'createPaginator', array($criteria, $sorting))
            ;

            $resources
                ->setCurrentPage($request->get('page', 1), true, true)
                ->setMaxPerPage($config->getPaginationMaxPerPage())
            ;
        } else {
            $resources = $this
                ->getResourceResolver()
                ->getResource($repository, $config, 'findBy', array($criteria, $sorting, $config->getLimit()))
            ;
        }

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('index.html'))
            ->setTemplateVar($pluralName)
            ->setData($resources)
        ;

        return $this->handleView($view);
    }

    public function createAction(Request $request)
    {
        $config = $this->getConfiguration();

        $resource = $this->createNew();
		$form = $this->createForm(new TopicType(), $resource, array(
    		'em' => $this->getDoctrine()->getManager(),
			'category' => $this->makeTree($this->getCategoryTree(), array()),
			'createAt' => new \DateTime(),
			'entity' => $resource,
		));

        if (($request->isMethod('POST') || $request->isMethod('PUT') ) && $form->bind($request)->isValid()) {
			// 创建专题
            $this->create($resource);

			// 更新文章的图片
            $this->setFlash('success', 'create');

            return $this->redirectTo($resource);
        }
//		var_dump($form->getErrorsAsString());

        if ($config->isApiRequest()) {
            return $this->handleView($this->view($form));
        }


        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('create.html'))
            ->setData(array(
                $config->getResourceName() => $resource,
                'form'                     => $form->createView(),
				// 传递到另外一个地方的表单
                'form2'                     => $this->getPostForm()->createView(),
            ))
        ;

        return $this->handleView($view);
    }

    public function updateAction(Request $request)
    {
        $config = $this->getConfiguration();

        $resource = $this->findOr404();
		$form = $this->createForm($config->getFormType(), $resource, array(
    		'em' => $this->getDoctrine()->getManager(),
			'category' => $this->makeTree($this->getCategoryTree(), array()),
			'createAt' => $resource->getCreateAt(),
			'entity' => $resource,
		));
	if ($request->isMethod('POST')) {
		if ($form->bind($request)->isValid()) {
            $this->update($resource);
            $this->setFlash('success', 'update');

            return $this->redirectToIndex();
		} else {
			echo $form->getErrorsAsString()."<br />";
		}
	}

        if ($config->isApiRequest()) {
            return $this->handleView($this->view($form));
        }

		$posts = $resource->getPosts();

		$em = $this->container->get('gospel.manager.topic');
		$repoTopicPost = $this->getDoctrine()->getRepository('GospelCoreBundle:TopicPost');

		$posts = $repoTopicPost->findBy(array(
			'topic' => $resource,
		), array(
			'sortOrder' => 'ASC',
		));

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('update.html'))
            ->setData(array(
                $config->getResourceName() => $resource,
                'form'                     => $form->createView(),
                'form2'                     => $this->getPostForm()->createView(),
				'posts' => $posts,
            ))
        ;

        return $this->handleView($view);
    }

	public function deleteAction() {
		$resource = $this->findOr404();

		$em = $this->container->get('gospel.manager.topic');
		$resource->clearPosts($em);
		$em->remove($resource);
		$em->flush();

		$this->setFlash('success', 'delete');

		return $this->redirectToIndex();
	}

    private function getPostForm()
    {
        $resource = new Post();
		$form = $this->createForm(new PostType(), $resource, array(
    		'em' => $this->getDoctrine()->getManager(),
			'user' => $this->get('security.context')->getToken()->getUser(),
			'category' => $this->makeTree($this->getCategoryTree(), array()),
			'categoryother' => $this->makeTree($this->getCategoryOtherTree(), array()),
			'area' => $this->makeTree($this->getAreaTree(), array()),
			'createAt' => new \DateTime(),
			'entity' => $resource,
		));
        return $form;
    }


	private function getAreaTree()
	{
		$repository = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:Area');
		$arrayTree = $repository->childrenHierarchy();
		return $arrayTree;
	}

	private function getCategoryTree()
	{
		$repository = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:CategoryTopic');
		$arrayTree = $repository->childrenHierarchy();
		return $arrayTree;
	}

	public function makeTree($data, $arr) 
	{
		foreach($data as $row) {
			$arr[$row['id']] = str_repeat("——", $row['lvl']).$row['title'];
			if(is_array($row['__children']) && count($row['__children']) > 0) {
				$arr = $this->makeTree($row['__children'], $arr);
			}
		}
		return $arr;
	}

	/**
	 * 添加文章到专题
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
    public function addPostToTopicAction(Request $request)
    {
		$topicId = $request->get('topic');
		$postId = $request->get('post');

		$em = $this->container->get('gospel.manager.topic');
		$repoTopic = $this->container->get('gospel.repository.topic');
		$repoPost = $this->container->get('gospel.repository.post');
		$repoTopicPost = $this->getDoctrine()->getRepository('GospelCoreBundle:TopicPost');

		$topic = $repoTopic->find($topicId);
		$post = $repoPost->find($postId);

		try {
			$topic->addPost($post);
			$topicPost = $repoTopicPost->findOneBy(array(
				'topic' => $topic, 
				'post' => $post,
			));

			// udpate the topic_post table (add extra field for sorting)
			if (!$topicPost) {
				$topicPost = new TopicPost();
				$topicPost->setTopic($topic);
				$topicPost->setPost($post);
				$topicPost->setSortOrder(1);
				$em->persist($topicPost);
			}
			$em->flush();
			$message = '成功添加文章:'.$post->getTitle().'
				到专题:'.$topic->getTitle()."";
			$data['postId'] = $postId;
			$data['postTitle'] = $post->getTitle();
		} catch (\Exception $e) {
			$message = $e->getMessage().$post->getTitle()."已经添加。";
		}
		$data['msg'] = $message;
		
        return new Response(json_encode($data));
    }

	/**
	 * 删除专题文章
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
    public function deletePostFromTopicAction(Request $request)
    {
		$topicId = $request->get('topic');
		$postId = $request->get('post');
		$em = $this->container->get('gospel.manager.topic');
		$repoTopic = $this->container->get('gospel.repository.topic');
		$repoPost = $this->container->get('gospel.repository.post');
		$repoTopicPost = $this->getDoctrine()->getRepository('GospelCoreBundle:TopicPost');

		$topic = $repoTopic->find($topicId);
		$post = $repoPost->find($postId);

		try {
			$topicPost = $repoTopicPost->findOneBy(array(
				'topic' => $topic, 
				'post' => $post,
			));
			if ($topicPost) {
				$em->remove($topicPost);
			}
			$topic->removePost($post);
			$em->flush();
			$code = 1;
			$message = "删除成功";
		}
		catch (\Exception $e) {
			$code = 0;
			$message = "删除失败";
		}
		$data = array(
				'success' => $code,
				'msg' => $message,
				);

        return new Response(json_encode($data));
    }

	public function reorderPostAction(Request $request)
	{
		$order = 1;
		$postsIds = $request->get('item');
		$topicId = $request->get('topic');

		$em = $this->container->get('gospel.manager.topic');
		$repoTopic = $this->container->get('gospel.repository.topic');
		$repoPost = $this->container->get('gospel.repository.post');
		$repoTopicPost = $this->getDoctrine()->getRepository('GospelCoreBundle:TopicPost');

		$topic = $repoTopic->find($topicId);

		foreach ($postsIds as $postId) {
			$post = $repoPost->find($postId);
			$topicPost = $repoTopicPost->findOneBy(array(
				'topic' => $topic, 
				'post' => $post,
			));
			if ($topicPost) {
				$topicPost->setSortOrder($order++);
				$em->persist($topicPost);
			}
		}
		$em->flush();
        return new Response(1);
	}

	/**
	 * 从旧的数据库里面导入专题内容
	 SET FOREIGN_KEY_CHECKS = 0;
	 	DELETE FROM gospel_topic;
	 	DELETE FROM gospel_topic_post;
	 SET FOREIGN_KEY_CHECKS = 1;
	 * 
	 * 导入数据之后需要做的工作
	 * 修改文件：/var/www/christiantimes.dev/src/Gospel/Bundle/CoreBundle/Entity/Topic.php
     * ORM\GeneratedValue(strategy="AUTO")
	 * 
	 */
	public function loadAction(Request $request)
	{
		$repositoryPost = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:Post');
		$repositoryTopic = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:Topic');


        $config = $this->getConfiguration();
		$customerEm =  $this->get('doctrine')->getManager('customer');
		$em =  $this->get('doctrine')->getManager('default');

		for ($start = 159; $start < 190; $start++ ) {
			$result = $repositoryTopic->findAllOld($customerEm, $start, 1);
			foreach ($result as $topic) {
				$entity = new Topic;
				$entity->setId($topic['id']);
				$entity->setTitle($topic['title']);
				$entity->setContent($topic['intro']);
				$entity->setStatus(1);

				$time_str = $topic['pub_date'];
				if (strpos($time_str, '年')) {
					$timestamp = strtotime(str_replace('日', '', str_replace(array('年', '月'), '-', $time_str)));
				} elseif (strpos($time_str, '-') && count(explode('-', $time_str)) == 3) {
					$timestamp = strtotime($time_str);
				}  elseif (strpos($time_str, '-') && count(explode('-', $time_str)) == 2) {
					$timestamp = strtotime('2013-05-21');
				} else {
					$timestamp = strtotime('2012-11-19');
				}
				$date = new \DateTime("@".$timestamp);
				$entity->setCreateAt($date);
				$entity->setPublishedAt($date);
				$entity->setUpdatedAt($date);
				$entity->setTopicBannerPath(basename($topic['pic']));
				// move the picture
				$fs = new Filesystem();
				$pics = array(
					$topic['pic'],
				);
				foreach($pics as $pic) {
					if($pic) {
						$oldImageFile = $this->getOldImageFolder() . '/' . basename($pic);
						$newImageFoler = $this->getNewImageRoot() . '/' 
						;
						$newImageFile = $newImageFoler . basename($pic);
						$fs->mkdir($newImageFoler);
						if (file_exists($oldImageFile)) {
							$fs->copy($oldImageFile, $newImageFile);
						}
					}
				}
				
				$em->persist($entity);
				$this->getConfiguration();
				$em->flush();

				// 导入专题的文章
				// 读取旧的专题文章
				$posts = $repositoryTopic->findAllOldPosts($customerEm, $entity->getId());
				foreach ($posts as $key => $row) {
					$post = $repositoryPost->find($row['article_id']);
					if($post instanceof Post) {
						$topicPost = new TopicPost();
						$topicPost->setPost($post);
						$topicPost->setTopic($entity);
						$topicPost->setSortOrder($key);
						$em->persist($topicPost);
						$this->getConfiguration();
						$em->flush();
					}
				}
			}

		}

		$msg = 'loading topic done';

		$view = $this
				->view()
				->setTemplate($config->getTemplate('load.html'))
				->setData(array(
					'data' => $result,
					'msg' => $msg,
				))
				;
		return $this->handleView($view);
		
	}

	/**
	 * 更新专题在数据库里面的分类
	 */
	public function updateCategoryAction(Request $request)
	{
		$repositoryTopic = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:Topic');
		$repositoryTopicCategory = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:CategoryTopic');

		$result = "success!";
        $config = $this->getConfiguration();
		$customerEm =  $this->get('doctrine')->getManager('customer');
		$em =  $this->get('doctrine')->getManager('default');

		for ($start = 0; $start < 190; $start += 10 ) {
			$result = $repositoryTopic->findAllOld($customerEm, $start, 10);
			foreach ($result as $topic) {
				$topicEntity = $repositoryTopic->find($topic['id']);
				$category = $repositoryTopicCategory->find(str_replace('|', '', $topic['cat_id']));
				if (!$category instanceof CategoryTopic) {
					$category = $repositoryTopicCategory->findOneBy(array(
						'title' => '其他',
					));
				}
				$topicEntity->setCategory($category);
				$em->persist($topicEntity);
				$this->getConfiguration();
				$em->flush();
			}

		}

		$msg = 'loading topic done';

		$view = $this
				->view()
				->setTemplate($config->getTemplate('load.html'))
				->setData(array(
					'data' => $result,
					'msg' => $msg,
				))
				;
		return $this->handleView($view);
		

	}

	public function getOldImageFolder()
	{
		if ($_SERVER['SERVER_ADDR'] == '127.0.0.1') {
			return '/var/www/www.christiantimes.cn/upload/special/';
		} else {
			return '/data/www/christiantimes.cn/upload/special/';
		}
	}

	/**
	 * 移动图片的时候新的图片文件夹的地址
	 * @return string
	 */
	public function getNewImageRoot()
	{
		if ($_SERVER['SERVER_ADDR'] == '127.0.0.1') {
			return '/var/www/christiantimes.dev/web/assets/media/topic/';
		} else {
			return '/data/www/gospeltimes/web/assets/media/topic/';
		}
	}

	private function getCategoryOtherTree()
	{
		$repository = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:CategoryOther');
		$arrayTree = $repository->childrenHierarchy();
		return $arrayTree;
	}
}
