<?php

namespace Gospel\Bundle\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Gospel\Bundle\CoreBundle\Form\LinkType;
use Gospel\Bundle\CoreBundle\Form\LinkSearchType;
use Gospel\Bundle\CoreBundle\Entity\Link;
use Symfony\Component\Filesystem\Filesystem;

/**
 *
 * @link Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class LinkController extends ResourceController
{
    /**
     * Get collection (paginated by default) of resources.
     */
    public function indexAction(Request $request)
    {
        $config = $this->getConfiguration();

        $resource = $this->createNew();
		$form = $this->createForm(new LinkSearchType(), $resource, array(
    		'em' => $this->getDoctrine()->getManager(),
			'category' => $this->makeTree($this->getCategoryTree(), array()),
		));

		$metadata = $this
                 ->getManager()
                 ->getMetadataFactory()
                 ->getMetadataFor("Gospel\Bundle\CoreBundle\Entity\Link");
        $criteria = $config->getCriteria();
		$gospel = $request->get('gospel_link_search', array());
		$likes = array('title', 'email', 'phone');
		foreach($gospel as $field => $val) {
			if($val) {
				if (in_array($field, $likes)) {
					$val = array(
						'type' => 'like',
						'data' => $val,
					);
				}
				if($field == 'category') {
					$val = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:CategoryLink')->find($val);
				}
				if (!$metadata->hasField($field) && $field != 'category') {
					// Make sure we only use existing fields (avoid any injection)
					continue;
				}
				$criteria[$field] = $val;
			}
		}
        $sorting = $config->getSorting();

        $pluralName = $config->getPluralResourceName();
        $repository = $this->getRepository();

        if ($config->isPaginated()) {
            $resources = $this
                ->getResourceResolver()
                ->getResource($repository, $config, 'createPaginator', array($criteria, $sorting))
            ;

            $resources
                ->setCurrentPage($request->get('page', 1), true, true)
                ->setMaxPerPage($config->getPaginationMaxPerPage())
            ;
        } else {
            $resources = $this
                ->getResourceResolver()
                ->getResource($repository, $config, 'findBy', array($criteria, $sorting, $config->getLimit()))
            ;
        }

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('index.html'))
            ->setTemplateVar($pluralName)
            ->setData(array(
				$pluralName => $resources,
				'form' => $form,
				)
			)
        ;

        return $this->handleView($view);
    }
    public function createAction(Request $request)
    {
        $config = $this->getConfiguration();

        $resource = $this->createNew();
		$form = $this->createForm(new LinkType(), $resource, array(
    		'em' => $this->getDoctrine()->getManager(),
			'category' => $this->makeTree($this->getCategoryTree(), array()),
			'createAt' => new \DateTime(),
			'entity' => $resource,
		));

        if (($request->isMethod('POST') || $request->isMethod('PUT') ) && $form->bind($request)->isValid()) {
			// 创建
            $this->create($resource);
            $this->setFlash('success', 'create');

            return $this->redirectTo($resource);
        }

        if ($config->isApiRequest()) {
            return $this->handleView($this->view($form));
        }

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('create.html'))
            ->setData(array(
                $config->getResourceName() => $resource,
                'form'                     => $form->createView(),
				// 传递到另外一个地方的表单
                'form2'                     => $form->createView(),
            ))
        ;

        return $this->handleView($view);
    }

    public function updateAction(Request $request)
    {
        $config = $this->getConfiguration();

        $resource = $this->findOr404();
		$form = $this->createForm(new LinkType(), $resource, array(
    		'em' => $this->getDoctrine()->getManager(),
			'category' => $this->makeTree($this->getCategoryTree(), array()),
			'createAt' => $resource->getCreateAt(),
			'entity' => $resource,
		));

        if (($request->isMethod('PUT') || $request->isMethod('POST')) && $form->bind($request)->isValid()) {
            $this->update($resource);
            $this->setFlash('success', 'update');

            return $this->redirectTo($resource);
        }

        if ($config->isApiRequest()) {
            return $this->handleView($this->view($form));
        }

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('update.html'))
            ->setData(array(
                $config->getResourceName() => $resource,
                'form'                     => $form->createView(),
                'form2'                     => $form->createView(),
                'temp_id'                     => $resource->getId(),
            ))
        ;

        return $this->handleView($view);
    }

	public function makeTree($data, $arr) 
	{
		foreach($data as $row) {
			$arr[$row['id']] = str_repeat("——", $row['lvl']).$row['title'];
			if(is_array($row['__children']) && count($row['__children']) > 0) {
				$arr = $this->makeTree($row['__children'], $arr);
			}
		}
		return $arr;
	}

	private function getCategoryTree()
	{
		$repository = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:CategoryLink');
		$arrayTree = $repository->childrenHierarchy();
		return $arrayTree;
	}

	/**
	 * 导入旧的数据库的数据
	 */
	public function loadAction()
	{
		$repositoryLink = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:Link');
		$customerEm =  $this->get('doctrine')->getManager('customer');
		$em =  $this->get('doctrine')->getManager('default');
		$result = $repositoryLink->findAllOld($customerEm);

		// 循环现在的所有的数据然后导入到数据
		foreach ($result as $row) {
			$partner = new Link();
			$partner->setTitle($row['title']);
			$partner->setCoverPath(basename($row['pic']));
			$partner->setUrl($row['link_address']);
			$partner->setStatus(1);
			$partner->setSortOrder($row['sequence']);
			$partner->setCreateAt(new \DateTime());
			$partner->setUpdatedAt(new \DateTime());
			$em->persist($partner);
			$em->flush();

			// 移动图片
			$fs = new Filesystem();
			$oldImageFile = '/var/www/www.christiantimes.cn/upload/friend_link/' . basename($row['pic']);
			$newImageFoler = '/var/www/christiantimes.dev/web/assets/media/link/cover/' 
			;
			$newImageFile = $newImageFoler . basename($row['pic']);
			$fs->mkdir($newImageFoler);
			if (file_exists($oldImageFile) && is_file($oldImageFile)) {
				$fs->copy($oldImageFile, $newImageFile);
			}
		}
	}
}
