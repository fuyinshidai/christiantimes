<?php

namespace Gospel\Bundle\CoreBundle\Controller;

use Gospel\Bundle\CoreBundle\Controller\BackendController;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Gospel\Bundle\CoreBundle\Form\PostType;
use Gospel\Bundle\CoreBundle\Entity\Tag;
use Gospel\Bundle\CoreBundle\Entity\Post;
use Gospel\Bundle\CoreBundle\Classes\FileUploadHandler;
use Gospel\Bundle\CoreBundle\Entity\PostImage;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Gospel\Bundle\CoreBundle\Entity\Author;
use Gospel\Bundle\CoreBundle\Twig\GospelExtension;
use Doctrine\ORM\Query\ResultSetMapping;


/**
 * 图片上传使用session变量tempUpload
 * 	初始化的时候清除session变量
 * 	
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class PostController extends BackendController
{
    /**
     * Get collection (paginated by default) of resources.
	 * 有三种显示列表情况：
	 * 1. 没有权限访问
	 * 2. 访问自己上传的文章列表
	 * 3. 访问所有的文章列表
	 * @Secure(roles="ROLE_POST_LIST, ROLE_POST_LIST_OWN")
     */
    public function indexAction(Request $request)
    {
        $config = $this->getConfiguration();

		// set the current page number to the session after the user has modified 
		// the post then redirect to this page
		$this->getRequest()->getSession()->set('page', $this->getRequest()->get('page', 1));

        $criteria = $config->getCriteria();
        $sorting = $config->getSorting();

		$keyword = $request->get('keywords', null);
		$gospel = $request->get('gospel_post', array());
		if(isset($gospel['status'])) {
			$listStatues = (int) $gospel['status'];
		} else {
			$listStatues = 3;
		}
		foreach($gospel as $k => $p) {
			if($k == 'status_unpublished' && $p == 1) {
				$criteria['status'] = 0;
			} else {
				if($k == 'status') {
					$p = (int) $p;
				}
				$criteria[$k] = $p;
			}
		}
		$type = $request->get('type', null);
		if($keyword) {
			if($type != 'author') {
				$criteria[$type] = array('data'=>$keyword,'type'=>'like');
			}
		}

		// 如果是只能显示自己的文件列表的权限,读取该用户的文件
		$criteria = $this->setCriteriaByRole($criteria);

        $pluralName = $config->getPluralResourceName();
        $repository = $this->getRepository();

		$isExport = $request->get('export', false);
		$categoryId = isset($criteria['category']) ? $criteria['category'] : false;
		// 如果是导出内容
		if($isExport && $categoryId) {
			$resources = array();
			// 设置导出文件名称
			$exportName = '基督时报文章列表';
			if($categoryId) {
				$repositoryCategory = $this->getDoctrine()->getRepository('GospelCoreBundle:Category');
				$categoryEntity = $repositoryCategory->find($categoryId);
				$categoryEntity->getChildren();
				$exportName .= '-分类：' . $categoryEntity->getTitle();
				$exportCategories = $repositoryCategory->getAllSubCategories($categoryEntity, false);
				foreach($exportCategories as $row) {
					$criteria['category'] = $row->getId();
					$resources = array_merge($resources, $this
						->getResourceResolver()
						->getResource($repository, $config, 'findBy', array($criteria, $sorting)));
				}
			}
			$areaId = $criteria['area'];
			if($areaId) {
				$repositoryArea = $this->getDoctrine()->getRepository('GospelCoreBundle:Area');
				$areaEntity = $repositoryArea->find($areaId);
				$exportName .= '-地区：' . $areaEntity->getTitle();
			}


			$criteria['category'] = $categoryId;
			$resources = array_merge($resources, $this
				->getResourceResolver()
				->getResource($repository, $config, 'findBy', array($criteria, $sorting)));

			// output the column headings
			$response = $this->render($config->getTemplate('csv.html'),array(
				$pluralName => $resources
			));

			$response->headers->set('Content-Type', 'text/csv');
			$response->headers->set('Content-Disposition', 'attachment; filename="'.$exportName.'.csv"');
			return $response;

		}  
		// 显示文章列表
		
        if ($config->isPaginated()) {
            $resources = $this
                ->getResourceResolver()
                ->getResource($repository, $config, 'createPaginator', array($criteria, $sorting))
            ;

            $resources
                ->setCurrentPage($request->get('page', 1), true, true)
                ->setMaxPerPage($config->getPaginationMaxPerPage())
            ;
        } else {
            $resources = $this
                ->getResourceResolver()
                ->getResource($repository, $config, 'findBy', array($criteria, $sorting, $config->getLimit()))
            ;
        }

        $resource = $this->createNew();
		$form = $this->createForm(new PostType(), $resource, array(
    		'em' => $this->getDoctrine()->getManager(),
			'user' => $this->get('security.context')->getToken()->getUser(),
			'category' => $this->makeTree($this->getCategoryTree(), array()),
			'categoryother' => $this->makeTree($this->getCategoryOtherTree(), array()),
			'area' => $this->makeTree($this->getAreaTree(), array(), 1),
			'createAt' => new \DateTime(),
			'entity' => $resource,
		));

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('index.html'))
            ->setTemplateVar($pluralName)
            ->setData(array(
				$pluralName => $resources,
				'listStatues' => $listStatues,
				'form' => $form,
				)
			)
        ;

        return $this->handleView($view);
    }

	/**
	 * 
	 * @Secure(roles="ROLE_POST_ADD")
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @return type
	 */
    public function createAction(Request $request)
    {
        $config = $this->getConfiguration();
		// 用来上传图片的临时id，用来创建临时文件夹
		$temp_id = 'temp-'.md5(mt_rand(1, time()));

        $resource = $this->createNew();
		$options = array(
    		'em' => $this->getDoctrine()->getManager(),
			'user' => $this->getCurrentUser(),
			'category' => $this->makeTree($this->getCategoryTree(), array()),
			'categoryother' => $this->makeTree($this->getCategoryOtherTree(), array()),
			'area' => $this->makeTree($this->getAreaTree(), array(), 1),
			'createAt' => new \DateTime(),
			'entity' => $resource,
			'attr' => array(),
		);

		// 根据当前的用户所在的组动态设置显示的上传文章选项
		$options = $this->setOptionsByRole($options);

		$form = $this->createForm(new PostType(), $resource, $options);
		$form->get('user')->setData($this->get('security.context')->getToken()->getUser());

        if (($request->isMethod('POST') || $request->isMethod('PUT') ) && $form->bind($request)->isValid()) {
			// 更新文章标签
			$gospel_post = $request->get('gospel_post');
			$tagsCommaString = $gospel_post['tagsCommaString'];
			$this->updateTag($tagsCommaString, $resource);

			// 更新文章的相关文章
			$this->updateRelated($request, $resource);
			
			// 创建post
			$this->setUser($resource);
            $this->create($resource);

			// 更新文章的图片
			$this->updatePostImages($resource);
            $this->setFlash('success', 'create');

			// 更新redis首页缓存
			$this->flushPostCache($resource);

			return $this->redirect($this->generateUrl('gospel_post_home'));
        }

        if ($config->isApiRequest()) {
            return $this->handleView($this->view($form));
        }

		// 获取所有的观点专栏的id
		$categoryIdColumnViews = 116;
		$categoryColumnIds = $this->getCategoryIds($categoryIdColumnViews);

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('create.html'))
            ->setData(array(
                $config->getResourceName() => $resource,
                'form'                     => $form->createView(),
				// 传递到另外一个地方的表单
                'form2'                     => $form->createView(),
                'temp_id'                     => $temp_id,
				'categoryColumnIds' => implode(',', $categoryColumnIds),
            ))
        ;

        return $this->handleView($view);
    }
	/**
	 * 
	 * @Secure(roles="ROLE_POST_EDIT, ROLE_POST_EDIT_OWN")
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @return type
	 */
    public function updateAction(Request $request)
    {
        $config = $this->getConfiguration();

        $resource = $this->findOr404();

		// 获取所有的观点专栏的id
		$categoryIdColumnViews = 116;
		$categoryColumnIds = $this->getCategoryIds($categoryIdColumnViews);

		// 检查访问权限
		// 如果是只能够修改自己的文章的情况下，检查该文章是否属于当前登录用户，如果是不属于
		// 扔出异常
		if ($this->hasRole('ROLE_POST_EDIT_OWN')) {
			if ($resource->getUser()->getId() != $this->getUser()->getId()) {
				throwException(new Exception('没有修改权限'));
			}
		}
		$user = $resource->getUser();
		if(!$user instanceof \Gospel\Bundle\UserBundle\Entity\User) {
			$user = $this->get('security.context')->getToken()->getUser();
		}
		$options = array(
    		'em' => $this->getDoctrine()->getManager(),
			'user' => $user, // 读取文章的用户
			'category' => $this->makeTree($this->getCategoryTree(), array()),
			'categoryother' => $this->makeTree($this->getCategoryOtherTree(), array()),
			'area' => $this->makeTree($this->getAreaTree(), array(), 1),
			'createAt' => $resource->getCreateAt(),
			'entity' => $resource,
			'attr' => array(),
		);
		$options = $this->setOptionsByRole($options);
		$form = $this->createForm(new PostType(), $resource, $options);
		$form2 = $this->createForm(new PostType(), new Post(), $options);

        if (($request->isMethod('PUT') || $request->isMethod('POST')) && $form->bind($request)->isValid()) {
			// 更新文章标签
			$gospel_post = $request->get('gospel_post');
			$tagsCommaString = $gospel_post['tagsCommaString'];
			$this->updateTag($tagsCommaString, $resource);

			// 更新文章的相关文章
			$this->updateRelated($request, $resource);

			$this->setUser($resource);
            $this->update($resource);
			$this->flushPostCache($resource);
            $this->setFlash('success', 'update');

			$this->get('redis')->flushIndex();
			

			return $this->redirect($this->generateUrl('gospel_post_home', array(
				'page' => $this->getRequest()->getSession()->get('page', 1)
			)));
        }

        if ($config->isApiRequest()) {
            return $this->handleView($this->view($form));
        }

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('update.html'))
            ->setData(array(
                $config->getResourceName() => $resource,
                'form'                     => $form->createView(),
                'form2'                     => $form2->createView(),
                'temp_id'                     => $resource->getId(),
				'categoryColumnIds' => implode(',', $categoryColumnIds),
            ))
        ;

        return $this->handleView($view);
    }

	/**
	 * 更新当前图片的相关属性，设置为封面图片或者是添加水印
	 * 使用Ajax方法，当用户在后台修改图片的相关属性的时候自动执行该方法
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @return type
	 */
	public function updatePostImageAction(Request $request)
	{
		$this->getConfiguration();
		$image = $this->getDoctrine()->getRepository('GospelCoreBundle:PostImage')->find($request->get('id', null));
		$data = array();
		$method = $request->get('name', null);
		$value = $request->get('value', null);
		// 有两种情况,two conditions：
		// - 添加文章的时候，图片信息是保存在临时session里面 ( when create post, the image 
		// 	data is stored in the session
		// - 修改文章的时候：图片信息保存在数据库里面 ( when update post, the image data is
		//  stored in the database
		if ($image) { // 如果图片已经保存在数据库
			// 如果是设置封面图片，首先将其他的封面图片设置为0
			if ($method == 'setIsCover') {
				$images = $image->getPost()->getImages();
				foreach ($images as $otherImage) {
					$otherImage->setIsCover(0);
				}
			}

			if ($method) {
				$image->{$method}($value);
				$this->getDoctrine()->getManager()->persist($image);
				// before each flush need getConfiguration() statement
			}
			$this->getConfiguration();
			$this->getDoctrine()->getManager()->flush();
			$data['success'] = 1;
			$data['msg'] = $request->get('id').'-'.$method.'-'.$value;
		} else { // 图片在数据库里面不存在，检查是否存在临时图片的id，如果存在，更新临时数据
			$session = $this->getRequest()->getSession();
			// 取得图片的临时id
			$tmpID = $session->get('tempId', null);
			if ($tmpID) {
				// get imageName
				$imageName = urldecode($request->get('imageName', null));
				$nameExp = explode('/', $imageName);
				$currentImageName = '';
				if (is_array($nameExp)) {
					$currentImageName = array_pop($nameExp);
				}
				$filesData = $session->get('tempUpload', array());
				foreach ($filesData as $k => $file) {
					if ($method == 'setIsCover') { 	// set as cover, unset the rest
													// files to not cover
						if ($currentImageName == $file->name) {
							$file->isCover = 1; 
						} else {
							$file->isCover = 0; 
						}
					}
					if ($method == 'setUseWatermark') {
						if ($currentImageName == $file->name) {
							$file->useWatermark = $value; 
						}
					}
					if ($method == 'setCaption') {
						if ($currentImageName == $file->name) {
							$file->caption = $value; 
						}
					}

					$filesData[$k] = $file;
				}
			}
		}
        $view = $this
            ->view()
			->setFormat('json')
			->setData($data)
        ;

        return $this->handleView($view);
	}

	/**
	 * 处理图片批量上传的方法，包括图片的添加，删除
	 * path: gospel_post_file_upload
	 * path 必须以'/'结尾
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function FileUploadAction(Request $request)
	{
		// 当前图片所属的文章的id
		$id = $request->get('id', null);

		// 读取上传类
		$upload = $this->getUploader($id);

		$repo = $this->getDoctrine()->getRepository('GospelCoreBundle:PostImage');
		$repoPost = $this->getDoctrine()->getRepository('GospelCoreBundle:Post');
		$em = $this->getDoctrine()->getManager();


		// 当id为空的时候，为添加新的post的情况
		// 该临时id用来临时上传文件使用，当文件上传结束之后
		// 需要
		// 1. 把该文件夹里面的文件移动到相对应的post的文件夹里面
		// 2. 把该文件夹下面的文件写入到数据库
		// 3. 删除该文件夹
		$session = $this->getRequest()->getSession();
//		$logger = $this->get('logger');
		switch($request->getMethod()) {
			case 'POST':
				$files = $upload->postFiles;
				// 如果是添加文章的时候
				if (strpos($id, 'temp') === 0) {
					// 设置临时id
					$session->set('tempId', $id);
					// 更新session数据
					$temp_data = $session->get('tempUpload', array());
//    				$logger->info('count temp data:'.print_r($session->get('tempUpload'), true));
					// 取得最后一个文件的order值
					if(count($temp_data)>0) {
						$tmp_v = $temp_data;
						$last_v = array_pop($tmp_v);
						$order = $last_v->order + 1;
					} else {
						$order = 1;
					}
					foreach($files as $file) {
						$file->order = $order++;
						$temp_data[$file->name] = $file;
					}

					$session->set('tempUpload', $temp_data);
				} else {
					// 修改文章的时候
					$imageE = $repo->findBy(array('post' => $repoPost->find($id)), array('sortOrder' => 'DESC'));
					$image_f = array_shift($imageE);
					$order = isset($image_f)? $image_f->getSortOrder(): 0;
					foreach($files as $file) {
						$image = new PostImage();
						$image->setName($file->name);
						$image->setCaption($file->caption);
						if (isset($file->isCover)) {
							$image->setIsCover($file->isCover);
						}
						if (isset($file->useWatermark)) {
							$image->setUseWatermark($file->useWatermark);
						}
						$image->setPost($repoPost->find($id));
						$image->setSortOrder(++$order);
						$em->persist($image);
					}
					$this->getConfiguration();
					$em->flush();
				}
				break;
		
			case 'DELETE':	// 删除文件的时候同步更新文件
				$files = $upload->getDeleteData();

				// 如果是添加文章的时候上传文件，把文件信息保存在session里面
				if (strpos($id, 'temp') === 0) {
					$temp_data = $session->get('tempUpload', array());
					foreach($files as $file) {
						unset($temp_data[$file]);
					}
					$session->set('tempUpload', $temp_data);

				} else { // 如果是修改文章的时候需要动态更新数据库的
					foreach($files as $file) {
						$imageE = $repo->findBy(array('post' => $repoPost->find($id)), array('sortOrder' => 'DESC'));
						foreach($imageE as $entity) {
							if($entity->getName() == $file) {
								$em->remove($entity);
								$this->getConfiguration();
								$em->flush();
							}
						}
					}
				}
				break;
			case 'GET':
				// 如果是添加文章的时候
				if (strpos($id, 'temp') === 0) {
				} else {//这个是在修改文章的情况下执行的代码
					$post = $repoPost->find($id);
					$imagesInDb = $repo->findBy(array('post' => $post), array('sortOrder' => 'ASC'));
					$images = $upload->get();
					$return = array('files' => array());
					foreach($imagesInDb as $row) {
						if(is_object($row)) {
							foreach($images['files'] as $image) {
								if($row->getName() == $image->name) {
									$image->id = $row->getId();
									$image->caption = $row->getCaption();
									$image->useWatermark = $row->getUseWatermark();
									$image->isCover = $row->getIsCover();
									$return['files'][] = $image;
								}
							}
						}
					}
					$upload->generate_response($return);
				}
			default:
				;
		}


		return new Response('');
	}

	/**
	 * 后台拖拽图片的时候进行图片排序的动作
	 */
	public function SortImageAction(Request $request)
	{
		// 当前图片所属的文章的id
		$id = $request->get('id', null);
		// 如果包含有images数组，则该条件下是对图片进行重新排序
		$images = $request->get('images', null);
		if ($images == null) return new Response('');

		// 读取上传类
		$upload = $this->getUploader($id);

		$repo = $this->getDoctrine()->getRepository('GospelCoreBundle:PostImage');
		$repoPost = $this->getDoctrine()->getRepository('GospelCoreBundle:Post');
		$em = $this->getDoctrine()->getManager();

		$session = $this->getRequest()->getSession();
		$files = $upload->postFiles;
		// 如果是添加文章的时候
		if (strpos($id, 'temp') === 0) {
			// 设置临时id
			$session->set('tempId', $id);
			// 更新session数据
			$temp_data = $session->get('tempUpload', array());
			if ($images) { // if is sorting images, then use this resort temp_data
				foreach ($images as $imageName) {
					foreach($files as $file) {
						if (basename($imageName == $file->name)) {
							$temp_data[$file->name] = $file;
							$file->order = $order++;
						}
					}
				}
			}
			$session->set('tempUpload', $temp_data);
		} else {
			// 修改文章的时候
			$imageE = $repo->findBy(array('post' => $repoPost->find($id)), array('sortOrder' => 'DESC'));
			$post = $repoPost->find($id);
			$postImages = $post->getImages();
			$order = 1;
			foreach ($images as  $image) {
				foreach ($imageE as $postImage) {
					if (urldecode(basename($image)) == $postImage->getName()) {
						$postImage->setSortOrder($order++);
						$em->persist($postImage);
						$em->flush();
					}
				}
			}
			$this->getConfiguration();
			$em->flush();
		}

		return new Response('');
	}

	/**
	 * 
	 * @param type $id
	 * @return \Gospel\Bundle\CoreBundle\Classes\FileUploadHandler
	 */
	private function getUploader($id = null)
	{
		// 如果是修改文章的时候
		if (strpos($id, 'temp') !== 0) {
			$post = $this->getDoctrine()->getRepository('GospelCoreBundle:Post')->find($id);
			$path = $post->getCreateAt()->format("Ym") . DIRECTORY_SEPARATOR . $id ;
		} else {
			$path = $id;
		}
		$options = array(
            'script_url' => $this->generateUrl('gospel_post_file_upload', array('id'=>$id)),
			'path' => Post::getUploadImagePath().$path.'/',
		);
		return new FileUploadHandler($options);
	}

	/**
	 * Check if the tag already in the database, if not has, then create a new 
	 * tag and then add this tag to the new resource
	 * 
	 * @param type $request
	 * @param type Post
	 */
	private function updateTag($tagsCommaString, $resource)
	{
		if($tagsCommaString) {
			$repo = $this->getDoctrine()->getRepository('GospelCoreBundle:Tag');
			$em = $this->getDoctrine()->getManager();
			foreach (array(',', '，') as $del) {
				foreach(explode($del, $tagsCommaString) as $tagName) {
					if(null === ($tag = $repo->findOneBy(array('name' => $tagName)))) {
						$tag = new Tag();
						$tag->setName($tagName);
						$tag->setCount(1);
						$em->persist($tag);
						$this->getConfiguration();
						$em->flush();
					}
					$tags = $resource->getTags();
					if(!$tags->contains($tag)) {
						$resource->addTag($tag);
					}

					//增加tag的count
					$tag->setCount($tag->getCount()+1);
					$this->getDoctrine()->getManager()->persist($tag);
					$this->getConfiguration();
					$this->getDoctrine()->getManager()->flush();
				}
			}
		}
		return $resource;
	}

	private function updateRelated($request, $resource)
	{
		$gospel_post = $request->get('gospel_post');
		preg_match_all("/\[(\d*)\]/", $gospel_post['relatedPosts'], $ids);
		$related = $ids[1];

		$resource->getRelated()->clear();
		if(count($related)>0) {
			foreach($related as $relate) {
				$resource->addRelated($this->getRepository()->find($relate));
			}
		}
		return $resource;
	}

	public function getAreaTree()
	{
		$repository = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:Area');
		$arrayTree = $repository->childrenHierarchy();
		return $arrayTree;
	}

	public function getCategoryTree()
	{
		$repository = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:Category');
		$arrayTree = $repository->childrenHierarchy();
		return $arrayTree;
	}

	public function getCategoryOtherTree()
	{
		$repository = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:CategoryOther');
		$arrayTree = $repository->childrenHierarchy();
		return $arrayTree;
	}

	/**
	 * 生成树状结构,根据所在的深读在前面添加"——"
	 */
	public function makeTree($data, $arr, $maxlevel = 10000) 
	{
		foreach($data as $row) {
			if ($maxlevel <= $row['lvl']) {
				continue;
			}
			$arr[$row['id']] = str_repeat("——", $row['lvl']).$row['title'];
			if(is_array($row['__children']) && count($row['__children']) > 0) {
				$arr = $this->makeTree($row['__children'], $arr, $maxlevel);
			}
		}
		return $arr;
	}

	/**
	 * Load data form the old database table
	 * @return type
	 */
	public function loadAction(Request $request)
	{
		// 开始载入的文章id
		$start = $request->get('start', 0);

		// 一次载入的文章数
		$total = $request->get('total', 10);

		$repositoryPost = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:Post');
		$repositoryArea = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:Area');
		$repositoryCategoryArea = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:AreaCategory');
		$repositoryCategory = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:Category');
		$repositoryTag = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:Tag');
		$repositoryAuthor = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:Author');
		$repositoryUser = $this->getDoctrine()->getManager()->getRepository('GospelUserBundle:User');
        $config = $this->getConfiguration();
		$customerEm =  $this->get('doctrine')->getManager('customer');
		$em =  $this->get('doctrine')->getManager('default');
		$result = $repositoryPost->findAllOld($customerEm, $total, $start);
		$msg = "";
//		$updateExistsPost = false;
		$updateExistsPost = true;
		if ($result) {
			$continue = 1;
		} else {
			$continue = 0;
		}
		foreach($result as $row) {
			$date = new \DateTime("@".$row['created']);
			$resource = $repositoryPost->find($row['id']);
			if(null === $resource) {
				echo 'Post:'.$row['id']." in processing...\n<br/>";
				$msg .= 'Post:'.$row['id']." created\n<br/>";
				if ($row['cat_id'] == 0) {
					$categoryName = '其他';
				} else {
					$categoryName = $repositoryPost->findOneByOldCategoryId($customerEm, $row['cat_id']);
				}
				$resource = new Post($row['id']);
				$resource->setTitle($row['title']);
				$resource->setIntroduction($row['intro']);
				$resource->setContent(html_entity_decode($row['content']));
				$resource->setCommentStatus(1);
				$categories = $repositoryCategory->findBy(array('title'=>$categoryName));
				// set the category to the child of the same category name
				if(count($categories) > 1) {
					$category0 = $categories[0];
					$category1 = $categories[1];
					if(is_object($category1->getParent()) && ($category1->getParent()->getId() == $category0->getId())) {
						$category = $category1;
					} else {
						$category = $category0;
					}
				} else {
					$category = array_pop($categories);
				}

				// 如果一级地区没有设置，则设置为其他地区
				if ($row['area_id'] == 0) {
					$row['area_id'] = $repositoryCategoryArea->findOneBy(array('title' => '无地区'))->getId();
				}

				// set area
				if(isset($row['province_id']) && $row['province_id']) {
					$provinces = explode(',', $row['province_id']);
					if (count($provinces) == 3) {
						$provinceName = isset($provinces[2])?$provinces[2]:'';
						$oldArea = $repositoryPost->findOldArea($customerEm, $provinceName);
						$area = $repositoryArea->findOneBy(array('title'=>$provinceName));
						if($area instanceof \Gospel\Bundle\CoreBundle\Entity\Area) {
							$em->persist($area);
							$this->getConfiguration();
							$em->flush();
							$resource->setArea($area);
						}
					}
				}

				// 导入作者
				// 一共有两种情况，一个是发布文章的人，另外一个是撰稿人
				// 首先处理撰稿人
				$authorName = $row['author'];
				//检查该作者是否存在，如果不存在则创建该作者
				if ($authorName) {
					$authorObject = $repositoryAuthor->findOneBy(array('title'=>$authorName));
					if($authorObject) {
						$resource->setAuthor($authorObject);
					}
				}
				
				//关键词
				$resource->setMetaKeywords($row['keywords']);

				// 标签
				$tagString = $row['tags'];
				$tagsData = array();
				$delimeters = array(',','，',' ','-','_','，',' ');
				foreach($delimeters as $del) {
					$tags_explode = explode($del, $tagString);
					$tagsData = array_merge($tagsData, $tags_explode);
				}
				// 暂时不导入文章标签
//				$resource = $this->updateTag(implode(',', array_unique($tagsData)), $resource);

				// 导入用户
				$u_id = 1;
				$user = $repositoryUser->find($u_id);
				$resource->setUser($user);
				$this->setUser($resource);

				// 设置点击量
				$resource->setHits($row['hits']);

				// 文章是否显示
				$resource->setStatus($row['is_show']);

				// 文章来源
				$resource->setComefrom($row['source']);

				$resource = $this->setPostFields($resource, $row);

				$resource->setCategory($category);
				$resource->setPromotionStatus(0);
				$resource->setStatus($row['is_show']);
				$resource->setCreateAt($date);
				$resource->setUpdatedAt($date);
				$resource->setPublishedAt($date);
				$this->create($resource);

				// 导入图片
				// 首先读取旧的数据表里面的相关图片信息
				// 然后根据旧图片的位置移动到新的图片的位置
				$fs = new Filesystem();
				$oldImages = $repositoryPost->findOldImages($customerEm, $row['id']);
				if(is_array($oldImages)) {
					foreach($oldImages as $oldImage) {
						$image = new PostImage();
						$image->setName(basename($oldImage['image']));
						$image->setPost($resource);
						$image->setSortOrder($oldImage['sequence']);
						$image->setCaption($oldImage['intro']);
						$image->setUseWatermark(1);
						$em->persist($image);
						$this->getConfiguration();
						$em->flush();

						// 移动图片到新的位置
						$oldImageFile = $this->getOldImageRoot() . '/' . $oldImage['image'];
						$newImageFoler = $this->getNewPostImageRoot() . '/'.$date->format('Ym').'/'
								.$resource->getId().'/'
								;
						$newImageFile = $newImageFoler . basename($oldImage['image']);
						if (!file_exists($newImageFile)) {
							$fs->mkdir($newImageFoler);
						}
						if(file_exists($oldImageFile)) {
							$fs->copy($oldImageFile, $newImageFile);
						}
						$fileUploader = $this->getUploader($resource->getId());
						$options = $fileUploader->options;
						$options['max_width'] = 80;
						$options['max_height'] = 80;
						$fileUploader->create_scaled_image(basename($oldImage['image']), 'thumbnail', $options);
//						$options['max_width'] = 800;
//						$options['max_height'] = 600;
//						$options['jpeg_quality'] = 90;
//						$fileUploader->create_scaled_image(basename($oldImage['image']), 'medium', $options);
					}
				}
			} else {
				if($updateExistsPost) {
					$msg .= "更新文章: ".$row['id'].':'.$row['title']."\n<br/>";

					// 导入图片
					// 首先读取旧的数据表里面的相关图片信息
					// 然后根据旧图片的位置移动到新的图片的位置
					$fs = new Filesystem();
					$oldImages = $repositoryPost->findOldImages($customerEm, $row['id']);
					if(is_array($oldImages)) {
						foreach($oldImages as $oldImage) {
							$image = new PostImage();
							$image->setName(basename($oldImage['image']));
							$image->setPost($resource);
							$image->setSortOrder($oldImage['sequence']);
							$image->setCaption($oldImage['intro']);
							$image->setUseWatermark(1);
							$em->persist($image);
							$this->getConfiguration();
							$em->flush();

							// 移动图片到新的位置
							$oldImageFile = $this->getOldImageRoot() . '/' . $oldImage['image'];
							$newImageFoler = $this->getNewPostImageRoot() . '/'.$date->format('Ym').'/'
									.$resource->getId().'/'
									;
							$newImageFile = $newImageFoler . basename($oldImage['image']);
							if (!file_exists($newImageFoler)) {
								$fs->mkdir($newImageFoler);
							}
							if(file_exists($oldImageFile)) {
								$fs->copy($oldImageFile, $newImageFile);
							}
							$fileUploader = $this->getUploader($resource->getId());
							$options = $fileUploader->options;
							$options['max_width'] = 80;
							$options['max_height'] = 80;
							$fileUploader->create_scaled_image(basename($oldImage['image']), 'thumbnail', $options);
	//						$options['max_width'] = 800;
	//						$options['max_height'] = 600;
	//						$options['jpeg_quality'] = 90;
	//						$fileUploader->create_scaled_image(basename($oldImage['image']), 'medium', $options);
						}
					}

					// 更新用户的密码，现有程序的一个bug，密码会被覆盖
//					$this->setUser($resource);
//					$resource->setContent(html_entity_decode($row['body']));
//					$this->setPostFields($resource, $row);
//					$resource->setWriter($row['reporter_name']);
					$em->persist($resource);
					$this->getConfiguration();
					$em->flush();
				}
			}
		}

		$data['success'] = 1;
		$data['msg'] = $msg;
		$data['start_id'] = (int) $start;
		$data['continue'] = $continue;
		 $view = $this
            ->view()
			->setFormat('json')
			->setData($data)
        ;

        return $this->handleView($view);
//		$view = $this
//				->view()
//				->setTemplate($config->getTemplate('load.html'))
//				->setData(array(
//					'data' => $result,
//					'msg' => $msg,
//				))
//				;
//		return $this->handleView($view);
	}
	/*
	 * @return type Post
	*/

	/**
	 * 
	 * @param \Gospel\Bundle\CoreBundle\Entity\Post $resource
	 * @param type $row
	 * @return \Gospel\Bundle\CoreBundle\Entity\Post
	 */
	private function setPostFields(Post $resource, $row)
	{
		// 设置相关文章
		foreach(explode(',', $row['relative_article']) as $relate) {
			$relatedPost = $this->getRepository()->find($relate);
			if($relatedPost) {
				$resource->addRelated($relatedPost);
			}
		}
		return $resource;
	}

	public function saveRow($row, $parent)
	{
	}

	/**
	 * 查看相关文章，可以根据不同的字段进行检索
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @return type
	 */
	public function relatedAction(Request $request)
	{
		$config = $this->getConfiguration();
        $criteria = $config->getCriteria();

		$keyword = $request->get('keywords', null);
		$gospel = $request->get('gospel_post', array());
		foreach($gospel as $k => $p) {
			$criteria[$k] = $p;
		}
		$type = $request->get('type', null);
		if($keyword) {
			$criteria[$type] = array('data'=>$keyword,'type'=>'like');
		}

        $sorting = $config->getSorting();

        $pluralName = $config->getPluralResourceName();
        $repository = $this->getRepository();

        if ($config->isPaginated()) {
            $resources = $this
                ->getResourceResolver()
                ->getResource($repository, $config, 'createPaginator', array($criteria, $sorting))
            ;

            $resources
                ->setCurrentPage($request->get('page', 1), true, true)
                ->setMaxPerPage($config->getPaginationMaxPerPage())
            ;
        } else {
            $resources = $this
                ->getResourceResolver()
                ->getResource($repository, $config, 'findBy', array($criteria, $sorting, $config->getLimit()))
            ;
        }

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('related.html'))
            ->setTemplateVar($pluralName)
            ->setData($resources)
        ;

        return $this->handleView($view);
	}

	/**
	 * 在添加文章的时候更新图片的内容
	 * 目的是保存临时文件夹里面的文件到目标文件夹
	 * 同时清理临时数据
	 * 根据session的临时数据读取图片的相关信息
	 */
	private function updatePostImages($resource)
	{
		$session = $this->getRequest()->getSession();
		$tempIdInSession = $session->get('tempId', null);
		// 保存图片到数据库
		if($tempIdInSession) {
			$temp_data = $session->get('tempUpload', array());
			foreach($temp_data as $file) {
				$image = new PostImage();
				$image->setName($file->name);
				$image->setCaption($file->caption);
				if (isset($file->useWatermark)) {
					$image->setUseWatermark($file->useWatermark);
				}
				if (isset($file->isCover)) {
					$image->setIsCover($file->isCover);
				}
				$image->setPost($resource);
				$image->setSortOrder($file->order);
				$this->getDoctrine()->getManager()->persist($image);
			}
			$this->getConfiguration();
			$this->getDoctrine()->getManager()->flush();

			// 重命名文件加到对应的文件下面
			$session = $this->getRequest()->getSession();
			$uploader = $this->getUploader($session->get('tempId'));
			$options = $uploader->getOptions();
			$dir = $options['upload_dir'].DIRECTORY_SEPARATOR.$resource->getCreateAt()->format('Ym');
			$temp_path = dirname($dir);
			$targetDir = dirname(dirname($dir)).DIRECTORY_SEPARATOR
					.$resource->getCreateAt()->format('Ym').DIRECTORY_SEPARATOR.$resource->getId();
			$fs = new Filesystem();
			$finder = new Finder();

			if(!is_dir($targetDir)) {
				$fs->mkdir($targetDir);
				$fs->mkdir($targetDir . '/thumbnail');
			}
			$finder->files()->in($temp_path. '/thumbnail');
			foreach ($finder as $file) {
				@copy($file, $targetDir . '/thumbnail/' . basename($file));
				@unlink($file);
			}
			$finder->files()->in($temp_path);
			foreach ($finder as $file) {
				@copy($file, $targetDir . '/' . basename($file));
				@unlink($file);
			}

			// 清除所有的临时文件夹
			
			//$finder->directories()->depth('== 1')->name('/temp.*/')->in(dirname(dirname($dir)));
			/*
			foreach ($finder as $file) {
				$fs->remove($file);
			}
			$fs->remove($temp_path. '/thumbnail');
			$fs->remove($temp_path);
			*/

		}
		// 清理临时id
		$session->set('tempId', null);
		$session->set('tempUpload', null);
	}

    /**
	 * @Secure(roles="ROLE_POST_DELETE")
     * Delete resource.
     */
    public function deleteAction()
    {
        $resource = $this->findOr404();
		$em = $this->getDoctrine()->getManager();
		foreach($resource->getImages() as $image) {
			$em->remove($image);
			$this->getConfiguration();
			$em->flush();
		}

		// 清除所有的临时文件夹
		$uploader = $this->getUploader($resource->getId());
		$options = $uploader->getOptions();

		// 取得上传文件夹
		$dir = $options['upload_dir'].DIRECTORY_SEPARATOR.$resource->getCreateAt()->format('Ym');

		// 如果存在图片文件路径，则删除该文件夹
		if(is_dir($dir)) {
			$fs = new Filesystem();
			$finder = new Finder();
			$finder->files()->in($dir);
			foreach ($finder as $file) {
				$fs->remove($file);
			}
			rmdir($dir);
		}

		// 更新redis首页缓存
		$this->flushPostCache($resource);
        $this->delete($resource);
        $this->setFlash('success', 'delete');

        return $this->redirectToIndex($resource);
    }

	/**
	 * 当修改或者保存的时候用户的密码会被修改，通过该方法确保密码没有被修改
	 * @param type $resource
	 */
	public function setUser($resource) {

			$stmt = $this->getDoctrine()
                   ->getConnection()
                   ->prepare('SELECT * FROM  gospel_users WHERE id = '.$resource->getUser()->getId());
      $stmt->execute();
      $user = $stmt->fetchAll();
	  $user = $user[0];
	  $userEn = $resource->getUser();
	  $userEn->setPassword($user['password']);
	  $resource->setUser($userEn);
	  return $resource;
	}

	/**
	 * 移动图片的时候旧的图片文件夹的地址
	 * @return string
	 */
	public function getOldImageRoot()
	{
		if ($_SERVER['SERVER_ADDR'] == '127.0.0.1') {
			return '/home/wwwroot/christiantimes/public_html/upload/';
//			return '/var/www/www.gospeltimes.cn/data/';
		} else {
			return '/home/wwwroot/christiantimes/public_html/upload/';
		}
	}

	/**
	 * 移动图片的时候新的图片文件夹的地址
	 * @return string
	 */
	public function getNewPostImageRoot()
	{
		if ($_SERVER['SERVER_ADDR'] == '127.0.0.1') {
			return '/data/www/christiantimes.cn/web/assets/media/post';
//			return '/var/www/gospeltimes.dev/symfony/web/assets/media/post';
		} else {
			return '/data/www/christiantimes.cn/web/assets/media/post/';
		}
	}

	/**
	 * 根据当前的id位置自动的载入接下来的文章,通过ajax请求
	 * @return type
	 */
	public function autoLoadAction()
	{
        $config = $this->getConfiguration();
		$view = $this
				->view()
				->setTemplate($config->getTemplate('autoLoad.html'))
				->setData(array(
				))
				;
		return $this->handleView($view);
	}

	public function updateProvinceListAction(Request $request)
	{
		$data = array('html' => '');
		$regionId = $request->get('regionId', null);
		if ($regionId) {

			$region = $this->getDoctrine()->getRepository('GospelCoreBundle:AreaCategory')->find($regionId);
			// 取得该分类下面的所有的地区分类
			$areas = $this->getDoctrine()->getRepository('GospelCoreBundle:Area')->findBy(array(
				'category' => $region,
			));
			$html = '';
			foreach ($areas as $row) {
				$html .= "<option value='".$row->getId()."'>".$row->getTitle()."</option>";
			}
			$data['html'] = $html;
		}
		 $view = $this
            ->view()
			->setFormat('json')
			->setData($data)
        ;

        return $this->handleView($view);
	}

	/**
	 * 
	 * @param Post $post
	 * @return array post related urls array
	 */
	protected function getPostUrls(Post $post)
	{
		$slugEn = GospelExtension::slugfyFilter($post->getAlias('en'));
		if(!$slugEn) {
			$slugEn = 'slug';
		}
		return array(
			$this->generateUrl('home'),
			/* zh_cn local url */
			$this->generateUrl(
						'news_show', array(
							'id'=>$post->getId(),'slug'=>GospelExtension::slugfyFilter($post->getAlias()
						))),
			/** en locale url */
			$this->generateUrl(
						'news_show', array(
							'id'=>$post->getId(),'slug'=>$slugEn
						)),
			$this->generateUrl(
						'news_preview', array(
							'id'=>$post->getId(),'slug'=>GospelExtension::slugfyFilter($post->getAlias()), 'preview' => true)
					),
		);
	}

	/**
	 *@param Post $entity
	 */
	public function flushPostCache($entity)
	{
		// 更新redis缓存
		$this->get('redis')->flushIndex();

		// 更新varnish缓存
		//$this->get('varnish_cache_updater')->purge($this->generateUrl('home'));
		$postRelatedUrls = $this->getPostUrls($entity);
		foreach($postRelatedUrls as $url) {
			//$this->get('varnish_cache_updater')->purge($url);
		}
	}

	private function getCategoryIds($categoryId) {
		$repoCategory = $this->getDoctrine()->getRepository('GospelCoreBundle:Category');
		$categoryColumn = $repoCategory->find($categoryId);
		$ids = [$categoryId];
		foreach ($categoryColumn->getChildren() as $subCategory) {
			$ids[] = $subCategory->getId();
		}
		return $ids;
	}

	/**
	 * 到处最新的一百条信息，点击量最高的2017年
	 */
	public function exportAction(Request $request) {
		$year = $request->get('year', '2018');
		$all = $request->get('all', true);
		if (!$all) {
			$count = $request->get('count', 300);
		} else {
			$count = 'all';
		}
		$startData = $year . '-01-01';
		$em = $this->getManager();
		$qb = $em->createQueryBuilder();
		$qb->select('m')
                 ->from('GospelCoreBundle:Post', 'm')
                 ->where('m.publishedAt > :last')
                 ->andWhere('m.status = 1')
				->orderBy('m.hits', 'desc')
                 ->setParameter('last', new \DateTime($startData), \Doctrine\DBAL\Types\Type::DATETIME)
				;
		if (!$all) {
			$qb->setMaxResults($count);
		}
		$data = $qb->getQuery()->getResult();
		


		$config = $this->getConfiguration();
        $pluralName = $config->getPluralResourceName();
		$exportName = 'ct-' . $year . '-top-' . $count;
		// output the column headings
		$response = $this->render($config->getTemplate('csv.html'),array(
			$pluralName => $data
		));

		$response->headers->set('Content-Type', 'text/csv');
		$response->headers->set('Content-Disposition', 'attachment; filename="'.$exportName.'.csv"');
		return $response;

	}
}
