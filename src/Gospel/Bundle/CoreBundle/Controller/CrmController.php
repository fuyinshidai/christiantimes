<?php

namespace Gospel\Bundle\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Gospel\Bundle\CoreBundle\Form\CrmType;
use Gospel\Bundle\CoreBundle\Form\CrmSearchType;
use Gospel\Bundle\CoreBundle\Entity\Crm;
use JMS\SecurityExtraBundle\Annotation\Secure;

/**
 *
 * @crm Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class CrmController extends ResourceController
{
    /**
     * Get collection (paginated by default) of resources.
     * @Secure(roles="ROLE_CRM_MANAGER")
     */
    public function indexAction(Request $request)
    {
        $config = $this->getConfiguration();

        $resource = $this->createNew();
		$form = $this->createForm(new CrmSearchType(), $resource, array(
    		'em' => $this->getDoctrine()->getManager(),
			'area' => $this->makeTree($this->getAreaTree(), array(), 1),
		));

        $criteria = $config->getCriteria();
		$gospel = $request->get('gospel_crm_search', array());
		$likes = array('title', 'email', 'phone');
		$others = array('area', 'gender');
		foreach($gospel as $field => $val) {
			if($val && in_array($field, $likes)) {
				$val = array(
					'type' => 'like',
					'data' => $val,
				);
				$criteria[$field] = $val;
			}
			if($val && in_array($field, $others)) {
				$criteria[$field] = $val;
			}
		}
        $sorting = $config->getSorting();

        $pluralName = $config->getPluralResourceName();
        $repository = $this->getRepository();

        if ($config->isPaginated()) {
            $resources = $this
                ->getResourceResolver()
                ->getResource($repository, $config, 'createPaginator', array($criteria, $sorting))
            ;

            $resources
                ->setCurrentPage($request->get('page', 1), true, true)
                ->setMaxPerPage($config->getPaginationMaxPerPage())
            ;
        } else {
            $resources = $this
                ->getResourceResolver()
                ->getResource($repository, $config, 'findBy', array($criteria, $sorting, $config->getLimit()))
            ;
        }

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('index.html'))
            ->setTemplateVar($pluralName)
            ->setData(array(
				$pluralName => $resources,
				'form' => $form,
				)
			)
        ;

        return $this->handleView($view);
    }

	/**
     * @Secure(roles="ROLE_CRM_MANAGER")
	 * 
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @return type
	 */
    public function createAction(Request $request)
    {
        $config = $this->getConfiguration();

        $resource = $this->createNew();
		$form = $this->createForm(new CrmType(), $resource, array(
    		'em' => $this->getDoctrine()->getManager(),
			'area' => $this->makeTree($this->getAreaTree(), array(), 1),
			'createAt' => new \DateTime(),
			'entity' => $resource,
		));

        if (($request->isMethod('POST') || $request->isMethod('PUT') ) && $form->bind($request)->isValid()) {
			// 创建
            $this->create($resource);
            $this->setFlash('success', 'create');

            return $this->redirectTo($resource);
        }

        if ($config->isApiRequest()) {
            return $this->handleView($this->view($form));
        }

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('create.html'))
            ->setData(array(
                $config->getResourceName() => $resource,
                'form'                     => $form->createView(),
				// 传递到另外一个地方的表单
                'form2'                     => $form->createView(),
            ))
        ;

        return $this->handleView($view);
    }

	/**
     * @Secure(roles="ROLE_CRM_MANAGER")
	 */
    public function updateAction(Request $request)
    {
        $config = $this->getConfiguration();

        $resource = $this->findOr404();
		$form = $this->createForm(new CrmType(), $resource, array(
    		'em' => $this->getDoctrine()->getManager(),
			'area' => $this->makeTree($this->getAreaTree(), array(), 1),
			'createAt' => $resource->getCreateAt(),
			'entity' => $resource,
		));

        if (($request->isMethod('PUT') || $request->isMethod('POST')) && $form->bind($request)->isValid()) {
            $this->update($resource);
            $this->setFlash('success', 'update');

            return $this->redirectTo($resource);
        }

        if ($config->isApiRequest()) {
            return $this->handleView($this->view($form));
        }

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('update.html'))
            ->setData(array(
                $config->getResourceName() => $resource,
                'form'                     => $form->createView(),
                'form2'                     => $form->createView(),
                'temp_id'                     => $resource->getId(),
            ))
        ;

        return $this->handleView($view);
    }

	public function makeTree($data, $arr) 
	{
		foreach($data as $row) {
			$arr[$row['id']] = str_repeat("——", $row['lvl']).$row['title'];
			if(is_array($row['__children']) && count($row['__children']) > 0) {
				$arr = $this->makeTree($row['__children'], $arr);
			}
		}
		return $arr;
	}

	private function getAreaTree()
	{
		$repository = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:Area');
		$arrayTree = $repository->childrenHierarchy();
		return $arrayTree;
	}

    /**
     * @Secure(roles="ROLE_CRM_MANAGER")
     */
    public function showAction()
    {
        $config = $this->getConfiguration();

        $view =  $this
            ->view()
            ->setTemplate($config->getTemplate('show.html'))
            ->setTemplateVar($config->getResourceName())
            ->setData($this->findOr404())
        ;

        return $this->handleView($view);
    }
}
