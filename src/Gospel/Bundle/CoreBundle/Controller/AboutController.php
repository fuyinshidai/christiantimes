<?php

namespace Gospel\Bundle\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Gospel\Bundle\CoreBundle\Entity\About;
use Gospel\Bundle\CoreBundle\Form\AboutType;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class AboutController extends ResourceController
{
    public function createAction(Request $request)
    {
        $config = $this->getConfiguration();

        $resource = $this->createNew();
		$form = $this->createForm(new AboutType(), $resource, array(
			'entity' => $resource,
		));

        if (($request->isMethod('POST') || $request->isMethod('PUT') ) && $form->bind($request)->isValid()) {
			// 创建专题
            $this->create($resource);

			// 更新文章的图片
            $this->setFlash('success', 'create');

            return $this->redirectTo($resource);
        }

        if ($config->isApiRequest()) {
            return $this->handleView($this->view($form));
        }

        $resource = $this->createNew();

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('create.html'))
            ->setData(array(
                $config->getResourceName() => $resource,
                'form'                     => $form->createView(),
            ))
        ;

        return $this->handleView($view);
    }

    public function updateAction(Request $request)
    {
        $config = $this->getConfiguration();

        $resource = $this->findOr404();
		$form = $this->createForm(new AboutType(), $resource, array(
			'entity' => $resource,
		));

        if (($request->isMethod('PUT') || $request->isMethod('POST')) && $form->bind($request)->isValid()) {
			// 更新文章的相关文章
            $this->update($resource);
            $this->setFlash('success', 'update');

            return $this->redirectTo($resource);
        }

        if ($config->isApiRequest()) {
            return $this->handleView($this->view($form));
        }

        $view = $this
            ->view()
            ->setTemplate($config->getTemplate('update.html'))
            ->setData(array(
                $config->getResourceName() => $resource,
                'form'                     => $form->createView(),
            ))
        ;

        return $this->handleView($view);
    }




}
