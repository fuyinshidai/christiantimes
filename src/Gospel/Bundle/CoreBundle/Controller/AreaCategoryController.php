<?php

namespace Gospel\Bundle\CoreBundle\Controller;

use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Symfony\Component\HttpFoundation\Request;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class AreaCategoryController extends ResourceController
{

	public function deleteAction() {
		$resource = $this->findOr404();

		$entities = $this->getDoctrine()->getRepository('GospelCoreBundle:Post')->findBy(array(
			'region' => $resource,
		));

		$areas = $this->getDoctrine()->getRepository('GospelCoreBundle:Area')->findBy(array(
			'category' => $resource,
		));

		if (count($entities) || count($areas)) {
			$msg = '';
			if (count($entities)) {
				$msg .= "该分两类包含文章，不能直接删除，请首先删除以下图片集或者将以下图片集分类改为非<strong>".$resource->getTitle()."</strong>分类:<br/>";
				foreach ($entities as $entity) {
					$msg .= "id:" . $entity->getId() . " - <a target='_blank' href='".$this->generateUrl('gospel_post_show', array('id'=>$entity->getId()))."'>" . $entity->getTitle()."</a><br/>";
				}
			}

			if (count($areas)) {
				$msg .= "该分两类包含地区，不能直接删除，请首先删除以下图片集或者将以下图片集分类改为非<strong>".$resource->getTitle()."</strong>分类:<br/>";
				foreach ($areas as $entity) {
					$msg .= "id:" . $entity->getId() . " - " . $entity->getTitle()."<br/>";
				}
			}

			$this
            ->get('session')
            ->getFlashBag()
            ->add('error', $msg)
        	;
		} else {
			$this->delete($resource);
			$this->setFlash('success', 'delete');
		}

		return $this->redirect($this->generateUrl('gospel_category_other_index'));
	}
}
