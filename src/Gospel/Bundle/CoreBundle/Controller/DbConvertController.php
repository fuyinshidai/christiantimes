<?php

/*
 * This file is part of the christiantimes.cn. Created Apr 24, 2014. Encoding UTF-8 
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * 
 * This file used to convert database data from old database to the new one
 */

namespace Gospel\Bundle\CoreBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Gospel\Bundle\CoreBundle\Entity\Post;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Filesystem\Filesystem;

class DbConvertController extends FOSRestController
{
	public function autoRunImageCheckAction()
	{
		$view = $this
				->view()
				->setTemplate('GospelCoreBundle:Post:dbConvertCheck.html.twig')
				;

		return $this->handleView($view);
				
	}

	/**
	 * check the new databse image is the same as the old db data
	 * steps:
	 *  - load and loop old data image
	 *  - compare with the new db data, check if they are the same
	 */
	public function checkImageIntegrityAction(Request $request)
	{
		$repositoryPost = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:Post');

		// As the data in db too many, so each time run a short slice
		// 开始载入的文章id
		$start = $request->get('start', 11000);

		// 一次载入的文章数
		$total = $request->get('total', 1000);

		// load the old data
		$customEm = $this->get('doctrine')->getManager('customer');
		$result = $repositoryPost->findAllOld($customEm, $total, $start);

		$msg = '';
		// loop each old row
		foreach ($result as $row) {
			$oldArticleId = $row['id'];
			$post = $repositoryPost->find($oldArticleId);
			if (gettype($post) !== 'object') {
				continue;
			}
			$msg .= $oldArticleId;
			// fetch the images belong to this article id from image table
			$oldImages = $repositoryPost->findOldImagesByArticleId($customEm, $oldArticleId);
			foreach ($oldImages as $image) {
				$this->savePostImage($post, $image);
//				$msg .= print_r($image, true);
			}
		}

		$continue = true;
		$data['success'] = 1;
		$data['msg'] = $msg;
		$data['start_id'] = (int) $start;
		$data['continue'] = $continue;
		$view = $this
            ->view()
			->setFormat('json')
			->setData($data)
        ;

        return $this->handleView($view);
	}

	public function savePostImage($post, $oldImage)
	{
		// First check of the image has been stored in db
		// if not, then save the image to the coresponde post
		$em = $this->getDoctrine()->getManager();
		// find the image by image name
		$imageName = basename($oldImage['image']);
		$imageInDb = $this
				->getDoctrine()
				->getRepository('GospelCoreBundle:PostImage')
				->findOneBy(array(
					'name' => $imageName,
					));
		if (gettype($imageInDb) !== 'object') {
			echo $post->getId() . ':' . $post->getTitle() . ': ' .$imageName . ': not exists;';

			// first copy the image then save into database
//			$oldImageFile = Post::getOldImageRoot()  . '/' . $oldImage['image'];
//			$imageFolder = realpath($this->get('kernel')->getRootDir().'/../web/'.Post::getUploadImagePath());
//			$newImageFoler = $imageFolder
//					. '/'.$post->getCreateAt()->format('Ym').'/'
//								.$post->getId().'/';
//			$newImageFile = $newImageFoler . basename($oldImage['image']);
//			$fs = new Filesystem();
//			if (!file_exists($newImageFile)) {
//				$fs->mkdir($newImageFoler);
//			}
//			if(file_exists($oldImageFile)) {
//				$fs->copy($oldImageFile, $newImageFile);
//			}
//
//			// create image thumbnail
//			$options = array(
//				'script_url' => $this->generateUrl('gospel_post_file_upload', array('id'=>$id)),
//				'path' => Post::getUploadImagePath().$post->getId().'/',
//			);
//			$fileUploader = new FileUploadHandler($options);
//			$newOptions = $fileUploader->options;
//			$options['max_width'] = 80;
//			$options['max_height'] = 80;
//			$fileUploader->create_scaled_image(basename($oldImage['image']), 'thumbnail', $newOptions);
//			
//			// save image into database
//			$image = new PostImage();
//			$image->setName(basename($oldImage['image']));
//			$image->setPost($post);
//			$image->setSortOrder($oldImage['sequence']);
//			$image->setCaption($oldImage['intro']);
//			$image->setUseWatermark(1);
//			$em->persist($image);
//			$em->flush();
		}
	}
}