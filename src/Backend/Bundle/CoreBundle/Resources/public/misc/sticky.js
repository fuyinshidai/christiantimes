(function($, Drupal, drupalSettings) {
    if ($('#toolbar').length > 0) {
        var Sticky = function ($obj, opts) {
            $(window).scroll(function (e) {
                Sticky.onScroll(e, $obj, opts);
            });
        }
        Sticky.onScroll = function (e, $o, opts) {
            var iScrollTop = $(window).scrollTop();
            var sClass = "subhead-fixed";
            if (!$o.data(sClass)) {
                $o.data(sClass, {
                    css: {
                        position: $o.css('position')
                    },
                    offset: $o.offset(),
                    height: $o.height(),
                    heightref: $('#article').height()
                });
            }
            var oOrig = $o.data(sClass);
            var bIsSticky = $o.hasClass(sClass);
            if (oOrig.heightref - iScrollTop - oOrig.height + oOrig.offset.top - 10 < 0) {
                toofar = true;
            } else {
                toofar = false;
            }
            if (iScrollTop > oOrig.offset.top && !bIsSticky && !toofar) {
                $o.css({
                    position: 'fixed',
                    top: '40px'
                }).addClass(sClass);
            } else if (iScrollTop < oOrig.offset.top && bIsSticky && !toofar) {
                $o.css(oOrig.css).removeClass(sClass);
            } else if (toofar) {
                newtop = oOrig.heightref - iScrollTop - oOrig.height + oOrig.offset.top;
                $o.css({
                    position: 'fixed'
                }).addClass(sClass);
            } else if (iScrollTop > oOrig.offset.top) {
                $o.css({
                    position: 'fixed',
                    top: '40px'
                }).addClass(sClass);
            }
        }
    	Sticky($('#toolbar'));
    }
}(jQuery, Drupal, drupalSettings));