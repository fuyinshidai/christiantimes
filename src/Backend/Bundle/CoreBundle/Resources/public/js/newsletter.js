/*
 * {undefined}Newsletter backend related js
 */
(function($) {
  var div;

  $.fn.outerHTML = function() {
    var elem = this[0],
      tmp;

    return !elem ? null
      : typeof ( tmp = elem.outerHTML ) === 'string' ? tmp
      : ( div = div || $('<div/>') ).html( this.eq(0).clone() ).html();
  };


	var buttons = $('<tr></tr>')
			.hide()
			.append($('<td><label for="gospel_newsletter_title" class="required">选择头条新闻</label></td><td>\n\
											\n\
					<a class="btn button search-news" data-order="1"  href="#">头条新闻1</a>\n\
					<a class="btn button search-news" data-order="2" href="#">头条新闻2</a>\n\
</td>')
			.addClass("buttons")
			)
	;
	var loadingSrc = $("#loading").val();
	var loading = $('<img src="'+loadingSrc+'" />').hide();
	$("#gospel_newsletter_category").after(loading);
	$("#gospel_newsletter_category").parent().parent().after(buttons);
	var categoryNews = $("#category-news").val();
	if ($("#gospel_newsletter_category").val() == categoryNews) {
		buttons.show();
	}
	var newsletter = {
		addSelectNewsButtons: function() {
			buttons.show();
		},
		removeSelectNewsButtons: function() {
			buttons.hide();
		}
	};

	// the default order of the top news
	var dataOrder = 1;
	var dialog;
	$(".search-news").on('click', function() {
		dialog = $("#dialog-form").dialog("open");
		dataOrder = $(this).data('order');
	})

	$('#wrapper-newsletter #related-content').on('click', '.setRelatedPosts', function(e) {
        e.preventDefault();
		var pid = $(this).attr('pid');
		var content = editor.getContent();
		$.post( $("#newsletter-news-update").val(), {
			id: dataOrder,
			pid: pid,
			content: content
		}, function(data) {
			editor.setContent(data);
		});
	});

	/**
	 * Backend end newsletter select different template update the newsletter body
	 */
	$("#gospel_newsletter_category").change(function() {
		if ($(this).val()) {
			if ($(this).val() == categoryNews) {
				newsletter.addSelectNewsButtons();
			} else {
				newsletter.removeSelectNewsButtons();
			}
			loading.show();
			$.get($("#newsletter-template").val() + "?id=" + $(this).val(), function(data) {
				editor.setContent(data);
				loading.hide();
			});
		} else {
			newsletter.removeSelectNewsButtons();
		}
	});


}(jQuery));
