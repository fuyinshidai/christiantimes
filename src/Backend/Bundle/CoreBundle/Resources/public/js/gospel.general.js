(function($) {
	$('.submit-closest-form').click(function(e) {
		e.preventDefault();
		$(this).closest('form').submit();
	});

	$("#gospel_post_category, #gospel_post_categoryother, #gospel_video_tags, #gospel_post_author").select2({width: 'resolve'});

	// 修改分类的时候，如果是观点专栏显示更多的产品分类
	$("#gospel_post_category2").select2({width: 'resolve'});

	// 当选择观点专栏的时候显示更多的分类
	// 观点专栏的ids
	if($('#category2-ids').val()) {
		var column_ids = $('#category2-ids').val().split(',');
		var selectedCategoryId = $("#gospel_post_category").val();
		if(column_ids.includes(selectedCategoryId)) {
			$("#s2id_gospel_post_category2").show();
		} else {
			$("#s2id_gospel_post_category2").hide();
		}
	}

	// 当分类改变的时候
	$("#gospel_post_category").change(function () {
		var current_id = $(this).val();
		console.log('current_id', current_id)
		if(column_ids.includes(current_id)) {
			$("#s2id_gospel_post_category2").show();
		} else {
			$("#s2id_gospel_post_category2").hide();
			$("#gospel_post_category2").val(null);
		}
	});

	$("#gospel_post_tags").select2({
		placeholder: "请选择标签",
		width: 'resolve'
	 });

	/** 
	 * ****************************************************
	 * 相关文章 
	 * */
	// 添加select2
	$("#gospel_post_relatedPosts, #gospel_video_relatedVideos, #gospel_gallery_relatedGalleries").select2({
		tags: [],
		width: 'resolve'
	});

	// 搜索相关文章对话框初始化设置
	$("#dialog-form").dialog({
		autoOpen: false,
		height: 500,
		width: 650,
		modal: true,
		buttons: {
			'关闭': function() {
				$(this).dialog("close");
			}
		},
		close: function() {
		}
	});

	// 打开相关文章搜索的弹出窗口
	$("#search-related, #add-topic-posts")
		.button()
			.click(function(e) {
			e.preventDefault();
			$("#dialog-form").dialog("open");
		});

	// 当相关文章窗口打开的时候载入文章列表
	$("#dialog-form").on("dialogopen", function(event, ui) {
		$.get($('#search-uri').val(), function(data) {
			$("#related-content").html(data);
		});
	});

	// 添加到相关文章的动作
	$('#related-content').on('click', '.setRelatedPosts', function(e) {
		e.preventDefault();
		// 更新相关文章
		$("#gospel_post_relatedPosts").select2("val",
				_.union(
					[$(this).attr('data')],
					$("#gospel_post_relatedPosts").select2("val")
				));
		alert("已添加："+$(this).attr('data'));
	});

	// 添加专题文章
	$('#topic-wrapper #related-content').on('click', '.setRelatedPosts', function(e) {
		e.preventDefault();
		console.log('adding function');
		// 添加文章到专题
		$.getJSON($('#add-action').val(), { post: $(this).attr('pid') },  function(data) {
			alert(data.msg);
		});

	});

	// 删除专题文章
	$('#topic-posts-list').on('click', '.delete-topic-post', function(e) {
		var that = $(this);
		e.preventDefault();
		$.getJSON($('#delete-topic-action').val(), { post: $(this).attr('pid') },  function(data) {
			alert(data.msg);
			if(data.success) {
				that.parent().remove();
			}
		});
	});



	// 添加到相关视频的动作
	$('#related-content').on('click', '.setRelatedVideos', function() {
		// 更新相关文章
		$("#gospel_video_relatedVideos").select2("val",
			_.union(
				[$(this).attr('data')],
				$("#gospel_video_relatedVideos").select2("val")
				));
	});

	// 添加到相关图片集的动作
	$('#related-content').on('click', '.setRelatedGalleries', function() {
		// 更新相关文章
		$("#gospel_gallery_relatedGalleries").select2("val",
			_.union(
				[$(this).attr('data')],
				$("#gospel_gallery_relatedGalleries").select2("val")
				));
	});

	// 搜索相关内容的表单使用ajax进行处理
	$('#btn-search-related').click(function(e){
		e.preventDefault();
		$("#related-content").html($("#ajax-loader").clone().show());
		var queryString = $('#searchForm').formSerialize();
		$.post($('#search-uri').val(), queryString, function(data) {
			$("#related-content").html(data);
		})
		.done(function() {
			});
	});

	// 添加相关文章的搜索的时候的翻页功能
	$('#related-content').on('click', '.pagination a', function(e) {
		// 翻页功能
		e.preventDefault();
		$("#related-content").html($("#ajax-loader").clone().show());
		updateRelatedList($(this).attr('href'));
	});

	function updateRelatedList(url) {
		var queryString = $('#searchForm').formSerialize()+"&page="+url.split('=').pop(); 
		$.post($('#search-uri').val(), queryString, function(data) {
			$("#related-content").html(data);
		});
	}

	// 标签
	$('#myTabs a').click(function (e) {
		e.preventDefault();
		$('.tab-content').removeClass('active');
		$(this).tab('show');
		$($(this).attr('href')).addClass('active');
	})
	$('#myTabs a:first').tab('show');
	/** 
	 * 相关文章结束
	 * ****************************************************
	 * */

	/**
	 * 切换视频上传类型，本地或者远程
	 */
	$("#gospel_video_type_0").click(function() {
		$('.video-content').hide();
		$('#form-url').show();
	});
	$("#gospel_video_type_1").click(function() {
		$('.video-content').hide();
		$('#form-video').show();
	});

	$("#goto-page").click(function (e) {
		e.preventDefault();
		var url = window.location.href;
		var number = parseInt($("#goto-number").val());
		if (number > 0) {
			if (url.indexOf('?') > -1) {
				// 含有query string
				window.location.href = url + "&page=" + number;
			} else {
				window.location.href = url + "?page=" + number;
			}
		}
	});

}(jQuery));
