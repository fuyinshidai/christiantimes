<?php

namespace Backend\Bundle\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\BufferedOutput;

use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class ConfigurationController extends Controller
{
	protected $_application;

	public function indexAction()
    {
		// 重定向到管理列表页面
		return $this->redirect($this->generateUrl(
				'backend_structure', array('type' => 'admin-content')));
//        return $this->render('BackendCoreBundle:Dashboard:index.html.twig');
    }

	/**
	 * 
	 */
	public function clearCacheAction(Request $request)
	{
		$form = $this->createFormBuilder()
				->add('更新缓存', 'submit')
            ->getForm();
		$form->handleRequest($request);
		if ($form->isValid()) {
//			$kernel = $this->get('kernel');
//			$application = new \Symfony\Bundle\FrameworkBundle\Console\Application($kernel);
//			$application->setAutoExit(false);
//
//			$options = array('command' => 'cache:clear --env=prod --no-debug');
//			$application->run(new \Symfony\Component\Console\Input\ArrayInput($options));
//
//			$options = array('command' => 'assetic:dump --no-dump');
//			$application->run(new \Symfony\Component\Console\Input\ArrayInput($options));
//			$kernel = new \AppKernel("prod", false);
//
//			$kernel->boot();
//			$this->_application = new \Symfony\Bundle\FrameworkBundle\Console\Application($kernel);
//			$this->_application->setAutoExit(false);
//			$this->runConsole("cache:clear", array("--no-debug" => true, '--env'=>'prod'));
//
//			$this->get('session')->getFlashBag()->add(
//				'success',
//				'缓存更新成功'
//        	);

			// try 2
//			$kernel = $this->get('kernel');
//			$application = new \Symfony\Bundle\FrameworkBundle\Console\Application($kernel);
//			$application->setAutoExit(false);
//
//// The input interface should contain the command name, and whatever arguments the command needs to run      
//			$input = new \Symfony\Component\Console\Input\ArrayInput(array("cache:clear --env=prod --no-debug"));
//
//// Run the command
//			$output = new \Symfony\Component\Console\Output\ConsoleOutput();
//			$retval = $application->run($input, $output);

			// try 3
//			$output = $this->execute('cache:clear');
//			var_dump($output);
//
//    		return new Response($output);

			// try 4
//			 $command = $this->get('ClearCacheService');
//  
//			 $inputDefinition = new InputDefinition( array(
//        		new InputOption('verbose', 'v', InputOption::VALUE_REQUIRED),
//        		new InputOption('--verbose', '-v', InputOption::VALUE_REQUIRED),
//      		));
//			$_SERVER['argv'] = array(
//				'php',
//				'assetic:dump',
//				'verbose'
//			);
//			$input = new ArgvInput($_SERVER['argv']);
//			$output = new ConsoleOutput();
//			$command->run($input, $output);

			// try 5
			exec('cd ../ && php app/console assetic:dump --env=dev' , $output);
			$this->get('session')->getFlashBag()->add(
				'success',
				'更新成功.<br/>'.implode("<br/>", $output)
        	);


			return $this->redirect($this->generateUrl('clear_cache'));
    	}
        return $this->render('BackendCoreBundle:Configuration:clearCache.html.twig', array(
            'form' => $form->createView(),
        ));
	}

//	protected function runConsole($command, Array $options = array())
//	{
//		$options["-e"] = "prod";
//		$options["-q"] = null;
//		$options = array_merge($options, array('command' => $command));
//		return $this->_application->run(new \Symfony\Component\Console\Input\ArrayInput($options));
//	}


	private function execute($command)
	{
		$app = new Application($this->get('kernel'));
		$app->setAutoExit(false);

		$input = new StringInput($command);
		$output = new BufferedOutput();

		$error = $app->run($input, $output);

		if ($error != 0)
			$msg = "Error: $error";
		else
			$msg = $output->getBuffer();
		return $msg;
	}

}
