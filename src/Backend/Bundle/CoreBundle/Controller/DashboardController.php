<?php

namespace Backend\Bundle\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DashboardController extends Controller
{
    public function indexAction()
    {
			return $this->redirect($this->generateUrl(
				'backend_structure', array('type' => 'admin-content')));
        //return $this->render('BackendCoreBundle:Dashboard:index.html.twig');
    }

	/**
	 * 点击内容，结构，用户等链接地址的时候显示的导航页面
	 * @param type $type
	 * @return type
	 */
    public function structureAction($type)
    {
		$menus = $this->container->getParameter('backend_core.menu');
		$data = $menus[$type];
			$str = '<div class="block block-system" id="block-content">
  
    

  <div class="content">
    <ul class="admin-list">';
			foreach($data as $name => $link) {
				$roles = explode(', ', $link['role']);
				foreach ($roles as $role) {
					if(in_array($role, $this->get('security.context')->getToken()->getUser()->getRoles())) {
						$str .= '<li><a href="'.$this->generateUrl($link['url']).'"><span class="">'.$link['name'].'</span><div class="description">'.$link['description'].'</div></a></li>';
					}
				}
			}
			$str .= '</ul></div></div>';
        return $this->render('BackendCoreBundle:Dashboard:structure.html.twig', array('str' => $str));
    }

	/**
	 * 自动生成的导航树
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function backendMenuAction()
	{
		$menus = $this->container->getParameter('backend_core.menu');
		$data = array();
		foreach($menus as $key => $menu) {
			$str = '<ul class="menu clearfix">';
			foreach($menu as $name => $link) {
				$roles = explode(', ', $link['role']);
				foreach ($roles as $role) {
					if(in_array($role, $this->get('security.context')->getToken()->getUser()->getRoles())) {
						$str .= '<li><a href="'.$this->generateUrl($link['url']).'">'.$link['name'].'</a></li>';
					}
				}
			}
			$str .= '</ul>';
			$data[$key] = $str;
		}
		return new \Symfony\Component\HttpFoundation\Response(json_encode($data));
	}
}
