<?php

namespace Frontend\WebBundle\Controller;

use Frontend\WebBundle\Controller\WebController;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
/**
 *
 * @Cache(expires="+15 hour", public="true")
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class BroadcastController extends WebController
{
	/**
	 */
	public function showAction($id, $slug)
	{
		$data = array();
		$broadcast = $this->getRepository('GospelCoreBundle:Broadcast')->find($id);
		if (! $broadcast->getPublishStatus()) {
			throw $this->createNotFoundException("您所查找的文章不存在！");
		}
		$data['broadcast'] = $broadcast;
		return $this->render('FrontendWebBundle:Broadcast:show.html.twig', $data);
	}

}
