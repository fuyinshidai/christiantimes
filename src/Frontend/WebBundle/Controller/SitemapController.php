<?php

namespace Frontend\WebBundle\Controller;

use Frontend\WebBundle\Controller\WebController;
use Symfony\Component\HttpFoundation\Response;
/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class SitemapController extends WebController
{
	/**
	 */
	public function indexAction()
	{
		$repoPost = $this->getCoreRepo('Post');
		$data = array();
		// TODO 读取rss的主要分类和分类下面的文章列表
		$menuCategories = $this->getCoreRepo('Category')->getMenuCategory();
		$para = array('r' => 1);
		$categoryData = array();
		foreach ($menuCategories as $key => $row) {
			$categoryData[$key]['category'] = $row;
		}
		$data['blockData'] = $categoryData;
		$data['about'] = $this->getCoreRepo('About')->findBy(array('publishStatus' => 1));

//		$data['topics'] = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:Topic')->findBy(array(), array('id'=>'DESC'));

		$data['provinces'] = $this->getCoreRepo('Area')->getMenuCategory();
		$data['videoCategories'] = $this->getCoreRepo('CategoryVideo')->findAll();
		$data['galleryCategories'] = $this->getCoreRepo('CategoryGallery')->findAll();


		return $this->render('FrontendWebBundle:Sitemap:indexSitemap.html.twig', $data);
	}


}
