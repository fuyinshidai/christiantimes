<?php

namespace Frontend\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class WebController extends Controller
{
	public $cachedSeconds = 300;

	static $postIds = array();

	const TEN_MINUTES = 600;
	const ONE_HOUR = 3600;
	const ONE_DAY = 86400;
	const  ONE_WEEK = 604800;
	const  ONE_MONTH = 304800000;
	const  ONE_YEAR = 30480000;
	/**
	 * 
	 * @param type $name
	 * @return \Doctrine\ORM\EntityRepository 
	 */
	public function getCoreRepo($name)
	{
		return $this->getDoctrine()->getRepository('GospelCoreBundle:'.$name);
	}

	public function getRepository($name)
	{
		return $this->getDoctrine()->getRepository($name);
	}

	/**
	 * 取得主要的导航
	 * @return type
	 */
	public function getMenus()
	{
		return $this->container->getParameter('frontend.navigation');
	}

	public function getMenuByName($name)
	{
		$menus = $this->getMenus();
		foreach ($menus as $key => $val) {
			if ($val['name'] == $name) {
				return $val;
			}
		}
		return false;
	}

	/**
	 * 读取当前登录的用户
	 * @return User Object
	 */
	public function getCurrentUser()
	{
		return $this->get('security.context')->getToken()->getUser();
	}

	/**
	 * 读取当前登录的用户所拥有的role
	 * @return type array(ROLE1, ROLE2)
	 */
	public function getRoles()
	{
		if ($this->getCurrentUser() instanceof  \Gospel\Bundle\UserBundle\Entity\User) {
			return $this->getCurrentUser()->getRoles();
		} else {
			return array();
		}
	}

	/**
	 * 查看当前登录用户是否拥有指定的role
	 * @param type $role
	 * @return boolean true or false
	 */
	public function hasRole($role)
	{
		return in_array($role, $this->getRoles());
	}

	/**
	 * 缓存的时间，用秒表示
	 * @param type $cachedSeconds
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function getCachedResponse($ttl = 300)
	{
		$response = new Response();
		//$response->setSharedMaxAge($ttl);
		// mark the response public
		//$response->setPublic();
		// set the private or shared max age
		//$response->setMaxAge($this->cachedSeconds);
		//$response->setSharedMaxAge($this->cachedSeconds);
		//$date = new \DateTime();
		//$response->setEtag(md5($response->getContent()));
		//$date->modify('+'.$this->cachedSeconds.' seconds');
		//$response->setExpires($date);
		return $response;
	}

	public function setCachedResponse(Response $response, $ttl = 300) {
		// mark the response public
		//$response->setPublic();

		// set the private or shared max age
		//$response->setMaxAge($this->cachedSeconds);
		//$response->setSharedMaxAge($ttl);
		//$response->setEtag(md5($response->getContent()));

		//$date = new \DateTime();
		//$date->modify('+'.$this->cachedSeconds.' seconds');
		//$response->setExpires($date);

		//if ($response->isNotModified($this->get('request'))) {
			//return $response;
		//} else {
			// create a fresh response
		//}
		//$response->setVary('Accept-Encoding');
		return $response;
	}

    public function slugfy($text, $remove = array('/','-'))
    {
		return str_replace($remove, '.', $text);
    }

	public function isAdmin()
	{
		return $this->hasRole('ROLE_ADMIN');
	}

	public function getHost()
	{
		return $this->getRequest()->getHost();
	}

	public function getBaseUrl()
	{
		return 'http://'.$this->getHost();
	}

	/**
	 * 
	 * @return \Redis
	 */
	public function getRedis()
	{
		return $this->get('redis')->getInstance();
	}

	public function __destruct()
	{
		$this->getDoctrine()->getConnection()->close();
	}

}
