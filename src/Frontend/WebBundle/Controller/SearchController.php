<?php

namespace Frontend\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Gospel\Bundle\CoreBundle\Entity\Post;
use Gospel\Bundle\CoreBundle\Classes\Pagination;

class SearchController extends WebController
{

	public function indexAction($keyword)
	{
		$data = array(
			'show' => false,
		);
		if ($keyword) {
			$repoPost = $this->getDoctrine()->getRepository('GospelCoreBundle:Post');
			$result = $repoPost->search($keyword, $this->getRequest()->get('page', 1));
			foreach ($result['data'] as $row)
				intval($row['status']) ? $posts[] = $row : --$result['count'];
			$result['data'] = $posts;
			$data['keyword'] = $keyword;
			$data['show'] = true;
			$data['result'] = $result;

			$pagination = new Pagination();
			$pagination->records($result['count']);
			$pagination->records_per_page(20);
			$showAllLink = false;
			$data['pagination'] = $pagination->render(true, $showAllLink);
		}
		$response = $this->render('FrontendWebBundle:Search:search.html.twig', $data);
		return $this->setCachedResponse($response);
	}

	public function galleryAction($keyword)
	{
		$data = array(
			'show' => false,
		);
		if ($keyword) {
			$repoGallery = $this->getDoctrine()->getRepository('GospelCoreBundle:Gallery');
			$result = $repoGallery->search($keyword, $this->getRequest()->get('page', 1));
			$data['keyword'] = $keyword;
			$data['show'] = true;
			$data['result'] = $result;

			$pagination = new Pagination();
			$pagination->records($result['count']);
			$pagination->records_per_page(20);
			$showAllLink = false;
			$data['pagination'] = $pagination->render(true, $showAllLink);
		}
		$response = $this->render('FrontendWebBundle:Search:searchGallery.html.twig', $data);
		return $this->setCachedResponse($response);
	}

	public function videoAction($keyword)
	{
		$data = array(
			'show' => false,
		);
		if ($keyword) {
			$repoVideo = $this->getDoctrine()->getRepository('GospelCoreBundle:Video');
			$result = $repoVideo->search($keyword, $this->getRequest()->get('page', 1));
			$data['keyword'] = $keyword;
			$data['show'] = true;
			$data['result'] = $result;

			$pagination = new Pagination();
			$pagination->records($result['count']);
			$pagination->records_per_page(20);
			$showAllLink = false;
			$data['pagination'] = $pagination->render(true, $showAllLink);
		}
		$response = $this->render('FrontendWebBundle:Search:searchVideo.html.twig', $data);
		return $this->setCachedResponse($response);
	}
}
