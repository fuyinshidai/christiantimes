<?php

namespace Frontend\WebBundle\Controller;

use Frontend\WebBundle\Controller\WebController;
use Symfony\Component\HttpFoundation\Response;

/**
 * 会使用自己的独特的头部导航，和主页面的导航进行区分，里面的分类主要是链接到自己所在省份的分类
 * 
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class ProvinceController extends WebController
{
	/**
	 * 省份页面的首页
	 */
	public function indexAction($province_id, $slug)
	{
		if (!$data = $this->getRedis()->get('provinceData'.$province_id)) {
			$repoPost = $this->getCoreRepo('Post');
			$repoCategory = $this->getCoreRepo('Category');
			$data = array();

			// 需要有相读应的页面模板，通过整合专栏页面和板块页面实现

			// 取得当前的省份
			if (!$province = $this->getRedis()->get('province'.$province_id)) {
				$province = $this->getCoreRepo('Area')->find($province_id);
				$this->getRedis()->set('province'.$province_id, $province, self::ONE_WEEK);
			}
			$data['province'] = $province;

			// 取得当前省份的图片头条
			/*** 读取教会相关分类 ***/
			if (!$categoryChurch = $this->getRedis()->get('categoryChurch')) {
				$categoryChurch = $repoCategory->findOneBy(array('title' => '教会'));

				// 设置该分类下面的子分类
				$categoryChurchSubcategories = array($categoryChurch);
				foreach ($categoryChurch->getChildren() as $subCategory) {
					$categoryChurchSubcategories[] = $subCategory;
				}
				$this->getRedis()->set('categoryChurch', $categoryChurch, self::ONE_DAY);
				$this->getRedis()->set('categoryChurchSubCategories', $categoryChurchSubcategories, self::ONE_DAY);
			}
			$categoryChurchSubCategories = $this->getRedis()->get('categoryChurchSubCategories');

			/*** 读取商业分类 ***/
			if (!$categoryBusiness = $this->getRedis()->get('categoryBusiness')) {
				$categoryBusiness = $repoCategory->findOneBy(array('title' => '商业'));
				$categoryBusinessSubCategories = array($categoryBusiness);
				foreach($categoryBusiness->getChildren() as $row) {
					$categoryBusinessSubCategories[] = $row;
				}
				$this->getRedis()->set('categoryBusiness', $categoryBusiness, self::ONE_DAY);
				$this->getRedis()->set('categoryBusinessSubCategories', $categoryBusinessSubCategories, self::ONE_DAY);
			}
			$data['categoryBusiness'] = $categoryBusiness;
			$categoryBusinessSubCategories = $this->getRedis()->get('categoryBusinessSubCategories');

			/*** 读取国际分类 ***/
			if (!$categoryGlobal = $this->getRedis()->get('categoryGlobal')) {
				$categoryGlobal = $repoCategory->findOneBy(array('title' => '国际'));
				$data['categoryGlobal'] = $categoryGlobal;
				$this->getRedis()->set('categoryGlobal', $categoryGlobal, self::ONE_DAY);

				$categoryGlobalSubCategories = array($categoryGlobal);
				foreach ($categoryGlobal->getChildren() as $row) {
					$categoryGlobalSubCategories[] = $row;
				}
				$this->getRedis()->set('categoryGlobalSubCategories', $categoryGlobalSubCategories, self::ONE_DAY);
			}
			$categoryGlobalSubCategories = $this->getRedis()->get('categoryGlobalSubCategories');

			/*** 读取文化分类 ***/
			if (!$categoryCulture = $this->getRedis()->get('categoryCulture')) {
				$categoryCulture = $repoCategory->findOneBy(array('title' => '文化'));

				$categoryCultureSubCategories = array($categoryCulture);
				foreach ($categoryCulture->getChildren() as $row) {
					$categoryCultureSubCategories[] = $row;
				}
				$this->getRedis()->set('categoryCulture', $categoryCulture, self::ONE_DAY);
				$this->getRedis()->set('categoryCultureSubCategories', $categoryCultureSubCategories, self::ONE_DAY);
			}
			$categoryCultureSubCategories = $this->getRedis()->get('categoryCultureSubCategories');

			/*** 读取社会相关分类 ***/
			if (!$categorySociety = $this->getRedis()->get('categorySociety')) {
				$categorySociety = $repoCategory->findOneBy(array('title' => '社会'));

				// 设置子分类
				$categorySocietySubCategories = array($categorySociety);
				foreach ($categorySociety->getChildren() as $subCategory) {
					$categorySocietySubCategories[] = $subCategory;
				}
				$this->getRedis()->set('categorySociety', $categorySociety, self::ONE_DAY);
				$this->getRedis()->set('categorySocietySubCategories', $categorySocietySubCategories, self::ONE_DAY);
			}
			$categorySocietySubCategories = $this->getRedis()->get('categorySocietySubCategories');

			/*** 读取家庭分类 ***/
			if (!$categoryFamily = $this->getRedis()->get('categoryFamily')) {
				$categoryFamily = $repoCategory->findOneBy(array('title' => '家庭'));
				$categoryFamilySubCategories = array($categoryFamily);
				foreach($categoryFamily->getChildren() as $row) {
					$categoryFamilySubCategories[] = $row;
				}
				$this->getRedis()->set('categoryFamily', $categoryFamily, self::ONE_DAY);
				$this->getRedis()->set('categoryFamilySubCategories', $categoryFamilySubCategories, self::ONE_DAY);
			}
			$categoryFamilySubCategories = $this->getRedis()->get('categoryFamilySubCategories');

			/*** 读取生活分类 ***/
			if (!$categoryLife = $this->getRedis()->get('categoryLife')) {
				$categoryLife = $repoCategory->findOneBy(array('title' => '生活'));
				$categoryLifeSubCategories = array($categoryLife);
				foreach($categoryLife->getChildren() as $row) {
					$categoryLifeSubCategories[] = $row;
				}
				$this->getRedis()->set('categoryLife', $categoryLife, self::ONE_DAY);
				$this->getRedis()->set('categoryLifeSubCategories', $categoryLifeSubCategories, self::ONE_DAY);
			}
			$categoryLifeSubCategories = $this->getRedis()->get('categoryLifeSubCategories');

			$data['categoryChurch'] = $categoryChurch;
			$data['categorySociety'] = $categorySociety;
			$data['categoryCulture'] = $categoryCulture;
			$data['categoryFamily'] = $categoryFamily;
			$data['categoryLife'] = $categoryLife;

			$categoryRecommend1 = array(
				$categoryChurchSubCategories,
			);
			$categoryRecommend2 = array(
				$categorySocietySubCategories,
				$categoryCultureSubCategories,
			);
			$recommendData = array(
				'province' => $province,
				'recommend' => array(
					'type' => 'isHomeHeadlinePic',
					'value' => 1,
				)
			);
			$recommendDataHome = array(
				'recommend' => array(
					'type' => 'isHomeHeadlinePic',
					'value' => 1,
				)
			);

			if (!$headline1 = $this->getRedis()->get('headline1Province'.$province_id)) {
				$headline1 = $repoPost->normalizeArray($repoPost->findByCategory($categoryRecommend1, 3, 0, false, $recommendData));
				$this->getRedis()->set('headline1Province'.$province_id, $headline1, self::ONE_DAY);
			}
			if (isset($headline1[0])) {
				$data['headline1Top'] = array_shift($headline1);
			}
			$data['headline1Rest'] = $headline1;

			if (!$headline2 = $this->getRedis()->get('headline2Province'.$province_id)) {
				$headline2 = $repoPost->normalizeArray($repoPost->findByCategory($categoryRecommend2, 3, 0, false, $recommendData));
				$this->getRedis()->set('headline2Province'.$province_id, $headline2, self::ONE_DAY);
			}
			if (isset($headline2[0])) {
				$data['headline2Top'] = array_shift($headline2);
			}
			$data['headline2Rest'] = $headline2;

			$categoryRecommend3 = array(
				$categoryGlobalSubCategories,
			);
			// 生活，信仰，商业大图内容
			if (!$headline3 = $this->getRedis()->get('headline3Province'.$province_id)) {
				$headline3Tmp = $repoPost->findByCategory($categoryRecommend3, 4, 0, false, $recommendDataHome);
				$headline3 = array();
				foreach ($headline3Tmp as $row) {
					$headline3[] = $repoPost->normalize($row);
				}
				$this->getRedis()->set('headline3Province'.$province_id, $headline3, self::ONE_HOUR);
			}
			if (isset($headline3[0])) {
				$data['headline3Top'] = array_shift($headline3);
			}
			$data['headline3Rest'] = $headline3;

			$comprehensiveInfoCategories = array(
				$categoryLife,
				$categoryFamily,
				$categoryBusiness,
			);
			$comprehensiveInfoSubCategories = array(
				$categoryLifeSubCategories,
				$categoryFamilySubCategories,
				$categoryBusinessSubCategories,
			);

			// 大图旁边的中国教会
			// 读取含有图片的最新的内容
			$em = $this->getDoctrine()->getManager();
			$recommendDataChianChurch = array(
				'recommend' => array(
					'type' => 'isHomeHeadlinePic',
					'value' => 1,
				)
			);

			if (!$chinaChurchData = $this->getRedis()->get('chinaChurchData')) {
				$data['chinaChurch'] = $repoPost->normalize($repoPost->findOneByCategory($categoryChurch, true, $em));
				$this->getRedis()->set('chinaChurchData', $data['chinaChurch'], self::ONE_DAY);
			}
			if (!$chinaChurch = $this->getRedis()->get('chinaChurch')) {
				$chinaChurch = $repoPost->normalizeArray($repoPost->findByCategory($categoryRecommend1, 2, 3, false, $recommendDataChianChurch));
				$this->getRedis()->set('chinaChurch', $chinaChurch, self::ONE_DAY);
			}
			if (isset($headline1[0])) {
				$data['chinaChurch'] = array_shift($chinaChurch);
				if ($data['chinaChurch']->title == $data['headline1Top']->title) {
					$data['chinaChurch'] = array_shift($chinaChurch);
				}
			}

			// 大图右侧的省头条
			$criteria = array(
				'recommend' => array(
					'type' => 'isLocalHeadlineText',
					'value' => 1
				),
				'province' => $province,
			);
			if (!$data['headlinesTop'] = $this->getRedis()->get('headlinesTopProvince'.$province_id)) {
				$data['headlinesTop'] = $repoPost->normalizeArray($repoPost->findByCategory(null, 4, 3, false, $criteria), true);
				$this->getRedis()->set('headlinesTopProvince'.$province_id, $data['headlinesTop'], self::ONE_DAY);
			}
			if (!$data['headlinesRest'] = $this->getRedis()->get('headlinesRestProvince'.$province_id)) {
				$data['headlinesRest'] = $repoPost->normalizeArray($repoPost->findByCategory(null, 3, 7, false, $criteria), true);
				$this->getRedis()->set('headlinesRestProvince'.$province_id, $data['headlinesRest'], self::ONE_DAY);
			}

			// 主站头版
			// 在下面的4个分类里面进行筛选
			$categoryRecommendMainHeadline = array(
				$categoryChurch,
				$categoryGlobal,
				$categorySociety,
				$categoryCulture,
			);
			// 需要是推荐到首页的内容
			$recommendDataMainHeadlines = array(
				'recommend' => array(
					'type' => 'isHomeHeadlinePic',
					'value' => 1,
				)
			);
			// 读取数据库内容
			if (!$mianHeadlines = $this->getRedis()->get('mianHeadlines')) {
				$mianHeadlines = $repoPost->normalizeArray($repoPost->findByCategory($categoryRecommendMainHeadline, 4, 0, false, $recommendDataMainHeadlines));
				$this->getRedis()->set('mianHeadlines', $mianHeadlines, self::ONE_DAY);
			}
			if (isset($mianHeadlines[0])) {
				$data['mainHeadlineTop'] = array_shift($mianHeadlines);
			}
			$data['mainHeadlineRest'] = $mianHeadlines;

			// 读取家庭 ● 信仰 ● 生活的新闻
			$categoryFamilyLife = array(
				$categoryFamily,
				$categoryLife,
				$categoryBusiness,
			);

			$familyLifePosts = $repoPost->findByCategoryWithImage($categoryFamilyLife, $em, 4);
			if (isset($familyLifePosts[0])) {
				$data['familyLifePostsTop'] = array_shift($familyLifePosts);
			}
			$data['familyLifePostsRest'] = $familyLifePosts;


			$this->getRedis()->set('provinceData'.$province_id, $data, self::ONE_DAY);
		}
		return $this->render('FrontendWebBundle:Area:indexProvince.html.twig', $data, $this->getCachedResponse());
	}

	public function listAction($province_id, $slug)
	{
		$data = array();
		// 取得当前的省份
		$province = $this->getCoreRepo('Area')->find($province_id);
		$data['province'] = $province;
		$repoPost = $this->getCoreRepo('Post');

		if (!$headline1 = $this->getRedis()->get('headline1Province'.$province_id)) {
			$repoCategory = $this->getCoreRepo('Category');
			/*** 读取教会相关分类 ***/
			if (!$categoryChurch = $this->getRedis()->get('categoryChurch')) {
				$categoryChurch = $repoCategory->findOneBy(array('title' => '教会'));

				// 设置该分类下面的子分类
				$categoryChurchSubcategories = array($categoryChurch);
				foreach ($categoryChurch->getChildren() as $subCategory) {
					$categoryChurchSubcategories[] = $subCategory;
				}
				$this->getRedis()->set('categoryChurch', $categoryChurch, self::ONE_DAY);
				$this->getRedis()->set('categoryChurchSubCategories', $categoryChurchSubcategories, self::ONE_DAY);
			}
			$categoryChurchSubCategories = $this->getRedis()->get('categoryChurchSubCategories');

			/*** 读取商业分类 ***/
			if (!$categoryBusiness = $this->getRedis()->get('categoryBusiness')) {
				$categoryBusiness = $repoCategory->findOneBy(array('title' => '商业'));
				$categoryBusinessSubCategories = array($categoryBusiness);
				foreach($categoryBusiness->getChildren() as $row) {
					$categoryBusinessSubCategories[] = $row;
				}
				$this->getRedis()->set('categoryBusiness', $categoryBusiness, self::ONE_DAY);
				$this->getRedis()->set('categoryBusinessSubCategories', $categoryBusinessSubCategories, self::ONE_DAY);
			}
			$data['categoryBusiness'] = $categoryBusiness;
			$categoryBusinessSubCategories = $this->getRedis()->get('categoryBusinessSubCategories');

			/*** 读取国际分类 ***/
			if (!$categoryGlobal = $this->getRedis()->get('categoryGlobal')) {
				$categoryGlobal = $repoCategory->findOneBy(array('title' => '国际'));
				$data['categoryGlobal'] = $categoryGlobal;
				$this->getRedis()->set('categoryGlobal', $categoryGlobal, self::ONE_DAY);

				$categoryGlobalSubCategories = array($categoryGlobal);
				foreach ($categoryGlobal->getChildren() as $row) {
					$categoryGlobalSubCategories[] = $row;
				}
				$this->getRedis()->set('categoryGlobalSubCategories', $categoryGlobalSubCategories, self::ONE_DAY);
			}
			$categoryGlobalSubCategories = $this->getRedis()->get('categoryGlobalSubCategories');

			/*** 读取文化分类 ***/
			if (!$categoryCulture = $this->getRedis()->get('categoryCulture')) {
				$categoryCulture = $repoCategory->findOneBy(array('title' => '文化'));

				$categoryCultureSubCategories = array($categoryCulture);
				foreach ($categoryCulture->getChildren() as $row) {
					$categoryCultureSubCategories[] = $row;
				}
				$this->getRedis()->set('categoryCulture', $categoryCulture, self::ONE_DAY);
				$this->getRedis()->set('categoryCultureSubCategories', $categoryCultureSubCategories, self::ONE_DAY);
			}
			$categoryCultureSubCategories = $this->getRedis()->get('categoryCultureSubCategories');

			/*** 读取社会相关分类 ***/
			if (!$categorySociety = $this->getRedis()->get('categorySociety')) {
				$categorySociety = $repoCategory->findOneBy(array('title' => '社会'));

				// 设置子分类
				$categorySocietySubCategories = array($categorySociety);
				foreach ($categorySociety->getChildren() as $subCategory) {
					$categorySocietySubCategories[] = $subCategory;
				}
				$this->getRedis()->set('categorySociety', $categorySociety, self::ONE_DAY);
				$this->getRedis()->set('categorySocietySubCategories', $categorySocietySubCategories, self::ONE_DAY);
			}
			$categorySocietySubCategories = $this->getRedis()->get('categorySocietySubCategories');

			/*** 读取家庭分类 ***/
			if (!$categoryFamily = $this->getRedis()->get('categoryFamily')) {
				$categoryFamily = $repoCategory->findOneBy(array('title' => '家庭'));
				$categoryFamilySubCategories = array($categoryFamily);
				foreach($categoryFamily->getChildren() as $row) {
					$categoryFamilySubCategories[] = $row;
				}
				$this->getRedis()->set('categoryFamily', $categoryFamily, self::ONE_DAY);
				$this->getRedis()->set('categoryFamilySubCategories', $categoryFamilySubCategories, self::ONE_DAY);
			}
			$categoryFamilySubCategories = $this->getRedis()->get('categoryFamilySubCategories');

			/*** 读取生活分类 ***/
			if (!$categoryLife = $this->getRedis()->get('categoryLife')) {
				$categoryLife = $repoCategory->findOneBy(array('title' => '生活'));
				$categoryLifeSubCategories = array($categoryLife);
				foreach($categoryLife->getChildren() as $row) {
					$categoryLifeSubCategories[] = $row;
				}
				$this->getRedis()->set('categoryLife', $categoryLife, self::ONE_DAY);
				$this->getRedis()->set('categoryLifeSubCategories', $categoryLifeSubCategories, self::ONE_DAY);
			}
			$categoryLifeSubCategories = $this->getRedis()->get('categoryLifeSubCategories');

			$data['categoryChurch'] = $categoryChurch;
			$data['categorySociety'] = $categorySociety;
			$data['categoryCulture'] = $categoryCulture;
			$data['categoryFamily'] = $categoryFamily;
			$data['categoryLife'] = $categoryLife;
			$categoryRecommend1 = array(
				$categoryChurchSubCategories,
				$categoryBusinessSubCategories,
				$categoryLifeSubCategories,
				$categoryFamilySubCategories,
				$categorySocietySubCategories,
				$categoryGlobalSubCategories,
			);

			$recommendData = array(
				'province' => $province,
				'recommend' => array(
					'type' => 'isHomeHeadlinePic',
					'value' => 1,
				)
			);
			$headline1 = $repoPost->normalizeArray($repoPost->findByCategory($categoryRecommend1, 3, 0, false, $recommendData));
			$this->getRedis()->set('headline1Province'.$province_id, $headline1, self::ONE_DAY);
		}
		if (isset($headline1[0])) {
			$data['headline1Top'] = array_shift($headline1);
		}
		$data['headline1Rest'] = $headline1;

		// 大图右侧的省头条
		$criteria = array(
			'recommend' => array(
				'type' => 'isLocalHeadlineText',
				'value' => 1
			),
			'province' => $province,
		);
		if (!$data['headlinesTop'] = $this->getRedis()->get('headlinesTopProvince'.$province_id)) {
			$data['headlinesTop'] = $repoPost->normalizeArray($repoPost->findByCategory(null, 4, 3, false, $criteria), true);
			$this->getRedis()->set('headlinesTopProvince'.$province_id, $data['headlinesTop'], self::ONE_DAY);
		}
		if (!$data['headlinesRest'] = $this->getRedis()->get('headlinesRestProvince'.$province_id)) {
			$data['headlinesRest'] = $repoPost->normalizeArray($repoPost->findByCategory(null, 3, 7, false, $criteria), true);
			$this->getRedis()->set('headlinesRestProvince'.$province_id, $data['headlinesRest'], self::ONE_DAY);
		}


		// 取得当前省份下面的文章列表，同时包含有分页功能
    	$paginator  = $this->get('knp_paginator');
		$page = $this->get('request')->query->get('page', 1);
		$data['pagination'] = $this->getCoreRepo('Post')->findByCategoryWithPagitaion($paginator, null, $page, $province);
		$data['page'] = $page;
		$data['top'] = $data['pagination']->current();
		return $this->render('FrontendWebBundle:Area:list.html.twig', $data, $this->getCachedResponse());
	}

	public function categoryAction($province_id, $category_id)
	{
		$data = array();
		// 取得当前的省份
		$province = $this->getCoreRepo('Area')->find($province_id);
		$data['province'] = $province;

		// 取得当前的分类
		$category = $this->getCoreRepo('Category')->find($category_id);
		$data['category'] = $category;

		// TODO 3. 取得当前省份下面的文章列表，同时包含有分页功能
    	$paginator  = $this->get('knp_paginator');
		$data['pagination'] = $this->getCoreRepo('Post')->findByCategoryWithPagitaion($paginator, $category, $this->get('request')->query->get('page', 1), $province);
		return $this->render('FrontendWebBundle:Area:categoryProvince.html.twig', $data, $this->getCachedResponse());
	}

}
