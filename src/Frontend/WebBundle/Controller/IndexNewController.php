<?php

namespace Frontend\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Gospel\Bundle\CoreBundle\Entity\Post;
use Symfony\Component\HttpFoundation\Response;
use Gospel\Bundle\CoreBundle\Classes\RedisService;

class IndexNewController extends WebController
{
	public $notUseRedis = true;

	public function indexAction(Request $request)
	{
		$etag = $request->getETags();
		$redis = $this->getRedis();
		$html = $redis->get(RedisService::$REDIS_KEY_INDEX_HTML);
		if(empty($html) || $this->needClearRedis($request)) {
			$data = array(); // 传递到模板里面的数据
			$repoPost = $this->getDoctrine()->getRepository('GospelCoreBundle:Post');
			$repoCategory = $this->getDoctrine()->getRepository('GospelCoreBundle:Category');

			/*----footer导航----*/
			$repoAbout = $this->getCoreRepo('About');
			$data['about'] = array();
			if (!$data['about'] = $this->getRedis()->get('data-about')) {
				$footerMenus = array('爱心奉献', '关于我们', '联系我们', '信仰宣言', '欢迎投稿', '版权声明',);
				foreach ($footerMenus as $title) {
					$entity = $repoAbout->findOneBy(array('title' => $title));
					if ($entity) {
						$data['about'][] = $entity;
					}
				}
				$this->getRedis()->set('data-about', $data['about'], self::ONE_YEAR);
			}

			/*----首页中间大图头条----*/
			/*** 读取教会相关分类 ***/
			if (!$categoryChurch = $this->getRedis()->get('categoryChurch')) {
				$categoryChurch = $repoCategory->findOneBy(array('title' => '教会'));

				// 设置该分类下面的子分类
				$categoryChurchSubcategories = array($categoryChurch);
				foreach ($categoryChurch->getChildren() as $subCategory) {
					$categoryChurchSubcategories[] = $subCategory;
				}
				$this->getRedis()->set('categoryChurch', $categoryChurch, self::ONE_MONTH);
				$this->getRedis()->set('categoryChurchSubCategories', $categoryChurchSubcategories, self::ONE_MONTH);
			}

			$categoryChurchSubCategories = $this->getRedis()->get('categoryChurchSubCategories');

			/*** 读取社会相关分类 ***/
			if (!$categorySociety = $this->getRedis()->get('categorySociety')) {
				$categorySociety = $repoCategory->findOneBy(array('title' => '社会'));

				// 设置子分类
				$categorySocietySubCategories = $repoCategory->getAllChild($categorySociety);
				$this->getRedis()->set('categorySociety', $categorySociety, self::ONE_MONTH);
				$this->getRedis()->set('categorySocietySubCategories', $categorySocietySubCategories, self::ONE_MONTH);
			}
			$categorySocietySubCategories = $this->getRedis()->get('categorySocietySubCategories');

			/*** 读取文化分类 ***/
			if (!$categoryCulture = $this->getRedis()->get('categoryCulture')) {
				$categoryCulture = $repoCategory->findOneBy(array('title' => '文化'));

				$categoryCultureSubCategories = $repoCategory->getAllChild($categoryCulture);
				$this->getRedis()->set('categoryCulture', $categoryCulture, self::ONE_DAY);
				$this->getRedis()->set('categoryCultureSubCategories', $categoryCultureSubCategories, self::ONE_DAY);
			}
			$categoryCultureSubCategories = $this->getRedis()->get('categoryCultureSubCategories');

			/*** 读取国际分类 ***/
			if (!$categoryGlobal = $this->getRedis()->get('categoryGlobal')) {
				$categoryGlobal = $repoCategory->findOneBy(array('title' => '国际'));
				$this->getRedis()->set('categoryGlobal', $categoryGlobal, self::ONE_DAY);

				$categoryGlobalSubCategories = $repoCategory->getAllChild($categoryGlobal);
				$this->getRedis()->set('categoryGlobalSubCategories', $categoryGlobalSubCategories, self::ONE_DAY);
			}
			$categoryGlobalSubCategories = $this->getRedis()->get('categoryGlobalSubCategories');
			$data['categoryGlobal'] = $categoryGlobal;

			/*** 读取公益分类 ***/
			if (!$categoryCharity = $this->getRedis()->get('categoryCharity')) {
				$categoryCharity = $repoCategory->findOneBy(array('title' => '公益'));
				if($categoryCharity) {
					$categoryCharitySubCategories = array($categoryCharity);
					foreach($categoryCharity->getChildren() as $row) {
						$categoryCharitySubCategories[] = $row;
					}
					$this->getRedis()->set('categoryCharity', $categoryCharity, self::ONE_DAY);
					$this->getRedis()->set('categoryCharitySubCategories', $categoryCharitySubCategories, self::ONE_DAY);
				} else {
					$this->getRedis()->set('categoryCharitySubCategories', array(), self::ONE_HOUR);
				}
			}
			$categoryCharitySubCategories = $this->getRedis()->get('categoryCharitySubCategories');

			/*** 读取家庭分类 ***/
			if (!$categoryFamily = $this->getRedis()->get('categoryFamily')) {
				$categoryFamily = $repoCategory->findOneBy(array('title' => '家庭'));
				$categoryFamilySubCategories = array($categoryFamily);
				foreach($categoryFamily->getChildren() as $row) {
					$categoryFamilySubCategories[] = $row;
				}
				$this->getRedis()->set('categoryFamily', $categoryFamily, self::ONE_DAY);
				$this->getRedis()->set('categoryFamilySubCategories', $categoryFamilySubCategories, self::ONE_DAY);
			}
			$categoryFamilySubCategories = $this->getRedis()->get('categoryFamilySubCategories');

			/*** 读取商业分类 ***/
			if (!$categoryBusiness = $this->getRedis()->get('categoryBusiness')) {
				$categoryBusiness = $repoCategory->findOneBy(array('title' => '商业'));
				$categoryBusinessSubCategories = array($categoryBusiness);
				foreach($categoryBusiness->getChildren() as $row) {
					$categoryBusinessSubCategories[] = $row;
				}
				$this->getRedis()->set('categoryBusiness', $categoryBusiness, self::ONE_DAY);
				$this->getRedis()->set('categoryBusinessSubCategories', $categoryBusinessSubCategories, self::ONE_DAY);
			}
			$categoryBusinessSubCategories = $this->getRedis()->get('categoryBusinessSubCategories');

			/*** 读取生活分类 ***/
			if (!$categoryLife = $this->getRedis()->get('categoryLife')) {
				$categoryLife = $repoCategory->findOneBy(array('title' => '生活'));
				$categoryLifeSubCategories = array($categoryLife);
				foreach($categoryLife->getChildren() as $row) {
					$categoryLifeSubCategories[] = $row;
				}
				$this->getRedis()->set('categoryLife', $categoryLife, self::ONE_DAY);
				$this->getRedis()->set('categoryLifeSubCategories', $categoryLifeSubCategories, self::ONE_DAY);
			}
			$categoryLifeSubCategories = $this->getRedis()->get('categoryLifeSubCategories');

			$recommendData = array(
				'recommend' => array(
					'type' => 'isHomeHeadlinePic',
					'value' => 1,
				)
			);

			$recommendDataRest = array(
				'recommend' => array(
					'type' => 'isHomePic',
					'value' => 1,
				)
			);

			// 记录已经读取的文章id，防止重复
			self::$postIds = array();

			// 首页图片推荐
			$categoryRecommend1 = array(
				$categoryChurchSubCategories,
				$categoryGlobalSubCategories,
				$categorySocietySubCategories,
				$categoryCultureSubCategories,
				$categoryCharitySubCategories,
				$categoryFamilySubCategories,
				$categoryBusinessSubCategories,
				$categoryLifeSubCategories,
			);
			$categoryRecommend2 = array(
				$categoryChurchSubCategories,
				$categoryGlobalSubCategories,
			);

			if (!$headline1 = $this->getRedis()->get('headlineNew1')) {
				$headline1Tmp = $repoPost->findByCategory($categoryRecommend1, 1, 0, false, $recommendData);
				$headline1 = array();
				foreach ($headline1Tmp as $row) {
					$headline1 = $repoPost->normalize($row, true);
				}
				$this->getRedis()->set('headlineNew1', $headline1, self::ONE_HOUR);
			}
			$data['headline1'] = $headline1;
			if ($headline1) {
				self::$postIds[] = $headline1->id;
			}

			if ((!$headlineRest = $this->getRedis()->get('headlineRestNew')) || $this->needClearRedis($request)) {
				$headlineRestTmp = $repoPost->findByCategory($categoryRecommend1, 5, 0, false, $recommendDataRest);
				$headlineRest = array();
				foreach ($headlineRestTmp as $row) {
					if ($row->getId() != $headline1->id) {
						$headlineRest[] = $repoPost->normalize($row);
					}
				}
				$this->getRedis()->set('headlineRestNew', $headlineRest, self::ONE_HOUR);
			}
			$data['headlineRest'] = $headlineRest;
			foreach ($headlineRest as $row) {
				self::$postIds[] = $row->id;
			}

			/*----今日头条----*/
			// 大图左侧的第一条推荐新闻
			if (!$subHeadlineTop = $this->getRedis()->get('subHeadlineTopNew')) {
				$subHeadlineTop = $repoPost->normalize($repoPost->findOne(REC_HEADLINE_TXT), true);
				$this->getRedis()->set('subHeadlineTopNew', $subHeadlineTop, self::ONE_HOUR);
			}
			$data['subHeadlineTop'] = $subHeadlineTop;
			if ($subHeadlineTop) {
				self::$postIds[] = $subHeadlineTop->id;
			}

			// 大图左侧的2条推荐新闻
			if (!$subHeadline2 = $this->getRedis()->get('subHeadline2New')) {
				$subHeadline2 = array(
					$repoPost->normalize($repoPost->findOne(REC_HEADLINE_TXT1), true),
					$repoPost->normalize($repoPost->findOne(REC_HEADLINE_TXT2), true),
					$repoPost->normalize($repoPost->findOne(REC_HEADLINE_TXT3), true),
				);
				$this->getRedis()->set('subHeadline2New', $subHeadline2, self::ONE_HOUR);
			}
			$data['subHeadline2'] = $subHeadline2;
			foreach ($subHeadline2 as $row) {
				self::$postIds[] = $row->id;
			}

			// 过期的头条文章
			if (!$subheadline3 = $this->getRedis()->get('subheadline3New')) {
				$subheadline3 = array();
				$subheadline3Tmp = $repoPost->findExpireHeadlines(17);
				$count = 0;
				foreach($subheadline3Tmp as $row) {
					if(!in_array($row->getId(), self::$postIds)) {
						if ($count < 7) {
							$subHeadline3[] = $repoPost->normalize($row, true);
							self::$postIds[] = $row->getId();
							$count++;
						}
					}
				}
				$this->getRedis()->set('subheadline3New', $subHeadline3, self::ONE_HOUR);
			}
			$data['subHeadline3'] = $subheadline3;
			foreach ($subheadline3 as $row) {
				self::$postIds[] = $row->id;
			}

			/*----深度阅读----*/
			// 精品栏目
			if (!$dataInside = $this->getRedis()->get('fineColumnsNew')) {
				$repoFineColumns = $this->getCoreRepo('Categoryother');
				$fineColumnsNames = array(
					'译稿',
					'特稿',
					'专访',
					'时评',
					'见证',
					'学术',
				);
				$fineColumns = array();
				foreach ($fineColumnsNames as $key => $name) {
					$fineColumns[$key]['category'] = $repoFineColumns->findOneBy(array('title' => $name));
					// 读取该精品栏目下的最新发布文章
					$fineColumns[$key]['post'] = $repoPost->normalize($repoPost->findOneByFineCategory($fineColumns[$key]['category'], 0));
					$start = 0;
					while (in_array($fineColumns[$key]['post']->id, self::$postIds)) {
						$fineColumns[$key]['post'] = $repoPost->normalize($repoPost->findOneByFineCategory($fineColumns[$key]['category'], $start++));
					}
				}
				$dataInside = $fineColumns;

				$this->getRedis()->set('fineColumnsNew', $dataInside, self::ONE_HOUR);
			}
			$data['insides'] = $dataInside;
			foreach ($dataInside as $row) {
				self::$postIds[] = $row['post']->id;
			}

			/*----读书----*/
			if (!$reading = $this->getRedis()->get('reading')) {
				$repoFineColumns = $this->getCoreRepo('Categoryother');
				$reading['category'] = $repoFineColumns->findOneBy(array('title' => '读书'));
				// 读取该精品栏目下的最新发布文章
				$reading['post'] = $repoPost->normalize($repoPost->findOneByFineCategory($reading['category']));

				$this->getRedis()->set('reading', $reading, self::ONE_HOUR);
			}
			$data['reading'] = $reading;

			/*----专栏----*/
			if (!$columnist = $this->getRedis()->get('columnist')) {
				$repoAuthor = $this->getCoreRepo('Author');

				$repoFineColumns = $this->getCoreRepo('Categoryother');
				$columnist['category'] = $repoFineColumns->findOneBy(array('title' => '专栏'));
				// 读取该精品栏目下的最新发布文章
				$columnistPosts = $repoPost->findPostByFineCategory($columnist['category'], 10, 0);
				// reading author data: img, description
				$columnist['data'] = array();
				$countColumnist = 0;
				foreach($columnistPosts as $key => $row) {
					if (!in_array($row->getId(), self::$postIds) && $countColumnist < 3) {
						$columnist['data'][$key]['post'] = $row;
						if (is_object($row->getAuthor())) {
							$authorEn = $repoAuthor->find($row->getAuthor()->getId());
							$author = new \stdClass();
							$author->id = $authorEn->getId();
							$author->coverWebPath = $authorEn->getCoverWebPath();
							$author->coverPath = $authorEn->getCoverPath();
							$author->title = $authorEn->getTitle();
							$author->title = $authorEn->getTitle();
							$columnist['data'][$key]['author'] = $author;
						} else {
							$columnist['data'][$key]['author'] = null;
						}
						$countColumnist++;
					}
				}

				$this->getRedis()->set('columnist', $columnist, self::ONE_HOUR);
			}
			$data['columnist'] = $columnist;


			/*----资讯频道----*/

			// 读取多个分类的文章
			$categoryChurch = $repoCategory->findOneBy(array('title' => '教会'));
			$categorySociety = $repoCategory->findOneBy(array('title' => '社会'));
			$categoryCulture = $repoCategory->findOneBy(array('title' => '文化'));
			$categoryStar = $repoCategory->findOneBy(array('title' => '名人'));
			$categoryGlobal = $repoCategory->findOneBy(array('title' => '国际'));
			$categoryGongyi = $repoCategory->findOneBy(array('title' => '公益'));
			$categoryCharity = $repoCategory->findOneBy(array('title' => '宗教慈善'));
			$categoryFamily = $repoCategory->findOneBy(array('title' => '家庭'));
			$categoryBusiness = $repoCategory->findOneBy(array('title' => '商业'));
			$categoryLife = $repoCategory->findOneBy(array('title' => '生活'));
			$dataCategory['categoryChurch'] = $categoryChurch;
			$dataCategory['categorySociety'] = $categorySociety;
			$dataCategory['categoryCulture'] = $categoryCulture;
			$dataCategory['categoryChurch'] = $categoryChurch;
			$dataCategory['categoryGlobal'] = $categoryGlobal;
			$dataCategory['categoryCharity'] = $categoryCharity;
			$dataCategory['categoryBusiness'] = $categoryBusiness;
			$dataCategory['categoryLife'] = $categoryLife;
			$dataCategory['categoryFamily'] = $categoryFamily;
			$dataCategory['categoryStar'] = $categoryStar;
			$dataCategory['categoryCharity'] = $categoryCharity;
			$dataCategory['categoryGongyi'] = $categoryGongyi;
			$categoryList1 = array(
				$categoryGlobal,
			);
			$categoryList2 = array(
				$categoryChurch,
			);
			$categoryList3 = array(
				$categorySociety,
				$categoryCharity
			);
			$categoryList4 = array(
				$categoryCulture,
			);
			$categoryList5 = array(
				$categoryFamily
			);
			$categoryList6 = array(
				$categoryBusiness,
				$categoryLife,
			);
			$em = $this->getDoctrine()->getManager();
			$dataCategory['contents'] = array(
				array(
					'top' => $this->removeDu($repoPost->findByCategoryWithImage($categoryList1, $em, 50, 0), self::$postIds, 1),
					'list' => $this->removeDu($repoPost->findByCategory($categoryList1, 15, 0, false), self::$postIds, 4),
				),
				array(
					'top' => $this->removeDu($repoPost->findByCategoryWithImage($categoryList2, $em, 50, 0), self::$postIds, 1),
					'list' => $this->removeDu($repoPost->findByCategory($categoryList2, 15, 0, false), self::$postIds, 4),
				),
				array(
					'top' => $this->removeDu($repoPost->findByCategoryWithImage($categoryList3, $em, 50, 0), self::$postIds, 1),
					'list' => $this->removeDu($repoPost->findByCategory($categoryList3, 15, 0, false), self::$postIds, 4),
				),
				array(
					'top' => $this->removeDu($repoPost->findByCategoryWithImage($categoryList4, $em, 50, 0), self::$postIds, 1),
					'list' => $this->removeDu($repoPost->findByCategory($categoryList4, 15, 0, false), self::$postIds, 4),
				),
				array(
					'top' => $this->removeDu($repoPost->findByCategoryWithImage($categoryList5, $em, 50, 0), self::$postIds, 1),
					'list' => $this->removeDu($repoPost->findByCategory($categoryList5, 15, 0, false), self::$postIds, 4),
				),
				array(
					'top' => $this->removeDu($repoPost->findByCategoryWithImage($categoryList6, $em, 50, 0), self::$postIds, 1),
					'list' => $this->removeDu($repoPost->findByCategory($categoryList6, 15, 0, false), self::$postIds, 4),
				),
			);

			$data['dataCategory'] = $dataCategory;

			/*----各地资讯----*/
			// 读取地区下面的文章内容
			// 读取北京，上海，深圳
			$repoProvince = $this->getCoreRepo('Area');
			$repoRegion = $this->getCoreRepo('AreaCategory');

			$regionsName = array(
				'华北',
				'华东',
				'中南',
				'东北',
				'西南',
				'西北',
			);

			if (!$regions = $this->getRedis()->get('regions')) {
				$regions = array();
				foreach ($regionsName as $key => $name) {
					$regions[$key]['region'] = $repoRegion->findOneBy(array('title' => $name));
					$posts = $repoPost->findByRegion($regions[$key]['region'], 2, 4);
					$topPost = $repoPost->findByRegionWithImage($regions[$key]['region'], 1, 0, $em);
					array_unshift($posts, $topPost[0]);
					foreach ($posts as $row) {
						$regions[$key]['posts'][] = $repoPost->normalize($row);
					}
				}
				$this->getRedis()->set('regions', $regions, self::ONE_HOUR);
			}
			$data['regions'] = $regions;

			/*----不容错过----*/
			// 精品栏目
			if (!$mustRead = $this->getRedis()->get('fineColumnsMustRead')) {
				$mustRead = array();
				$repoFineColumns = $this->getCoreRepo('Categoryother');
				$fineColumnsNames = array(
					'译稿',
					'特稿',
					'专访',
					'时评',
					'见证',
					'学术',
				);
				foreach ($fineColumnsNames as $key => $name) {
					$category = $repoFineColumns->findOneBy(array('title' => $name));
					// 读取该精品栏目下的最新发布文章
					$post = $repoPost->normalize($repoPost->findOneByFineCategory($category, 1));
					$start = 1;
					while (in_array($post->id, self::$postIds)) {
						$post = $repoPost->normalize($repoPost->findOneByFineCategory($category, ++$start));
					}
					$mustRead[] = $post;
				}
				usort($mustRead, function($a, $b)
				{
					return strcmp($b->id, $a->id);
				});

				$mustRead = array_slice($mustRead, 0, 5);
				$this->getRedis()->set('fineColumnsMustRead', $mustRead, self::ONE_HOUR);
			}
			$data['mustRead'] = $mustRead;

			$html = $this->renderView('FrontendWebBundle:IndexNew:index.html.twig', $data);
			$redis->set(RedisService::$REDIS_KEY_INDEX_HTML, $html, self::ONE_HOUR);
		}


		$response = new Response($html);
		$response->setEtag(md5($html));
		$response->setMaxAge(600);
		$response->setPublic();
		return $response;
	}

	/**
	 * Remove the dulicated post
	 */
	private function removeDu($postData, $postExistsIds, $total)
	{
		$count = 0;
		$data = array();
		foreach ($postData as $row) {
			if (!in_array($row->getId(), $postExistsIds) && $count < $total)
			{
				$data[] = $row;
				self::$postIds[] = $row->getId();
				$count++;
			}
		}
		return $data;
	}

	public function needClearRedis(Request $request) {
		if($this->notUseRedis) {
			return true;
		}
		return $request->get('clearRedis') === 'true';
	}
}
