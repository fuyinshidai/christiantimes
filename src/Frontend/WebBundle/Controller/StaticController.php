<?php

namespace Frontend\WebBundle\Controller;

use Gospel\Bundle\AlipayBundle\Controller\AlipayController;
use Symfony\Component\HttpFoundation\Request;
use Gospel\Bundle\AlipayBundle\Alipay\AlipaySubmit;
use Gospel\Bundle\AlipayBundle\Alipay\AlipayNotify;
use Gospel\Bundle\CoreBundle\Entity\OrderItem;

class StaticController  extends AlipayController
{
    public function bodeliAction()
    {
		$msg = '';
		if ($this->getRequest()->get('trade_status') == 'TRADE_SUCCESS') {
			$msg = '支付成功，感谢您的购买！';
			$this->updateOrderItem();
		}
        return $this->render('FrontendWebBundle:Static:bodeli.html.twig', array(
			'msg' => $msg,
		));
    }

	public function yadifenqiAction()
	{
		$msg = '';
		if ($this->getRequest()->get('trade_status') == 'TRADE_SUCCESS') {
			$msg = '支付成功，感谢您的购买！';
			$this->updateOrderItem();
		}
        return $this->render('FrontendWebBundle:Static:yadifenqi.html.twig', array(
			'msg' => $msg,
		));
	}

	/**
	 * 生成支付按钮后自动跳转到支付网关
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @return type
	 */
	public function payProductAction(Request $request)
	{
		// create OrderItem
		$order = new OrderItem();
		$order->setName($request->get('name'));
		$order->setAddress($request->get('address'));
		$order->setPhone($request->get('phone'));
		$now = new \DateTime();
		$order->setCreated($now);
		$title = $request->get('title');
		$order->setTitle($title . ' - ' . date('Y-m-d-H:i:s', time()));
		$order->setPrice($request->get('price'));
		$order->setStatus(0);
		$this->getDoctrine()->getManager()->persist($order);
		$this->getDoctrine()->getManager()->flush();


		/*		 * ************************请求参数************************* */

		//支付类型
		$payment_type = "1";
		//必填，不能修改
		//服务器异步通知页面路径
		$notify_url = $this->getBaseUrl() . $this->generateUrl('gospel_client_bodeli_notify');
		//需http://格式的完整路径，不能加?id=123这类自定义参数
		//页面跳转同步通知页面路径
		$return_url = $this->getBaseUrl() . $request->get('return_url');
		//需http://格式的完整路径，不能加?id=123这类自定义参数，不能写成http://localhost/
		//卖家支付宝帐户
		$seller_email = $this->container->getParameter('gospel_alipay.email');
		//必填
		//商户订单号
		$out_trade_no = $order->getTitle();
		//商户网站订单系统中唯一订单号，必填
		//订单名称
		$subject = $title;
		//必填
		//付款金额
		$total_fee = $request->get('price');
		if (is_array($total_fee)) {
			$total_fee = array_shift($total_fee);
		}
		//必填
		//订单描述

		$body = $request->get('title');
		//商品展示地址
		$show_url = $this->getBaseUrl() . $this->generateUrl('gospel_client_bodeli');
		//需以http://开头的完整路径，例如：http://www.xxx.com/myorder.html
		//防钓鱼时间戳
		$anti_phishing_key = "";
		//若要使用请调用类文件submit中的query_timestamp函数
		//客户端的IP地址
		$exter_invoke_ip = "";
		//非局域网的外网IP地址，如：221.0.0.1


		/*		 * ********************************************************* */

		$payType = $request->get('type', 'alipay');
		$parameter = array(
			"service" => "create_direct_pay_by_user",
			"partner" => trim($this->getParam('pid')),
			"payment_type" => $payment_type,
			"notify_url" => $notify_url,
			"return_url" => $return_url,
			"seller_email" => $seller_email,
			"out_trade_no" => $out_trade_no,
			"subject" => $subject,
			"total_fee" => $total_fee,
			"body" => $body,
			"show_url" => $show_url,
			"anti_phishing_key" => $anti_phishing_key,
			"exter_invoke_ip" => $exter_invoke_ip,
			"_input_charset" => trim(strtolower('utf-8'))
		);

		switch ($payType) {
			case 'alipay':

				break;

			case 'bank':
				//构造要请求的参数数组，无需改动
				$parameter["paymethod"] = "bankPay";
				$parameter["defaultbank"] = $request->get('WIDdefaultbank', 'CMB');

				break;

			default:
				break;
		}



//建立请求
		$alipaySubmit = new AlipaySubmit($this->getAlipayConfig());
		$html_text = $alipaySubmit->buildRequestForm($parameter, "get", "确认");

		return $this->render('GospelAlipayBundle:Alipay:api.html.twig', array(
					'text' => $html_text,
		));
	}

	/**
	 * 
	 * http://gospeltimes.dev/app_dev.php/christmas-gift?body=%E5%9C%A3%E8%AF%9E%E4%BA%A7%E5%93%81%E8%B4%AD%E4%B9%B0&buyer_email=zhili850702%40gmail.com&buyer_id=2088702986940012&exterface=create_direct_pay_by_user&is_success=T&notify_id=RqPnCoPT3K9%252Fvwbh3InQ%252Fmg2BNk96uHPJyLDuZOCnuOAI%252BGWxNQpSb2pRUooc5r9PIfs&notify_time=2014-12-17+10%3A21%3A30&notify_type=trade_status_sync&out_trade_no=%E4%BA%A7%E5%93%81%EF%BC%9A2014-12-17-02%3A20%3A54_127.0.0.1&payment_type=1&seller_email=fuyinshidaish%40sina.com&seller_id=2088711155407722&subject=%E5%9C%A3%E8%AF%9E%E4%BA%A7%E5%93%81_1418782854&total_fee=0.01&trade_no=2014121710940401&trade_status=TRADE_SUCCESS&sign=38bdbbc0a078395ec008868f80ff5ed2&sign_type=MD5
	 * @param Request $request
	 * @return \Frontend\WebBundle\Controller\Response
	 */
	public function notifyAction(Request $request)
	{

		//计算得出通知验证结果
		$alipayNotify = new AlipayNotify($this->getAlipayConfig());
		$verify_result = $alipayNotify->verifyNotify();

		if ($verify_result) {//验证成功
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//请在这里加上商户的业务逻辑程序代
			//——请根据您的业务逻辑来编写程序（以下代码仅作参考）——
			//获取支付宝的通知返回参数，可参考技术文档中服务器异步通知参数列表
			//商户订单号
			$out_trade_no = $request->get('out_trade_no');

			//支付宝交易号

			$trade_no = $request->get('trade_no');

			//交易状态
			$trade_status = $request->get('trade_status');


			if ($request->get('trade_status') == 'TRADE_FINISHED') {
				//判断该笔订单是否在商户网站中已经做过处理
				//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
				//如果有做过处理，不执行商户的业务程序
				//注意：
				//该种交易状态只在两种情况下出现
				//1、开通了普通即时到账，买家付款成功后。
				//2、开通了高级即时到账，从该笔交易成功时间算起，过了签约时的可退款时限（如：三个月以内可退款、一年以内可退款等）后。
				//调试用，写文本函数记录程序运行情况是否正常
				//logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
			} else if ($request->get('trade_status') == 'TRADE_SUCCESS') {
				//判断该笔订单是否在商户网站中已经做过处理
				//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
				//如果有做过处理，不执行商户的业务程序
				//注意：
				//该种交易状态只在一种情况下出现——开通了高级即时到账，买家付款成功后。
				//调试用，写文本函数记录程序运行情况是否正常
				//logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
				
				// 根据用户订单号在数据库中查询
				// 如果没有处理过，则在数据库中保存相关记录
				$this->updateOrderItem();
			}

			//——请根据您的业务逻辑来编写程序（以上代码仅作参考）——

			$msg = "success";  //请不要修改或删除
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		} else {
			//验证失败
			$msg = "fail";

			//调试用，写文本函数记录程序运行情况是否正常
			//logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
		}

		return new Response($msg);
	}

	public function updateOrderItem()
	{
		$order = $this->getCoreRepo('OrderItem')->findOneBy(array(
			'title' => $this->getRequest()->get('out_trade_no')
		));
		if ($order) {
			$order->setStatus(1);
			$this->getDoctrine()->getManager()->persist($order);
			$this->getDoctrine()->getManager()->flush();
		}
	}

}
