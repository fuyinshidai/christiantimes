<?php

/*
 * 精品栏目
 */

namespace Frontend\WebBundle\Controller;

use Frontend\WebBundle\Controller\WebController;
use Symfony\Component\HttpFoundation\Request;
use Frontend\WebBundle\Classes\StringUnity;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class FineColumnController extends WebController
{

	public function indexAction()
	{
		return $this->categoryAction(null, null);
	}

	public function categoryAction($id = null, $slug = null)
	{
		$data = array(
			'id' => $id,
		);
		$repoPost = $this->getCoreRepo('Post');
		$data['categoryType'] = 'CategoryOther';
		$repoCategory = $this->getCoreRepo('CategoryOther');

		$data['title'] = '精品栏目';
		$data['fineCategories'] = $repoCategory->findBy(array(
			'isActive' => true,
		), array(
			'sortOrder' => 'ASC',
		));


		// 查找分类树
		$category = $repoCategory->find($id);
		$data['category'] = $category;

    	$paginator  = $this->get('knp_paginator');
		$data['pagination'] = $repoPost->findByCategoryWithPagitaion($paginator, $category, $this->get('request')->query->get('page', 1));
		$data['top'] = $data['pagination']->current();
		return $this->render('FrontendWebBundle:Post:list.html.twig', $data, $this->getCachedResponse());
	}

}
