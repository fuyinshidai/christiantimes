<?php

namespace Frontend\WebBundle\Controller;

use Frontend\WebBundle\Controller\WebController;
use Symfony\Component\HttpFoundation\Response;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class ChurchController extends WebController
{
	/**
	 */
	public function indexAction($name)
	{
		$data = $this->getChurchData($name);
		return $this->render('FrontendWebBundle:Church:churchIndex.html.twig', $data);
	}

	public function getChurchData($name)
	{
		$postRepo = $this->getCoreRepo('Post');
		$categoryRepo = $this->getCoreRepo('Category');
		$data = array();
		if ($name == '卡城福禾堂') {
			$name = '福禾堂';
		}
		switch ($name) {
			case '福禾堂':
				$category_info = 137;
				$category_news = 134;
				$category_pic = 136;

				$post_about = 13443;
				$post_banner1 = 13686;

				$data['name'] = '卡城福禾网';
				$data['banner'] = '/assets/images/church/fuhetang.jpg';
				$category = $categoryRepo->find($category_news);

				// 搜索最新的图片新闻
				$posts = $postRepo->findByCategory($category, 10, 0, false);
				foreach ($posts as $post) {
					if ($post->getCover()) {
						$data['headline'] = $post;
					}
				}

				// 相关资料
				$category1 = $categoryRepo->find($category_info);
				$data['category1'] = $category1;
				$data['category1Post'] = $postRepo->findByCategory($category1, 4, 0, false);

				// 最新资讯
				$category2 = $categoryRepo->find($category_news);
				$data['category2'] = $category2;
				$data['category2Post'] = $postRepo->findByCategory($category2, 8, 0, false);
				$data['about'] = $postRepo->find($post_about);

				$data['banner1'] = array(
					'src' => '/assets/images/church/fuhetang-banner1.jpg',
					'link' => $this->generateUrl('news_show', array('id' => $post_banner1, 'slug' => '卡城福禾堂')),
				);

				$data['banner2'] = array(
					'src' => '/assets/images/church/fuhetang-banner2.jpg',
					'link' => 'http://s.cnzz.net/?kw=%20%E5%9C%A3%E5%9C%B0%E5%BD%92%E6%9D%A5&site=gospeltimes.cn',
				);

				// 图片新闻
				$data['category_pic'] = $categoryRepo->find($category_pic);
				$data['posts_pic'] = $postRepo->findByCategory($data['category_pic'], 4, 0, false);
				

				break;

			case '青州基督教会':
				// 讲章分类
				$category_info = 45;

				// 最新资讯分类
				$category_news = 48;

				// 图片新闻分类
				$category_pic = 47;

				$post_about = 26638;
				$post_banner1 = 13686;

				$data['name'] = $name;
				$data['banner'] = '/assets/images/church/qingzhoujiaohui.jpg';
				$category = $categoryRepo->find($category_news);

				// 搜索最新的图片新闻
				$posts = $postRepo->findByCategory($category, 10, 0, false);
				foreach ($posts as $key => $post) {
					if ($key == 0) {
						$data['headline'] = $post;
					}
					if ($post->getCover()) {
						$data['headline'] = $post;
					}
				}

				// id 28140 26643 26642 
				// cateory id 45
				// 相关资料
				$category1 = $categoryRepo->find($category_news);
				$data['category1'] = $category1;
				$data['category1Post'] = $postRepo->findByCategory($category1, 4, 0, false);

				// 最新资讯
				$category2 = $categoryRepo->find($category_info);
				$data['category2'] = $category2;
				$data['category2Post'] = $postRepo->findByCategory($category2, 8, 0, false);
				$data['about'] = $postRepo->find($post_about);

				// 图片新闻
				$data['category_pic'] = $categoryRepo->find($category_pic);
				$data['posts_pic'] = $postRepo->findByCategory($data['category_pic'], 4, 0, false);
				

				break;

			default:
				break;
		}

		return $data;
	}

}
