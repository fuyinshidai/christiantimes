<?php

/*
 * 用户读取网站通用数据的Controller，例如header部分，footer部分，右侧通用的内容
 */

namespace Frontend\WebBundle\Controller;

use Frontend\WebBundle\Controller\WebController;
use Symfony\Component\HttpFoundation\Response;

class CommonController extends WebController
{
	/**
	 * 
	 * @param type $section 用户主导航的板块的英文标识，参考service.yml文件里的paramaters部分
	 * @param type $isProvince 是否是省份的主导航，在省份页面里面的主导航和和默认主页的主导航有区别
	 * @return type
	 */
	public function headerAction($section = null, $province = null, $category_id = null, $is_news = null)
	{
		$data = $this->getHeaderData($section, $province, $category_id);
		$data['uri'] = basename($_SERVER['REQUEST_URI']);
		$data['is_news'] = $is_news;
        return $this->render('FrontendWebBundle:Common:header.html.twig', $data, $this->getCachedResponse());
	}

	/**
	 * 
	 * @param type $section 用户主导航的板块的英文标识，参考service.yml文件里的paramaters部分
	 * @param type $isProvince 是否是省份的主导航，在省份页面里面的主导航和和默认主页的主导航有区别
	 * @return type
	 */
	public function headerNewAction($section = null, $province = null, $category_id = null, $is_news = null)
	{
		if(!$headerNewData = $this->getRedis()->get('headerNewData')) {
			/*-----地区版链接----*/
			$repoArea = $this->getCoreRepo('Area');
			$data['beijing'] = $repoArea->findOneBy(array('title'=>'北京'));
			$data['shanghai'] = $repoArea->findOneBy(array('title'=>'上海'));
			$data['guangdong'] = $repoArea->findOneBy(array('title'=>'广东'));
			$data['inHomepage'] = $_SERVER['REQUEST_URI'] === $this->generateUrl('home');
			$data['provinces'] = array(
				$data['beijing'],
				$data['shanghai'],
				$data['guangdong'],
				$repoArea->findOneBy(array('title'=>'浙江')),
				$repoArea->findOneBy(array('title'=>'湖北')),
				$repoArea->findOneBy(array('title'=>'四川')),
				$repoArea->findOneBy(array('title'=>'云南')),
				$repoArea->findOneBy(array('title'=>'陕西')),
			); 

			/*----主导航----*/
			$menus = $this->container->getParameter('frontend.navigation');
			$menusPadShow = $this->container->getParameter('frontend.navigation.pad.show');
			$menusPadHide = $this->container->getParameter('frontend.navigation.pad.hide');
			$data['navigation'] = $menus;
			$data['navigationPadShow'] = $menusPadShow;
			$data['navigationPadHide'] = $menusPadHide;
			$data['uri'] = basename($_SERVER['REQUEST_URI']);
			$data['is_news'] = $is_news;

			// newsletter
			$creteria = array(
				'isActive' => true,
			);
			$categories = $this->getDoctrine()->getRepository('GospelNewsletterBundle:Category')->findBy($creteria);

			$data['newsletterCategorys'] = $categories;


			$headerNewData = $this->renderView('FrontendWebBundle:Common:headerNew.html.twig', $data, $this->getCachedResponse());

			
			$this->getRedis()->set('headerNewData', $headerNewData, self::ONE_WEEK);
		}

		return $this->setCachedResponse(new Response($headerNewData), self::ONE_MONTH);
	}

	public function headerVideoAction()
	{
		$data = $this->getHeaderData();
		// 读取视频分类
		$data['categories'] = $this->getCoreRepo('CategoryVideo')->findAll();
        return $this->render('FrontendWebBundle:Common:headerVideo.html.twig', $data, $this->getCachedResponse());
	}

	public function headerProvinceAction($section = null, $province = null)
	{
		$data = $this->getHeaderData($section, $province);
        return $this->render('FrontendWebBundle:Common:headerProvince.html.twig', $data, $this->getCachedResponse());
	}

	public function footerAction()
	{
		if (!$data = $this->getRedis()->get('footer')) {
			$data['menus'] = $this->container->getParameter('frontend.navigation');

			$repoAbout = $this->getCoreRepo('About');
			$menus = array('爱心奉献', '关于我们', '联系我们', '信仰宣言', '欢迎投稿', '版权声明',);
			$data['about'] = array();
			foreach ($menus as $title) {
				$entity = $repoAbout->findOneBy(array('title' => $title));
				if ($entity) {
					$data['about'][] = $entity;
				}
			}
			$this->getRedis()->set('footer', $data, self::ONE_WEEK);
		}
        return $this->render('FrontendWebBundle:Common:footer.html.twig', $data, $this->getCachedResponse());
	}

	public function footerNewAction()
	{
		if(!$footerNewData = $this->getRedis()->get('footerNewData')) {
			$repoPost = $this->getCoreRepo('Post');
			$data['menus'] = $this->container->getParameter('frontend.navigation');

			$repoAbout = $this->getCoreRepo('About');
			$menus = array('爱心奉献', '关于我们', '联系我们', '信仰宣言', '欢迎投稿', '版权声明',);
			$data['about'] = array();
			foreach ($menus as $title) {
				$entity = $repoAbout->findOneBy(array('title' => $title));
				if ($entity) {
					$data['about'][] = $entity;
				}
			}
			/*----主导航----*/
			$menus = $this->container->getParameter('frontend.navigation');
			$menusPadShow = $this->container->getParameter('frontend.navigation.pad.show');
			$menusPadHide = $this->container->getParameter('frontend.navigation.pad.hide');
			$data['navigation'] = $menus;
			$data['navigationPadShow'] = $menusPadShow;
			$data['navigationPadHide'] = $menusPadHide;

			$footerNewData = $this->renderView('FrontendWebBundle:Common:footerNew.html.twig', $data, $this->getCachedResponse());
			$this->getRedis()->set('footerNewData', $footerNewData, self::ONE_YEAR);
		}

		return $this->setCachedResponse(new Response($footerNewData), self::ONE_MONTH);
	}

	public function bottomAdsAction()
	{
		if ($this->isHomepage()) {
			$view = 'bottom.adsIndex.html';
        	return $this->render('FrontendWebBundle:Common:'.$view.'.twig', array(), $this->getCachedResponse());
		} 
		return new Response('');
	}
	public function bottomAdsArticleAction()
	{
		if ($this->isHomepage()) {
		} else {
			$view = 'bottom.adsSubpage.html';
        	return $this->render('FrontendWebBundle:Common:'.$view.'.twig');
		}
		return new Response('');
	}

	/**
	 * check if current page is homepage
	 * @return type
	 */
	public function isHomepage() {
		return $_SERVER['REQUEST_URI']  === $this->generateUrl('home');
	}

    public function rightSiderbarAction()
    {
		$data = array();
        return $this->render('FrontendWebBundle:Common:right.siderbar.html.twig', $data, $this->getCachedResponse());
    }

    public function rightSiderbarNewAction()
    {
		$redisKey = 'data-right-siderbar-new';
		$data = array();
		$repoPost = $this->getCoreRepo('Post');
		$repoCategory = $this->getCoreRepo('Category');
		/*----每日灵修----*/

		$categorySpirit = $repoCategory->findOneBy(array('title' => '每日灵修'));
		$data['categorySpirit'] = $categorySpirit;
		$headerSpirit = $repoPost->findByCategory($categorySpirit, 1, 0, false);
		$data['headerSpirit'] = $headerSpirit[0];
		$data['headerSpiritRest'] = $repoPost->findByCategory($categorySpirit, 1, 1, false);

		/*----信仰问答----*/
		$categoryQA = $repoCategory->findOneBy(array('title' => '双语灵修'));
		$data['categoryQA'] = $categoryQA;
		$headerSpirit = $repoPost->findByCategory($categoryQA, 1, 0, false);
		$data['headerQA'] = $headerSpirit[0];
		$data['headerQARest'] = $repoPost->findByCategory($categoryQA, 2, 1, false);

		/*----热门视频----*/
		if(!$rightVideoNew = $this->getRedis()->get('rightVideoNew')) {
			// 读取视频
			$videos = array();
			$videosTmp = $this->getRepository('GospelCoreBundle:Video')->getPromoted(3);
			foreach ($videosTmp as $key => $video) {
				$video->setEmbedCode(str_ireplace(array(
					'width',
					'height',
					'true', // auto paly
					'><',
					'auto=1', // autoplay
				), array(
					'data-width',
					'data-height',
					'false',
					' style="width:100%;" height="237" ><',
					'auto=0', // autoplay
				), html_entity_decode($video->getEmbedCode())));
				$videos[] = $this->getRepository('GospelCoreBundle:Video')->normalize($video);
			}
			$data['video'] = array_shift($videos);
			$data['videos'] = isset($videos)?$videos:null;
			$this->getRedis()->set('rightVideoNew', $rightVideoNew, self::ONE_DAY);
		}

		/*----最新专题----*/
		if (!$dataTopics = $this->getRedis()->get('rightTopicsNew')) {
			$repoTopic = $this->getDoctrine()->getRepository('GospelCoreBundle:Topic');
			// 读取最新专题
			$dataTopics['topicsTop'] = $repoTopic->normalizeArray($repoTopic->findBy(array('status' => 1), array('id' => 'DESC'), 1));
			$dataTopics['topicsList'] = $repoTopic->normalizeArray($repoTopic->findBy(array('status' => 1), array('id' => 'DESC'), 3, 1));
			$this->getRedis()->set('rightTopicsNew', $dataTopics, self::ONE_DAY);
		}
		$data['dataTopics'] = $dataTopics;


		/*----公告----*/
		if (!$dataBroadcast = $this->getRedis()->get('rightBroadcasts')) {
			$repoPost = $this->getCoreRepo('Post');
			$repoFineColumns = $this->getCoreRepo('Categoryother');
			// 右侧公告
			$categoryBroadcast = $repoFineColumns->findOneBy(array('title' => '公告'));
			$dataBroadcast['broadcasts'] = $repoPost->normalizeArray($repoPost->findByFineCategory($categoryBroadcast, 2));
			$dataBroadcast['categoryBroadcast'] = $categoryBroadcast;
			$this->getRedis()->set('rightBroadcasts', $dataBroadcast, self::ONE_DAY);
		}
		$data['broadCast'] = $dataBroadcast;

		/*----今日热门----*/
		if (!$dataHots = $this->getRedis()->get('rightHots')) {
			$postHots = $this->getRepository('GospelCoreBundle:Post')->normalizeArray($this->getRepository('GospelCoreBundle:Post')->getHot(10, '-3 days', '0 days'));
			$this->getRedis()->set('rightHots', $postHots, self::ONE_DAY);
		}
		$data['hots'] = $dataHots;

		/*----图片集----*/
		if (!$galleries = $this->getRedis()->get('rightGalleryNew')) {
			$data = array();
			// 读取图片集内容
			$repoGallery = $this->getCoreRepo('Gallery');
			$galleries = $repoGallery->normalizeArray($repoGallery->findLatest(3));
			$this->getRedis()->set('rightGalleryNew', $galleries, self::ONE_DAY);
		}
		$data['gallery'] = array_shift($galleries);
		$data['galleries'] = $galleries;

		/*----一周热门----*/
		if (!$dataHotsWeek = $this->getRedis()->get('rightHotsWeek')) {
			$postHotsWeek = $this->getRepository('GospelCoreBundle:Post')->normalizeArray($this->getRepository('GospelCoreBundle:Post')->getHot(10, '-10 days', '0 days'));
			$this->getRedis()->set('rightHotsWeek', $postHotsWeek, self::ONE_DAY);
		}
		$data['hotsWeek'] = $dataHotsWeek;

		$this->getRedis()->set($redisKey, $data, self::ONE_HOUR);
        return $this->render('FrontendWebBundle:Common:right.siderbarNew.html.twig', $data, $this->getCachedResponse());
    }

	/**
	 * 右侧推荐视频模块
	 * @return type
	 */
	public function rightVideoAction()
	{
		if(!$data = $this->getRedis()->get('rightVideo')) {
			$data = array();
			// 读取视频
			$videos = array();
			$videosTmp = $this->getRepository('GospelCoreBundle:Video')->getPromoted(3);
			foreach ($videosTmp as $key => $video) {
				$video->setEmbedCode(str_ireplace(array(
					'true', // auto paly
					'><',
					'auto=1', // autoplay
				), array(
					'false',
					' width="359" height="240" ><',
					'auto=0', // autoplay
				), html_entity_decode($video->getEmbedCode())));
				$videos[] = $this->getRepository('GospelCoreBundle:Video')->normalize($video);
			}
			$data['videos'] = isset($videos)?$videos:null;
			$data['video'] = isset($data['videos'][0])?$data['videos'][0]:null;

			$this->getRedis()->set('rightVideo', $data, self::ONE_DAY);
		}

        return $this->render('FrontendWebBundle:Common:right.video.html.twig', $data, $this->getCachedResponse());
	}

	/**
	 * 页面右侧热门文章
	 * @return type
	 */
	public function rightHotNewsAction()
	{
		if (!$data = $this->getRedis()->get('rightHot')) {
			$data = array();
			// 热门文章
			$posts = $this->getRepository('GospelCoreBundle:Post')->normalizeArray($this->getRepository('GospelCoreBundle:Post')->getHot(10));
			$data['posts'] = $posts;
			$this->getRedis()->set('rightHot', $data, self::ONE_DAY);
		}

        return $this->render('FrontendWebBundle:Common:right.hot.html.twig', $data, $this->getCachedResponse());
	}

	public function editorChoiceAction($province = null)
	{
		if (!$data = $this->getRedis()->get('editorChoice')) {
			$data['posts'] = $this->getRepository('GospelCoreBundle:Post')->normalizeArray($this->getRepository('GospelCoreBundle:Post')->findMany(REC_EDITOR, 4, null, array('province'=>$province)));
			$class = '';
			if ($province) {
				$class = 'mb30';
			}
			$data['options'] = array(
				'class' => $class,
			);
			$this->getRedis()->set('editorChoice', $data, self::ONE_DAY);
			
		}

        return $this->render('FrontendWebBundle:Common:editorChoice.html.twig', $data, $this->getCachedResponse());
	}

	/**
	 * 右侧广告
	 * @return type
	 */
	public function rightAdAction()
	{
        return $this->render('FrontendWebBundle:Common:rightAd.html.twig', array(), $this->getCachedResponse());
	}

	/**
	 * 右侧广告
	 * @return type
	 */
	public function rightArticleAdAction()
	{
        return $this->render('FrontendWebBundle:Common:rightArticleAd.html.twig', array(), $this->getCachedResponse());
	}

	private function getHeaderData($section = null, $province = null, $category_id = null)
	{
		if (!$headerData = $this->getRedis()->get('headerData')) {
			$repoArea = $this->getCoreRepo('Area');
			$data = array();
			$menus = $this->container->getParameter('frontend.navigation');
			foreach ($menus as $key => $menu) {
				if ($menu['slug'] == $section) {
					$menus[$key]['current'] = 1;
				} else {
					$menus[$key]['current'] = 0;
				}
			}
			$data['menus'] = $menus;
			$data['navigation'] = $menus;
			$data['section'] = $section;
			$data['category_id'] = $category_id;



			// 读取相关的省份
			$data['beijing'] = $repoArea->findOneBy(array('title'=>'北京'));
			$data['shanghai'] = $repoArea->findOneBy(array('title'=>'上海'));
			$data['guangdong'] = $repoArea->findOneBy(array('title'=>'广东'));
			$data['inHomepage'] = $_SERVER['REQUEST_URI'] === $this->generateUrl('home');
			$data['provinces'] = array(
				$data['beijing'],
				$data['shanghai'],
				$data['guangdong'],
				$repoArea->findOneBy(array('title'=>'浙江')),
				$repoArea->findOneBy(array('title'=>'湖北')),
				$repoArea->findOneBy(array('title'=>'四川')),
				$repoArea->findOneBy(array('title'=>'云南')),
				$repoArea->findOneBy(array('title'=>'陕西')),
			);
		
			$headerData = $data;
			$this->getRedis()->set('headerData', $data, self::ONE_WEEK);

		}

		return $headerData;
	}


	/**
	 * 右侧热门文章
	 */
	public function rightHotAction()
	{
		if (!$data = $this->getRedis()->get('rightHot')) {
			$repoPost = $this->getCoreRepo('Post');
			$data['hotsOneDay'] = $repoPost->normalizeArray($repoPost->getHot(10, '-5 days', '-1 days'));
			$data['hotsTenOneDay'] = $repoPost->normalizeArray($repoPost->getHot(10, '-10 days', '-0 days'));
			$this->getRedis()->set('rightHot', $data, self::ONE_DAY);
		}
        return $this->render('FrontendWebBundle:Common:rightHot.html.twig', $data, $this->getCachedResponse());
	}

	/**
	 * right side bar other section's newest articles
	 */
	public function rightOtherSectionNewestAction($excludeCategory)
	{
		$repoPost = $this->getCoreRepo('Post');
		$posts = $repoPost->findByCategoryExclude($excludeCategory, 7, 0, false);
		return $this->render('FrontendWebBundle:Common:rightOtherSection.html.twig', array(
			'posts' => $posts
		), $this->getCachedResponse());
	}

	/**
	 * right topics
	 */
	public function rightTopicsAction()
	{
		if (!$data = $this->getRedis()->get('rightTopics')) {
			$repoTopic = $this->getDoctrine()->getRepository('GospelCoreBundle:Topic');
			// 读取最新专题
			$data['topicsTop'] = $repoTopic->normalizeArray($repoTopic->findBy(array('status' => 1), array('id' => 'DESC'), 1));
			$data['topicsLeft'] = $repoTopic->normalizeArray($repoTopic->findBy(array('status' => 1), array('id' => 'DESC'), 3, 1));
			$data['topicsRight'] = $repoTopic->normalizeArray($repoTopic->findBy(array('status' => 1), array('id' => 'DESC'), 3, 4));
			$this->getRedis()->set('rightTopics', $data, self::ONE_DAY);
		}

		return $this->render('FrontendWebBundle:Common:rightTopics.html.twig', $data, $this->getCachedResponse());
	}

	/**
	 * Right gallery
	 */
	public function rightGalleryAction()
	{
		if (!$data = $this->getRedis()->get('rightGallery')) {
			$data = array();
			// 读取图片集内容
			$repoGallery = $this->getCoreRepo('Gallery');
			$galleries = $repoGallery->normalizeArray($repoGallery->findLatest(4));
			$data['gallery'] = array_shift($galleries);
			$data['galleries'] = $galleries;
			$this->getRedis()->set('rightGallery', $data, self::ONE_DAY);
		}
		return $this->render('FrontendWebBundle:Common:rightGallery.html.twig', $data, $this->getCachedResponse());
	}


	/**
	 * 精品栏目列表
	 * @return type
	 */
	public function fineColumnAction()
	{
		// 精品栏目
		if (!$data = $this->getRedis()->get('fineColumns')) {
			$repoPost = $this->getCoreRepo('Post');
			$repoFineColumns = $this->getCoreRepo('Categoryother');
			$fineColumnsNames = array(
				'译稿',
				'专访',
				'特稿',
				'时评',
				'基督文化',
				'基督徒艺人',
				'环球视野',
				'学术',
			);
			$fineColumns = array();
			foreach ($fineColumnsNames as $key => $name) {
				$fineColumns[$key]['category'] = $repoFineColumns->findOneBy(array('title' => $name));
				// 读取该精品栏目下的最新发布文章
				$fineColumns[$key]['post'] = $repoPost->normalize($repoPost->findOneByFineCategory($fineColumns[$key]['category']));
			}
			$data['fineColumns'] = $fineColumns;

			$this->getRedis()->set('fineColumns', $data, self::ONE_HOUR);
		}

		return $this->render('FrontendWebBundle:Common:fineColumn.html.twig', $data);
	}

	public function categoryListAction()
	{
		if (!$data = $this->getRedis()->get('categoryList')) {
			$repoPost = $this->getCoreRepo('Post');
			$repoCategory = $this->getCoreRepo('Category');

			// 读取多个分类的文章
			$categoryChurch = $repoCategory->findOneBy(array('title' => '教会'));
			$categorySociety = $repoCategory->findOneBy(array('title' => '社会'));
			$categoryCulture = $repoCategory->findOneBy(array('title' => '文化'));
			$categoryGlobal = $repoCategory->findOneBy(array('title' => '国际'));
			$categoryCharity = $repoCategory->findOneBy(array('title' => '公益'));
			$categoryFamily = $repoCategory->findOneBy(array('title' => '家庭'));
			$categoryBusiness = $repoCategory->findOneBy(array('title' => '商业'));
			$categoryLife = $repoCategory->findOneBy(array('title' => '生活'));
			$data['categoryChurch'] = $categoryChurch;
			$data['categorySociety'] = $categorySociety;
			$data['categoryCulture'] = $categoryCulture;
			$data['categoryGlobal'] = $categoryGlobal;
			$data['categoryCharity'] = $categoryCharity;
			$data['categoryBusiness'] = $categoryBusiness;
			$data['categoryLife'] = $categoryLife;
			$data['categoryFamily'] = $categoryFamily;
			$categoryList1 = array(
				$categoryGlobal,
			);
			$categoryList2 = array(
				$categoryChurch,
			);
			$categoryList3 = array(
				$categorySociety,
				$categoryCharity
			);
			$categoryList4 = array(
				$categoryCulture,
			);
			$categoryList5 = array(
				$categoryFamily
			);
			$categoryList6 = array(
				$categoryBusiness,
				$categoryLife,
			);
			$data['categoryList1'] = $repoPost->findByCategory($categoryList1, 3, 4, false);
			$data['categoryList2'] = $repoPost->findByCategory($categoryList2, 3, 4, false);
			$data['categoryList3'] = $repoPost->findByCategory($categoryList3, 3, 4, false);
			$data['categoryList4'] = $repoPost->findByCategory($categoryList4, 3, 4, false);
			$data['categoryList5'] = $repoPost->findByCategory($categoryList5, 3, 4, false);
			$data['categoryList6'] = $repoPost->findByCategory($categoryList6, 3, 4, false);

			$this->getRedis()->set('categoryList', $data, self::ONE_DAY);
		}

		return $this->render('FrontendWebBundle:Common:categoryList.html.twig', $data, $this->getCachedResponse());
	}

	public function rightInsideAction()
	{
		if (!$data = $this->getRedis()->get('rightInside')) {
			$repoPost = $this->getCoreRepo('Post');
			$data['insides'] = $repoPost->normalizeArray($repoPost->findMany(REC_INSIDE, 7, 10));
			$this->getRedis()->set('rightInside', $data, self::ONE_DAY);
		}
		return $this->render('FrontendWebBundle:Common:rightInside.html.twig', $data, $this->getCachedResponse());
	}

	public function rightViewpointsAction()
	{
		if (!$data = $this->getRedis()->get('rightViewPoints')) {
			$repoPost = $this->getCoreRepo('Post');
			$repoFineColumns = $this->getCoreRepo('Categoryother');

			// 读取右侧观点专栏列表
			$categoryViewpoint = $repoFineColumns->findOneBy(array('title' => '观点.专栏'));
			$data['viewpoints'] = $repoPost->normalizeArray($repoPost->findByFineCategory($categoryViewpoint, 3));
			$data['categoryViewpoint'] = $categoryViewpoint;
			$this->getRedis()->set('rightViewPoints', $data, self::ONE_DAY);
		}
		return $this->render('FrontendWebBundle:Common:rightViewpoints.html.twig', $data, $this->getCachedResponse());
	}

	/**
	 * Find post by category
	 * @return type
	 */
	public function bibleAndManagementAction() {
		if (!$data = $this->getRedis()->get('bibleAndManagement')) {
			$repoPost = $this->getCoreRepo('Post');
			$repoCategory = $this->getCoreRepo('Category');
			$categoryTitle = '基督徒在职场';

			$category = $repoCategory->findOneBy(array(
				'title' => $categoryTitle,
			));
			$posts = $repoPost->normalizeArray($repoPost->findByCategory($category, 1, 0, false));
			$posts2 = $repoPost->normalizeArray($repoPost->findByCategory($category, 2, 1, false));

			$data = array(
				'category' => $category,
				'posts' => $posts,
				'posts2' => $posts2,
			);

			$this->getRedis()->set('bibleAndManagement', $data, self::ONE_DAY);
		}


		return $this->render('FrontendWebBundle:Common:bibleAndManagement.html.twig', $data, $this->getCachedResponse());
	}

	public function rightBroadcastsAction()
	{
		if (!$data = $this->getRedis()->get('rightBroadcasts')) {
			$repoPost = $this->getCoreRepo('Post');
			$repoFineColumns = $this->getCoreRepo('Categoryother');
			// 右侧公告
			$categoryBroadcast = $repoFineColumns->findOneBy(array('title' => '公告'));
			$data['broadcasts'] = $repoPost->normalizeArray($repoPost->findByFineCategory($categoryBroadcast, 2));
			$data['categoryBroadcast'] = $categoryBroadcast;
			$this->getRedis()->set('rightBroadcasts', $data, self::ONE_DAY);
		}
		return $this->render('FrontendWebBundle:Common:rightBroadcasts.html.twig', $data, $this->getCachedResponse());
	}

	public function rightMediaFocusAction()
	{
		if (!$data = $this->getRedis()->get('rightMediaFocus')) {
			$repoPost = $this->getCoreRepo('Post');
			$repoFineColumns = $this->getCoreRepo('Categoryother');
			// 媒体聚焦
			$mediaFocus = $repoFineColumns->findOneBy(array('title' => '媒体聚焦'));
			$data['mediaFocus'] = $repoPost->normalizeArray($repoPost->findByFineCategory($mediaFocus, 5));
			$data['categorymediaFocus'] = $mediaFocus;
			$this->getRedis()->set('rightMediaFocus', $data, self::ONE_DAY);
		}

		return $this->render('FrontendWebBundle:Common:rightMeidaFocus.html.twig', $data, $this->getCachedResponse());
	}
}
