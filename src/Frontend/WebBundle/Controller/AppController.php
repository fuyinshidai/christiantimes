<?php

/*
 * 给移动客户端做的后台数据接口
 */

namespace Frontend\WebBundle\Controller;

use Frontend\WebBundle\Controller\WebController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class AppController extends WebController
{
	/**
	 * 拉取频道列表
	 * @return type
	 */
	public function channelsAction()
	{
		$categories = $this->getCoreRepo('Category')->getMenuCategory();
		return $this->renderXml('channels', array(
			'categories' => $categories
		));
	}

	public function channelsDevAction()
	{
		$categories = $this->getCoreRepo('Category')->getMenuCategory();
		return $this->renderXml('channelsDev', array(
			'categories' => $categories
		));
	}

	/**
	 * 拉取指定频道的新闻
	 * @return type
	 */
	public function channelNewsAction($channel_id, $type)
	{
		
		switch ($type) {
			case 'news':
				if ($channel_id == 10000) { // 列表新闻20条包括：文字头条1条，深读新闻5条，小图新闻14条.
				// url为/app/channels/{homepageId}/news
					$repoPost = $this->getDoctrine()->getRepository('GospelCoreBundle:Post');
					$news = array();
					//$news = array_merge($news, $repoPost->findMany(REC_INSIDE, 5));
					$news = array_merge($news, $repoPost->findMany(REC_HEADLINE_PIC,20));
				}else if($channel_id == 999){
					$repoPost = $this->getCoreRepo('Post');
					$news = array();

					$news = array_merge($news, $repoPost->findMany(REC_INSIDE, 20));
				}else {
					$news = array();
					$para = array('r' => 1);
					$category = $this->getCoreRepo('Category')->find($channel_id);
					$headlines = $this->getCoreRepo('Post')->findByCategory($category, 20, 0, false, $para);
					if (is_array($headlines) && count($headlines)) {
						foreach ($headlines as $headline) {
							if ($headline->getCover()) {
								$news =array_diff($headlines, array($headline));
								break;
							}
						}
					}
				}
				break;

			case 'Headline':
				// 头条大图新闻，url为/app/channels/{homepageId}/Headline
				if($channel_id == 10000){
					$news = array($this->getCoreRepo('Post')->findOne(REC_HEADLINE_PIC));
					break;
				}else{
				$news = array();
					$para = array('r' => 1);
					$category = $this->getCoreRepo('Category')->find($channel_id);
					$headlines = $this->getCoreRepo('Post')->findByCategory($category, 20, 0, false, $para);
					if (is_array($headlines) && count($headlines)) {
						foreach ($headlines as $headline) {
							if ($headline->getCover()) {
								$news =array($headline);
								break;
							}
						}
					}
				}
				break;
			default:
				break;
		}
		return $this->renderXml('news', array(
			'host' => $this->get('request')->server->get('HTTP_HOST'),
			'newsSet' => $news,
		));
	}

	/**
	 * 
	 * 拉取指定频道更多的新闻, 或者是读取修
	 * @param integer $channel_id 新闻分类id
	 * @param integer $lastNewsId 新闻id
	 * @param string $type more 或者 recent
	 * @return Response
	 */
	public function channelNewsMoreAction($channel_id, $lastNewsId, $type)
	{
		if ($type == 'more') {
			$key = 'lt';
		} else {
			$key = 'gt';
		}
		$para = array(
			'r' => 1,
			$key => array(
				'id' => $lastNewsId,
			),
		);
		
		$repoPost = $this->getCoreRepo('Post');
		if($channel_id == 10000){
			$news = $repoPost->findMany(REC_HEADLINE_PIC, 20, 0, $para);
		}else if($channel_id == 999){
			$news =$repoPost->findMany(REC_HEADLINE_PIC, 20, 0, $para);
		}else{
			$category = $this->getCoreRepo('Category')->find($channel_id);
			$headlines = $this->getCoreRepo('Post')->findByCategory($category, 20, 0, false, $para);
			if (is_array($headlines) && count($headlines)) {
				foreach ($headlines as $headline) {
					if ($headline->getCover()) {
						$news =array_diff($headlines, array($headline));
						break;
					}
				}
			}
		}
		
		
		return $this->renderXml('news', array(
			'host' => $this->get('request')->server->get('HTTP_HOST'),
			'newsSet' => $news,
		));
	}

	/**
	 * @return type
	 */
	public function newsAction($newsId, $type)
	{
		$news = $this->getCoreRepo('Post')->find($newsId);

		if ($type === 'prev') {
			$news = $this->getCoreRepo('Post')->findPrev($newsId);
		}
		if ($type === 'next') {
			$news = $this->getCoreRepo('Post')->findNext($newsId);
		}
		// 如果文章不存在或者文章没有发布:退出
		if (!$news) {
			$news = null;
		}

		// 文章没有发布并且没有预览的权限
		if ($news instanceof \Gospel\Bundle\CoreBundle\Entity\Post) {
			if ($news->getStatus() != POST_STATUS_PUBLISHED) {
				throw $this->createNotFoundException("您所查找的文章不存在！");
			}
		}

		if($type === 'show'){
			$response = new Response();
			$response->headers->set('Content-Type', 'Text');
			return $this->render('FrontendWebBundle:App:show.html.twig', array(
				'host' => $this->get('request')->server->get('HTTP_HOST'),
				'news' => $news,
				'type' => $type,
			), $response);
		}


		return $this->renderXml('show', array(
			'host' => $this->get('request')->server->get('HTTP_HOST'),
			'news' => $news,
			'type' => $type,
		));
	}

	/**
	 * @return type
	 */
	public function topicsAction()
	{
		$repoTopic = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:Topic');
		$paginator  = $this->get('knp_paginator');
		$pagination = $repoTopic->findAllWithPagitaion($paginator, $this->get('request')->query->get('page', 1), null,20);
		$topics = $pagination->getItems();
		
		return $this->renderXml('topics', array(
			'topics' => $topics,
			'host' => $this->get('request')->server->get('HTTP_HOST'),
		));
	}

	/**
	 * @return type
	 */
	public function topicsMoreRecentAction($topicId, $type)
	{
		switch ($type) {
			case 'more':
				$para = array(
						'r' => 1,
						'lt' => array(
								'id' => $topicId,
						),
				);
				$repoTopic = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:Topic');
						$topics = $repoTopic->findMore(20, 0 ,$para);
				return $this->renderXml('topics', array(
					'topics' => $topics,
					'host' => $this->get('request')->server->get('HTTP_HOST'),
				));
				break;

			case 'recent':
				$para = array(
						'r' => 1,
						'gt' => array(
								'id' => $topicId,
						),
				);
				$repoTopic = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:Topic');
						$topics = $repoTopic->findMore(20, 0 ,$para);
				return $this->renderXml('topics', array(
					'topics' => $topics,
					'host' => $this->get('request')->server->get('HTTP_HOST'),
				));
				break;

			case 'news':
				$repoTopic = $this->getCoreRepo('Topic');
				
				// TODO: 读取当前的专题
				$topic= $repoTopic->find($topicId);
				//var_dump($postsInTopic);
				return $this->renderXml('topicNews', array(
					'host' => $this->get('request')->server->get('HTTP_HOST'),
					'newsSet' => $topic,
				));
				break;

			default:
				break;
		}
	}

	public function slideshowsAction()
	{
		$repo = $this->getCoreRepo('Post');
		$data = $repo->getPostHasCover(20);
		$news = array();
		foreach ($data as $row) {
			$news[] = $repo->find($row['id']);
		}
		return $this->renderXml('news', array(
			'host' => $this->get('request')->server->get('HTTP_HOST'),
			'newsSet' => $news,
		));
	}

	/**
	 */
	public function slideshowsMoreAction($id, $type)
	{
		if ($type == 'more') {
			$key = 'lt';
		} else {
			$key = 'gt';
		}

		$repo = $this->getCoreRepo('Post');
		$data = $repo->getPostHasCover(20, $type, $id);
		$news = array();
		foreach ($data as $row) {
			$news[] = $repo->find($row['id']);
		}

		return $this->renderXml('news', array(
			'host' => $this->get('request')->server->get('HTTP_HOST'),
			'newsSet' => $news,
		));
	}

	/**
	 * @return type
	 */
	public function versionAction()
	{
		$version = 8;
		$name = "christiantimes2.0.apk";
		$url = "http://www.christiantimes.cn/assets/download/christiantimes2.0.apk";
		$version_information = "基督时报新的版本更新了！全新的界面，全新的功能。";
		return $this->renderXml('version', array(
			'version' => $version,
			'name' => $name,
			'url' => $url,
			'information' => $version_information,
		));
	}

	/**
	 * @return type
	 */
	public function getVersionAction($id)
	{
		$content = "download version $id";

        $response = new Response();

        $response->headers->set('Content-Type', 'text/csv');
		$filename = dirname(dirname($path)).'/Resources/app/new.txt';
        $response->headers->set('Content-Disposition', 'attachment;filename="'.$filename);

        $response->setContent($content);
        return $response;
	}

	/**
	 * shortcut for render xml format view
	 * @param type $view the view name without .xml.twig post fix
	 * @param array $data the key => value array passed to the view file
	 * @return Response
	 */
	private function renderXml($view, $data)
	{
		$response = new Response();
		$response->headers->set('Content-Type', 'xml');
		return $this->render('FrontendWebBundle:App:'.$view.'.xml.twig', $data, $response);
	}


}
