<?php

/*
 * 网站json接口
 */

namespace Frontend\WebBundle\Controller;

use Frontend\WebBundle\Controller\WebController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class ApiController extends WebController {

	private $cacheTime = 360;

	/**
	 * 文章分类列表页面
	 * @param type $id
	 * @return type
	 */
	public function categoryAction(Request $request) {
		define('RECOMMEND_CATEGORY', 'recommend');
		define('CATEGORY_DEEP', 'deep');
		$numberPerPage = 20;
		$categoryId = $request->get('id');
		$limit = $request->get('limit', $numberPerPage);
		$offset = $request->get('offset', 0);
		$sortOrder = $request->get('order', 'DESC');


		$redisKey = "api-category-$categoryId-$limit-$offset";
		$redisKeyLink = "api-category-$categoryId-$limit-$offset-link";
		if (!$data = $this->getRedis()->get($redisKey) && !$link = $this->getRedis()->get($redisKeyLink)) {

			$data = array();
			$posts = array();
			$repoPost = $this->getCoreRepo('Post');

			// 首页推荐
			if($categoryId == RECOMMEND_CATEGORY) {

				if($offset > 0) {
					$posts = $repoPost->getRecommended($limit, $offset, $this->getDoctrine()->getManager());
				} else { // 当请求为第一页的时候读取首屏内容
					// 首屏内容
					$repoPost = $this->getDoctrine()->getRepository('GospelCoreBundle:Post');
					$repoCategory = $this->getDoctrine()->getRepository('GospelCoreBundle:Category');

					/*----首页中间大图头条----*/
					/*** 读取教会相关分类 ***/
					if (!$categoryChurch = $this->getRedis()->get('categoryChurch')) {
						$categoryChurch = $repoCategory->findOneBy(array('title' => '教会'));

						// 设置该分类下面的子分类
						$categoryChurchSubcategories = array($categoryChurch);
						foreach ($categoryChurch->getChildren() as $subCategory) {
							$categoryChurchSubcategories[] = $subCategory;
						}
						$this->getRedis()->set('categoryChurch', $categoryChurch, self::ONE_DAY);
						$this->getRedis()->set('categoryChurchSubCategories', $categoryChurchSubcategories, self::ONE_DAY);
					}

					$categoryChurchSubCategories = $this->getRedis()->get('categoryChurchSubCategories');

					/*** 读取社会相关分类 ***/
					if (!$categorySociety = $this->getRedis()->get('categorySociety')) {
						$categorySociety = $repoCategory->findOneBy(array('title' => '社会'));

						// 设置子分类
						$categorySocietySubCategories = $repoCategory->getAllChild($categorySociety);
						$this->getRedis()->set('categorySociety', $categorySociety, self::ONE_DAY);
						$this->getRedis()->set('categorySocietySubCategories', $categorySocietySubCategories, self::ONE_DAY);
					}
					$categorySocietySubCategories = $this->getRedis()->get('categorySocietySubCategories');

					/*** 读取文化分类 ***/
					if (!$categoryCulture = $this->getRedis()->get('categoryCulture')) {
						$categoryCulture = $repoCategory->findOneBy(array('title' => '文化'));

						$categoryCultureSubCategories = $repoCategory->getAllChild($categoryCulture);
						$this->getRedis()->set('categoryCulture', $categoryCulture, self::ONE_DAY);
						$this->getRedis()->set('categoryCultureSubCategories', $categoryCultureSubCategories, self::ONE_DAY);
					}
					$categoryCultureSubCategories = $this->getRedis()->get('categoryCultureSubCategories');

					/*** 读取国际分类 ***/
					if (!$categoryGlobal = $this->getRedis()->get('categoryGlobal')) {
						$categoryGlobal = $repoCategory->findOneBy(array('title' => '国际'));
						$this->getRedis()->set('categoryGlobal', $categoryGlobal, self::ONE_DAY);

						$categoryGlobalSubCategories = $repoCategory->getAllChild($categoryGlobal);
						$this->getRedis()->set('categoryGlobalSubCategories', $categoryGlobalSubCategories, self::ONE_DAY);
					}
					$categoryGlobalSubCategories = $this->getRedis()->get('categoryGlobalSubCategories');

					/*** 读取公益分类 ***/
					if (!$categoryCharity = $this->getRedis()->get('categoryCharity')) {
						$categoryCharity = $repoCategory->findOneBy(array('title' => '公益'));
						if($categoryCharity) {
							$categoryCharitySubCategories = array($categoryCharity);
							foreach($categoryCharity->getChildren() as $row) {
								$categoryCharitySubCategories[] = $row;
							}
							$this->getRedis()->set('categoryCharity', $categoryCharity, self::ONE_DAY);
							$this->getRedis()->set('categoryCharitySubCategories', $categoryCharitySubCategories, self::ONE_DAY);
						} else {
							$this->getRedis()->set('categoryCharitySubCategories', array(), self::ONE_HOUR);
						}
					}
					$categoryCharitySubCategories = $this->getRedis()->get('categoryCharitySubCategories');

					/*** 读取家庭分类 ***/
					if (!$categoryFamily = $this->getRedis()->get('categoryFamily')) {
						$categoryFamily = $repoCategory->findOneBy(array('title' => '家庭'));
						$categoryFamilySubCategories = array($categoryFamily);
						foreach($categoryFamily->getChildren() as $row) {
							$categoryFamilySubCategories[] = $row;
						}
						$this->getRedis()->set('categoryFamily', $categoryFamily, self::ONE_DAY);
						$this->getRedis()->set('categoryFamilySubCategories', $categoryFamilySubCategories, self::ONE_DAY);
					}
					$categoryFamilySubCategories = $this->getRedis()->get('categoryFamilySubCategories');

					/*** 读取商业分类 ***/
					if (!$categoryBusiness = $this->getRedis()->get('categoryBusiness')) {
						$categoryBusiness = $repoCategory->findOneBy(array('title' => '商业'));
						$categoryBusinessSubCategories = array($categoryBusiness);
						foreach($categoryBusiness->getChildren() as $row) {
							$categoryBusinessSubCategories[] = $row;
						}
						$this->getRedis()->set('categoryBusiness', $categoryBusiness, self::ONE_DAY);
						$this->getRedis()->set('categoryBusinessSubCategories', $categoryBusinessSubCategories, self::ONE_DAY);
					}
					$categoryBusinessSubCategories = $this->getRedis()->get('categoryBusinessSubCategories');

					/*** 读取生活分类 ***/
					if (!$categoryLife = $this->getRedis()->get('categoryLife')) {
						$categoryLife = $repoCategory->findOneBy(array('title' => '生活'));
						$categoryLifeSubCategories = array($categoryLife);
						foreach($categoryLife->getChildren() as $row) {
							$categoryLifeSubCategories[] = $row;
						}
						$this->getRedis()->set('categoryLife', $categoryLife, self::ONE_DAY);
						$this->getRedis()->set('categoryLifeSubCategories', $categoryLifeSubCategories, self::ONE_DAY);
					}
					$categoryLifeSubCategories = $this->getRedis()->get('categoryLifeSubCategories');

					$recommendData = array(
						'recommend' => array(
							'type' => 'isHomeHeadlinePic',
							'value' => 1,
						)
					);

					$recommendDataRest = array(
						'recommend' => array(
							'type' => 'isHomePic',
							'value' => 1,
						)
					);

					// 记录已经读取的文章id，防止重复
					self::$postIds = array();

					// 首页图片推荐
					$categoryRecommend1 = array(
						$categoryChurchSubCategories,
						$categoryGlobalSubCategories,
						$categorySocietySubCategories,
						$categoryCultureSubCategories,
						$categoryCharitySubCategories,
						$categoryFamilySubCategories,
						$categoryBusinessSubCategories,
						$categoryLifeSubCategories,
					);
					$categoryRecommend2 = array(
						$categoryChurchSubCategories,
						$categoryGlobalSubCategories,
					);
					$headline1 = $repoPost->findByCategory($categoryRecommend1, 1, 0, false, $recommendData);

					$posts = array();
					$ids = array();
					foreach($headline1 as $row) {
						$ids[] = $row->getId();
						$posts[] = $row;
					}

					$posts[] = $repoPost->findOne(REC_HEADLINE_TXT);
					$posts[] = $repoPost->findOne(REC_HEADLINE_TXT1);
					$posts[] = $repoPost->findOne(REC_HEADLINE_TXT2);
					$posts[] = $repoPost->findOne(REC_HEADLINE_TXT3);

					$headlineRestTmp = $repoPost->findByCategory($categoryRecommend1, 4, 0, false, $recommendDataRest);
					foreach ($headlineRestTmp as $row) {
						if(!in_array($row->getId(), $ids)) {
							$posts[] = $row;
							$ids[] = $row->getId();
						}
					}

					$subheadline3Tmp = $repoPost->findExpireHeadlines(17);
					
					foreach($subheadline3Tmp as $row) {
						if(!in_array($row->getId(), $ids)) {
							if (count($posts) < 20) {
								$posts[] = $row;
							}
						}
					}

				}/* 首屏内容结束 */  
			} else if ($categoryId == CATEGORY_DEEP) { // 深读
				/*----深度阅读----*/
				// 精品栏目
				if (!$dataInside = $this->getRedis()->get('fineColumnsNew')) {
					$repoPost = $this->getCoreRepo('Post');
					$repoFineColumns = $this->getCoreRepo('Categoryother');
					$fineColumnsNames = array(
						'译稿',
						'特稿',
						'专访',
						'时评',
						'见证',
						'学术',
					);
					$fineColumns = array();
					foreach ($fineColumnsNames as $key => $name) {
						$tCategory = $repoFineColumns->findOneBy(array('title' => $name));
						if($tCategory) {
							$tCategoryIds[] = $tCategory->getId();
						}
					}

				}
				$dataInside = $repoPost->getSelected($tCategoryIds, $limit, $offset, $this->getDoctrine()->getManager());
				foreach ($dataInside as $row) {
					$posts[] = $row;
				}

			}
			else { // 其他分类

				$repoCategory = $this->getCoreRepo('Category');
				$category = $repoCategory->find($categoryId);


				// 当前主栏目内容
				$posts = $repoPost->findByCategory(array($category), $limit, $offset, false, [], $sortOrder);
			}
			foreach ($posts as $key => $row) {
				$withCover = ($key == 0) && ($offset == 0);
				$post = $this->getPostDetail($request, $row, $withCover);
				$data[] = $post;
			}

			if(count($posts) == $limit || ($categoryId == RECOMMEND_CATEGORY && $offset == 0)) {
				$nextUrl = $this->generateUrl('api_category', array(
					'id' => $categoryId,
					'offset' => $offset + $limit
				));
				$link = sprintf('<%s>; rel="next"', $nextUrl);
			} else {
				$link = false;
			}

			$this->getRedis()->set($redisKey, $data, self::ONE_HOUR * 3);
			$this->getRedis()->set($redisKeyLink, $link, self::ONE_HOUR * 3);
		}

		$response = $this->jsonReponse($data);

		$response->headers->set('Access-Control-Allow-Origin', '*');
		$response->headers->set('link', $link);
		$response->headers->set('Access-Control-Expose-Headers', 'link, Content-Type');
		$response->headers->set('Access-Control-Allow-Headers', 'X-Requested-With');
		$response->setPublic();
		$response->setMaxAge(10000);
		$response->setEtag(md5(serialize($data)));
		return $response;
	}

	/**
	 * 文章分类列表页面
	 * @param type $id
	 * @return type
	 */
	public function listAction(Request $request) {
		define('RECOMMEND_CATEGORY', 'recommend');
		define('CATEGORY_DEEP', 'deep');
		$numberPerPage = 20;
		$limit = $request->get('limit', $numberPerPage);
		$offset = $request->get('offset', 0);
		$sortOrder = $request->get('order', 'asc');


		$redisKey = "api-category-$limit-$offset";
		$redisKeyLink = "api-category-$limit-$offset-link";
		if (!$data = $this->getRedis()->get($redisKey) && !$link = $this->getRedis()->get($redisKeyLink)) {

			$data = array();
			$posts = array();
			$repoPost = $this->getCoreRepo('Post');


			// 当前主栏目内容
			$posts = $repoPost->getList($limit, $offset, false, [], $sortOrder);

			foreach ($posts as $key => $row) {
				$withCover = ($key == 0) && ($offset == 0);
				$post = $this->getPostDetail($request, $row, $withCover);
				$data[] = $post;
			}

			if(count($posts) == $limit) {
				$nextUrl = $this->generateUrl('api_list', array(
					'offset' => $offset + $limit
				));
				$link = sprintf('<%s>; rel="next"', $nextUrl);
			} else {
				$link = false;
			}

			$this->getRedis()->set($redisKey, $data, self::ONE_HOUR * 3);
			$this->getRedis()->set($redisKeyLink, $link, self::ONE_HOUR * 3);
		}

		$response = $this->jsonReponse($data);

		$response->headers->set('Access-Control-Allow-Origin', '*');
		$response->headers->set('link', $link);
		$response->headers->set('Access-Control-Expose-Headers', 'link, Content-Type');
		$response->headers->set('Access-Control-Allow-Headers', 'X-Requested-With');
		$response->setPublic();
		$response->setMaxAge(10000);
		$response->setEtag(md5(serialize($data)));
		return $response;
	}

	/*
	 * 精品栏目
	 */

	public function selectedAction(Request $request) {
		$id = $request->get('id');
		$data = array(
			'id' => $id,
		);
		$repoPost = $this->getCoreRepo('Post');
		$data['categoryType'] = 'CategoryOther';
		$repoCategory = $this->getCoreRepo('CategoryOther');


		// 查找分类树
		$category = $repoCategory->find($id);
		$data['category'] = $category;

		$paginator = $this->get('knp_paginator');
		$pagination = $repoPost->findByCategoryWithPagitaion($paginator, $category, $this->get('request')->query->get('page', 1));
		$posts = $pagination->getItems();
		
		foreach ($posts as $row) {
			$post = $this->getPostDetail($request, $row);
			$data[] = $post;
		}

		return $this->jsonReponse($data);
	}

	/**
	 * 读取单个文章
	 */
	public function postAction($id)
	{
		$request = $this->get('request');
		$repoPost = $this->getDoctrine()->getRepository('GospelCoreBundle:Post');
		$repoCategory = $this->getDoctrine()->getRepository('GospelCoreBundle:Category');

		$data = array();
		$post = $repoPost->find($id);
		// 如果文章不存在或者文章没有发布:退出
		if (null === $post) {
			throw $this->createNotFoundException("您所查找的文章不存在！");
		}

		// 文章没有发布并且没有预览的权限
		if ($post->getStatus() != POST_STATUS_PUBLISHED) {
			throw $this->createNotFoundException("您所查找的文章不存在！");
		}

		$response = $this->jsonReponse($this->getPostDetail($request, $post));
		return $response;
	}

	public function getRelatedAction($id) {
		$request = $this->getRequest();
		$repoPost = $this->getDoctrine()->getRepository('GospelCoreBundle:Post');

		$post = $repoPost->find($id);
		$related = array();
		foreach($post->getRelated() as $v) {
			$relatedId = $v->getId();
			$relatedPost = $repoPost->find($relatedId);
			$related[] = $this->getPostDetail($request, $relatedPost);
		}

		$response = $this->jsonReponse($related);
		$response->setPublic();
		$response->setMaxAge(10000);
		$response->setEtag(md5(serialize($related)));
		return $response;

	}

	public function hitAction(Request $request) {
		$id = $request->get('id');
		$repoPost = $this->getDoctrine()->getRepository('GospelCoreBundle:Post');
		$post = $repoPost->find($id);
		if(!$post) {
			return $this->jsonReponse(['success' => false]);
		}
		$post->setHits($post->getHits() + 1);
		$this->getDoctrine()->getManager()->persist($post);
		$this->getDoctrine()->getManager()->flush();
		$response = $this->jsonReponse(['hits' => $post->getHits()]);
		return $response;
	}

	/**
	 * 获取文章的返回内容
	 * @param boolean $withCover 是否生成大图cover
	 */
	private function getPostDetail($request, $row, $withCover = false) {
		$offset = $request->get('offset', 0);


		$post = array();
		$post['id'] = $row->getId();
		$post['title'] = $row->getTitle();
		$post['titleEn'] = $row->getTitleEn();
		$post['related'] = array();
		foreach($row->getRelated() as $v) {
			$relatedId = $v->getId();
			$post['related'][] = $relatedId;
		}
		$post['intro'] = $row->getIntroduction();
		$post['content'] = $row->getContent();
		$post['contentEn'] = $row->getContentEn();
		$post['created'] = $row->getCreateAt()->format('Y年m月d日 H:i');
		$post['from'] = $row->getComefrom();
		if($row->getCategory()) {
			$post['category'] = $row->getCategory()->getId();
		} else {
			$post['category'] = null;
		}
		if($row->getCategory2()) {
			$post['category2'] = $row->getCategory2()->getId();
		} else {
			$post['category2'] = null;
		}
		if($row->getCategoryOther()) {
			$post['category-other'] = $row->getCategoryOther()->getId();
		} else {
			$post['category-other'] = null;
		}
		$post['keywords'] = $row->getTagsCommaString();
		$post['hits'] = $row->getHits();
		$area = $row->getArea();
		if($area) {
			$post['area_id'] = $area->getId();
		} else {
			$post['area_id'] = null;
		}
		$post['donate'] = $row->getIsDonate();
		if ($row->getAuthor()) {
			$post['author'] = $row->getAuthor()->getTitle();
		} else {
			$post['author'] = $row->getWriter();
		}
		$images = array();
		$imagesCoverBig = null;
		$imagesCoverThumbs = array();
		$imagesDetail = array();
		$imageRoot = $this->get('kernel')->getRootDir() . '/../web/';

		$filterCoverBig = '620x418';
		$filterCoverThumb = '252x168';
		$filterDetail = 'widen_780';
		try {
			foreach ($row->getImages() as $image) {
				// 相对于web文件夹的路径
				$imagePathToWeb = $row->getImagePath() . $image->getName();
				$images[] = $request->getSchemeAndHttpHost() . '/' . $imagePathToWeb;

				// 完整的文件路径
				$imageFullPath = realpath($imageRoot . $imagePathToWeb);
				if(!file_exists($imageFullPath)) {
					continue;
				}

				if(!$imagesCoverBig && $withCover && file_exists($imageFullPath)) {
					$imageFullCoverFile = realpath($imageRoot. $this->get('liip_imagine.cache.manager')->getBrowserPath($imagePathToWeb, $filterCoverBig));
					if(!file_exists($imageFullCoverFile)) {
						$imagemanagerResponse = $this->container
							->get('liip_imagine.controller')
								->filterAction(
									$this->getRequest(),          // http request
									$imagePathToWeb,
									$filterCoverBig              // filter defined in config.yml
						);
					}

					$imagesCoverBig = $request->getSchemeAndHttpHost() . $this->get('liip_imagine.cache.manager')->getBrowserPath($imagePathToWeb, $filterCoverBig);
				}

				if(count($imagesCoverThumbs) < 3 && file_exists($imageFullPath)) {
					$imageFullThumbFile = realpath($imageRoot. $this->get('liip_imagine.cache.manager')->getBrowserPath($imagePathToWeb, $filterCoverThumb));
					if(!file_exists($imageFullThumbFile)) {
						$imagemanagerResponse = $this->container
							->get('liip_imagine.controller')
								->filterAction(
									$this->getRequest(),          // http request
									$imagePathToWeb,
									$filterCoverThumb              // filter defined in config.yml
						);
					}
					$imagesCoverThumbs[] = $request->getSchemeAndHttpHost() . $this->get('liip_imagine.cache.manager')->getBrowserPath($imagePathToWeb, $filterCoverThumb);
				}

				$imageFullDetailFile = realpath($imageRoot. $this->get('liip_imagine.cache.manager')->getBrowserPath($imagePathToWeb, $filterDetail));
				if(!file_exists($imageFullDetailFile)) {
					$imagemanagerResponse = $this->container
						->get('liip_imagine.controller')
							->filterAction(
								$this->getRequest(),          // http request
								$imagePathToWeb,
								$filterDetail              // filter defined in config.yml
					);
				}

				$imagesDetail[] = $request->getSchemeAndHttpHost() . $this->get('liip_imagine.cache.manager')->getBrowserPath($imagePathToWeb, $filterDetail);

			}

		} catch (\Exception $ex) {

		}
		$post['images'] = $images;

		// 是否是第一个文章
		$post['images_top_big'] = $withCover;

		$post['images_cover_big'] = $imagesCoverBig;
		$post['images_thumbs'] = $imagesCoverThumbs;
		$post['images_details'] = $imagesDetail;
		$post['images_count'] = count($images);

		return $post;
	}

	public function jsonReponse($data) {
		
		$response = new JsonResponse($data);
		$response->headers->set('Access-Control-Allow-Origin', '*');
		//$response->setSharedMaxAge($this->cacheTime);
		$response->headers->set('Access-Control-Expose-Headers', 'link, Content-Type');
		$response->headers->set('Access-Control-Allow-Headers', 'X-Requested-With');
		return $response;
	}

	/**
	 * 观点专栏接口
	 * @param type $id
	 * @return type
	 */
	public function opinionsAction(Request $request) {
		define('RECOMMEND_CATEGORY', 'recommend');
		define('CATEGORY_DEEP', 'deep');
		$numberPerPage = 20;
		$limit = $request->get('limit', $numberPerPage);
		$offset = $request->get('offset', 0);
		$filter = $request->get('filter', false);
		$order = $request->get('order', 'asc');


		$redisKey = "api-opinion-$limit-$offset";
		$redisKeyLink = "api-opinion-$limit-$offset-link-$order";
		if (!$data = $this->getRedis()->get($redisKey) && !$link = $this->getRedis()->get($redisKeyLink) || true) {

			$data = array();
			$repoPost = $this->getCoreRepo('Post');
			
			$conn = $this->getDoctrine()->getManager()->getConnection();
			if($filter) {
				$query = "select id from gospel_post where status = 1 and categoryother_id > 0 and category2_id > 0  order by id $order limit $offset, $limit;";
			} else {
				$query = "select id from gospel_post where status = 1 and categoryother_id > 0  order by id $order limit $offset, $limit;";
			}
			$posts = $conn->fetchAll($query);
			foreach($posts as $row) {
				$post = $this->getPostDetail($request, $repoPost->find($row['id']));
				$data[] = $post;
			}



			if(count($posts) == $limit ||  $offset == 0) {
				$nextUrl = $this->generateUrl('api_opinions', array(
					'offset' => $offset + $limit
				));
				$link = sprintf('<%s>; rel="next"', $nextUrl);
			} else {
				$link = false;
			}

			$this->getRedis()->set($redisKey, $data, self::ONE_HOUR * 3);
			$this->getRedis()->set($redisKeyLink, $link, self::ONE_HOUR * 3);

		}
		$response = $this->jsonReponse($data);

		$response->headers->set('Access-Control-Allow-Origin', '*');
		$response->headers->set('link', $link);
		$response->headers->set('Access-Control-Expose-Headers', 'link, Content-Type');
		$response->headers->set('Access-Control-Allow-Headers', 'X-Requested-With');
		return $response;
	}

}
