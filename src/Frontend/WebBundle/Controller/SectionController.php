<?php

/*
 * 各个板块的程序文件
 */

namespace Frontend\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Gospel\Bundle\CoreBundle\Entity\Post;
use Frontend\WebBundle\Classes\StringUnity;

class SectionController extends WebController
{
    public function indexAction($section)
    {
		$section = $this->getRequest()->get('section');


		if(strpos($section, 'M1') === 0) { // 不是category的板块页面
			switch ($section) {
				default:
					break;
			}
		} else { // 如果当前板块是要显示分类里面的文章
			return $this->renderCategory($section);
			//$category = $this->getCategory($section);
			//$sectionNewPath = $this->generateUrl(
			//	'section_new',
			//	array(
			//		'id' => $category->getId(),
			//		'section' => $section
			//	)
			//);
			//return $this->redirect($sectionNewPath);
		}
    }


    public function indexNewAction($id, $section)
    {
    	$section = $this->getRequest()->get('section');

	if(strpos($section, 'M1') === 0) { // 不是category的板块页面
		switch ($section) {
			default:
				break;
		}
	} else { // 如果当前板块是要显示分类里面的文章
		$result = $this->renderCategory($section);
	}

	return $result;
    }

    public function getCategory($sectionName) {
    	$repoCategory = $this->getDoctrine()->getRepository('GospelCoreBundle:Category');
    	$navCategory = $this->getNavCategory($sectionName);
	return $repoCategory->findOneBy(array('title'=>$navCategory['name']));
    }

	/**
	 * 读取专栏页面的相关数据
	 * @return type
	 */
	public function renderColumn()
	{
		$data = array();

		// Repository数据仓库
		$repoPost = $this->getDoctrine()->getRepository('GospelCoreBundle:Post');
		$repoCategory = $this->getDoctrine()->getRepository('GospelCoreBundle:Category');
		$repoAuthor = $this->getDoctrine()->getRepository('GospelCoreBundle:Author');

		// 读取分类信息
		$category = $repoCategory->findOneBy(array('id'=>65));
		$data['category'] = $category;

		// 读取专栏头条1+4
		$categories = array($category);
		$subCategories = $category->getChildren();
		foreach ($subCategories as $subcategory) {
			$categories[] = $subcategory;
		}
		$headlines = $repoPost->findByCategory($categories, 20);
		if (is_array($headlines) && count($headlines)) {
			foreach ($headlines as $headline) {
				if ($headline->getCover()) {
					$data['headline'] = $headline;
					break;
				}
			}
		}
		$data['headlines'] = array();
		$headlines = $repoPost->findByCategory($categories, 5);
		foreach ($headlines as $row) {
			if ($row != $data['headline']) {
				$data['headlines'][] = $row;
			}
		}

		// 读取编者的话 3
		$categoryEditor = $repoCategory->findOneBy(array('title'=>'编者的话'));
		$data['categoryEditor'] = $categoryEditor;
		$data['postEditor'] = $repoPost->findByCategory($categoryEditor, 3, 0, false);


		// TODO: 读取专栏作家 12
		$data['columnists'] = $repoAuthor->findRecommendAuthors(8);

		// 读取专访 5
		$categoryInterview = $repoCategory->findOneBy(array('id'=>72));
		$data['categoryInterview'] = $categoryInterview;
		$data['postInterview'] = $repoPost->findByCategory($categoryInterview, 5, 0, false);

		// TODO: 读取牧者博客 4
		$categoryBlog = $repoCategory->findOneBy(array('title'=>'牧者博客'));
		$data['categoryBlog'] = $categoryBlog;
		$data['postBlog'] = $repoPost->findByCategory($categoryBlog, 4, 0, false);


        return $this->render('FrontendWebBundle:Section:column.html.twig', $data, $this->getCachedResponse());
	}

	/**
	 * 文章分类的首页
	 * @param type $section
	 * @return type
	 */
	public function renderCategory($section)
	{
		if ($section == 'global') {
			return $this->renderCategoryGlobal($section);
		}
		$data = array(); // 传递到模板里面的数据
		// Repository数据仓库
		$repoPost = $this->getDoctrine()->getRepository('GospelCoreBundle:Post');
		$repoCategory = $this->getDoctrine()->getRepository('GospelCoreBundle:Category');
		$repoAreaCategory = $this->getDoctrine()->getRepository('GospelCoreBundle:AreaCategory');

		// 根据当前的路径读取对应的分类
		$navCategory = $this->getNavCategory($section);
		$category = $repoCategory->findOneBy(array('title'=>$navCategory['name']));
		$data['category'] = $category;
		$data['title'] = StringUnity::add_blank($category->getTitle());

		// 读取图片头条
		$recommendData = array(
			'recommend' => array(
				'type' => 'isSectionHeadlinePic',
				'value' => 1,
			)
		);
		$recommendHeadline = array(
			'recommend' => array(
				'type' => 'isSectionHeadlineTxt',
				'value' => 1,
			)
		);

		// 大图
		$data['top'] = $repoPost->findByCategory(array($category), 1, 2, false, $recommendData);
		// 大图下面的两个小图
		$data['topRest'] = $repoPost->findByCategory(array($category), 2, 3, false, $recommendData);
		// 大图右侧的4条新闻列表
		$data['headlines'] = $repoPost->findByCategory(array($category), 4, 2, false, $recommendHeadline);

		// 读取该分类下面的列表
    	$paginator  = $this->get('knp_paginator');
		$data['pagination'] = $repoPost->findByCategoryWithPagitaion($paginator, $category, $this->get('request')->query->get('page', 1));

        $response = $this->render('FrontendWebBundle:Section:index.html.twig', $data);
		return $this->setCachedResponse($response);
	}

	/**
	 * 导航中的环球部分
	 * @param type $section
	 * @return type
	 */
	public function renderCategoryGlobal($section)
	{
		$data = array(); // 传递到模板里面的数据
		// Repository数据仓库
		$repoPost = $this->getDoctrine()->getRepository('GospelCoreBundle:Post');
		$repoCategory = $this->getDoctrine()->getRepository('GospelCoreBundle:Category');
		$repoArea = $this->getDoctrine()->getRepository('GospelCoreBundle:Area');

		// 根据当前的路径读取对应的分类
		$navCategory = $this->getNavCategory($section);
		$category = $repoCategory->findOneBy(array('title'=>$navCategory['name']));
		$categories = array($category);
		$subCategories = $category->getChildren();
		foreach ($subCategories as $subcategory) {
			$categories[] = $subcategory;
		}
		$data['category'] = $category;
		$data['navCategory'] = $navCategory;

		// 读取头条数据
		$headlines = $repoPost->findByCategory($categories, 50, 0, false);
		if (is_array($headlines) && count($headlines)) {
			foreach ($headlines as $headline) {
				if ($headline->getCover()) {
					$data['headline'] = $headline;
					break;
				}
			}
		}
		
		// 读取分类数据
		/*
		 * array blockData
		 * - type: category, region
		 *   - if type is category: there is a key of category
		 *   - if type is region: there is a key of region
		 * - data: post array
		 */
		$categoriesData = array(
			array( 'categoryId' => Post::$CATEGORY_CHURCH),
			array( 'categoryId' => Post::$CATEGORY_THEOLOGY),
			array( 'categoryId' => Post::$CATEGORY_MINISTRY),
			array( 'categoryId' => Post::$CATEGORY_CULTURE),
			array( 'categoryId' => Post::$CATEGORY_FAMILY),
			array( 'categoryId' => Post::$CATEGORY_SOCIETY),
		);
		$blockData = array();
		foreach ($categoriesData as $key => $categoryData) {
			$category = $repoCategory->find($categoryData['categoryId']);
			$blockData[$key]['category'] = $category;
			$blockData[$key]['type'] = 'category';
			$categories = array($category);
			foreach ($category->getChildren() as $subcategory) {
				$categories[] = $subcategory;
			}
			$blockData[$key]['data'] = $repoPost->findByCategory($categories, 6, 0, false, array(
				'province' => $repoArea->findOneBy(array('title'=>'环球'))
			));
		}


		$data['blockData'] = $blockData;

        return $this->render('FrontendWebBundle:Section:index.html.twig', $data, $this->getCachedResponse());
	}

	/**
	 * 根据配置文件返回对应的导航数据
	 * @param string $section
	 * @return array
	 */
	public function getNavCategory($section)
	{
		$categories = $this->container->getParameter('frontend.navigation');
		foreach ($categories as $val) {
			if($val['slug'] == $section) {
				return $val;
			}
		}
	}
}
