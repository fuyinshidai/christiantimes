<?php

namespace Frontend\WebBundle\Controller;

use Frontend\WebBundle\Controller\WebController;
use Symfony\Component\HttpFoundation\Response;
/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class LinkController extends WebController
{
	/**
	 */
	public function indexAction()
	{
		$data = array();

		$repoLink = $this->getCoreRepo('Link');
		$data['links'] = $repoLink->findBy(array(
				'status' => 1,
			), array('sortOrder' => 'ASC')
				);

		// TODO 读取不同的友情链接的分类，然后掉去该分类里面的所有的友情链接的内容
		return $this->render('FrontendWebBundle:Link:indexLink.html.twig', $data);
	}


}
