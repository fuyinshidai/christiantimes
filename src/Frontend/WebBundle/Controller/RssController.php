<?php

namespace Frontend\WebBundle\Controller;

use Frontend\WebBundle\Controller\WebController;
use Symfony\Component\HttpFoundation\Response;
/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class RssController extends WebController
{
	/**
	 */
	public function indexAction()
	{
		$repoPost = $this->getCoreRepo('Post');
		$data = array();
		// TODO 读取rss的主要分类和分类下面的文章列表
		$menuCategories = $this->getCoreRepo('Category')->getMenuCategory();
		$para = array('r' => 1);
		$categoryData = array();
		foreach ($menuCategories as $key => $row) {
			$rowCategoryData = $repoPost->findByCategory($row, 7, 0, false, $para);
			if ($rowCategoryData) {
				$categoryData[$key]['data'] = $rowCategoryData;
				$categoryData[$key]['category'] = $row;
			}
		}
		$data['blockData'] = $categoryData;

		// TODO 参考网易的rss做一个订阅到各个网站的功能

		return $this->render('FrontendWebBundle:Rss:rssIndex.html.twig', $data);
	}

	public function xmlAction($category_id)
	{
		$data = array();
		// 读取和生成当前分类下面的rss xml文件, 读取分类下面的最新的20个新闻
		$para = array('r' => 1);
		$category = $this->getCoreRepo('Category')->find($category_id);
		$data['category'] = $category;
		$data['posts'] = $this->getCoreRepo('Post')->findByCategory($category, 20, 0, false, $para);
		$data['now'] = date('r');
		
		return $this->render('FrontendWebBundle:Rss:rss.xml.twig', $data);
	}

}
