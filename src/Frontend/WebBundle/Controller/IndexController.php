<?php

namespace Frontend\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Gospel\Bundle\CoreBundle\Entity\Post;

class IndexController extends WebController
{

	public function indexAction()
	{
		if (!$data = $this->getRedis()->get('indexData')) {
			$data = array(); // 传递到模板里面的数据
			// Repository数据仓库
			$repoPost = $this->getDoctrine()->getRepository('GospelCoreBundle:Post');
			$repoCategory = $this->getDoctrine()->getRepository('GospelCoreBundle:Category');
			$repoAreaCategory = $this->getDoctrine()->getRepository('GospelCoreBundle:AreaCategory');

			// Entity Manager
	//		$em = $this->getDoctrine()->getManager();
			// *******抓取首页的相关数据 *****************
			// 首页图片头条
			// 查找首页的教会事工分类下面的图片头条
			// 查找首页的文化社会分类下面的图片头条
			$recommendData = array(
				'recommend' => array(
					'type' => 'isHomeHeadlinePic',
					'value' => 1,
				)
			);

			/*** 读取教会相关分类 ***/
			if (!$categoryChurch = $this->getRedis()->get('categoryChurch')) {
				$categoryChurch = $repoCategory->findOneBy(array('title' => '教会'));

				// 设置该分类下面的子分类
				$categoryChurchSubcategories = array($categoryChurch);
				foreach ($categoryChurch->getChildren() as $subCategory) {
					$categoryChurchSubcategories[] = $subCategory;
				}
				$this->getRedis()->set('categoryChurch', $categoryChurch, self::ONE_DAY);
				$this->getRedis()->set('categoryChurchSubCategories', $categoryChurchSubcategories, self::ONE_DAY);
			}

			$categoryChurchSubCategories = $this->getRedis()->get('categoryChurchSubCategories');

			/*** 读取社会相关分类 ***/
			if (!$categorySociety = $this->getRedis()->get('categorySociety')) {
				$categorySociety = $repoCategory->findOneBy(array('title' => '社会'));

				// 设置子分类
				$categorySocietySubCategories = $repoCategory->getAllChild($categorySociety);
				$this->getRedis()->set('categorySociety', $categorySociety, self::ONE_DAY);
				$this->getRedis()->set('categorySocietySubCategories', $categorySocietySubCategories, self::ONE_DAY);
			}
			$categorySocietySubCategories = $this->getRedis()->get('categorySocietySubCategories');

			/*** 读取文化分类 ***/
			if (!$categoryCulture = $this->getRedis()->get('categoryCulture')) {
				$categoryCulture = $repoCategory->findOneBy(array('title' => '文化'));

				$categoryCultureSubCategories = $repoCategory->getAllChild($categoryCulture);
				$this->getRedis()->set('categoryCulture', $categoryCulture, self::ONE_DAY);
				$this->getRedis()->set('categoryCultureSubCategories', $categoryCultureSubCategories, self::ONE_DAY);
			}
			$categoryCultureSubCategories = $this->getRedis()->get('categoryCultureSubCategories');

			/*** 读取国际分类 ***/
			if (!$categoryGlobal = $this->getRedis()->get('categoryGlobal')) {
				$categoryGlobal = $repoCategory->findOneBy(array('title' => '国际'));
				$this->getRedis()->set('categoryGlobal', $categoryGlobal, self::ONE_DAY);

				$categoryGlobalSubCategories = $repoCategory->getAllChild($categoryGlobal);
				$this->getRedis()->set('categoryGlobalSubCategories', $categoryGlobalSubCategories, self::ONE_DAY);
			}
			$categoryGlobalSubCategories = $this->getRedis()->get('categoryGlobalSubCategories');

			/*** 读取公益分类 ***/
			if (!$categoryCharity = $this->getRedis()->get('categoryCharity')) {
				$categoryCharity = $repoCategory->findOneBy(array('title' => '公益'));
				$categoryCharitySubCategories = array($categoryCharity);
				foreach($categoryCharity->getChildren() as $row) {
					$categoryCharitySubCategories[] = $row;
				}
				$this->getRedis()->set('categoryCharity', $categoryCharity, self::ONE_DAY);
				$this->getRedis()->set('categoryCharitySubCategories', $categoryCharitySubCategories, self::ONE_DAY);
			}
			$categoryCharitySubCategories = $this->getRedis()->get('categoryCharitySubCategories');

			/*** 读取家庭分类 ***/
			if (!$categoryFamily = $this->getRedis()->get('categoryFamily')) {
				$categoryFamily = $repoCategory->findOneBy(array('title' => '家庭'));
				$categoryFamilySubCategories = array($categoryFamily);
				foreach($categoryFamily->getChildren() as $row) {
					$categoryFamilySubCategories[] = $row;
				}
				$this->getRedis()->set('categoryFamily', $categoryFamily, self::ONE_DAY);
				$this->getRedis()->set('categoryFamilySubCategories', $categoryFamilySubCategories, self::ONE_DAY);
			}
			$categoryFamilySubCategories = $this->getRedis()->get('categoryFamilySubCategories');

			/*** 读取商业分类 ***/
			if (!$categoryBusiness = $this->getRedis()->get('categoryBusiness')) {
				$categoryBusiness = $repoCategory->findOneBy(array('title' => '商业'));
				$categoryBusinessSubCategories = array($categoryBusiness);
				foreach($categoryBusiness->getChildren() as $row) {
					$categoryBusinessSubCategories[] = $row;
				}
				$this->getRedis()->set('categoryBusiness', $categoryBusiness, self::ONE_DAY);
				$this->getRedis()->set('categoryBusinessSubCategories', $categoryBusinessSubCategories, self::ONE_DAY);
			}
			$categoryBusinessSubCategories = $this->getRedis()->get('categoryBusinessSubCategories');

			/*** 读取生活分类 ***/
			if (!$categoryLife = $this->getRedis()->get('categoryLife')) {
				$categoryLife = $repoCategory->findOneBy(array('title' => '生活'));
				$categoryLifeSubCategories = array($categoryLife);
				foreach($categoryLife->getChildren() as $row) {
					$categoryLifeSubCategories[] = $row;
				}
				$this->getRedis()->set('categoryLife', $categoryLife, self::ONE_DAY);
				$this->getRedis()->set('categoryLifeSubCategories', $categoryLifeSubCategories, self::ONE_DAY);
			}
			$categoryLifeSubCategories = $this->getRedis()->get('categoryLifeSubCategories');

			$data['categoryChurch'] = $categoryChurch;
			$data['categorySociety'] = $categorySociety;
			$data['categoryCulture'] = $categoryCulture;

			// 首页图片推荐
			$categoryRecommend1 = array(
				$categoryChurchSubCategories,
//				$categoryGlobalSubCategories,
			);
			$categoryRecommend2 = array(
				$categoryGlobalSubCategories,
//				$categorySocietySubCategories,
//				$categoryCultureSubCategories,
			);
			$categoryRecommend3 = array(
				$categoryCultureSubCategories,
//				$categoryLifeSubCategories,
//				$categoryFamilySubCategories,
//				$categoryBusinessSubCategories,
			);

			if (!$headline1 = $this->getRedis()->get('headline1')) {
				$headline1Tmp = $repoPost->findByCategory($categoryRecommend1, 3, 0, false, $recommendData);
				$headline1 = array();
				foreach ($headline1Tmp as $row) {
					$headline1[] = $repoPost->normalize($row);
				}
				$this->getRedis()->set('headline1', $headline1, self::ONE_HOUR);
			}

			if (isset($headline1[0])) {
				$data['headline1Top'] = array_shift($headline1);
			}
			$data['headline1Rest'] = $headline1;

			if (!$headline2 = $this->getRedis()->get('headline2')) {
				$headline2Tmp = $repoPost->findByCategory($categoryRecommend2, 3, 0, false, $recommendData);
				$headline2 = array();
				foreach ($headline2Tmp as $row) {
					$headline2[] = $repoPost->normalize($row);
				}
				$this->getRedis()->set('headline2', $headline2, self::ONE_HOUR);
			}
			if (isset($headline2[0])) {
				$data['headline2Top'] = array_shift($headline2);
			}
			$data['headline2Rest'] = $headline2;

			$categoryRecommendSociety = array(
				$categorySocietySubCategories,
			);
			if (!$headlineSociety = $this->getRedis()->get('headlineSociety')) {
				$headlineSociety = $repoPost->normalizeArray($repoPost->findByCategory($categoryRecommendSociety, 4, 0, false, $recommendData));
				$this->getRedis()->set('headlineSociety', $headlineSociety, self::ONE_HOUR);
			}
			if (isset($headlineSociety[0])) {
				$data['headlineSocietyTop'] = array_shift($headlineSociety);
			}
			$data['headlineSocietyRest'] = $headlineSociety;

			// 文化
			if (!$headline3 = $this->getRedis()->get('headline3')) {
				$headline3Tmp = $repoPost->findByCategory($categoryRecommend3, 4, 0, false, $recommendData);
				$headline3 = array();
				foreach ($headline3Tmp as $row) {
					$headline3[] = $repoPost->normalize($row);
				}
				$this->getRedis()->set('headline3', $headline3, self::ONE_HOUR);
			}
			if (isset($headline3[0])) {
				$data['headline3Top'] = array_shift($headline3);
			}
			$data['headline3Rest'] = $headline3;
			

			// 首页文字头条
			// 大图右侧的第一条推荐新闻
			if (!$subHeadlineTop = $this->getRedis()->get('subHeadlineTop')) {
				$subHeadlineTop = $repoPost->normalize($repoPost->findOne(REC_HEADLINE_TXT), true);
				$this->getRedis()->set('subHeadlineTop', $subHeadlineTop, self::ONE_HOUR);
			}
			$data['subHeadlineTop'] = $subHeadlineTop;

			// 大图右侧的3条推荐新闻
			if (!$subHeadline2 = $this->getRedis()->get('subHeadline2')) {
				$subHeadline2 = array(
					$repoPost->normalize($repoPost->findOne(REC_HEADLINE_TXT1), true),
					$repoPost->normalize($repoPost->findOne(REC_HEADLINE_TXT2), true),
					$repoPost->normalize($repoPost->findOne(REC_HEADLINE_TXT3), true),
				);
				$this->getRedis()->set('subHeadline2', $subHeadline2, self::ONE_HOUR);
			}
			$data['subHeadline2'] = $subHeadline2;

			// 过期的头条文章
			if (!$subheadline3 = $this->getRedis()->get('subheadline3')) {
				$subheadline3Tmp = array_merge(
					$repoPost->findMany(REC_HEADLINE_TXT, 5, 1),
					$repoPost->findMany(REC_HEADLINE_TXT1, 4, 1),
					$repoPost->findMany(REC_HEADLINE_TXT2, 3, 1),
					$repoPost->findMany(REC_HEADLINE_TXT3, 3, 1)
				);
				$subheadline3 = array();
				foreach ($subheadline3Tmp as $row) {
					$subheadline3[] = $repoPost->normalize($row);
				}
				$this->getRedis()->set('subheadline3', $subheadline3, self::ONE_HOUR);
			}
			$data['subHeadline3'] = $subheadline3;

			// 读取多个分类的文章
			$data['categoryGlobal'] = $categoryGlobal;
			$data['categoryCharity'] = $categoryCharity;
			$data['categoryBusiness'] = $categoryBusiness;
			$data['categoryLife'] = $categoryLife;
			$data['categoryFamily'] = $categoryFamily;

			// 读取图片集内容
			$repoGallery = $this->getCoreRepo('Gallery');
			if (!$galleries = $this->getRedis()->get('galleries')) {
				$galleriesTmp = $repoGallery->findLatest(4);
				$galleries = array();
				foreach ($galleriesTmp as $row) {
					$galleries[] = $repoGallery->normalize($row);
				}
				$this->getRedis()->set('galleries', $galleries, self::ONE_HOUR);
			}
			$data['gallery'] = array_shift($galleries);
			$data['galleries'] = $galleries;

			// 读取地区下面的文章内容
			// 读取北京，上海，深圳
			$repoProvince = $this->getCoreRepo('Area');
			$repoRegion = $this->getCoreRepo('AreaCategory');
			$citiesName = array(
				'北京',
				'上海',
				'广东',
			);

			if (!$cities = $this->getRedis()->get('cities')) {
				$cities = array();
				foreach ($citiesName as $key => $name) {
					$cities[$key]['city'] = $repoProvince->findOneBy(array('title' => $name));
					$cities[$key]['postTop'] = $repoPost->normalize($repoPost->findOneByProvince(
							$cities[$key]['city'],
							true, 
							$this->getDoctrine()->getManager(), 1));
					$cities[$key]['posts'] = $repoPost->findByProvince($cities[$key]['city'], 2, 5);
				}
				$this->getRedis()->set('cities', $cities, self::ONE_HOUR);
			}
			$data['cities'] = $cities;


			// 综合资讯
			$comprehensiveInfoCategories = array(
				$categoryLife,
				$categoryFamily,
				$categoryBusiness,
			);
			$comprehensiveInfoSubCategories = array(
				$categoryLifeSubCategories,
				$categoryFamilySubCategories,
				$categoryBusinessSubCategories,
			);

			if (!$comprehensiveInfo = $this->getRedis()->get('comprehensiveInfo')) {
				$comprehensiveInfo = array();
				foreach ($comprehensiveInfoCategories as $key => $category) {
					$comprehensiveInfo[$key]['category'] = $category;

					$headlinePosts = $repoPost->normalizeArray($repoPost->findByCategoryWithImage($comprehensiveInfoSubCategories[$key], $this->getDoctrine()->getManager(), 4));
					if (isset($headlinePosts[0])) {
						$comprehensiveInfo[$key]['postTop'] = array_shift($headlinePosts);
					}
					$comprehensiveInfo[$key]['posts'] = $headlinePosts;
				}
				$this->getRedis()->set('comprehensiveInfo', $comprehensiveInfo, self::ONE_HOUR);
			}
			$data['comprehensiveInfo'] = $comprehensiveInfo;


			$regionsName = array(
				'华东',
				'东北',
				'中南',
				'华北',
				'西南',
				'西北',
			);

			if (!$regions = $this->getRedis()->get('regions')) {
				$regions = array();
				foreach ($regionsName as $key => $name) {
					$regions[$key]['region'] = $repoRegion->findOneBy(array('title' => $name));
					$posts = $repoPost->findByRegion($regions[$key]['region'], 3, 5);
					foreach ($posts as $row) {
						$regions[$key]['posts'][] = $repoPost->normalize($row);
					}
				}
				$this->getRedis()->set('regions', $regions, self::ONE_HOUR);
			}
			$data['regions'] = $regions;

			// 热门新闻
			if (!$hots = $this->getRedis()->get('hots')) {
				$hots = $repoPost->normalizeArray($repoPost->getHot(10));
				$this->getRedis()->set('hots', $hots, self::ONE_DAY);
			}
			$data['hots'] = $hots;

			$this->getRedis()->set('indexData', $data, self::ONE_HOUR);
		}

		$response = $this->render('FrontendWebBundle:Index:index.html.twig', $data);
		return $this->setCachedResponse($response);
	}

	public function getRepoitory($repository)
	{

		return $this->getDoctrine()->getRepository($repository);
	}

	public function redirectAction(Request $request)
	{
		$uri = $request->getRequestUri();

		$fileName = $request->get('name', null);

		switch ($fileName) {
			// 首页
			case 'index': 
				return $this->redirect($this->generateUrl('home'));

			// 各种频道
			case 'list': 
				$map = array(
					0 => '',
					1 => 'section/international',
					2 => 'section/church',
					3 => 'section/society',
					4 => 'section/family',
					5 => 'section/culture',
					7 => 'fine-column/7/观点.专栏',
					8 => 'section/business',
					9 => 'section/life',

				);
				$id = $request->get('catid', 0);
				return $this->redirect($map[$id]);

			// 家庭频道
			case 'colum_family':
				return $this->redirect('section/family');

			// 婚恋专栏
			case 'list-column':
				return $this->redirect('category/105/恋爱');

			// 专题
			case 'special-list' :
				return $this->redirect('topics');

			case  'special':
				return $this->redirect($this->generateUrl('topic', array(
					'id' => $request->get('sid'),
					'slug' => 'show',
				)));

			// region .php?area_id=8
			case 'area-list':
				return $this->redirect($this->generateUrl('region', array(
					'id' => $request->get('area_id'),
					'slug' => 'show',
				)));

			// 文章页面article-detail.php?aid=13660
			case 'article-detail':
				return $this->redirect($this->generateUrl('news_show', array(
					'id' => $request->get('aid'),
					'slug' => 'show',
				)));

			case 'video':
				return $this->redirect($this->generateUrl('videos'));
			
			case 'video-view':
				return $this->redirect($this->generateUrl('video', array(
					'id' => $request->get('vid'),
					'slug' => 'play',
				)));
		}




		return $this->generateUrl('home');
	}

}
