<?php

/*
 * 图片集控制器
 */

namespace Frontend\WebBundle\Controller;

use Frontend\WebBundle\Controller\WebController;
use Symfony\Component\HttpFoundation\Request;
use Gospel\Bundle\CoreBundle\Entity\Gallery;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class GalleryController extends WebController
{

	public function indexAction(Request $request)
	{
		
		$repoGallery = $this->getCoreRepo('Gallery');
		$repoGalleryCategory = $this->getCoreRepo('CategoryGallery');

		$data = array();
		$category = null;

		$categoryId = $request->get('category_id', null);
		if ($categoryId) {
			$category = $repoGalleryCategory->find($categoryId);
		}
		$data['category'] = $category;

		// 按照分类显示图片集
		// 1. 读取图片集分类
		$categories = $repoGalleryCategory->findAll();
		$data['categories'] = $categories;

		$paginator  = $this->get('knp_paginator');
		$data['pagination'] = $repoGallery->findByCategoryWithPagitaion($paginator, $category, $this->get('request')->query->get('page', 1));
		$items = $data['pagination']->getItems();
		// 读取头条 3个
		$data['entityTops'] = array_slice($items, 0, 3);
		$data['entityRest'] = array_slice($items, 3, 12);

		return $this->render('FrontendWebBundle:Gallery:index.html.twig', $data);
	}

	public function showAction(Request $request)
	{
		$repoGallery = $this->getCoreRepo('Gallery');
		$repoGalleryCategory = $this->getCoreRepo('CategoryGallery');

		$data = array();
		$category = null;

		$galleryId = $request->get('id', null);
		if ($galleryId) {
			$gallery = $repoGallery->find($galleryId);
			if (!$gallery instanceof Gallery) {
				throw $this->createNotFoundException("您查找的页面不存在");
			}
		} 
		$data['category'] = $gallery->getCategoryGallery();

		// reading the cache image width
		$images = array();
		$imagine = $this->get('liip_imagine.cache.manager');
		$baseUrl = $this->getRequest()->getBaseUrl();
		$webRoot = $this->getRequest()->server->get('DOCUMENT_ROOT');
		foreach ($gallery->getImages() as $image) {
			$imageAbsolutePath = $webRoot . substr($imagine->getBrowserPath($gallery->getImagePath() . $image->getName(), '800'), strlen($baseUrl));
			if(!file_exists($imageAbsolutePath)) {
				$imagemanagerResponse = $this->container
					->get('liip_imagine.controller')
						->filterAction(
							$this->getRequest(),          // http request
							$gallery->getImagePath() . $image->getName(),      // original image you want to apply a filter to
							'800'              // filter defined in config.yml
				);
				$width = 800;
				$height = 500;
				if (file_exists($imageAbsolutePath)) {
					list($width, $height, $type, $attr) = getimagesize($imageAbsolutePath);
				}
				$image->setWidth($width);
				$image->setHeight($height);
				$image->setTop((500 - $height)/2);
				$images[] = $image;
			}
			else
            {
                $images[] = $image;
            }
		}
		$data['images'] = $images;

		$data['gallery']  = $gallery;

		// 按照分类显示图片集
		// 1. 读取图片集分类
		$categories = $repoGalleryCategory->findAll();
		$data['categories'] = $categories;

		// 推荐
		$data['recommend'] = $repoGallery->findRecommend(12);

		// 更新文章点击量
		$gallery->setHits($gallery->getHits() + 1);
		$this->getDoctrine()->getManager()->persist($gallery);
		$this->getDoctrine()->getManager()->flush();

		return $this->render('FrontendWebBundle:Gallery:show.html.twig', $data);
	}

}
