<?php

namespace Frontend\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Gospel\Bundle\NewsletterBundle\Form\SubscriberType;
use Gospel\Bundle\NewsletterBundle\Entity\Subscriber;
use Doctrine\DBAL\DBALException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class NewsletterController extends WebController
{
	/**
	 * 用户订阅newsletter
	 * @param Request $request
	 */
	public function subscribeAction(Request $request)
	{
		// 检查邮件是否重复
		$requestStr = $request->get('data');
		parse_str($requestStr, $requestArray);
		$email = $requestArray['email'];
		if (! $this->validateAction($email)) {
			return new Response(json_encode(array(
				'error' => 1,
				'msg' => '该邮件已被使用'
			)));
		} else { // save into the database
			$subscriber = new Subscriber;
			$subscriber->setEmail($email);
			$subscriber->setStatus(1);
			foreach ($requestArray['categories'] as $catId) {
				$category = $this->getDoctrine()->getRepository('GospelNewsletterBundle:Category')->find($catId);
				if ($category) {
					$subscriber->addCategory($category);
				}
			}
			$subscriber->setSubscribedTime(new \DateTime());
			$this->getDoctrine()->getManager()->persist($subscriber);
			$this->getDoctrine()->getManager()->flush();

			// synchronize with mailchimp list
			$emails = array();
			$emails[] = array('email' => array('email' => $subscriber->getEmail()));
			$mc = $this->get('hype_mailchimp');

			// 读取分类
			$categories = $subscriber->getCategories();
			$cates = array();
			$mailchimpCategories = array(
				1 => '资讯',
				2 => '话题',
				3 => '杂志',
			);
			foreach($categories as $category) {
				$cates[] = $mailchimpCategories[$category->getId()];
			}
			$merge_vars = array(
				'GROUPINGS' => array(
					array(
						'name' => '订阅分类',
						'groups' => $cates,
					),
				)
			);
			$mcData = $mc->getList()->setMerge_vars($merge_vars)->subscribe($subscriber->getEmail());

			// send email
			$msg = $this->sendMagazineNewsletters($subscriber);

			return new Response(json_encode(array(
				'error' => 0,
				'msg' => $msg,
			)));
		}
	}

	/**
	 * Send email to the new subscirbers of new last magazine newsletters
	 * @param Subscriber $subscriber
	 */
	public function sendMagazineNewsletters(Subscriber $subscriber)
	{
		$categories = $subscriber->getCategories();
		foreach ($categories as $category) {
			//echo $category->getTitle()."\n";
			if (strpos($category->getTitle(), '杂志') !== false ) {

				$newsletters = $this->getDoctrine()->getRepository('GospelNewsletterBundle:Newsletter')->findBy(array(
					'category' => $category,
				));

				foreach ($newsletters as $newsletter) {
					$body = $newsletter->getBody();
					$message = \Swift_Message::newInstance()
						->setSubject($newsletter->getTitle())
						->setFrom(array('jidushibao@gmail.com' => '基督时报'))
						->setBcc($subscriber->getEmail())
						->setBody($body, 'text/html')
						;
					return $result = $this->get('mailer')->send($message);
				}
			}
		}
		return '无邮件发送';
	}

	/**
	 * check if this email already in register
	 */
	public function validateAction($email)
	{
		$subscribers = $this->getDoctrine()->getRepository('GospelNewsletterBundle:Subscriber')->findOneBy(array(
			'email' => $email
		));
		if ($subscribers) {
			$isUnique = false;
		} else {
			$isUnique = true;
		}
		return $isUnique;
	}

	public function viewAction($id)
	{
		$newsletter = $this->getDoctrine()->getRepository('GospelNewsletterBundle:Newsletter')->find($id);

		return new Response($newsletter->getBody());
	}
}
