<?php

namespace Frontend\WebBundle\Controller;

use Frontend\WebBundle\Controller\WebController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Gospel\Bundle\CoreBundle\Services\OSS;
/**
 * application/octet-stream
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class VideoController extends WebController
{
	public $typeLocal = 1;
	/**
	 * 视频主要页面
	 */
	public function indexAction(Request $request)
	{
		$repoVideo = $this->getCoreRepo('Video');
		$repoVideoCategory = $this->getCoreRepo('CategoryVideo');

		$data = array();
		$category = null;

		$categoryId = $request->get('id', null);
		if ($categoryId) {
			$category = $repoVideoCategory->find($categoryId);
		}
		$data['category'] = $category;

		// 读取相关的视频数据
		
		$data['videoTops'] = $repoVideo->getPromoted(3);

		// 按照分类显示视频
		// 1. 读取视频分类
		$categories = $repoVideoCategory->findAll();
		$data['categories'] = $categories;

		$paginator  = $this->get('knp_paginator');
		$data['pagination'] = $repoVideo->findByCategoryWithPagitaion($paginator, $category, $this->get('request')->query->get('page', 1));
		$items = $data['pagination']->getItems();
		// 读取头条视频 3个
		$data['videoTops'] = array_slice($items, 0, 3);
		$data['videoRest'] = array_slice($items, 3, 12);

		return $this->render('FrontendWebBundle:Video:videoIndex.html.twig', $data);
	}

	/**
	 * 最多浏览的视频模块
	 * @return type
	 */
	public function mostViewedAction($category = null)
	{
		$data = array();
		$data['videos'] = $this->getCoreRepo('Video')->findMostViewed($category);
		return $this->render('FrontendWebBundle:Video:mostViewed.html.twig', $data, $this->getCachedResponse());
	}

	/**
	 * 推荐视频
	 * @return type
	 */
	public function recommendAction($category = null)
	{
		$data = array();
		$data['videos'] = $this->getCoreRepo('Video')->findRecommend(12);
		return $this->render('FrontendWebBundle:Video:mostViewed.html.twig', $data, $this->getCachedResponse());
	}

	/**
	 */
	public function showAction($id, $slug)
	{
		$data = array();
		$repoVideo = $this->getCoreRepo('Video');
		$repoVideoCategory = $this->getCoreRepo('CategoryVideo');

		// 1. 读取视频
		$video = $repoVideo->find($id);
		if (!$video instanceof \Gospel\Bundle\CoreBundle\Entity\Video) {
			throw $this->createNotFoundException('查看视频不存在！');
		}
		$video->setEmbedCode(html_entity_decode($video->getEmbedCode()));
		$data['video'] = $video;
		if ($video->getType() == $this->typeLocal) {
			list($videoName, $ext) = explode('.', $video->getVideoPath());

			// reading the video url
			$webUrl = $video->getVideoWebPath().'/'.$videoName;
			$video->setUrl($webUrl);
			$data['webUrl'] = $webUrl;

			/*
			$oss = new OSS();
			$url = $oss->getUrl($video->getVideoPath());
			$data['videoName'] = $videoName;
			$data['videoOgg'] = $oss->getUrl($videoName . '.ogg');
			$data['videoWebm'] = $oss->getUrl($videoName . '.webm');
			$data['videoM4v'] = $oss->getUrl($videoName . '.mp4');
			 */
		}

		$data['category'] = $video->getCategory();

		$categories = $repoVideoCategory->findAll();
		$data['categories'] = $categories;

		// 2. 更新视频的点击量
		$video->setHits($video->getHits()+1);
		$em = $this->getDoctrine()->getManager();
		$em->persist($video);
		$em->flush();


		return $this->render('FrontendWebBundle:Video:showVideo.html.twig', $data, $this->getCachedResponse());
	}

	/**
	 */
	public function showTestAction($id, $slug)
	{
		$data = array();
		$repoVideo = $this->getCoreRepo('Video');
		$repoVideoCategory = $this->getCoreRepo('CategoryVideo');

		// 1. 读取视频
		$video = $repoVideo->find($id);
		if (!$video instanceof \Gospel\Bundle\CoreBundle\Entity\Video) {
//			throw $this->createNotFoundException('查看视频不存在！');
		}
		$video->setEmbedCode(html_entity_decode($video->getEmbedCode()));
		$data['video'] = $video;
		if ($video->getType() == $this->typeLocal) {
			list($videoName, $ext) = explode('.', $video->getVideoPath());

			// reading the video url
			$webUrl = $video->getVideoWebPath().'/'.$videoName;
			$video->setUrl($webUrl);
			$data['webUrl'] = $webUrl;

			/*
			$oss = new OSS();
			$url = $oss->getUrl($video->getVideoPath());
			$data['videoName'] = $videoName;
			$data['videoOgg'] = $oss->getUrl($videoName . '.ogg');
			$data['videoWebm'] = $oss->getUrl($videoName . '.webm');
			$data['videoM4v'] = $oss->getUrl($videoName . '.mp4');
			 */
		}

		$data['category'] = $video->getCategory();

		$categories = $repoVideoCategory->findAll();
		$data['categories'] = $categories;

		// 2. 更新视频的点击量
		$video->setHits($video->getHits()+1);
		$em = $this->getDoctrine()->getManager();
		$em->persist($video);
		$em->flush();


		return $this->render('FrontendWebBundle:Video:showVideoTest.html.twig', $data, $this->getCachedResponse());
	}

	/**
	 * 推荐视频模块当单击视频的封面图片的时候动态更新视频
	 * @param type $id
	 * @return type
	 */
	public function ajaxAction($id)
	{
		$video = $this->getRepository('GospelCoreBundle:Video')->find($id);
		return $this->render('FrontendWebBundle:Video:ajax.html.twig', array('video' => $video), $this->getCachedResponse());
	}


	public function editorChoiceAction()
	{
		$repoVideo = $this->getCoreRepo('Video');
		$data = array();
		$data['editorChoice'] = $repoVideo->getEditorsChoice(1);
		$data['editorChoices'] = $repoVideo->getEditorsChoice(2, 1);
		return $this->render('FrontendWebBundle:Video:videoEditorChoice.html.twig', $data);
	}

}
