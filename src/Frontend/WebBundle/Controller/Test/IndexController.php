<?php

namespace Frontend\WebBundle\Controller\Test;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Frontend\WebBundle\Controller\WebController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Gospel\Bundle\CoreBundle\Entity\Post;
use Redis;

class IndexController extends WebController
{


	public function indexAction()
	{
		$data = array(); // 传递到模板里面的数据
		// Repository数据仓库
		$repoPost = $this->getDoctrine()->getRepository('GospelCoreBundle:Post');
		$repoCategory = $this->getDoctrine()->getRepository('GospelCoreBundle:Category');
		$repoAreaCategory = $this->getDoctrine()->getRepository('GospelCoreBundle:AreaCategory');

		// Entity Manager
//		$em = $this->getDoctrine()->getManager();
		// *******抓取首页的相关数据 *****************
		// 首页图片头条
		// 查找首页的教会事工分类下面的图片头条
		// 查找首页的文化社会分类下面的图片头条
		$recommendData = array(
			'recommend' => array(
				'type' => 'isHomeHeadlinePic',
				'value' => 1,
			)
		);
		$categoryChurch = $repoCategory->findOneBy(array('title' => '教会'));
		$categorySociety = $repoCategory->findOneBy(array('title' => '社会'));
		$categoryCulture = $repoCategory->findOneBy(array('title' => '文化'));
		$categoryGlobal = $repoCategory->findOneBy(array('title' => '国际'));
		$categoryCharity = $repoCategory->findOneBy(array('title' => '公益'));
		$categoryFamily = $repoCategory->findOneBy(array('title' => '家庭'));
		$categoryBusiness = $repoCategory->findOneBy(array('title' => '商业'));
		$categoryLife = $repoCategory->findOneBy(array('title' => '生活'));
		$data['categoryChurch'] = $categoryChurch;
		$data['categorySociety'] = $categorySociety;
		$data['categoryCulture'] = $categoryCulture;

		// 首页图片推荐
		$categoryRecommend1 = array(
			$categoryChurch,
			$categoryGlobal,
		);
		$categoryRecommend2 = array(
			$categorySociety,
			$categoryCulture,
		);
		$categoryRecommend3 = array(
			$categoryLife,
			$categoryFamily,
			$categoryBusiness,
		);
		$headline1 = $repoPost->findByCategory($categoryRecommend1, 3, 0, false, $recommendData);
		if (isset($headline1[0])) {
			$data['headline1Top'] = array_shift($headline1);
		}
		$data['headline1Rest'] = $headline1;

		$headline2 = $repoPost->findByCategory($categoryRecommend2, 3, 0, false, $recommendData);
		if (isset($headline2[0])) {
			$data['headline2Top'] = array_shift($headline2);
		}
		$data['headline2Rest'] = $headline2;

		// 生活，信仰，商业大图内容
		$headline3 = $repoPost->findByCategory($categoryRecommend3, 4, 0, false, $recommendData);
		if (isset($headline3[0])) {
			$data['headline3Top'] = array_shift($headline3);
		}
		$data['headline3Rest'] = $headline3;
		

		// 首页文字头条
		// 大图右侧的第一条推荐新闻
		$data['subHeadlineTop'] = $repoPost->findOne(REC_HEADLINE_TXT);

		// 大图右侧的3条推荐新闻
		$data['subHeadline2'] = array(
			$repoPost->findOne(REC_HEADLINE_TXT1),
			$repoPost->findOne(REC_HEADLINE_TXT2),
			$repoPost->findOne(REC_HEADLINE_TXT3),
		);

		// 过期的头条文章
		$data['subHeadline3'] = array_merge(
			$repoPost->findMany(REC_HEADLINE_TXT, 5, 1),
			$repoPost->findMany(REC_HEADLINE_TXT1, 4, 1),
			$repoPost->findMany(REC_HEADLINE_TXT2, 3, 1),
			$repoPost->findMany(REC_HEADLINE_TXT3, 3, 1)
		);

		// 读取多个分类的文章
		$data['categoryGlobal'] = $categoryGlobal;
		$data['categoryCharity'] = $categoryCharity;
		$data['categoryBusiness'] = $categoryBusiness;
		$data['categoryLife'] = $categoryLife;
		$data['categoryFamily'] = $categoryFamily;
		$categoryList1 = array(
			$categoryGlobal,
		);
		$categoryList2 = array(
			$categoryChurch,
		);
		$categoryList3 = array(
			$categorySociety,
			$categoryCharity
		);
		$categoryList4 = array(
			$categoryCulture,
		);
		$categoryList5 = array(
			$categoryFamily
		);
		$categoryList6 = array(
			$categoryBusiness,
			$categoryLife,
		);
		$data['categoryList1'] = $repoPost->findByCategory($categoryList1, 3, 4, false);
		$data['categoryList2'] = $repoPost->findByCategory($categoryList2, 3, 4, false);
		$data['categoryList3'] = $repoPost->findByCategory($categoryList3, 3, 4, false);
		$data['categoryList4'] = $repoPost->findByCategory($categoryList4, 3, 4, false);
		$data['categoryList5'] = $repoPost->findByCategory($categoryList5, 3, 4, false);
		$data['categoryList6'] = $repoPost->findByCategory($categoryList6, 3, 4, false);

		// 读取图片集内容
		$repoGallery = $this->getCoreRepo('Gallery');
		$galleries = $repoGallery->findLatest(4);
		$data['gallery'] = array_shift($galleries);
		$data['galleries'] = $galleries;

		// 读取地区下面的文章内容
		// 读取北京，上海，深圳
		$repoProvince = $this->getCoreRepo('Area');
		$repoRegion = $this->getCoreRepo('AreaCategory');
		$citiesName = array(
			'北京',
			'上海',
			'广东',
		);
		$cities = array();
		foreach ($citiesName as $key => $name) {
			$cities[$key]['city'] = $repoProvince->findOneBy(array('title' => $name));
			$cities[$key]['postTop'] = $repoPost->findOneByProvince(
					$cities[$key]['city'],
					true, 
					$this->getDoctrine()->getManager());
			$cities[$key]['posts'] = $repoPost->findByProvince($cities[$key]['city'], 2, 2);
		}
		$data['cities'] = $cities;


		$regionsName = array(
			'华东',
			'东北',
			'中南',
			'华北',
			'西南',
			'西北',
		);
		$regions = array();
		foreach ($regionsName as $key => $name) {
			$regions[$key]['region'] = $repoRegion->findOneBy(array('title' => $name));
			$regions[$key]['posts'] = $repoPost->findByRegion($regions[$key]['region'], 3);
		}
		$data['regions'] = $regions;

		// 热门新闻
		$data['hots'] = $repoPost->getHot(10);

		$response = $this->render('FrontendWebBundle:Index:index.html.twig', $data);
		return $response;
	}


	public function indexRedisAction()
	{
		//if (!$data = $this->getRedis()->get('indexData')) {
			$data = array(); // 传递到模板里面的数据
			// Repository数据仓库
			$repoPost = $this->getDoctrine()->getRepository('GospelCoreBundle:Post');
			$repoCategory = $this->getDoctrine()->getRepository('GospelCoreBundle:Category');
			$repoAreaCategory = $this->getDoctrine()->getRepository('GospelCoreBundle:AreaCategory');

		/*----每日灵修----*/
		$categorySpirit = $repoCategory->findOneBy(array('title' => '每日灵修'));
		$data['categorySpirit'] = $categorySpirit;
		$headerSpirit = $repoPost->findByCategory($categorySpirit, 1, 0, false);
		$data['headerSpirit'] = $headerSpirit[0];
		$data['headerSpiritRest'] = $repoPost->findEverydaySpirit($categorySpirit, 2, 1, false);
foreach($data['headerSpiritRest'] as $row) {
echo $row->getTitle();
}
echo count($data['headerSpiritRest']);

			// Entity Manager
	//		$em = $this->getDoctrine()->getManager();
			// *******抓取首页的相关数据 *****************
			// 首页图片头条
			// 查找首页的教会事工分类下面的图片头条
			// 查找首页的文化社会分类下面的图片头条
			$recommendData = array(
				'recommend' => array(
					'type' => 'isHomeHeadlinePic',
					'value' => 1,
				)
			);

			/*** 读取教会相关分类 ***/
			if (!$categoryChurch = $this->getRedis()->get('categoryChurch')) {
				$categoryChurch = $repoCategory->findOneBy(array('title' => '教会'));

				// 设置该分类下面的子分类
				$categoryChurchSubcategories = array($categoryChurch);
				foreach ($categoryChurch->getChildren() as $subCategory) {
					$categoryChurchSubcategories[] = $subCategory;
				}
				$this->getRedis()->set('categoryChurch', $categoryChurch, self::ONE_DAY);
				$this->getRedis()->set('categoryChurchSubCategories', $categoryChurchSubcategories, self::ONE_DAY);
			}

			$categoryChurchSubCategories = $this->getRedis()->get('categoryChurchSubCategories');

			/*** 读取社会相关分类 ***/
			if (!$categorySociety = $this->getRedis()->get('categorySociety')) {
				$categorySociety = $repoCategory->findOneBy(array('title' => '社会'));

				// 设置子分类
				$categorySocietySubCategories = array($categorySociety);
				foreach ($categorySociety->getChildren() as $subCategory) {
					$categorySocietySubCategories[] = $subCategory;
				}
				$this->getRedis()->set('categorySociety', $categorySociety, self::ONE_DAY);
				$this->getRedis()->set('categorySocietySubCategories', $categorySocietySubCategories, self::ONE_DAY);
			}
			$categorySocietySubCategories = $this->getRedis()->get('categorySocietySubCategories');

			/*** 读取文化分类 ***/
			if (!$categoryCulture = $this->getRedis()->get('categoryCulture')) {
				$categoryCulture = $repoCategory->findOneBy(array('title' => '文化'));

				$categoryCultureSubCategories = array($categoryCulture);
				foreach ($categoryCulture->getChildren() as $row) {
					$categoryCultureSubCategories[] = $row;
				}
				$this->getRedis()->set('categoryCulture', $categoryCulture, self::ONE_DAY);
				$this->getRedis()->set('categoryCultureSubCategories', $categoryCultureSubCategories, self::ONE_DAY);
			}
			$categoryCultureSubCategories = $this->getRedis()->get('categoryCultureSubCategories');

			/*** 读取国际分类 ***/
			if (!$categoryGlobal = $this->getRedis()->get('categoryGlobal')) {
				$categoryGlobal = $repoCategory->findOneBy(array('title' => '国际'));
				$this->getRedis()->set('categoryGlobal', $categoryGlobal, self::ONE_DAY);

				$categoryGlobalSubCategories = array($categoryGlobal);
				foreach ($categoryGlobal->getChildren() as $row) {
					$categoryGlobalSubCategories[] = $row;
				}
				$this->getRedis()->set('categoryGlobalSubCategories', $categoryGlobalSubCategories, self::ONE_DAY);
			}
			$categoryGlobalSubCategories = $this->getRedis()->get('categoryGlobalSubCategories');

			/*** 读取公益分类 ***/
			if (!$categoryCharity = $this->getRedis()->get('categoryCharity')) {
				$categoryCharity = $repoCategory->findOneBy(array('title' => '公益'));
				$categoryCharitySubCategories = array($categoryCharity);
				foreach($categoryCharity->getChildren() as $row) {
					$categoryCharitySubCategories[] = $row;
				}
				$this->getRedis()->set('categoryCharity', $categoryCharity, self::ONE_DAY);
				$this->getRedis()->set('categoryCharitySubCategories', $categoryCharitySubCategories, self::ONE_DAY);
			}
			$categoryCharitySubCategories = $this->getRedis()->get('categoryCharitySubCategories');

			/*** 读取家庭分类 ***/
			if (!$categoryFamily = $this->getRedis()->get('categoryFamily')) {
				$categoryFamily = $repoCategory->findOneBy(array('title' => '家庭'));
				$categoryFamilySubCategories = array($categoryFamily);
				foreach($categoryFamily->getChildren() as $row) {
					$categoryFamilySubCategories[] = $row;
				}
				$this->getRedis()->set('categoryFamily', $categoryFamily, self::ONE_DAY);
				$this->getRedis()->set('categoryFamilySubCategories', $categoryFamilySubCategories, self::ONE_DAY);
			}
			$categoryFamilySubCategories = $this->getRedis()->get('categoryFamilySubCategories');

			/*** 读取商业分类 ***/
			if (!$categoryBusiness = $this->getRedis()->get('categoryBusiness')) {
				$categoryBusiness = $repoCategory->findOneBy(array('title' => '商业'));
				$categoryBusinessSubCategories = array($categoryBusiness);
				foreach($categoryBusiness->getChildren() as $row) {
					$categoryBusinessSubCategories[] = $row;
				}
				$this->getRedis()->set('categoryBusiness', $categoryBusiness, self::ONE_DAY);
				$this->getRedis()->set('categoryBusinessSubCategories', $categoryBusinessSubCategories, self::ONE_DAY);
			}
			$categoryBusinessSubCategories = $this->getRedis()->get('categoryBusinessSubCategories');

			/*** 读取生活分类 ***/
			if (!$categoryLife = $this->getRedis()->get('categoryLife')) {
				$categoryLife = $repoCategory->findOneBy(array('title' => '生活'));
				$categoryLifeSubCategories = array($categoryLife);
				foreach($categoryLife->getChildren() as $row) {
					$categoryLifeSubCategories[] = $row;
				}
				$this->getRedis()->set('categoryLife', $categoryLife, self::ONE_DAY);
				$this->getRedis()->set('categoryLifeSubCategories', $categoryLifeSubCategories, self::ONE_DAY);
			}
			$categoryLifeSubCategories = $this->getRedis()->get('categoryLifeSubCategories');

			$data['categoryChurch'] = $categoryChurch;
			$data['categorySociety'] = $categorySociety;
			$data['categoryCulture'] = $categoryCulture;

			// 首页图片推荐
			$categoryRecommend1 = array(
				$categoryChurchSubCategories,
				$categoryGlobalSubCategories,
			);
			$categoryRecommend2 = array(
				$categorySocietySubCategories,
				$categoryCultureSubCategories,
			);
			$categoryRecommend3 = array(
				$categoryLifeSubCategories,
				$categoryFamilySubCategories,
				$categoryBusinessSubCategories,
			);

			if (!$headline1 = $this->getRedis()->get('headline1')) {
				$headline1Tmp = $repoPost->findByCategory($categoryRecommend1, 3, 0, false, $recommendData);
				$headline1 = array();
				foreach ($headline1Tmp as $row) {
					$headline1[] = $repoPost->normalize($row);
				}
				$this->getRedis()->set('headline1', $headline1, self::ONE_HOUR);
			}

			if (isset($headline1[0])) {
				$data['headline1Top'] = array_shift($headline1);
			}
			$data['headline1Rest'] = $headline1;

			if (!$headline2 = $this->getRedis()->get('headline2')) {
				$headline2Tmp = $repoPost->findByCategory($categoryRecommend2, 3, 0, false, $recommendData);
				$headline2 = array();
				foreach ($headline2Tmp as $row) {
					$headline2[] = $repoPost->normalize($row);
				}
				$this->getRedis()->set('headline2', $headline2, self::ONE_HOUR);
			}
			if (isset($headline2[0])) {
				$data['headline2Top'] = array_shift($headline2);
			}
			$data['headline2Rest'] = $headline2;

			// 生活，信仰，商业大图内容
			if (!$headline3 = $this->getRedis()->get('headline3')) {
				$headline3Tmp = $repoPost->findByCategory($categoryRecommend3, 4, 0, false, $recommendData);
				$headline3 = array();
				foreach ($headline3Tmp as $row) {
					$headline3[] = $repoPost->normalize($row);
				}
				$this->getRedis()->set('headline3', $headline3, self::ONE_HOUR);
			}
			if (isset($headline3[0])) {
				$data['headline3Top'] = array_shift($headline3);
			}
			$data['headline3Rest'] = $headline3;
			

			// 首页文字头条
			// 大图右侧的第一条推荐新闻
			if (!$subHeadlineTop = $this->getRedis()->get('subHeadlineTop')) {
				$subHeadlineTop = $repoPost->findOne(REC_HEADLINE_TXT);
				$this->getRedis()->set('subHeadlineTop', $repoPost->normalize($subHeadlineTop, true), self::ONE_HOUR);
			}
			$data['subHeadlineTop'] = $subHeadlineTop;

			// 大图右侧的3条推荐新闻
			if (!$subHeadline2 = $this->getRedis()->get('subHeadline2')) {
				$subHeadline2 = array(
					$repoPost->normalize($repoPost->findOne(REC_HEADLINE_TXT1), true),
					$repoPost->normalize($repoPost->findOne(REC_HEADLINE_TXT2), true),
					$repoPost->normalize($repoPost->findOne(REC_HEADLINE_TXT3), true),
				);
				$this->getRedis()->set('subHeadline2', $subHeadline2, self::ONE_HOUR);
			}
			$data['subHeadline2'] = $subHeadline2;

			// 过期的头条文章
			if (!$subheadline3 = $this->getRedis()->get('subheadline3')) {
				$subheadline3Tmp = array_merge(
					$repoPost->findMany(REC_HEADLINE_TXT, 5, 1),
					$repoPost->findMany(REC_HEADLINE_TXT1, 4, 1),
					$repoPost->findMany(REC_HEADLINE_TXT2, 3, 1),
					$repoPost->findMany(REC_HEADLINE_TXT3, 3, 1)
				);
				$subheadline3 = array();
				foreach ($subheadline3Tmp as $row) {
					$subheadline3[] = $repoPost->normalize($row);
				}
				$this->getRedis()->set('subheadline3', $subheadline3, self::ONE_HOUR);
			}
			$data['subHeadline3'] = $subheadline3;

			// 读取多个分类的文章
			$data['categoryGlobal'] = $categoryGlobal;
			$data['categoryCharity'] = $categoryCharity;
			$data['categoryBusiness'] = $categoryBusiness;
			$data['categoryLife'] = $categoryLife;
			$data['categoryFamily'] = $categoryFamily;

			// 读取图片集内容
			$repoGallery = $this->getCoreRepo('Gallery');
			if (!$galleries = $this->getRedis()->get('galleries')) {
				$galleriesTmp = $repoGallery->findLatest(4);
				$galleries = array();
				foreach ($galleriesTmp as $row) {
					$galleries[] = $repoGallery->normalize($row);
				}
				$this->getRedis()->set('galleries', $galleries, self::ONE_HOUR);
			}
			$data['gallery'] = array_shift($galleries);
			$data['galleries'] = $galleries;

			// 读取地区下面的文章内容
			// 读取北京，上海，深圳
			$repoProvince = $this->getCoreRepo('Area');
			$repoRegion = $this->getCoreRepo('AreaCategory');
			$citiesName = array(
				'北京',
				'上海',
				'广东',
			);

			if (!$cities = $this->getRedis()->get('cities')) {
				$cities = array();
				foreach ($citiesName as $key => $name) {
					$cities[$key]['city'] = $repoProvince->findOneBy(array('title' => $name));
					$cities[$key]['postTop'] = $repoPost->normalize($repoPost->findOneByProvince(
							$cities[$key]['city'],
							true, 
							$this->getDoctrine()->getManager()));
					$cities[$key]['posts'] = $repoPost->findByProvince($cities[$key]['city'], 2, 2);
				}
				$this->getRedis()->set('cities', $cities, self::ONE_HOUR);
			}
			$data['cities'] = $cities;


			$regionsName = array(
				'华东',
				'东北',
				'中南',
				'华北',
				'西南',
				'西北',
			);

			if (!$regions = $this->getRedis()->get('regions')) {
				$regions = array();
				foreach ($regionsName as $key => $name) {
					$regions[$key]['region'] = $repoRegion->findOneBy(array('title' => $name));
					$posts = $repoPost->findByRegion($regions[$key]['region'], 3);
					foreach ($posts as $row) {
						$regions[$key]['posts'][] = $repoPost->normalize($row);
					}
				}
				$this->getRedis()->set('regions', $regions, self::ONE_HOUR);
			}
			$data['regions'] = $regions;

			// 热门新闻
			if (!$hots = $this->getRedis()->get('hots')) {
				$hots = $repoPost->normalizeArray($repoPost->getHot(10));
				$this->getRedis()->set('hots', $hots, self::ONE_DAY);
			}
			$data['hots'] = $hots;

			$this->getRedis()->set('indexData', $data, self::ONE_HOUR);
		//}

		$response = $this->render('FrontendWebBundle:Index:index.html.twig', $data);
		return $response;
	}
}
