<?php

namespace Frontend\WebBundle\Controller;

use Frontend\WebBundle\Controller\WebController;
use Symfony\Component\HttpFoundation\Response;
/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class TopicController extends WebController
{
	public function indexAction($category_id = null, $slug = null)
	{
		$repoTopic = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:Topic');

		$repoTopicCategory = $this->getDoctrine()->getManager()->getRepository('GospelCoreBundle:CategoryTopic');

		// 读取专题分类
		$data['categories'] = $repoTopicCategory->findAll();

		// 读取当前分类，默认为null
		$data['category'] = null;
		if ($category_id) {
			$data['category'] = $repoTopicCategory->find($category_id);
		}
		$data['category_id'] = $category_id;

		// 读取该专栏作家的文章列表
    	$paginator  = $this->get('knp_paginator');
		$pagination = $repoTopic->findAllWithPagitaion($paginator, $this->get('request')->query->get('page', 1), $data['category']);
		$data['pagination'] = $pagination;
		$items = $pagination->getItems();
		$top = array_slice($items, 0, 1);
		$data['topicFirst'] = $top[0];
		$data['topicsRest'] = array_slice($items, 1, 10);

		return $this->render('FrontendWebBundle:Topic:topicList.html.twig', $data, $this->getCachedResponse());
	}

	/**
	 */
	public function showAction($id, $slug)
	{
		$repoTopic = $this->getCoreRepo('Topic');

		// TODO: 读取当前的专题
		$data['topic'] = $repoTopic->findOneBy(array(
			'id' => $id,
			'status' => 1,
		));
		if(!$data['topic']) {
			throw $this->createNotFoundException("您所查找的专题不存在！");
		}
		$repoTopicPost = $this->getDoctrine()->getRepository('GospelCoreBundle:TopicPost');

		$data['posts'] = $repoTopicPost->findBy(array(
				'topic' => $data['topic'],
			), array(
				'sortOrder' => 'ASC',
		));

		// TODO: 读取最新的8个专题
		$data['topicsLeft'] = $repoTopic->findLatest(4);
		$data['topicsRight'] = $repoTopic->findLatest(4, 4);

		return $this->render('FrontendWebBundle:Topic:topicShow.html.twig', $data, $this->getCachedResponse());
	}

}
