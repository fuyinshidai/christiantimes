<?php

namespace Frontend\WebBundle\Controller;

use Frontend\WebBundle\Controller\WebController;
use Symfony\Component\HttpFoundation\Response;
/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class CopyrightController extends WebController
{
	/**
	 */
	public function indexAction()
	{
		$data = array();
		$data['about'] = array(
			'title' => '版权声明',
			'content' => '<h3>版权声明</h3>
<p>
1.凡本网来源标注是“基督时报”的文章权归基督时报所有。未经基督时报授权，任何印刷性书籍刊物、公共网站、电子刊物不得转载或引用本网图文。欢迎个体读者转载或分享于您个人的博客、微博、微信及其他社交媒体，但请务必清楚标明出处、作者与链接地址（URL）。其他公共微博、微信公众号等公共平台如需转载引用，请通过电子邮件（<a href="mailto:jidushibao@gmail.com?subject=申请转载或版权事宜" target="_blank">jidushibao@gmail.com</a>）、电话（010-82233254）或微博（<a href="http://weibo.com/cnchristiantimes" target="_blank">http://weibo.com/cnchristiantimes</a>），微信（jidushibao2013）联络我们，得到授权方可转载或做其他使用。
</p>
<p>
2.基督时报上呈现的网站页面设计与版式编排，为基督时报独立拥有版权。除非中国法律另有规定，未经基督时报书面许可，对于基督时报拥有版权和/或其他知识产权的任何内容，任何人不得复制或在非基督时报所属服务器上做镜像处理或以其他任何方式进行使用。
<br />
<br />
违反上述声明擅自使用基督时报以上内容的，基督时报将依法追究其法律责任。
</p>'
		);

		return $this->render('FrontendWebBundle:About:show.contact.html.twig', $data);
	}
}
