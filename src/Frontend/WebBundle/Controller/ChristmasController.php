<?php

namespace Frontend\WebBundle\Controller;

use Gospel\Bundle\CoreBundle\Entity\Post;
use Frontend\WebBundle\Controller\WebController;
use Symfony\Component\HttpFoundation\Request;

class ChristmasController extends WebController
{
    public function indexAction(Request $request)
    {
		$data = array(); // 传递到模板里面的数据

		// Repository数据仓库
		$repoPost = $this->getDoctrine()->getRepository('GospelCoreBundle:Post', 'christmas');
		$repoCategory = $this->getDoctrine()->getRepository('GospelCoreBundle:Category', 'christmas');

		$categories = $repoCategory->findAll();

			$view = 'index.html.twig';

		if ($request->get('category', null)) {
			$category = $repoCategory->find($request->get('category', null));
		} else {
			// 如果不是分类的请求使用默认的模板
			$category = null;
		}


    	$paginator  = $this->get('knp_paginator');
		$pagination = $repoPost->findByCategoryWithPagitaion($paginator, $category, $this->get('request')->query->get('page', 1), null, 
				5);

        return $this->render('FrontendWebBundle:Christmas:'.$view, array(
			'categories' => $categories,
			'pagination' => $pagination,
			'current_category' => $category,
		), $this->getCachedResponse());
    }

	/**
	 * 文章显示页面，首先检查文章是否发布，如果是没有发布的文章是禁止查看的
	 * @param type $id
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function showAction($id = null, $slug, $preview = false)
	{
		if ($id == null) {
			$id = $_GET['rid']; // 和旧的网站进行兼容
		}
		$repoPost = $this->getDoctrine()->getRepository('GospelCoreBundle:Post', 'christmas');
		$repoCategory = $this->getDoctrine()->getRepository('GospelCoreBundle:Category', 'christmas');

		$data = array();
		$post = $repoPost->find($id);
		// 如果文章不存在或者文章没有发布:退出
		if (null === $post) {
			throw $this->createNotFoundException("您所查找的文章不存在！");
		}

		// 文章没有发布并且没有预览的权限
		if ($post->getStatus() != POST_STATUS_PUBLISHED) {
			if ( $preview === false) {
				throw $this->createNotFoundException("您所查找的文章不存在！");
			}
		}
		$response = $this->getCachedResponse();

		// 如果是preview 的请求但是没有预览的权限
		if ($preview == true) {
			if (! $this->hasRole('ROLE_POST_PREVIEW')) {
				throw $this->createNotFoundException("您所查找的文章不存在！");
			}
			$response->setPrivate();
		}

		// 更新文章点击量
		$post->setHits($post->getHits() + 1);
		$this->getDoctrine()->getManager('christmas')->persist($post);
		$this->getDoctrine()->getManager('christmas')->flush();


		$data['post'] = $post;
		return $this->render('FrontendWebBundle:Christmas:show.html.twig', $data, $response);
	}

	public function mostReadAction()
	{
		$repoPost = $this->getDoctrine()->getRepository('GospelCoreBundle:Post', 'christmas');
		$mostRead = $repoPost->findBy(array(
			'status' => 1,
		), array('hits' =>'desc'), 10);
        return $this->render('FrontendWebBundle:Christmas:mostRead.html.twig', array(
			'posts' => $mostRead,
		), $this->getCachedResponse());
	}
}
