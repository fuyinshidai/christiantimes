<?php

namespace Frontend\WebBundle\Controller;

use Frontend\WebBundle\Controller\WebController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Gospel\Bundle\NewsletterBundle\Entity\Address;
use Gospel\Bundle\NewsletterBundle\Form\AddressType;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class MagazineController extends WebController
{

	/**
	 * show the magazine display page
	 */
	public function downloadAction($slug)
	{
		$magazine = $this->getDoctrine()->getRepository('GospelCoreBundle:Magazine')->findOneBy(array('slug' => $slug));
		if (!$magazine) {
			throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('查询杂志不存在');
		}
		$request = $this->get('request');
		$address = new Address;
		$form = $this->createForm(new AddressType(), $address, array(
			'action' => $this->generateUrl('magazine', array('slug' => '201411')),
			'method' => 'POST',
		));
		$form->handleRequest($request);
		if ($form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$em->persist($address);
			$em->flush();
			$this->get('session')->getFlashBag()->add(
				'success',
				'提交成功!'
			);

			return $this->redirect($this->generateUrl('magazine', array('slug' => '201411')));
		}
		// newsletter
		$creteria = array(
			'isActive' => true,
		);
		$categories = $this->getDoctrine()->getRepository('GospelNewsletterBundle:Category')->findBy($creteria);
		return $this->render('FrontendWebBundle:Magazine:download.html.twig', array(
			'form' => $form->createView(),
			'newsletterCategorys' => $categories,
			'magazine' => $magazine,
		));
	}

	/**
	 * do the download action
	 */
	public function getAction($slug)
	{
		$magazine = $this->getDoctrine()->getRepository('GospelCoreBundle:Magazine')->findOneBy(array('slug' => $slug));
		if (!$magazine) {
			throw \Symfony\Component\HttpKernel\Exception\NotFoundHttpException();
		}

		$magazine->setHit($magazine->getHit() + 1);
		$this->getDoctrine()->getManager()->persist($magazine);
		$this->getDoctrine()->getManager()->flush();

		$file = $magazine->getFullPath();


		$response = new Response();
		$response->headers->set('Cache-Control', 'private');
		$response->headers->set('Content-Type', mime_content_type($file));
		$response->headers->set('Content-Disposition', 'attachment; filename=' . $magazine->getDisplayName());
		$response->headers->set('Content-length', filesize($file));

		// Send headers before output anything
		$response->sendHeaders();
		$response->setContent(readfile($file));
	}

	public function viewerAction($pdf)
	{
		return $this->render('FrontendWebBundle:Magazine:viewer.html.twig', array(
			'pdf' => $pdf,
		));
	}
}
