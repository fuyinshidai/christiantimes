<?php

/*
 * 网站关于我们的控制器，包括关于我们，联系我们，爱心奉献，欢迎投稿等
 */

namespace Frontend\WebBundle\Controller;

use Frontend\WebBundle\Controller\WebController;
use Symfony\Component\HttpFoundation\Request;
use Gospel\Bundle\CoreBundle\Entity\Contact;
use Gospel\Bundle\CoreBundle\Form\ContactType;
use Gospel\Bundle\CoreBundle\Entity\Submission;
use Gospel\Bundle\CoreBundle\Form\SubmissionType;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class AboutController extends WebController
{

	/**
	 * 根据文章id显示关于我们的内容
	 */
	public function showAction($id, $slug)
	{
		$data = array();
		$data['abouts'] = array();
		$repoAbout = $this->getRepository('GospelCoreBundle:About');

		// 关于我们相关页面的导航
		$menus = array('爱心奉献', '关于我们', '联系我们', '欢迎投稿', '信仰宣言', '版权声明');
		foreach ($menus as $title) {
			// 读取每个文章
			$entity = $repoAbout->findOneBy(array('title' => $title));
			if ($entity) {
				$data['abouts'][] = $entity;
			}

		}
		$broadcast = $repoAbout->find($id);
			switch ($broadcast->getTitle()) {
				case "爱心奉献":
					$view = 'donation';
					$data['bank'] = $repoAbout->findOneBy(array('title' => '银行账号'));
					$data['equipment'] = $repoAbout->findOneBy(array('title' => '所需设备'));
					$data['letter'] = $repoAbout->findOneBy(array('title' => '爱心奉献'));
					break;
				case "关于我们":
					$view = 'about';
					$data['vision'] = $repoAbout->findOneBy(array('title' => '异象'));
					$data['mission'] = $repoAbout->findOneBy(array('title' => '使命'));
					$data['purpose'] = $repoAbout->findOneBy(array('title' => '简介'));
					$data['confession'] = $repoAbout->findOneBy(array('title' => '信仰宣言'));
					break;
				case "联系我们":
					$view = 'contact';
					break;
				case "欢迎投稿":
					$view = 'submission';
					// 读取用户提交的临时数据
					$submission = $this->get('session')->get('submission', null);
					if ($submission == null) {
						$submission = new Submission();
					}
					$form = $this->createForm(new SubmissionType(), $submission, array(
						'entity' => $submission,
						'action' => $this->generateUrl('saveSubmission')
					));
					$data['form'] = $form->createView();
					break;
				case "信仰宣言":
					$view = 'support';
					$data['support'] = $repoAbout->findOneBy(array('title' => '信仰宣言'));
					break;
				case "版权声明":
					$view = 'copyright';
					$data['copyright'] = $repoAbout->findOneBy(array('title' => '版权声明'));
					break;
			}
		if (!$broadcast->getPublishStatus()) {
			throw $this->createNotFoundException("您所查找的文章不存在！");
		}
		$data['about'] = $broadcast;
		return $this->render('FrontendWebBundle:About:show.'.$view.'.html.twig', $data);
	}

	public function showDonationHelpAction() {
		return $this->render('FrontendWebBundle:About:show.donation.help.html.twig');
	}

	/**
	 * 保存网站留言
	 */
	public function saveContactAction(Request $request)
	{
		$contact = new Contact();
		$form = $this->createForm(new ContactType(), $contact, array(
			'entity' => $contact,
		));
		$aboutContact = $this->getCoreRepo('About')->findOneBy(array('title' => '联系我们'));

		// 保存表单
		$session = $this->get('session');
		$request = $this->getRequest();
		if ($request->isMethod('POST')) {
			$form->handleRequest($request);
			if ($form->isValid()) {
				$em = $this->getDoctrine()->getManager();
				$em->persist($contact);
				$em->flush();
				$this->get('session')->getFlashBag()->add(
						'notice', '您的留言已经保存'
				);
				$session->set('contact', null);
			} else {
				$session->set('contact', $contact);
				$this->get('session')->getFlashBag()->add(
						'notice', '您提交的信息有误，请重新提交'
				);
			}
		}
		return $this->redirect($this->generateUrl('about', array(
							'id' => $aboutContact->getId(),
							'slug' => $this->slugfy($aboutContact->getTitle()),
		)));
	}

	/**
	 * 保存投稿
	 */
	public function saveSubmissionAction(Request $request)
	{
		$submission = new Submission();
		$form = $this->createForm(new SubmissionType(), $submission, array(
			'entity' => $submission,
		));
		$aboutSubmission = $this->getCoreRepo('About')->findOneBy(array('title' => '欢迎投稿'));

		// 保存表单
		$session = $this->get('session');
		$request = $this->getRequest();
		if ($request->isMethod('POST')) {
			$form->handleRequest($request);
			if ($form->isValid()) {
				$em = $this->getDoctrine()->getManager();
				$em->persist($submission);
				$em->flush();
				$this->get('session')->getFlashBag()->add(
						'notice', '您的投稿已经保存'
				);
				$session->set('submission', null);
			} else {
//				$session->set('submission', $submission);
				$this->get('session')->getFlashBag()->add(
						'notice', '您提交的信息有误，请重新提交'
				);
			}
		}
		return $this->redirect($this->generateUrl('about', array(
							'id' => $aboutSubmission->getId(),
							'slug' => $this->slugfy($aboutSubmission->getTitle()),
		)));
	}

}
