<?php

namespace Frontend\WebBundle\Controller;

use Frontend\WebBundle\Controller\WebController;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Security\Authorization\Expression\Expression;
use Frontend\WebBundle\Classes\StringUnity;
use Gospel\Bundle\CoreBundle\Classes\Pagination;
use Gospel\Bundle\CoreBundle\Twig\GospelExtension;
/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class PostController extends WebController
{
	/**
	 * The content page breaker
	 */
	protected $pageBreaker = '_page_break_tag_';
/**
	 * 文章显示页面，首先检查文章是否发布，如果是没有发布的文章是禁止查看的
	 * The page braker of the content
	 * _page_break_tag_
	 * @param type $id
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function showAction($id = null, $slug, $preview = false)
	{
		$request = $this->get('request');
		if ($id == null) {
			$id = $_GET['rid']; // 和旧的网站进行兼容
		}
		$repoPost = $this->getDoctrine()->getRepository('GospelCoreBundle:Post');
		$repoCategory = $this->getDoctrine()->getRepository('GospelCoreBundle:Category');

		$data = array();
		$post = $repoPost->find($id);
		// 如果文章不存在或者文章没有发布:退出
		if (null === $post) {
			throw $this->createNotFoundException("您所查找的文章不存在！");
		}
		if ($slug !== GospelExtension::slugfyFilter($post->getAlias($request->getLocale()))) {
			if ($this->container->getParameter('locale') == $request->getLocale()) {
				return $this->redirect($this->generateUrl(
						'news_show', array(
							'id'=>$id,
							'slug'=>GospelExtension::slugfyFilter($post->getAlias($request->getLocale())),
							'page' => $request->get('page', 1),
						)));
			} else {
				return $this->redirect($this->generateUrl(
						'news_show_'.$request->getLocale(), array(
							'id'=>$id,
							'slug'=> GospelExtension::slugfyFilter($post->getAlias($request->getLocale())),
							'page' => $request->get('page', 1),
						)
					));
				
			}
		}

		// 文章没有发布并且没有预览的权限
		if ($post->getStatus() != POST_STATUS_PUBLISHED) {
			if ( $preview === false) {
				throw $this->createNotFoundException("您所查找的文章不存在！");
			}
		}
		$response = $this->getCachedResponse();

		// 如果是preview 的请求但是没有预览的权限
		if ($preview == true) {
			if (! $this->hasRole('ROLE_POST_PREVIEW')) {
				throw $this->createNotFoundException("您所查找的文章不存在！");
			}
			$response->setPrivate();
		}
		

		// 文章显示模板
		$displayStyle = $post->getIsSlideImage() ? DISPLAY_STYLE_SLIDE : DISPLAY_STYLE_DEFAULT;

		// 更新文章点击量
		$post->setHits($post->getHits() + 1);
		$this->getDoctrine()->getManager()->persist($post);
		$this->getDoctrine()->getManager()->flush();

		// 查找分类树
		$category = $post->getCategory();

		$data['post'] = $post;

		// remove font-size
		$content = preg_replace('/font-size[^;"]+;?/', '', html_entity_decode($post->getContent($request->getLocale()), ENT_COMPAT, 'GB2312'));


		/** start pagination **/
		// 文章分页相关
		// 数该文章一共有多少分页，如果分页大于1，默认显示第一页
		// 分页标示符: _page_break_tag_
		$postContentsArray = explode($this->pageBreaker, $content);
		$pagesTotal = count($postContentsArray);
		$pagesCurrent = $request->get('page', 1);
		$showPhoto = true;
		if($pagesCurrent > 1 || $pagesCurrent == 'rest') {
			// 不是第一页的时候不显示大图
			$showPhoto = false;
		} 
		$data['showPhoto'] = $showPhoto;

		$pagination = new Pagination();
		$pagination->records($pagesTotal);
		$pagination->records_per_page(1);
		// show all the page content in one page
		if ($pagesCurrent == 'all') {
			$data['content'] = implode('', $postContentsArray);
		} elseif ($pagesCurrent == 'rest') { // show the rest content from last page
			preg_match('/page=(\d+)/', urldecode($request->headers->get('referer')), $matches);
			$lastPage = 1;
			$lastPage = isset($matches[1]) ? $matches[1] : 1;
			$data['content'] = implode('', array_slice($postContentsArray, $lastPage));
		} else {
			$data['content'] = $postContentsArray[$pagesCurrent-1];
		}
		$data['content'] = str_replace('http://#', $request->getBaseUrl() . $request->getPathInfo() . '#', $data['content']);
		$showAllLink = true;
		$data['pagination'] = $pagination->render(true, $showAllLink);

		/** end pagination **/

		$data['category'] = $category;
		$data['domain'] = $request->server->get('HTTP_HOST'); 

		// 主板头条
		// 读取首页推荐文章数据库内容
		if (!$mianHeadlines = $this->getRedis()->get('mianHeadlines')) {
			if (!$categoryChurch = $this->getRedis()->get('categoryChurch')) {
				$categoryChurch = $repoCategory->findOneBy(array('title' => '教会'));

				// 设置该分类下面的子分类
				$categoryChurchSubcategories = array($categoryChurch);
				foreach ($categoryChurch->getChildren() as $subCategory) {
					$categoryChurchSubcategories[] = $subCategory;
				}
				$this->getRedis()->set('categoryChurch', $categoryChurch, self::ONE_DAY);
				$this->getRedis()->set('categoryChurchSubCategories', $categoryChurchSubcategories, self::ONE_DAY);
			}
			$categoryChurchSubCategories = $this->getRedis()->get('categoryChurchSubCategories');

			$categoryGlobal = $repoCategory->findOneBy(array('title' => '国际'));
			/*** 读取国际分类 ***/
			if (!$categoryGlobal = $this->getRedis()->get('categoryGlobal')) {
				$categoryGlobal = $repoCategory->findOneBy(array('title' => '国际'));
				$this->getRedis()->set('categoryGlobal', $categoryGlobal, self::ONE_DAY);

				$categoryGlobalSubCategories = array($categoryGlobal);
				foreach ($categoryGlobal->getChildren() as $row) {
					$categoryGlobalSubCategories[] = $row;
				}
				$this->getRedis()->set('categoryGlobalSubCategories', $categoryGlobalSubCategories, self::ONE_DAY);
			}
			$categoryGlobalSubCategories = $this->getRedis()->get('categoryGlobalSubCategories');

			/*** 读取文化分类 ***/
			if (!$categoryCulture = $this->getRedis()->get('categoryCulture')) {
				$categoryCulture = $repoCategory->findOneBy(array('title' => '文化'));

				$categoryCultureSubCategories = array($categoryCulture);
				foreach ($categoryCulture->getChildren() as $row) {
					$categoryCultureSubCategories[] = $row;
				}
				$this->getRedis()->set('categoryCulture', $categoryCulture, self::ONE_DAY);
				$this->getRedis()->set('categoryCultureSubCategories', $categoryCultureSubCategories, self::ONE_DAY);
			}
			$categoryCultureSubCategories = $this->getRedis()->get('categoryCultureSubCategories');

			/*** 读取社会相关分类 ***/
			if (!$categorySociety = $this->getRedis()->get('categorySociety')) {
				$categorySociety = $repoCategory->findOneBy(array('title' => '社会'));

				// 设置子分类
				$categorySocietySubCategories = array($categorySociety);
				foreach ($categorySociety->getChildren() as $subCategory) {
					$categorySocietySubCategories[] = $subCategory;
				}
				$this->getRedis()->set('categorySociety', $categorySociety, self::ONE_DAY);
				$this->getRedis()->set('categorySocietySubCategories', $categorySocietySubCategories, self::ONE_DAY);
			}
			$categorySocietySubCategories = $this->getRedis()->get('categorySocietySubCategories');

			$categoryRecommendMainHeadline = array(
				$categoryChurchSubCategories,
				$categoryGlobalSubCategories,
				$categoryCultureSubCategories,
				$categorySocietySubCategories,
			);
			// 需要是推荐到首页的内容
			$recommendDataMainHeadlines = array(
				'recommend' => array(
					'type' => 'isHomeHeadlinePic',
					'value' => 1,
				)
			);
			$mianHeadlines = $repoPost->normalizeArray($repoPost->findByCategory($categoryRecommendMainHeadline, 4, 0, false, $recommendDataMainHeadlines));
			$this->getRedis()->set('mianHeadlines', $mianHeadlines, self::ONE_HOUR);
		}
		if (isset($mianHeadlines[0])) {
			$data['mainHeadlineTop'] = array_shift($mianHeadlines);
		}
		$data['mainHeadlineRest'] = $mianHeadlines;

		// 图片新闻
		if (!$postImages = $this->getRedis()->get('postImages')) {
			$postImages = $repoPost->normalizeArray($repoPost->findByCategoryWithImage(null, $this->getDoctrine()->getManager(), 4));
		}
		$data['postImages'] = $postImages;

		// 各地资讯
		if (!$regionsPost = $this->getRedis()->get('regionsPost')) {
			$regionsPost = $repoPost->normalizeArray($repoPost->findRegionsLatest(7));
		}
		$data['regionsPost'] = $regionsPost;

		if($preview) {
        return $response = $this->render('FrontendWebBundle:Post:show.'.$displayStyle.'.html.twig', $data);
		} else {
      		$response = $this->render('FrontendWebBundle:Post:show.'.$displayStyle.'.html.twig', $data, $response);
			$response->setPublic();
			$response->setMaxAge(3600);
			$response->setEtag(md5($response->getContent()));
			$response->setLastModified($post->getUpdatedAt());
			return $response;
		}
	}

	/**
	 * 文章分类列表页面
	 * @param type $id
	 * @param type $slug
	 * @param boolean $other if true, then use CategoryOther for category
	 * @return type
	 */
	public function categoryAction($category_id, $slug, $other = false)
	{
		$data = array(
			'id' => $category_id,
		);
		$repoPost = $this->getCoreRepo('Post');
		if ($other) {
			$data['categoryType'] = 'CategoryOther';
			$repoCategory = $this->getCoreRepo('CategoryOther');
		} else {
			$data['categoryType'] = 'Category';
			$repoCategory = $this->getCoreRepo('Category');
		}



		// 查找分类树
		$category = $repoCategory->find($category_id);
		$data['category'] = $category;

		// 当前主栏目内容
		$menus = $this->container->getParameter('frontend.navigation');
		foreach ($menus as $menu) {
			if ($menu['name'] == $category->getTitle()) {
				$data['menu'] = $menu;
				break;
			}
		}
		if (!isset($data['menu'])) {
			$parentCategory = $category->getParent();
			if (is_object($parentCategory)) {
				foreach ($menus as $menu) {
					if ($menu['name'] == $parentCategory->getTitle()) {
						$data['menu'] = $menu;
						break;
					}
				}
			}
		}
		if(isset($data['menu']['name'])) {
			$data['title'] = StringUnity::add_blank($data['menu']['name']);
		} else {
			$data['title'] = '';
		}

    	$paginator  = $this->get('knp_paginator');
		$data['pagination'] = $repoPost->findByCategoryWithPagitaion($paginator, $category, $this->get('request')->query->get('page', 1));
		$data['top'] = $data['pagination']->current();
		return $this->render('FrontendWebBundle:Post:list.html.twig', $data, $this->getCachedResponse());
	}

	/**
	 * 区域新闻
	 * @param type $id
	 * @param type $slug
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function regionAction($id, $slug)
	{
		$data = array();
		// 取得当前的省份
		$region = $this->getCoreRepo('AreaCategory')->find($id);
		$data['province'] = $region;

		// 取得当前省份下面的文章列表，同时包含有分页功能
    	$paginator  = $this->get('knp_paginator');
		$data['pagination'] = $this->getCoreRepo('Post')->findByCategoryWithPagitaion($paginator, null, $this->get('request')->query->get('page', 1), $region);
		$data['top'] = $data['pagination']->current();
		return $this->render('FrontendWebBundle:Area:list.html.twig', $data, $this->getCachedResponse());
	}

	/**
	 * 预览函数，如果没有发布的文章也可以查看
	 * @param int $id
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function previewAction($id)
	{
		return new Response($id);
	}

	public function columnistAction($id, $slug)
	{
		$data = array();
		$repoPost = $this->getCoreRepo('Post');
		$repoAuthor = $this->getCoreRepo('Author');

		// 读取当前的专栏作家
		$author = $repoAuthor->find($id);
		$data['author'] = $author;

		if (!$author->getStatus()) {
			throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('所查找专栏作家不存在.');
		}

		// 读取该专栏作家的文章列表
    	$paginator  = $this->get('knp_paginator');
		$data['pagination'] = $repoPost->findByAuthorWithPagitaion($paginator, $author, $this->get('request')->query->get('page', 1));

		return $this->render('FrontendWebBundle:Post:columnist.html.twig', $data, $this->getCachedResponse());
	}

	/**
	 * 深读
	 * @return type
	 */
	public function insideAction()
	{
		$data = array();
    	$paginator  = $this->get('knp_paginator');
		$data['title'] = '深读';
		$data['pagination'] = $this->getCoreRepo('Post')
				->getInsidePosts($paginator, $this->get('request')->query->get('page', 1));
		$data['top'] = $data['pagination']->current();
		return $this->render('FrontendWebBundle:Post:list.html.twig', $data, $this->getCachedResponse());
	}

	/**
	 * 观点专栏页面
	 * @return type
	 */
	public function viewsAction($category = null, $slug)
	{
		$repoPost = $this->getCoreRepo('Post');
		$repoAuthor = $this->getCoreRepo('Author');
		$repoAuthorCategory = $this->getCoreRepo('CategoryAuthor');

		$data = array();
		// 读取该专栏作家的文章列表
		$categories = $repoAuthorCategory->findBy(array(
			'inMenu' => 1,
		));
		$data['categories'] = $categories;
		$data['category'] = null;

		$category_id = $this->getRequest()->get('id', null);
		// if category id is not set, find post of all the categories
		if ($category_id !== null) {
			$category = $repoAuthorCategory->findOneBy(array('id'=>$category_id));
			$authors = $repoAuthor->findAuthors(array($category));
			$data['category'] = $category;
		} else { // If the category id is set, then find the posts under this category
			$authors = $repoAuthor->findAuthors($categories);
		}

    	$paginator  = $this->get('knp_paginator');
		$data['pagination'] = $repoPost->findByAuthorsWithPagitaion(
				$paginator, $authors, 
				$this->get('request')->query->get('page', 1),
				15
				);
		$data['top'] = $data['pagination']->current();


		// 右侧数据
		// 推荐作家
		$data['recommendAuthors'] = $repoAuthor->findRecommendAuthors(3);
		foreach ($data['recommendAuthors'] as $key => $row) {
			$data['recommendAuthorsPost'][$key] = $repoPost->findOneBy(array('author' => $row),
					array('id' => 'DESC'));
		}

		// 专栏作家
		$category1 = $repoAuthorCategory->findOneBy(array('title'=>'专栏作家'));
		$data['category1'] = $category1;
		$data['columnists1'] = $repoAuthor->findBy(array(
			'category' => $category1,
			'status' => 1,
		), array('id' => 'ASC'));

		$category2 = $repoAuthorCategory->findOneBy(array('title'=>'学人智库'));
		$data['category2'] = $category2;
		$data['columnists2'] = $repoAuthor->findBy(array(
			'category' => $category2,
			'status' => 1,
		), array('id' => 'ASC'));

		$category3 = $repoAuthorCategory->findOneBy(array('title'=>'评论嘉宾'));
		$data['category3'] = $category3;
		$data['columnists3'] = $repoAuthor->findBy(array(
			'category' => $category3,
			'status' => 1,
		), array('id' => 'ASC'));
		return $this->render('FrontendWebBundle:Post:views.html.twig', $data);
	}

	public function authorAction($name)
	{
		$data = array(
			'writer' => $name,
		);
		$repoPost = $this->getCoreRepo('Post');
		$repoAuthor = $this->getCoreRepo('Author');
		$repoAuthorCategory = $this->getCoreRepo('CategoryAuthor');

		// 右侧数据
		// 推荐作家
		$data['recommendAuthors'] = $repoAuthor->findRecommendAuthors(3);
		foreach ($data['recommendAuthors'] as $key => $row) {
			$data['recommendAuthorsPost'][$key] = $repoPost->findOneBy(array('author' => $row),
					array('id' => 'DESC'));
		}

		// 专栏作家
		$category1 = $repoAuthorCategory->findOneBy(array('title'=>'专栏作家'));
		$data['category1'] = $category1;
		$data['columnists1'] = $repoAuthor->findBy(array(
			'category' => $category1,
			'status' => 1,
		), array('id' => 'ASC'));

		$category2 = $repoAuthorCategory->findOneBy(array('title'=>'学人智库'));
		$data['category2'] = $category2;
		$data['columnists2'] = $repoAuthor->findBy(array(
			'category' => $category2,
			'status' => 1,
		), array('id' => 'ASC'));

		$category3 = $repoAuthorCategory->findOneBy(array('title'=>'评论嘉宾'));
		$data['category3'] = $category3;
		$data['columnists3'] = $repoAuthor->findBy(array(
			'category' => $category3,
			'status' => 1,
		), array('id' => 'ASC'));

		// 读取该专栏作家的文章列表
    	$paginator  = $this->get('knp_paginator');
		$data['pagination'] = $repoPost->findByWriteNameithPagitaion($paginator, $name, $this->get('request')->query->get('page', 1));

		return $this->render('FrontendWebBundle:Post:authorPosts.html.twig', $data, $this->getCachedResponse());
	}
}
