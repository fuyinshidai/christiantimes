<?php

namespace Frontend\WebBundle\Controller;

use Frontend\WebBundle\Controller\WebController;

/**
 * 特刊的控制器
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class SpecialController extends WebController
{
	/**
	 * 特刊列表
	 */
	public function indexAction()
	{
		$this->render('FrontendWebBundle:Special:showSpecial.html.twig');
	}

	/**
	 * 显示特刊
	 * @param type $id
	 * @param type $slug
	 * @throws type
	 */
	public function showAction($id, $slug)
	{
		$repoPost = $this->getCoreRepo('Post');
		$repoCategoryOther = $this->getCoreRepo('CategoryOther');
		$data = array();
		$special = $this->getCoreRepo('Special')->find($id);
		if (!$special || !$special->getStatus()) {
			throw $this->createNotFoundException('您查看的页面不存在.');
		}

		$data['special'] = $special;
		$data['headline'] = $repoPost->find($special->getHeadline());
		
		// 文字头条
		$ids = explode('|', $special->getSubheadline());
		foreach ($ids as $postId) {
			$data['subheadline'][] = $repoPost->find($postId);
		}

		// 编辑推荐
		$idsEditors = explode('|', $special->getRecommend());
		foreach ($idsEditors as $postId) {
			$data['editors'][] = $repoPost->find($postId);
		}

		// 解读
		$data['interpretation'] = $repoPost->find($special->getInterpretation());

		// 视频
		$data['video'] = $this->getCoreRepo('Video')->find($special->getVideo());

		// 读取各个分类以及分类文章
		$category1 = $repoCategoryOther->find($special->getCategory1());
		$post1 = $repoPost->findOneByCategoryother($category1, array('id' => 'DESC'));
		$postsCategory1 = $repoPost->findByCategoryOther($category1, 6, 1);

		$category2 = $repoCategoryOther->find($special->getCategory2());
		$post2 = $repoPost->findOneByCategoryother($category2, array('id' => 'DESC'));
		$postsCategory2 = $repoPost->findByCategoryOther($category2, 5, 1);

		$category3 = $repoCategoryOther->find($special->getCategory3());
		$postCategory3 = $repoPost->findByCategoryOther($category3, 4, 0);
		$post3 = $repoPost->findOneByCategoryother($category3, array('id' => 'DESC'));

		$category4 = $repoCategoryOther->find($special->getCategory4());
		$postCategory4 = $repoPost->findByCategoryOther($category4, 4, 0);
		$post4 = $repoPost->findOneByCategoryother($category4, array('id' => 'DESC'));

		$category5 = $repoCategoryOther->find($special->getCategory5());
		$postCategory5 = $repoPost->findByCategoryOther($category5, 6, 0);

		$category6 = $repoCategoryOther->find($special->getCategory6());
		$postCategory6 = $repoPost->findByCategoryOther($category6, 10, 0);

		$data['categoryData'] = array(
			'category1' => array(
				'category' => $category1,
				'posts' => $postsCategory1,
				'post' => $post1,
			),
			'category2' => array(
				'category' => $category2,
				'post' => $post2,
				'posts' => $postsCategory2,
			),
			'category3' => array(
				'category' => $category3,
				'posts' => $postCategory3,
				'post' => $post3,
			),
			'category4' => array(
				'category' => $category4,
				'posts' => $postCategory4,
				'post' => $post4,
			),
			'category5' => array(
				'category' => $category5,
				'posts' => $postCategory5,
			),
			'category6' => array(
				'category' => $category6,
				'posts' => $postCategory6,
			),
		);

		return $this->render('FrontendWebBundle:Special:showSpecial.html.twig', $data);
	}
}
