$(document).ready(function(){
	var ua = navigator.userAgent; 
	var event = (ua.match(/iPad/i)) ? 'touchstart' : 'click'; 
	//返回顶部
	$(".to-top").click(function(){
                $('body,html').animate({scrollTop:0},600);
                return false;
            });
	/** 菜单收缩 */
	$('.nav-mobile').bind(event, function(e) { 
		e.preventDefault();
		var elm = $('nav');
		elm.toggle();
	})


	$('.bt-w.read-more').toggle(function(){
		$(this).prev().prev().slideUp();
		$(this).prev().slideDown();
		$(this).val('收起');
	}, function() {
		$(this).prev().prev().slideDown();
		$(this).prev().slideUp();
		$(this).val('查看全文');
	})

	// donate other value
	$('.dn-other').focus(function() {
		$(this).prev().attr('checked', 'checked');
		$(this).val('');
	});
	$('.dn-other').change(function() {
		$(this).prev().val($(this).val());
	});

	// donate form submit
	$(".bt-donate").click(function(e) {
		e.preventDefault();
		$(this).parents('form').attr('action', $(this).attr('data-src'));
		$(this).parents('form').submit();
	});
	
})