var gospelApp = angular.module('gospelApp', ['ngDialog', 'ipCookie']);

gospelApp.config(function($interpolateProvider) {
	$interpolateProvider.startSymbol('//');
	$interpolateProvider.endSymbol('//');
});

function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

gospelApp.controller('EmailController', function ($scope, ngDialog, $http, ipCookie) {

	var date = new Date();
	var currentHour = date.getHours();
    $scope.expires = 96 - currentHour;
    //scope.expirationUnit = 'seconds', 'minutes', 'days', 'hours', default is days;
    $scope.expirationUnit = 'hours';

	// if the ip is first time visit the website, popup newsletter subscribe dialog
	$scope.init = function () {
		var cookieName = 'popup'
		var popup = ipCookie(cookieName);
		if (popup != 'no') {
			// Setting a cookie
			ipCookie(cookieName, 'no', {
			    expires: $scope.expires,
				expirationUnit: $scope.expirationUnit
			})
			//$scope.open();
		}
	};
	$scope.open = function () {
		$scope.email = '请输入邮箱';
		$scope.placeholder = '请输入邮箱';

		$scope.dialog = ngDialog.open({
			template: 'newsletterDialog',
			controller: 'EmailController',
			scope: $scope
		});
		if ($("#interemail").val()) {
			$scope.email = $("#interemail").val();
			$("#email").val($("#interemail").val());
		}
	};

	$scope.prevent = function () {
		return false;
	};

	$scope.submit = function () {
		var email = $("#email").val();
		if (IsEmail(email)) {
			$http({
				url: $("#subscribe").data("action"),
				method: "POST",
				data: {data: $("#subscribe").serialize() }
			}).success(function(data, status, headers, config) {
				$scope.data = data;
				if ($scope.data.error) {
					jQuery.alertWindow("⊙_⊙ 很抱歉，您未能成功订阅基督时报~ " + data.msg);
				} else {
            		jQuery.alertWindow(" ☺ 恭喜！您已成功订阅基督时报");
					$scope.dialog.close();
				}
			}).error(function(data, status, headers, config) {
				$scope.status = status;
			});
		} else {
			jQuery.alertWindow("⊙o⊙ 请您输入正确邮箱");
		}
	}

});

// the global scope controller
gospelApp.controller('GospelController', function ($scope) {
});
