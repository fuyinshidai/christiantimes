$(document).ready(function(){
	//其他城市
	var ua = navigator.userAgent; 
	var event = (ua.match(/iPad/i)) ? 'touchstart' : 'click'; 
	/** 其他城市下拉菜单 */
	$('span.city-site').bind(event, function(e) { 
		var elm = $(this).parent();
		if(elm.hasClass('city-on')) {
			elm.removeClass('city-on');
			$('.other-version').hide();
		} else {
			elm.addClass('city-on');
			$('.other-version').show();
		}
	})
	$('.close').bind(event, function(e) { 
		e.preventDefault();
		$('.ht-city').removeClass('city-on');
		$('.other-version').hide();
	})
	
	/** 菜单收缩 */
	$('.nav-more').click(function(e) {
		e.preventDefault();
		var elm = $('.nd-whole');
		elm.toggle();
	});
	
	/** lightbox */
	$(".fancybox").fancybox();
	
	$(".various").fancybox({
		maxWidth	: 800,
		maxHeight	: 600,
		fitToView	: false,
		width		: '70%',
		height		: '70%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});
	/*文章页小图放大*/
	$(".fancybox-effects-d").fancybox({
				padding: 0,

				openEffect : 'elastic',
				openSpeed  : 150,

				closeEffect : 'elastic',
				closeSpeed  : 150,

				closeClick : true,

				helpers : {
					overlay : null
				}
			});
	/*文章页滚动图片*/
	if (typeof($.flexslider) != "undefined") { 
    	// safe to use the function
//		$('.asb-img').flexslider();
	}

	function showVideo(path) {
		$.ajax({
			url:path,
			type:'GET',
			dataType:'html',
			timeout:7000,
			error:function(){
				alert('Error loading php file');return false;
			},
			success:function(data){
				
				$('.v-headline').html(data);
				return false;
			}
		})
	}

	$('.video-thumb a').click(function(e) {
		e.preventDefault();
//		$('.v-headline').html('Loading...');
		$('.video-thumb span').removeClass('video-on');
		$(this).find('span').removeClass('video-play').addClass('video-on');
		showVideo($(this).attr('path'));
	});

	// booking rss
	$('.rss-share').hover(function() {
		$(this).next().show();
	}, function(){
	})
	$('.rss-booking').hover(function() {
	}, function(){
		$(this).hide();
	})

	// lazy loading 
	$("img.lazy").lazyload({
		effect : "fadeIn",
		threshold : 200,
		failure_limit : 10,
		skip_invisible : false
	});

		$('.cycle-banner').cycle({ 
			pause: 3,
			speed: 3000,
			timeout: 2500,
			slideResize: true,
			containerResize: false,
			width: '100%',
			fit: 1
		});

		$('.cycle-banner-wrapper .cycle-banner').cycle({ 
			pause: 3,
			speed: 3000,
			timeout: $(this).parent().data('interval'),
			slideResize: true,
			containerResize: false,
			width: '100%',
			fit: 1
		});

	$('.bt-search').click(function(e) {
		e.preventDefault();
		var url = "http://www.baidu.com/s?wd=site%3Achristiantimes.cn+" +$('#search-keywords').val();
		window.open(url,'_blank');
	});
	$('#search-form').submit(function(e) {
		e.preventDefault();
		var url = "http://www.baidu.com/s?wd=site%3Achristiantimes.cn+" +$('#search-keywords').val();
		window.open(url,'_blank');
	})

	// 右侧热门
	$('.sub-remen h1').click(function(e) {
		e.preventDefault();
	})
	$('.sub-remen h1').hover(function() {
		$('.sub-remen h1').removeClass('mt-on');
		$(this).addClass('mt-on');
		$('.sub-remen ul').hide();
		$($(this).attr('to')).show();
	})

	$(".ht-ocity").click(function(e) {
		e.preventDefault()
		$('.other-version').toggle('normal');
	});

	// 查看更多专栏作家
	$("#more-columnists").click(function (e) {
		e.preventDefault();
		$(this).prev().find('.more').toggle('normal');
	});

	$(".share_block").each(function(index, value) {
		var href = encodeURIComponent($(this).data('href'));
		var img = encodeURIComponent($(this).data('img'));
		var title = encodeURI($(this).data('title'));
		var intro = encodeURI($(this).data('intro'));
		var html = '<div class="share_live24"><div class="share_i"><span class="sha" id="sha">分享到：</span><span class="icon-weixin" id="sha"><a title="分享到微信朋友圈" class="share-to-weixin" href="#" target="_blank"><img src="/bundles/frontendweb/images/weixin.png" /></a></span><span class="icon-weibo" id="sha"><a title="分享到新浪微博" class="share-to-weibo" href="#" target="_blank"><img src="/bundles/frontendweb/images/sina.png" /></a></span><span class="icon-qzone" id="sha"><a title="分享到qq空间" class="share-to-qq" href="#" target="_blank"><img src="/bundles/frontendweb/images/qzong.png" /></a></span></div></div>';
		$(value).html(html);
	});
	$(".share_block .share-to-weixin").click(function(e) {
		e.preventDefault();
		var href = encodeURIComponent($(this).closest('.share_block').data('href'));
		showToWeixin(href);
	});
	$(".share_block .share-to-weibo").click(function(e) {
		e.preventDefault();
		var href = encodeURIComponent($(this).closest('.share_block').data('href'));
		var img = encodeURIComponent($(this).closest('.share_block').data('img'));
		var title = encodeURI($(this).closest('.share_block').data('title'));
		var intro = encodeURI($(this).closest('.share_block').data('intro'));
		showToSina(title, img, href, intro);
	});
	$(".share_block .share-to-qq").click(function(e) {
		e.preventDefault();
		var href = encodeURIComponent($(this).closest('.share_block').data('href'));
		var img = $(this).closest('.share_block').data('img');
		var title = encodeURI($(this).closest('.share_block').data('title'));
		var intro = $(this).closest('.share_block').data('intro');
		showToTencent(title, img, href, intro);
	});


	$(".social-share").hover(function(){
		$(this).find('.share_block').show();
	}, function() {
		$(this).find('.share_block').hide();
	});
})

var base_url = 'http://www.christiantimes.cn';

function showToTencent(title,picUrl,url,content){
		var _t = title+" - 基督时报 -- 基督教资讯平台" + content;
		var _url = base_url + url;
		var _appkey = encodeURI("801495950");//你从腾讯获得的appkey
		var _pic = null;
		if (picUrl != 'undefined') {
			_pic = '&pic='+base_url + picUrl;//（例如：var _pic='图片url1|图片url2|图片url3....）
		}
		var _site = '';//你的网站地址
		var _u = 'http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?url='+_url+'&appkey='+_appkey+'&desc='+encodeURI(content)+'&site='+_site+_pic+'&title='+_t;
		window.open( _u,'', 'width=700, height=680, top=0, left=0, toolbar=no, menubar=no, scrollbars=no, location=yes, resizable=no, status=no' );
}

function showToSina(title,picUrl,url, content){
		var _t = "【@基督时报基督教资讯平台："+title+"】" + content;
		var _url = base_url + url;
		var _appkey = encodeURI("441808809");//你从微薄获得的appkey
		var _pic = null;
		if (picUrl != 'undefined') {
			_pic = '&pic='+base_url + picUrl;//（例如：var _pic='图片url1|图片url2|图片url3....）
		}
		var _site = '';//你的网站地址
		var _u = 'http://service.weibo.com/share/share.php?url='+_url+'&appkey='+_appkey+_pic+'&title='+_t;
		window.open( _u,'', 'width=700, height=680, top=0, left=0, toolbar=no, menubar=no, scrollbars=no, location=yes, resizable=no, status=no' );
}

function showToWeixin(url){
		var _url = encodeURIComponent(base_url + url);
		var _u = 'http://s.jiathis.com/qrcode.php?url=' + _url;
		window.open( _u,'', 'width=260, height=260, top=0, left=0, toolbar=no, menubar=no, scrollbars=no, location=yes, resizable=no, status=no' );
}
