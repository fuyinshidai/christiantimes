var base_url = 'http://www.christiantimes.cn';

function showToTencent(title,picUrl,url,content){
		var _t = title+" - 基督时报 -- 基督教资讯平台" + content;
		var _url = base_url + url;
		var _appkey = encodeURI("801495950");//你从腾讯获得的appkey
		var _pic = null;
		if (picUrl != 'undefined') {
			_pic = '&pic='+base_url + picUrl;//（例如：var _pic='图片url1|图片url2|图片url3....）
		}
		var _site = '';//你的网站地址
		var _u = 'http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?url='+_url+'&appkey='+_appkey+'&desc='+encodeURI(content)+'&site='+_site+_pic+'&title='+_t;
		window.open( _u,'', 'width=700, height=680, top=0, left=0, toolbar=no, menubar=no, scrollbars=no, location=yes, resizable=no, status=no' );
}

function showToSina(title,picUrl,url, content){
		var _t = "【@基督时报基督教资讯平台："+title+"】" + content;
		var _url = base_url + url;
		var _appkey = encodeURI("441808809");//你从微薄获得的appkey
		var _pic = null;
		if (picUrl != 'undefined') {
			_pic = '&pic='+base_url + picUrl;//（例如：var _pic='图片url1|图片url2|图片url3....）
		}
		var _site = '';//你的网站地址
		var _u = 'http://service.weibo.com/share/share.php?url='+_url+'&appkey='+_appkey+_pic+'&title='+_t;
		window.open( _u,'', 'width=700, height=680, top=0, left=0, toolbar=no, menubar=no, scrollbars=no, location=yes, resizable=no, status=no' );
}

function showToWeixin(url){
		var _url = encodeURIComponent(base_url + url);
		var _u = 'http://s.jiathis.com/qrcode.php?url=' + _url;
		window.open( _u,'', 'width=260, height=260, top=0, left=0, toolbar=no, menubar=no, scrollbars=no, location=yes, resizable=no, status=no' );
}

$(document).ready(function(){
	$(".share_block .weixin").click(function(e) {
		e.preventDefault();
		var href = encodeURIComponent($(this).closest('.share_block').data('href'));
		showToWeixin(href);
	});
	$(".share_block .xinlang").click(function(e) {
		e.preventDefault();
		var href = encodeURIComponent($(this).closest('.share_block').data('href'));
		var img = encodeURIComponent($(this).closest('.share_block').data('img'));
		var title = encodeURI($(this).closest('.share_block').data('title'));
		var intro = encodeURI($(this).closest('.share_block').data('intro'));
		showToSina(title, img, href, intro);
	});
	$(".share_block .qq").click(function(e) {
		e.preventDefault();
		var href = encodeURIComponent($(this).closest('.share_block').data('href'));
		var img = $(this).closest('.share_block').data('img');
		var title = encodeURI($(this).closest('.share_block').data('title'));
		var intro = $(this).closest('.share_block').data('intro');
		showToTencent(title, img, href, intro);
	});
})
