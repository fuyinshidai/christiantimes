$(document).ready(function(){
	//其他城市
	var ua = navigator.userAgent; 
	var event = (ua.match(/iPad/i)) ? 'touchstart' : 'click'; 
	/** 其他城市下拉菜单 */
	$('.l-ht-ocity').bind(event, function(e) { 
		e.preventDefault();
		var elm = $('.l-other-version');
		elm.toggle();
	})
	$('.l-nav-hide').bind(event, function(e) { 
		e.preventDefault();
		var elm = $('.l-nav-show');
		elm.slideToggle("1000");
	})
	
	// lazy loading 
	$("img.lazy").lazyload({
		effect : "fadeIn",
		threshold : 200,
		failure_limit : 10,
		skip_invisible : false
	});

	$('.cycle-banner').cycle({ 
		pause: 3,
		speed: 3000,
		timeout: 2500,
		slideResize: true,
		containerResize: false,
		width: '100%',
		fit: 1
	});

	$('.cycle-banner-wrapper .cycle-banner').cycle({ 
		pause: 3,
		speed: 3000,
		timeout: $(this).parent().data('interval'),
		slideResize: true,
		containerResize: false,
		width: '100%',
		fit: 1
	});

	// search
	$(".bt-search").click(function() {
		if($("#keyword").val()) {
			location.href = "/search/"+$("#keyword").val();
		}
	});

	// 右侧热门
	$('.sub-remen h1').click(function(e) {
		e.preventDefault();
	})
	$('.sub-remen h1').hover(function() {
		$('.sub-remen h1').removeClass('mt-on');
		$(this).addClass('mt-on');
		$('.sub-remen ul').hide();
		$($(this).attr('to')).show();
	})
	// 赞赏
	$("#codeBtn").click(function() {
		console.log('clicked');
		$(".admireCode").toggle();
	})
})
