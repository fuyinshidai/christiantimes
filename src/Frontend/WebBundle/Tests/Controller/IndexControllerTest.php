<?php

namespace Frontend\WebBundle\Tests\Controller;

use Gospel\Bundle\CoreBundle\Tests\GospelTestCase;
use Gospel\Bundle\CoreBundle\Entity\Post;

class IndexControllerTest extends GospelTestCase
{
	/**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
//        static::$kernel = static::createKernel();
//        static::$kernel->boot();
//        $this->em = static::$kernel->getContainer()
//            ->get('doctrine')
//            ->getManager()
//        ;
    }

	public function testIndex()
	{
//		$client = static::createClient();
//
//		$crawler = $client->request('GET', $this->baseUrl.'/');
//
//		$this->assertTrue($crawler->filter('html:contains("登录")')->count() > 0);
	}

	/**
	 * Test user login
	 */
	public function testIndexLogin()
	{
//		$client = $this->login('ctadmin', '1234');
//		$client->request('GET', $this->baseUrl.'/');
//		$this->assertRegExp(
//				'/ctadmin/', $client->getResponse()->getContent()
//		);
	}

	public function testDashboardAccess()
	{
//		$client = $this->login('ctadmin', '1234');
//		$client->request('GET', $this->baseUrl.'/gospel-admin/user');
//		$this->assertRegExp(
//				'/添加用户/', $client->getResponse()->getContent()
//		);
	}

	public function testLogout()
	{
//		$client = $this->login('ctadmin', '1234');
//		$client = $this->logout($client);
//		$crawler = $client->request('GET', $this->baseUrl.'/');
//		$this->assertTrue($crawler->filter('html:contains("登录")')->count() > 0);
	}

	public function testIndexPosts()
	{
//		$repoPost = $this->em->getRepository('GospelCoreBundle:Post');
//		$repoCategory = $this->em->getRepository('GospelCoreBundle:Category');
//
//		// 真道百问
//		$data = array();
//		$categoryQuestions = $repoCategory->find(Post::$CATEGORY_QUESTION);
//		$data['questionsTop'] = $repoPost->findByCategory($categoryQuestions, 1, $offset = 0);
//		$data['questionsOthers'] = $repoPost->findByCategory($categoryQuestions, 3, $offset = 1);
//		$this->assertTrue(count($data['questionsTop']) == 1);
//		$this->assertTrue(count($data['questionsOthers']) == 3);
//
//		// 教会思考
//		$categoryQuestions = $repoCategory->find(Post::$CATEGORY_CHURCH_REFLECTION);
//		$data['churchReflectionTop'] = $repoPost->findByCategory($categoryQuestions, 1, $offset = 0);
//		$data['churchReflectionOthers'] = $repoPost->findByCategory($categoryQuestions, 3, $offset = 1);
//		$this->assertTrue(count($data['churchReflectionTop']) == 1);
//		$this->assertTrue(count($data['churchReflectionOthers']) == 3);
//
//		// 读取多个分类的文章
//		$categoriesData = array(
//			array( 'categoryId' => Post::$CATEGORY_CHURCH),
//			array( 'categoryId' => Post::$CATEGORY_TWO_CHURCH),
//			array( 'categoryId' => Post::$CATEGORY_MINISTRY),
//			array( 'categoryId' => Post::$CATEGORY_SOCIETY),
//			array( 'categoryId' => Post::$CATEGORY_CULTURE),
//			array( 'categoryId' => Post::$CATEGORY_FAMILY),
//		);
//		foreach ($categoriesData as $key => $categoryData) {
//			$category = $repoCategory->find($categoryData['categoryId']);
//			$categoriesData[$key]['category'] = $category;
//			$categories = array($category);
//			foreach ($category->getChildren() as $subcategory) {
//				$categories[] = $subcategory;
//			}
//			$categoriesData[$key]['data'] = $repoPost->findByCategory($categories, 6);
//			$this->assertEquals(6, count($categoriesData[$key]['data']));
//		}
//
//		$data['categoriesData'] = $categoriesData;
	}

	/**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
//        parent::tearDown();
//        $this->em->close();
    }
}
