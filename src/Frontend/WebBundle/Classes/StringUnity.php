<?php

/*
 * 字符串处理类
 */

namespace Frontend\WebBundle\Classes;

/**
 *
 * @author Zhili He <zhili850702@gmail.com, http://zhilihe.com/>
 */
class StringUnity
{

	/**
	 * 将字符串转换为数组
	 * @param string $str 输入的字符
	 * @param int $l 
	 * @return array
	 */
	static public function str_split_unicode($str, $l = 0)
	{
		if ($l > 0) {
			$ret = array();
			$len = mb_strlen($str, "UTF-8");
			for ($i = 0; $i < $len; $i += $l) {
				$ret[] = mb_substr($str, $i, $l, "UTF-8");
			}
			return $ret;
		}
		return preg_split("//u", $str, -1, PREG_SPLIT_NO_EMPTY);
	}


	/**
	 * 在字符串之间添加空格
	 * @param string $str
	 * @param string $blank 默认是空格，可以添加其他的内容
	 * @return string
	 */
	static public function add_blank($str, $blank = ' ')
	{
		$arr = self::str_split_unicode($str);
		return implode($arr, $blank);
	}
}
