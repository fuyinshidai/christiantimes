基督时报
========================

本版本库只有程序部分，不包括网站运作产生的图片资源。 在以下版本的php上测试通过：

* PHP 5.6
* PHP 7.0
* PHP 7.1


搭建开发环境
----------------------------------

以ubuntu系统为例

* 使用git获取最新的版本
* 将app/config/parameters.yml.dist拷贝为app/config/parameters.yml，并根据本地环境调整配置
* 如果需要图片资源辅助开发，自行拷贝网站运作产生的图片资源(web/assets)到web目录下
* [ ! -d app/cache ] || mkdir -p app/cache
* chmod -R 777 app/cache
* [ ! -d app/var ] || mkdir -p app/var
* chmod -R 777 app/var
* [ ! -d web/assets ] || mkdir -p web/assets
* chmod -R 777 web/assets
* sudo apt-get update
* sudo apt-get install -y apache2 redis-server mysql-server php-redis php7.1-mysql php7.1-mcrypt libapache2-mod-php7.1 php7.1-xml php7.1-mbstring php7.1-intl php7.1-zip php7.1-gd php7.1-curl zip unzip composer
* 执行composer install
* 执行app/console server:run


部署
----------------------------------

以ubuntu系统为例

* 使用git获取最新的版本
* 将app/config/parameters.yml.dist拷贝为app/config/parameters.yml，并根据服务器环境调整配置
* 将网站运作产生的图片资源(web/assets)拷贝到web目录下
* [ ! -d app/cache ] || mkdir -p app/cache
* chmod -R 777 app/cache
* [ ! -d app/var ] || mkdir -p app/var
* chmod -R 777 app/var
* [ ! -d web/assets ] || mkdir -p web/assets
* chmod -R 777 web/assets
* [ ! -d web/assets/cache ] || mkdir -p web/assets/cache
* chmod -R 777 web/assets/cache
* sudo apt-get update
* sudo apt-get install -y apache2 redis-server mysql-server php-redis php7.1-mysql php7.1-mcrypt libapache2-mod-php7.1 php7.1-xml php7.1-mbstring php7.1-intl php7.1-zip php7.1-gd php7.1-curl zip unzip composer
* composer install --no-dev
* app/console assetic:dump
* app/console assets:install


需要完成的工作
----------------------------------

* 分离出图片上传部分成为单独的图片管理服务
* 修改程序使用图片管理服务来加载图片
* 数据库迁移
